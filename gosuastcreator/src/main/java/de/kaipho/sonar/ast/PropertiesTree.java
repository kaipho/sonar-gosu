package de.kaipho.sonar.ast;

import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.*;

import javax.annotation.Nullable;
import java.util.List;

/**
 * 'property' ('get' | 'set') id parameters (':' typeLiteral)?
 */
public interface PropertiesTree extends StatementTree {
    ModifiersTree modifiers();

    TypeParameters typeParameters();

    /**
     * @return null in case of constructor
     */
    @Nullable
    TypeTree returnType();

    IdentifierTree simpleName();

    SyntaxToken openParenToken();

    List<VariableTree> parameters();

    SyntaxToken closeParenToken();

    @Nullable
    BlockTree block();

    @Nullable
    SyntaxToken semicolonToken();
}
