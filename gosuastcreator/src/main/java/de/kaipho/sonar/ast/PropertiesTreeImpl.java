package de.kaipho.sonar.ast;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.sonar.java.ast.parser.FormalParametersListTreeImpl;
import org.sonar.java.ast.parser.QualifiedIdentifierListTreeImpl;
import org.sonar.java.ast.parser.TypeParameterListTreeImpl;
import org.sonar.java.model.JavaTree;
import org.sonar.java.model.ModifiersUtils;
import org.sonar.java.model.declaration.MethodTreeImpl;
import org.sonar.java.model.declaration.ModifiersTreeImpl;
import org.sonar.java.resolve.JavaSymbol;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.*;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.util.List;

public class PropertiesTreeImpl extends MethodTreeImpl implements PropertiesTree {

    public PropertiesTreeImpl(FormalParametersListTreeImpl parameters, @Nullable SyntaxToken defaultToken, @Nullable ExpressionTree defaultValue) {
        super(parameters, defaultToken, defaultValue);
    }

    public PropertiesTreeImpl(
            @Nullable TypeTree returnType,
            IdentifierTree simpleName,
            FormalParametersListTreeImpl parameters,
            @Nullable BlockTree block,
            @Nullable SyntaxToken semicolonToken) {

        super(returnType, simpleName, parameters, null, QualifiedIdentifierListTreeImpl.emptyList(), block, semicolonToken);
    }
}
