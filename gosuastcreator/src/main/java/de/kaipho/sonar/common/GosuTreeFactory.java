/*
 * SonarQube Java
 * Copyright (C) 2012-2017 SonarSource SA
 * mailto:info AT sonarsource DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.kaipho.sonar.common;

import com.google.common.collect.ImmutableList;
import com.sonar.sslr.api.typed.Optional;
import de.kaipho.sonar.ast.PropertiesTree;
import de.kaipho.sonar.ast.PropertiesTreeImpl;
import org.sonar.java.ast.api.JavaKeyword;
import org.sonar.java.ast.api.JavaPunctuator;
import org.sonar.java.ast.api.JavaTokenType;
import org.sonar.java.ast.parser.*;
import org.sonar.java.model.*;
import org.sonar.java.model.JavaTree.*;
import org.sonar.java.model.declaration.*;
import org.sonar.java.model.expression.*;
import org.sonar.java.model.statement.*;
import org.sonar.plugins.java.api.tree.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static de.kaipho.sonar.common.TokenResolver.getSemicolonToken;
import static de.kaipho.sonar.common.TokenResolver.getToken;

public class GosuTreeFactory extends TreeFactory {

    public VariableDeclaratorListTreeImpl completeFieldDeclaration(
            InternalSyntaxToken token,
            VariableDeclaratorListTreeImpl partial,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> optionalType,
            Optional<Triple<InternalSyntaxToken, Optional<InternalSyntaxToken>, InternalSyntaxToken>> asProperty,
            Optional<Tuple<InternalSyntaxToken, ExpressionTree>> initialisationExpression,
            Optional<InternalSyntaxToken> optionalSemicolonToken) {

        TypeTree type = optionalType.isPresent() ? optionalType.get().second() : new InferedTypeTree();

        // TODO save comments
        for (VariableTreeImpl variable : partial) {
            variable.completeType(type);
        }


        if (initialisationExpression.isPresent()) {
            partial.get(partial.size() - 1)
                    .completeTypeAndInitializer(type, initialisationExpression.get().first(), initialisationExpression.get().second());
        }

        // store the semicolon as endToken for the last variable
        partial.get(partial.size() - 1).setEndToken(getSemicolonToken(optionalSemicolonToken, partial));

        return partial;
    }

    public VariableTreeImpl completeVariableDeclarator(
            InternalSyntaxToken identifierToken,
            Optional<List<Tuple<Optional<List<AnnotationTreeImpl>>, Tuple<InternalSyntaxToken, InternalSyntaxToken>>>> dimensions) {
        IdentifierTreeImpl identifier = new IdentifierTreeImpl(identifierToken);

        ArrayTypeTreeImpl nestedDimensions = newArrayTypeTreeWithAnnotations(dimensions);

        return new VariableTreeImpl(identifier, nestedDimensions);
    }

    public <T extends Tree> T completeBlockType(
            InternalSyntaxToken block,
            FormalParametersListTreeImpl parameter,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> type) {
        return (T) new InferedTypeTree();
    }

    public ExpressionTree newTypeOf(InternalSyntaxToken typeof, ExpressionTree expression) {
        return new MethodInvocationTreeImpl(
                new MemberSelectExpressionTreeImpl(expression, getToken(Optional.absent(), (JavaTree) expression, "."), new IdentifierTreeImpl(getToken(Optional.absent(), (JavaTree) expression, "getClass"))),
                null,
                new ArgumentListTreeImpl(getToken(Optional.absent(), (JavaTree) expression, "("), getToken(Optional.absent(), (JavaTree) expression, ")")));
    }

    public BreakStatementTreeImpl breakStatement(InternalSyntaxToken breakToken, Optional<InternalSyntaxToken> identifierToken, Optional<InternalSyntaxToken> semicolonSyntaxToken) {
        return breakStatement(breakToken, identifierToken, getSemicolonToken(semicolonSyntaxToken, breakToken));
    }

    public ContinueStatementTreeImpl continueStatement(InternalSyntaxToken continueToken, Optional<InternalSyntaxToken> identifierToken, Optional<InternalSyntaxToken> semicolonToken) {
        return continueStatement(continueToken, identifierToken, getSemicolonToken(semicolonToken, continueToken));
    }

    public VariableTreeImpl newForEachVariable(
            Optional<InternalSyntaxToken> var,
            InternalSyntaxToken identifierToken) {
        IdentifierTreeImpl identifier = new IdentifierTreeImpl(identifierToken);

        return new VariableTreeImpl(identifier);
    }

    public ExpressionTree newDataStructureInitialisation(
            InternalSyntaxToken openBraces,
            Optional<List<ExpressionTree>> expressions,
            InternalSyntaxToken closeBraces) {
        return new NewClassTreeImpl(
                new ArgumentListTreeImpl(
                        getToken(Optional.absent(), openBraces, "("),
                        getToken(Optional.absent(), openBraces, ")")), null)
                .completeWithNewKeyword(getToken(Optional.absent(), openBraces, "n"))
                .completeWithIdentifier(new IdentifierTreeImpl(getToken(Optional.absent(), openBraces, "A")));
    }

    public List<ExpressionTree> newMapListInitialisation(List<ExpressionTree> expressions) {
        return new ArrayList<ExpressionTree>();
    }

    public List<ExpressionTree> newListInitialisation(ExpressionTree expression, Optional<List<Tuple<InternalSyntaxToken, ExpressionTree>>> expressions) {
        return new ArrayList<>();
    }

    public List<ExpressionTree> newMapInitialisation(ExpressionTree expression, Optional<List<Tuple<InternalSyntaxToken, ExpressionTree>>> expressions) {
        return new ArrayList<>();
    }

    public ExpressionTree newMapElement(ExpressionTree expression1, InternalSyntaxToken pfeil, ExpressionTree expression2) {
        return expression1;
    }

    @Override
    public ModifiersTreeImpl modifiers(Optional<List<ModifierTree>> modifierNodes) {
        if (!modifierNodes.isPresent()) {
            return ModifiersTreeImpl.emptyModifiers();
        }
        return new ModifiersTreeImpl(modifierNodes.get()
                .stream()
                .filter(it -> !(it instanceof ModifierKeywordTreeImpl) || !((ModifierKeywordTreeImpl) it).keyword().text().equals(JavaKeyword.INTERNAL.getValue()))
                .collect(Collectors.toList()));
    }

    public VariableDeclaratorListTreeImpl completeLocalVariableDeclaration(
            ModifiersTreeImpl modifiers,
            InternalSyntaxToken var,
            VariableDeclaratorListTreeImpl partial,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> optionalType,
            Optional<Tuple<InternalSyntaxToken, ExpressionTree>> initialisationExpression,
            Optional<InternalSyntaxToken> optionalSemicolonToken) {

        TypeTree type = optionalType.isPresent() ? optionalType.get().second() : new InferedTypeTree();

        // TODO save comments
        for (VariableTreeImpl variable : partial) {
            variable.completeModifiersAndType(modifiers, type);
        }

        if (initialisationExpression.isPresent()) {
            partial.get(partial.size() - 1)
                    .completeTypeAndInitializer(type, initialisationExpression.get().first(), initialisationExpression.get().second());
        }

        // store the semicolon as endToken for the last variable
        partial.get(partial.size() - 1).setEndToken(getSemicolonToken(optionalSemicolonToken, partial));

        return partial;
    }

    public ExpressionTree completeCastExpression(TypeCastExpressionTreeImpl partial) {
        return partial.complete((InternalSyntaxToken) partial.closeParenToken());
    }

    public TypeCastExpressionTreeImpl newBasicTypeCastExpression(ExpressionTree expression, InternalSyntaxToken closeParenToken, PrimitiveTypeTreeImpl basicType) {
        return new TypeCastExpressionTreeImpl(basicType, closeParenToken, expression);
    }

    public TypeCastExpressionTreeImpl newClassCastExpression(ExpressionTree expression,
                                                             InternalSyntaxToken closeParenToken,
                                                             TypeTree type,
                                                             Optional<Tuple<InternalSyntaxToken, BoundListTreeImpl>> classTypes) {
        BoundListTreeImpl bounds = BoundListTreeImpl.emptyList();
        InternalSyntaxToken andToken = null;
        if (classTypes.isPresent()) {
            andToken = classTypes.get().first();
            bounds = classTypes.get().second();
        }
        return new TypeCastExpressionTreeImpl(type, andToken, bounds, closeParenToken, expression);
    }

    public MethodTreeImpl newMethod(
            InternalSyntaxToken function,
            InternalSyntaxToken identifierToken,
            Optional<List<Tuple<Optional<List<AnnotationTreeImpl>>, Tuple<InternalSyntaxToken, InternalSyntaxToken>>>> annotatedDimensions,
            Optional<TypeParameterListTreeImpl> typeParameters,
            FormalParametersListTreeImpl parameters,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> type,
            Optional<JavaTree> blockOrSemicolon) {

        JavaTree tree;
        if (blockOrSemicolon.isPresent()) {
            tree = blockOrSemicolon.get();
        } else
            tree = getSemicolonToken(Optional.absent(), identifierToken);

        identifierToken.trivias().addAll(function.trivias());
        MethodTreeImpl methodTree = newMethodOrConstructor(identifierToken, parameters, annotatedDimensions, type, tree);
        if (typeParameters.isPresent()) methodTree.completeWithTypeParameters(typeParameters.get());
        return methodTree;
    }

    public PropertiesTreeImpl newProperty(
            InternalSyntaxToken property,
            InternalSyntaxToken propertyType,
            InternalSyntaxToken identifierToken,
            Optional<List<Tuple<Optional<List<AnnotationTreeImpl>>, Tuple<InternalSyntaxToken, InternalSyntaxToken>>>> annotatedDimensions,
            FormalParametersListTreeImpl parameters,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> type,
            Optional<JavaTree> blockOrSemicolon) {

        JavaTree tree;
        if (blockOrSemicolon.isPresent()) {
            tree = blockOrSemicolon.get();
        } else
            tree = getSemicolonToken(Optional.absent(), identifierToken);

        IdentifierTreeImpl identifier = new IdentifierTreeImpl(identifierToken);

        ArrayTypeTreeImpl nestedDimensions = newArrayTypeTreeWithAnnotations(annotatedDimensions);
        TypeTree actualType;
        if (type.isPresent()) {
            actualType = applyDim(type.get().second(), nestedDimensions);
        } else {
            actualType = new InferedTypeTree();
        }
        BlockTreeImpl block = null;
        InternalSyntaxToken semicolonToken = null;
        if (tree.is(Tree.Kind.BLOCK)) {
            block = (BlockTreeImpl) tree;
        } else {
            semicolonToken = (InternalSyntaxToken) tree;
        }

        ListTree<TypeTree> throwsClauses = QualifiedIdentifierListTreeImpl.emptyList();

        return new PropertiesTreeImpl(
                actualType, identifier, parameters, block, semicolonToken
        );
    }

    public MethodTreeImpl newConstructor(
            InternalSyntaxToken constructor,
            FormalParametersListTreeImpl parameters,
            Optional<List<Tuple<Optional<List<AnnotationTreeImpl>>, Tuple<InternalSyntaxToken, InternalSyntaxToken>>>> annotatedDimensions,
            Optional<Tuple<InternalSyntaxToken, QualifiedIdentifierListTreeImpl>> throwsClause,
            Optional<JavaTree> blockOrSemicolon) {

        JavaTree tree;
        if (blockOrSemicolon.isPresent()) {
            tree = blockOrSemicolon.get();
        } else
            tree = getSemicolonToken(Optional.absent(), constructor);

        return newMethodOrConstructor(constructor, parameters, annotatedDimensions, Optional.absent(), tree);
    }

    private static MethodTreeImpl newMethodOrConstructor(
            InternalSyntaxToken identifierToken,
            FormalParametersListTreeImpl parameters,
            Optional<List<Tuple<Optional<List<AnnotationTreeImpl>>, Tuple<InternalSyntaxToken, InternalSyntaxToken>>>> annotatedDimensions,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> type,
            JavaTree blockOrSemicolon) {

        IdentifierTreeImpl identifier = new IdentifierTreeImpl(identifierToken);

        ArrayTypeTreeImpl nestedDimensions = newArrayTypeTreeWithAnnotations(annotatedDimensions);
        TypeTree actualType;
        if (type.isPresent()) {
            actualType = applyDim(type.get().second(), nestedDimensions);
        } else if (!identifierToken.text().equals(JavaPunctuator.CONSTRUCT.getValue())) {
            actualType = new InferedTypeTree();
        } else {
            actualType = null;
        }
        BlockTreeImpl block = null;
        InternalSyntaxToken semicolonToken = null;
        if (blockOrSemicolon.is(Tree.Kind.BLOCK)) {
            block = (BlockTreeImpl) blockOrSemicolon;
        } else {
            semicolonToken = (InternalSyntaxToken) blockOrSemicolon;
        }

        ListTree<TypeTree> throwsClauses = QualifiedIdentifierListTreeImpl.emptyList();

        return new MethodTreeImpl(
                actualType,
                identifier,
                parameters,
                null,
                throwsClauses,
                block,
                semicolonToken);
    }

    public Optional<FormalParametersListTreeImpl> completeTypeFormalParameters(
            Optional<FormalParametersListTreeImpl> first,
            Optional<List<Tuple<InternalSyntaxToken, FormalParametersListTreeImpl>>> rest) {
        if (!first.isPresent()) return Optional.absent();

        if (rest.isPresent()) {
            for (Tuple<InternalSyntaxToken, FormalParametersListTreeImpl> actual : rest.get()) {
                InternalSyntaxToken comma = actual.first();
                FormalParametersListTreeImpl partial = actual.second();
                FormalParametersListTreeImpl partialFirst = first.get();

                partialFirst.add(0, partial.get(0));
                partialFirst.get(0).setEndToken(comma);
            }
        }

        return first;
    }

    public FormalParametersListTreeImpl prependNewFormalParameter(
            ModifiersTreeImpl modifiers,
            VariableTreeImpl variable,
            Optional<Tuple<InternalSyntaxToken, TypeTree>> type,
            Optional<Tuple<InternalSyntaxToken, ExpressionTree>> initialisationExpression) {

        TypeTree resolvedType = type.isPresent() ? type.get().second() : new InferedTypeTree();

        if (initialisationExpression.isPresent()) {
            variable.completeTypeAndInitializer(resolvedType, initialisationExpression.get().first(), initialisationExpression.get().second())
                    .completeModifiers(modifiers);
        } else
            variable.completeType(type.get().second()).completeModifiers(modifiers);
        return new FormalParametersListTreeImpl(variable);
    }

    public FormalParametersListTreeImpl completeParentFormalParameters(
            InternalSyntaxToken openParenToken,
            Optional<Optional<FormalParametersListTreeImpl>> partial,
            InternalSyntaxToken closeParenToken) {

        return partial.get().isPresent() ?
                partial.get().get().complete(openParenToken, closeParenToken) :
                new FormalParametersListTreeImpl(openParenToken, closeParenToken);
    }

    public ReturnStatementTreeImpl returnStatement(InternalSyntaxToken returnToken, Optional<ExpressionTree> expression, Optional<InternalSyntaxToken> semicolonSyntaxToken) {
        return new ReturnStatementTreeImpl(returnToken, expression.orNull(), getSemicolonToken(semicolonSyntaxToken, returnToken));
    }

    public ImportTreeImpl newImportDeclaration(
            InternalSyntaxToken importToken,
            Optional<InternalSyntaxToken> staticToken, ExpressionTree qualifiedIdentifier,
            Optional<Tuple<InternalSyntaxToken, InternalSyntaxToken>> dotStar,
            Optional<InternalSyntaxToken> semicolonToken) {

        ExpressionTree target = qualifiedIdentifier;
        if (dotStar.isPresent()) {
            IdentifierTreeImpl identifier = new IdentifierTreeImpl(dotStar.get().second());
            InternalSyntaxToken dotToken = dotStar.get().first();
            target = new MemberSelectExpressionTreeImpl(qualifiedIdentifier, dotToken, identifier);
        }

        InternalSyntaxToken staticKeyword = staticToken.orNull();
        return new ImportTreeImpl(importToken, staticKeyword, target, getSemicolonToken(semicolonToken, importToken));
    }

    public CompilationUnitTreeImpl newCompilationUnit(
            JavaTree spacing,
            Optional<PackageDeclarationTree> packageDeclaration,
            Optional<List<ImportClauseTree>> importDeclarations,
            Optional<List<Tree>> typeDeclarations,
            InternalSyntaxToken eof) {

        ImmutableList.Builder<ImportClauseTree> imports = ImmutableList.builder();
        if (importDeclarations.isPresent()) {
            for (ImportClauseTree child : importDeclarations.get()) {
                imports.add(child);
            }
        }

        ImmutableList.Builder<Tree> types = ImmutableList.builder();
        if (typeDeclarations.isPresent()) {
            for (Tree child : typeDeclarations.get()) {
                types.add(child);
            }
        }

        return new CompilationUnitTreeImpl(
                packageDeclaration.orNull(),
                imports.build(),
                types.build(),
                null,
                eof);
    }

    public ExpressionStatementTreeImpl expressionStatement(ExpressionTree expression, Optional<InternalSyntaxToken> semicolonToken) {
        return new ExpressionStatementTreeImpl(expression, getSemicolonToken(semicolonToken, (JavaTree) expression));
    }

    public PackageDeclarationTreeImpl newPackageDeclaration(
            Optional<List<AnnotationTreeImpl>> annotations,
            InternalSyntaxToken packageToken,
            ExpressionTree qualifiedIdentifier,
            Optional<InternalSyntaxToken> semicolonToken) {
        List<AnnotationTree> annotationList = ImmutableList.copyOf(annotations.or(ImmutableList.of()));
        return new PackageDeclarationTreeImpl(annotationList, packageToken, qualifiedIdentifier, getSemicolonToken(semicolonToken, packageToken));
    }

    public ExpressionTree lambdaExpression(
            Optional<InternalSyntaxToken> backslash,
            Optional<LambdaParameterListTreeImpl> parameters,
            InternalSyntaxToken arrowToken,
            Tree body) {
        if (!parameters.isPresent()) {
            parameters = Optional.of(new LambdaParameterListTreeImpl(null, new ArrayList<>(), null));
        }

        return new LambdaExpressionTreeImpl(
                parameters.get().openParenToken(),
                ImmutableList.<VariableTree>builder().addAll(parameters.get()).build(),
                parameters.get().closeParenToken(),
                arrowToken,
                body);
    }

    public VariableTreeImpl newSimpleParameter(InternalSyntaxToken identifierToken, Optional<Tuple<InternalSyntaxToken, TypeTree>> optionalType) {
        IdentifierTreeImpl identifier = new IdentifierTreeImpl(identifierToken);
        VariableTreeImpl result = new VariableTreeImpl(identifier);

        if (optionalType.isPresent())
            result.completeType(optionalType.get().second());

        return result;
    }

    public ConditionalExpressionTreeImpl newTernaryExpression(InternalSyntaxToken queryToken, ExpressionTree ifNullExpression) {
        return new ConditionalExpressionTreeImpl(queryToken, ifNullExpression, queryToken, ifNullExpression); // TODO in nullcheck umformen!
    }

    @Override
    public JavaTree completeMember(ModifiersTreeImpl modifiers, JavaTree partial) {

        if (partial instanceof ClassTreeImpl) {
            ((ClassTreeImpl) partial).completeModifiers(modifiers);
        } else if (partial instanceof VariableDeclaratorListTreeImpl) {
            for (VariableTreeImpl variable : (VariableDeclaratorListTreeImpl) partial) {
                variable.completeModifiers(modifiers);
            }
        } else if (partial instanceof MethodTreeImpl) {
            ((MethodTreeImpl) partial).completeWithModifiers(modifiers);
        } else if (partial instanceof PropertiesTreeImpl) {
            ((PropertiesTreeImpl) partial).completeWithModifiers(modifiers);
        } else if (partial instanceof ArgumentListTreeImpl) {
            return new EmptyStatementTreeImpl(getSemicolonToken(Optional.absent(), partial));
        } else {
            throw new IllegalArgumentException();
        }

        return partial;
    }

    public ExpressionTree newNamedExpression(Optional<Triple<InternalSyntaxToken, InternalSyntaxToken, InternalSyntaxToken>> name,  ExpressionTree expression) {
        return expression;
    }

    public ThrowStatementTreeImpl throwStatement(InternalSyntaxToken throwToken, ExpressionTree expression, Optional<InternalSyntaxToken> semicolonToken) {
        return super.throwStatement(throwToken, expression, getSemicolonToken(semicolonToken, (JavaTree) expression));
    }

    public AssignmentExpressionTreeImpl newElementValuePair(Optional<Tuple<InternalSyntaxToken, InternalSyntaxToken>> operator, ExpressionTree elementValue) {
        if (operator.isPresent())

            return new AssignmentExpressionTreeImpl(
                    Tree.Kind.ASSIGNMENT,
                    new IdentifierTreeImpl(operator.get().first()),
                    operator.get().second(),
                    elementValue);
        else
            return new AssignmentExpressionTreeImpl(
                    Tree.Kind.ASSIGNMENT,
                    new IdentifierTreeImpl(getToken(Optional.absent(), (JavaTree) elementValue, "name...")),
                    getToken(Optional.absent(), (JavaTree) elementValue, "="),
                    elementValue);
    }

    public VariableTreeImpl newCatchFormalParameter(ModifiersTreeImpl modifiers, VariableTreeImpl parameter, Optional<Tuple<InternalSyntaxToken, TypeTree>> optionalType) {
        if (!modifiers.isEmpty()) {
            parameter.completeModifiers(modifiers);
        }

        TypeTree type = optionalType.isPresent() ? optionalType.get().second() : new InferedTypeTree();
        return parameter.completeType(type);
    }

    public ForEachStatementImpl newForeachStatement(
            InternalSyntaxToken forKeyword,
            InternalSyntaxToken openParenToken,
            VariableTreeImpl variable, InternalSyntaxToken colonToken, ExpressionTree expression,
            Optional<Tuple<InternalSyntaxToken, InternalSyntaxToken>> index,
            InternalSyntaxToken closeParenToken,
            StatementTree statement) {
        return new ForEachStatementImpl(forKeyword, openParenToken, variable, colonToken, expression, closeParenToken, statement);
    }

    public ExpressionTree newIdentifierOrMethodInvocation(InternalSyntaxToken identifierToken,
                                                          Optional<TypeArgumentListTreeImpl> typeArguments,
                                                          Optional<ArgumentListTreeImpl> arguments) {
        return super.newIdentifierOrMethodInvocation(typeArguments, identifierToken, arguments);
    }


    public <T, U, V> Triple<T, U, V> newTriple(T first, U second, V third) {
        return new Triple(first, second, third);
    }

    public static class Triple<T, U, V> {
        private final T first;
        private final U second;
        private final V third;

        public Triple(T first, U second, V third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }

        public T first() {
            return first;
        }

        public U second() {
            return second;
        }

        public V third() {
            return third;
        }
    }
}
