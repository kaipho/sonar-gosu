package de.kaipho.sonar.common;

import com.sonar.sslr.api.typed.Optional;
import org.sonar.java.model.InternalSyntaxToken;
import org.sonar.java.model.JavaTree;
import org.sonar.plugins.java.api.tree.InferedTypeTree;
import org.sonar.plugins.java.api.tree.Tree;

import java.util.ArrayList;

public enum TokenResolver {
    ;

    public static InternalSyntaxToken getSemicolonToken(Optional<InternalSyntaxToken> optionalSemicolonToken, JavaTree tree) {
        return getToken(optionalSemicolonToken, tree, ";");
    }
    public static InternalSyntaxToken getToken(Optional<InternalSyntaxToken> optionalSemicolonToken, JavaTree tree, String token) {
        if (optionalSemicolonToken.isPresent()) return optionalSemicolonToken.get();
        InternalSyntaxToken highestToken = highestToken(tree);
        int column = highestToken.column() + highestToken.text().length() - 1;
        return new InternalSyntaxToken(highestToken.line(), column, token, new ArrayList<>(), highestToken.fromIndex(), highestToken.fromIndex(), false);
    }

    private static InternalSyntaxToken highestToken(JavaTree tree) {
        if(tree instanceof InternalSyntaxToken) return (InternalSyntaxToken) tree;
        if(tree instanceof InferedTypeTree) return null;
        InternalSyntaxToken highestToken = null;
        for (Tree tree1 : tree.getChildren()) {
            if (tree1 instanceof InternalSyntaxToken) {
                InternalSyntaxToken ist = (InternalSyntaxToken) tree1;
                highestToken = getHigher(highestToken, ist);
            } else
                highestToken = getHigher(highestToken, highestToken((JavaTree) tree1));
        }
        return highestToken;
    }

    private static InternalSyntaxToken getHigher(InternalSyntaxToken st1, InternalSyntaxToken st2) {
        if (st1 == null) return st2;
        if (st2 == null) return st1;

        if (st1.line() > st2.line()) {
            return st1;
        }
        if (st1.line() < st2.line()) {
            return st2;
        }
        if (st1.column() < st2.column()) {
            return st2;
        }
        return st1;
    }
}
