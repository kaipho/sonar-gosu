package de.kaipho.sonar.gosu;

import com.sonar.sslr.api.typed.ActionParser;
import de.kaipho.sonar.common.GosuTreeFactory;
import org.sonar.java.ast.parser.*;
import org.sonar.java.model.JavaTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.sslr.grammar.LexerlessGrammarBuilder;

import java.io.File;
import java.nio.charset.StandardCharsets;

public class GosuParser extends ActionParser<Tree> {

    private GosuParser(LexerlessGrammarBuilder grammarBuilder, Class<GosuGrammar> javaGrammarClass,
                       GosuTreeFactory treeFactory, JavaNodeBuilder javaNodeBuilder, JavaLexer compilationUnit) {
        super(StandardCharsets.UTF_8, grammarBuilder, javaGrammarClass, treeFactory, javaNodeBuilder, compilationUnit);
    }

    public static ActionParser<Tree> createParser() {
        return new GosuParser(JavaLexer.createGrammarBuilder(),
                GosuGrammar.class,
                new GosuTreeFactory(),
                new JavaNodeBuilder(),
                JavaLexer.COMPILATION_UNIT);
    }

    @Override
    public Tree parse(File file) {
        return createParentLink((JavaTree) super.parse(file));
    }

    @Override
    public Tree parse(String source) {
        return createParentLink((JavaTree) super.parse(source));
    }

    private static Tree createParentLink(JavaTree parent) {
        if (!parent.isLeaf()) {
            for (Tree nextTree : parent.getChildren()) {
                JavaTree next = (JavaTree) nextTree;
                if (next != null) {
                    next.setParent(parent);
                    createParentLink(next);
                }
            }
        }
        return parent;
    }
}
