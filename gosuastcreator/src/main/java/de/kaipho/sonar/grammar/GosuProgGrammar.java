package de.kaipho.sonar.grammar;

import com.sonar.sslr.api.typed.ActionParser;
import de.kaipho.sonar.gosu.GosuGrammar;
import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.debug.ParseTreeBuilder;
import org.antlr.runtime.tree.ParseTree;
import org.sonar.java.ast.parser.*;
import org.sonar.java.model.JavaTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.sslr.grammar.LexerlessGrammarBuilder;

import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class GosuProgGrammar extends ActionParser<Tree> {

    private GosuProgGrammar(LexerlessGrammarBuilder grammarBuilder, Class<GosuGrammar> javaGrammarClass,
                            TreeFactory treeFactory, JavaNodeBuilder javaNodeBuilder, JavaLexer compilationUnit) {
        super(StandardCharsets.UTF_8, grammarBuilder, javaGrammarClass, treeFactory, javaNodeBuilder, compilationUnit);
    }

    @Override
    public Tree parse(File file) {
        try {
            return this.parse(new ANTLRFileStream(file.getAbsolutePath()));
        } catch (IOException e) {
            throw new IOError(e);
        }
    }

    public static ActionParser<Tree> createParser() {
        return new GosuProgGrammar(JavaLexer.createGrammarBuilder(),
                GosuGrammar.class,
                new TreeFactory(),
                new JavaNodeBuilder(),
                JavaLexer.COMPILATION_UNIT);
    }

    @Override
    public Tree parse(String source) {
        return this.parse(new ANTLRStringStream(source));
    }

    private Tree parse(ANTLRStringStream source) {
        GosuProgLexer lexer = new GosuProgLexer(source);
        CommonTokenStream stream = new CommonTokenStream(lexer);
        ParseTreeBuilder builder = new ParseTreeBuilder("GosuProg");
        GosuProgParser parser = new GosuProgParser(stream, builder);

        try {
            parser.start();
            ParseTree tree = builder.getTree();
        } catch (RecognitionException e) {
            throw new RuntimeException(e);
        }

        return null;
        // return createParentLink(javaAst); // TODO tree!
    }

    private static Tree createParentLink(JavaTree parent) {
        if (!parent.isLeaf()) {
            for (Tree nextTree : parent.getChildren()) {
                JavaTree next = (JavaTree) nextTree;
                if (next != null) {
                    next.setParent(parent);
                    createParentLink(next);
                }
            }
        }
        return parent;
    }
}
