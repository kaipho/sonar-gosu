// $ANTLR 3.5.2 GosuProg.g 2017-06-19 20:52:51
package de.kaipho.sonar.grammar;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings("all")
public class GosuProgLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int T__98=98;
	public static final int T__99=99;
	public static final int T__100=100;
	public static final int T__101=101;
	public static final int T__102=102;
	public static final int T__103=103;
	public static final int T__104=104;
	public static final int T__105=105;
	public static final int T__106=106;
	public static final int T__107=107;
	public static final int T__108=108;
	public static final int T__109=109;
	public static final int T__110=110;
	public static final int T__111=111;
	public static final int T__112=112;
	public static final int T__113=113;
	public static final int T__114=114;
	public static final int T__115=115;
	public static final int T__116=116;
	public static final int T__117=117;
	public static final int T__118=118;
	public static final int T__119=119;
	public static final int T__120=120;
	public static final int T__121=121;
	public static final int T__122=122;
	public static final int T__123=123;
	public static final int T__124=124;
	public static final int T__125=125;
	public static final int T__126=126;
	public static final int T__127=127;
	public static final int T__128=128;
	public static final int T__129=129;
	public static final int T__130=130;
	public static final int T__131=131;
	public static final int T__132=132;
	public static final int T__133=133;
	public static final int T__134=134;
	public static final int T__135=135;
	public static final int T__136=136;
	public static final int T__137=137;
	public static final int T__138=138;
	public static final int T__139=139;
	public static final int T__140=140;
	public static final int T__141=141;
	public static final int T__142=142;
	public static final int T__143=143;
	public static final int T__144=144;
	public static final int T__145=145;
	public static final int T__146=146;
	public static final int T__147=147;
	public static final int T__148=148;
	public static final int T__149=149;
	public static final int T__150=150;
	public static final int T__151=151;
	public static final int T__152=152;
	public static final int T__153=153;
	public static final int T__154=154;
	public static final int T__155=155;
	public static final int T__156=156;
	public static final int T__157=157;
	public static final int T__158=158;
	public static final int T__159=159;
	public static final int T__160=160;
	public static final int T__161=161;
	public static final int T__162=162;
	public static final int T__163=163;
	public static final int T__164=164;
	public static final int T__165=165;
	public static final int T__166=166;
	public static final int T__167=167;
	public static final int T__168=168;
	public static final int T__169=169;
	public static final int T__170=170;
	public static final int BinLiteral=4;
	public static final int COMMENT=5;
	public static final int CharLiteral=6;
	public static final int Digit=7;
	public static final int EscapeSequence=8;
	public static final int Exponent=9;
	public static final int FloatTypeSuffix=10;
	public static final int HASHBANG=11;
	public static final int HexDigit=12;
	public static final int HexLiteral=13;
	public static final int Ident=14;
	public static final int IntOrFloatPointLiteral=15;
	public static final int IntegerTypeSuffix=16;
	public static final int LINE_COMMENT=17;
	public static final int Letter=18;
	public static final int NonZeroDigit=19;
	public static final int NumberLiteral=20;
	public static final int OctalEscape=21;
	public static final int StringLiteral=22;
	public static final int UnicodeEscape=23;
	public static final int WS=24;

	  @Override
	  public void reportError(RecognitionException e) {
	    throw new RuntimeException(e);
	  }


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public GosuProgLexer() {} 
	public GosuProgLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public GosuProgLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "GosuProg.g"; }

	// $ANTLR start "T__25"
	public final void mT__25() throws RecognitionException {
		try {
			int _type = T__25;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:10:7: ( '!' )
			// GosuProg.g:10:9: '!'
			{
			match('!'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__25"

	// $ANTLR start "T__26"
	public final void mT__26() throws RecognitionException {
		try {
			int _type = T__26;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:11:7: ( '!*' )
			// GosuProg.g:11:9: '!*'
			{
			match("!*"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__26"

	// $ANTLR start "T__27"
	public final void mT__27() throws RecognitionException {
		try {
			int _type = T__27;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:12:7: ( '!+' )
			// GosuProg.g:12:9: '!+'
			{
			match("!+"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__27"

	// $ANTLR start "T__28"
	public final void mT__28() throws RecognitionException {
		try {
			int _type = T__28;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:13:7: ( '!-' )
			// GosuProg.g:13:9: '!-'
			{
			match("!-"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__28"

	// $ANTLR start "T__29"
	public final void mT__29() throws RecognitionException {
		try {
			int _type = T__29;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:14:7: ( '!=' )
			// GosuProg.g:14:9: '!='
			{
			match("!="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__29"

	// $ANTLR start "T__30"
	public final void mT__30() throws RecognitionException {
		try {
			int _type = T__30;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:15:7: ( '!==' )
			// GosuProg.g:15:9: '!=='
			{
			match("!=="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__30"

	// $ANTLR start "T__31"
	public final void mT__31() throws RecognitionException {
		try {
			int _type = T__31;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:16:7: ( '#' )
			// GosuProg.g:16:9: '#'
			{
			match('#'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__31"

	// $ANTLR start "T__32"
	public final void mT__32() throws RecognitionException {
		try {
			int _type = T__32;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:17:7: ( '%' )
			// GosuProg.g:17:9: '%'
			{
			match('%'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__32"

	// $ANTLR start "T__33"
	public final void mT__33() throws RecognitionException {
		try {
			int _type = T__33;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:18:7: ( '%=' )
			// GosuProg.g:18:9: '%='
			{
			match("%="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__33"

	// $ANTLR start "T__34"
	public final void mT__34() throws RecognitionException {
		try {
			int _type = T__34;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:19:7: ( '&&' )
			// GosuProg.g:19:9: '&&'
			{
			match("&&"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__34"

	// $ANTLR start "T__35"
	public final void mT__35() throws RecognitionException {
		try {
			int _type = T__35;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:20:7: ( '&&=' )
			// GosuProg.g:20:9: '&&='
			{
			match("&&="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__35"

	// $ANTLR start "T__36"
	public final void mT__36() throws RecognitionException {
		try {
			int _type = T__36;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:21:7: ( '&' )
			// GosuProg.g:21:9: '&'
			{
			match('&'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__36"

	// $ANTLR start "T__37"
	public final void mT__37() throws RecognitionException {
		try {
			int _type = T__37;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:22:7: ( '&=' )
			// GosuProg.g:22:9: '&='
			{
			match("&="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__37"

	// $ANTLR start "T__38"
	public final void mT__38() throws RecognitionException {
		try {
			int _type = T__38;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:23:7: ( '(' )
			// GosuProg.g:23:9: '('
			{
			match('('); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__38"

	// $ANTLR start "T__39"
	public final void mT__39() throws RecognitionException {
		try {
			int _type = T__39;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:24:7: ( ')' )
			// GosuProg.g:24:9: ')'
			{
			match(')'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__39"

	// $ANTLR start "T__40"
	public final void mT__40() throws RecognitionException {
		try {
			int _type = T__40;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:25:7: ( '*' )
			// GosuProg.g:25:9: '*'
			{
			match('*'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__40"

	// $ANTLR start "T__41"
	public final void mT__41() throws RecognitionException {
		try {
			int _type = T__41;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:26:7: ( '*.' )
			// GosuProg.g:26:9: '*.'
			{
			match("*."); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__41"

	// $ANTLR start "T__42"
	public final void mT__42() throws RecognitionException {
		try {
			int _type = T__42;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:27:7: ( '*=' )
			// GosuProg.g:27:9: '*='
			{
			match("*="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__42"

	// $ANTLR start "T__43"
	public final void mT__43() throws RecognitionException {
		try {
			int _type = T__43;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:28:7: ( '+' )
			// GosuProg.g:28:9: '+'
			{
			match('+'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__43"

	// $ANTLR start "T__44"
	public final void mT__44() throws RecognitionException {
		try {
			int _type = T__44;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:29:7: ( '++' )
			// GosuProg.g:29:9: '++'
			{
			match("++"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__44"

	// $ANTLR start "T__45"
	public final void mT__45() throws RecognitionException {
		try {
			int _type = T__45;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:30:7: ( '+=' )
			// GosuProg.g:30:9: '+='
			{
			match("+="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__45"

	// $ANTLR start "T__46"
	public final void mT__46() throws RecognitionException {
		try {
			int _type = T__46;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:31:7: ( ',' )
			// GosuProg.g:31:9: ','
			{
			match(','); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__46"

	// $ANTLR start "T__47"
	public final void mT__47() throws RecognitionException {
		try {
			int _type = T__47;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:32:7: ( '-' )
			// GosuProg.g:32:9: '-'
			{
			match('-'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__47"

	// $ANTLR start "T__48"
	public final void mT__48() throws RecognitionException {
		try {
			int _type = T__48;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:33:7: ( '--' )
			// GosuProg.g:33:9: '--'
			{
			match("--"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__48"

	// $ANTLR start "T__49"
	public final void mT__49() throws RecognitionException {
		try {
			int _type = T__49;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:34:7: ( '-=' )
			// GosuProg.g:34:9: '-='
			{
			match("-="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__49"

	// $ANTLR start "T__50"
	public final void mT__50() throws RecognitionException {
		try {
			int _type = T__50;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:35:7: ( '->' )
			// GosuProg.g:35:9: '->'
			{
			match("->"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__50"

	// $ANTLR start "T__51"
	public final void mT__51() throws RecognitionException {
		try {
			int _type = T__51;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:36:7: ( '.' )
			// GosuProg.g:36:9: '.'
			{
			match('.'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__51"

	// $ANTLR start "T__52"
	public final void mT__52() throws RecognitionException {
		try {
			int _type = T__52;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:37:7: ( '..' )
			// GosuProg.g:37:9: '..'
			{
			match(".."); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__52"

	// $ANTLR start "T__53"
	public final void mT__53() throws RecognitionException {
		try {
			int _type = T__53;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:38:7: ( '..|' )
			// GosuProg.g:38:9: '..|'
			{
			match("..|"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__53"

	// $ANTLR start "T__54"
	public final void mT__54() throws RecognitionException {
		try {
			int _type = T__54;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:39:7: ( '/' )
			// GosuProg.g:39:9: '/'
			{
			match('/'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__54"

	// $ANTLR start "T__55"
	public final void mT__55() throws RecognitionException {
		try {
			int _type = T__55;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:40:7: ( '/=' )
			// GosuProg.g:40:9: '/='
			{
			match("/="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__55"

	// $ANTLR start "T__56"
	public final void mT__56() throws RecognitionException {
		try {
			int _type = T__56;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:41:7: ( ':' )
			// GosuProg.g:41:9: ':'
			{
			match(':'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__56"

	// $ANTLR start "T__57"
	public final void mT__57() throws RecognitionException {
		try {
			int _type = T__57;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:42:7: ( ';' )
			// GosuProg.g:42:9: ';'
			{
			match(';'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__57"

	// $ANTLR start "T__58"
	public final void mT__58() throws RecognitionException {
		try {
			int _type = T__58;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:43:7: ( '<' )
			// GosuProg.g:43:9: '<'
			{
			match('<'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__58"

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:44:7: ( '<>' )
			// GosuProg.g:44:9: '<>'
			{
			match("<>"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "T__60"
	public final void mT__60() throws RecognitionException {
		try {
			int _type = T__60;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:45:7: ( '=' )
			// GosuProg.g:45:9: '='
			{
			match('='); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__60"

	// $ANTLR start "T__61"
	public final void mT__61() throws RecognitionException {
		try {
			int _type = T__61;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:46:7: ( '==' )
			// GosuProg.g:46:9: '=='
			{
			match("=="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__61"

	// $ANTLR start "T__62"
	public final void mT__62() throws RecognitionException {
		try {
			int _type = T__62;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:47:7: ( '===' )
			// GosuProg.g:47:9: '==='
			{
			match("==="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__62"

	// $ANTLR start "T__63"
	public final void mT__63() throws RecognitionException {
		try {
			int _type = T__63;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:48:7: ( '>' )
			// GosuProg.g:48:9: '>'
			{
			match('>'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__63"

	// $ANTLR start "T__64"
	public final void mT__64() throws RecognitionException {
		try {
			int _type = T__64;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:49:7: ( '?%' )
			// GosuProg.g:49:9: '?%'
			{
			match("?%"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__64"

	// $ANTLR start "T__65"
	public final void mT__65() throws RecognitionException {
		try {
			int _type = T__65;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:50:7: ( '?' )
			// GosuProg.g:50:9: '?'
			{
			match('?'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__65"

	// $ANTLR start "T__66"
	public final void mT__66() throws RecognitionException {
		try {
			int _type = T__66;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:51:7: ( '?*' )
			// GosuProg.g:51:9: '?*'
			{
			match("?*"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__66"

	// $ANTLR start "T__67"
	public final void mT__67() throws RecognitionException {
		try {
			int _type = T__67;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:52:7: ( '?+' )
			// GosuProg.g:52:9: '?+'
			{
			match("?+"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__67"

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:53:7: ( '?-' )
			// GosuProg.g:53:9: '?-'
			{
			match("?-"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:54:7: ( '?.' )
			// GosuProg.g:54:9: '?.'
			{
			match("?."); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "T__70"
	public final void mT__70() throws RecognitionException {
		try {
			int _type = T__70;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:55:7: ( '?/' )
			// GosuProg.g:55:9: '?/'
			{
			match("?/"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__70"

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:56:7: ( '?:' )
			// GosuProg.g:56:9: '?:'
			{
			match("?:"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:57:7: ( '?[' )
			// GosuProg.g:57:9: '?['
			{
			match("?["); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:58:7: ( '@' )
			// GosuProg.g:58:9: '@'
			{
			match('@'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:59:7: ( 'Infinity' )
			// GosuProg.g:59:9: 'Infinity'
			{
			match("Infinity"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:60:7: ( 'NaN' )
			// GosuProg.g:60:9: 'NaN'
			{
			match("NaN"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:61:7: ( '[' )
			// GosuProg.g:61:9: '['
			{
			match('['); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "T__77"
	public final void mT__77() throws RecognitionException {
		try {
			int _type = T__77;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:62:7: ( '\\\\' )
			// GosuProg.g:62:9: '\\\\'
			{
			match('\\'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__77"

	// $ANTLR start "T__78"
	public final void mT__78() throws RecognitionException {
		try {
			int _type = T__78;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:63:7: ( ']' )
			// GosuProg.g:63:9: ']'
			{
			match(']'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__78"

	// $ANTLR start "T__79"
	public final void mT__79() throws RecognitionException {
		try {
			int _type = T__79;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:64:7: ( '^' )
			// GosuProg.g:64:9: '^'
			{
			match('^'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__79"

	// $ANTLR start "T__80"
	public final void mT__80() throws RecognitionException {
		try {
			int _type = T__80;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:65:7: ( '^=' )
			// GosuProg.g:65:9: '^='
			{
			match("^="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__80"

	// $ANTLR start "T__81"
	public final void mT__81() throws RecognitionException {
		try {
			int _type = T__81;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:66:7: ( 'abstract' )
			// GosuProg.g:66:9: 'abstract'
			{
			match("abstract"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__81"

	// $ANTLR start "T__82"
	public final void mT__82() throws RecognitionException {
		try {
			int _type = T__82;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:67:7: ( 'and' )
			// GosuProg.g:67:9: 'and'
			{
			match("and"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__82"

	// $ANTLR start "T__83"
	public final void mT__83() throws RecognitionException {
		try {
			int _type = T__83;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:68:7: ( 'application' )
			// GosuProg.g:68:9: 'application'
			{
			match("application"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__83"

	// $ANTLR start "T__84"
	public final void mT__84() throws RecognitionException {
		try {
			int _type = T__84;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:69:7: ( 'as' )
			// GosuProg.g:69:9: 'as'
			{
			match("as"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__84"

	// $ANTLR start "T__85"
	public final void mT__85() throws RecognitionException {
		try {
			int _type = T__85;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:70:7: ( 'assert' )
			// GosuProg.g:70:9: 'assert'
			{
			match("assert"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__85"

	// $ANTLR start "T__86"
	public final void mT__86() throws RecognitionException {
		try {
			int _type = T__86;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:71:7: ( 'block' )
			// GosuProg.g:71:9: 'block'
			{
			match("block"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__86"

	// $ANTLR start "T__87"
	public final void mT__87() throws RecognitionException {
		try {
			int _type = T__87;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:72:7: ( 'break' )
			// GosuProg.g:72:9: 'break'
			{
			match("break"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__87"

	// $ANTLR start "T__88"
	public final void mT__88() throws RecognitionException {
		try {
			int _type = T__88;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:73:7: ( 'case' )
			// GosuProg.g:73:9: 'case'
			{
			match("case"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__88"

	// $ANTLR start "T__89"
	public final void mT__89() throws RecognitionException {
		try {
			int _type = T__89;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:74:7: ( 'catch' )
			// GosuProg.g:74:9: 'catch'
			{
			match("catch"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__89"

	// $ANTLR start "T__90"
	public final void mT__90() throws RecognitionException {
		try {
			int _type = T__90;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:75:7: ( 'class' )
			// GosuProg.g:75:9: 'class'
			{
			match("class"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__90"

	// $ANTLR start "T__91"
	public final void mT__91() throws RecognitionException {
		try {
			int _type = T__91;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:76:7: ( 'classpath' )
			// GosuProg.g:76:9: 'classpath'
			{
			match("classpath"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__91"

	// $ANTLR start "T__92"
	public final void mT__92() throws RecognitionException {
		try {
			int _type = T__92;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:77:7: ( 'construct' )
			// GosuProg.g:77:9: 'construct'
			{
			match("construct"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__92"

	// $ANTLR start "T__93"
	public final void mT__93() throws RecognitionException {
		try {
			int _type = T__93;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:78:7: ( 'contains' )
			// GosuProg.g:78:9: 'contains'
			{
			match("contains"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__93"

	// $ANTLR start "T__94"
	public final void mT__94() throws RecognitionException {
		try {
			int _type = T__94;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:79:7: ( 'continue' )
			// GosuProg.g:79:9: 'continue'
			{
			match("continue"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__94"

	// $ANTLR start "T__95"
	public final void mT__95() throws RecognitionException {
		try {
			int _type = T__95;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:80:7: ( 'default' )
			// GosuProg.g:80:9: 'default'
			{
			match("default"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__95"

	// $ANTLR start "T__96"
	public final void mT__96() throws RecognitionException {
		try {
			int _type = T__96;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:81:7: ( 'delegate' )
			// GosuProg.g:81:9: 'delegate'
			{
			match("delegate"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__96"

	// $ANTLR start "T__97"
	public final void mT__97() throws RecognitionException {
		try {
			int _type = T__97;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:82:7: ( 'do' )
			// GosuProg.g:82:9: 'do'
			{
			match("do"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__97"

	// $ANTLR start "T__98"
	public final void mT__98() throws RecognitionException {
		try {
			int _type = T__98;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:83:7: ( 'else' )
			// GosuProg.g:83:9: 'else'
			{
			match("else"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__98"

	// $ANTLR start "T__99"
	public final void mT__99() throws RecognitionException {
		try {
			int _type = T__99;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:84:7: ( 'enhancement' )
			// GosuProg.g:84:9: 'enhancement'
			{
			match("enhancement"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__99"

	// $ANTLR start "T__100"
	public final void mT__100() throws RecognitionException {
		try {
			int _type = T__100;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:85:8: ( 'enum' )
			// GosuProg.g:85:10: 'enum'
			{
			match("enum"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__100"

	// $ANTLR start "T__101"
	public final void mT__101() throws RecognitionException {
		try {
			int _type = T__101;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:86:8: ( 'eval' )
			// GosuProg.g:86:10: 'eval'
			{
			match("eval"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__101"

	// $ANTLR start "T__102"
	public final void mT__102() throws RecognitionException {
		try {
			int _type = T__102;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:87:8: ( 'except' )
			// GosuProg.g:87:10: 'except'
			{
			match("except"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__102"

	// $ANTLR start "T__103"
	public final void mT__103() throws RecognitionException {
		try {
			int _type = T__103;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:88:8: ( 'execution' )
			// GosuProg.g:88:10: 'execution'
			{
			match("execution"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__103"

	// $ANTLR start "T__104"
	public final void mT__104() throws RecognitionException {
		try {
			int _type = T__104;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:89:8: ( 'exists' )
			// GosuProg.g:89:10: 'exists'
			{
			match("exists"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__104"

	// $ANTLR start "T__105"
	public final void mT__105() throws RecognitionException {
		try {
			int _type = T__105;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:90:8: ( 'extends' )
			// GosuProg.g:90:10: 'extends'
			{
			match("extends"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__105"

	// $ANTLR start "T__106"
	public final void mT__106() throws RecognitionException {
		try {
			int _type = T__106;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:91:8: ( 'false' )
			// GosuProg.g:91:10: 'false'
			{
			match("false"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__106"

	// $ANTLR start "T__107"
	public final void mT__107() throws RecognitionException {
		try {
			int _type = T__107;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:92:8: ( 'final' )
			// GosuProg.g:92:10: 'final'
			{
			match("final"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__107"

	// $ANTLR start "T__108"
	public final void mT__108() throws RecognitionException {
		try {
			int _type = T__108;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:93:8: ( 'finally' )
			// GosuProg.g:93:10: 'finally'
			{
			match("finally"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__108"

	// $ANTLR start "T__109"
	public final void mT__109() throws RecognitionException {
		try {
			int _type = T__109;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:94:8: ( 'find' )
			// GosuProg.g:94:10: 'find'
			{
			match("find"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__109"

	// $ANTLR start "T__110"
	public final void mT__110() throws RecognitionException {
		try {
			int _type = T__110;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:95:8: ( 'for' )
			// GosuProg.g:95:10: 'for'
			{
			match("for"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__110"

	// $ANTLR start "T__111"
	public final void mT__111() throws RecognitionException {
		try {
			int _type = T__111;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:96:8: ( 'foreach' )
			// GosuProg.g:96:10: 'foreach'
			{
			match("foreach"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__111"

	// $ANTLR start "T__112"
	public final void mT__112() throws RecognitionException {
		try {
			int _type = T__112;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:97:8: ( 'function' )
			// GosuProg.g:97:10: 'function'
			{
			match("function"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__112"

	// $ANTLR start "T__113"
	public final void mT__113() throws RecognitionException {
		try {
			int _type = T__113;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:98:8: ( 'get' )
			// GosuProg.g:98:10: 'get'
			{
			match("get"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__113"

	// $ANTLR start "T__114"
	public final void mT__114() throws RecognitionException {
		try {
			int _type = T__114;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:99:8: ( 'hide' )
			// GosuProg.g:99:10: 'hide'
			{
			match("hide"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__114"

	// $ANTLR start "T__115"
	public final void mT__115() throws RecognitionException {
		try {
			int _type = T__115;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:100:8: ( 'if' )
			// GosuProg.g:100:10: 'if'
			{
			match("if"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__115"

	// $ANTLR start "T__116"
	public final void mT__116() throws RecognitionException {
		try {
			int _type = T__116;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:101:8: ( 'implements' )
			// GosuProg.g:101:10: 'implements'
			{
			match("implements"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__116"

	// $ANTLR start "T__117"
	public final void mT__117() throws RecognitionException {
		try {
			int _type = T__117;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:102:8: ( 'in' )
			// GosuProg.g:102:10: 'in'
			{
			match("in"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__117"

	// $ANTLR start "T__118"
	public final void mT__118() throws RecognitionException {
		try {
			int _type = T__118;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:103:8: ( 'index' )
			// GosuProg.g:103:10: 'index'
			{
			match("index"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__118"

	// $ANTLR start "T__119"
	public final void mT__119() throws RecognitionException {
		try {
			int _type = T__119;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:104:8: ( 'interface' )
			// GosuProg.g:104:10: 'interface'
			{
			match("interface"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__119"

	// $ANTLR start "T__120"
	public final void mT__120() throws RecognitionException {
		try {
			int _type = T__120;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:105:8: ( 'internal' )
			// GosuProg.g:105:10: 'internal'
			{
			match("internal"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__120"

	// $ANTLR start "T__121"
	public final void mT__121() throws RecognitionException {
		try {
			int _type = T__121;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:106:8: ( 'iterator' )
			// GosuProg.g:106:10: 'iterator'
			{
			match("iterator"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__121"

	// $ANTLR start "T__122"
	public final void mT__122() throws RecognitionException {
		try {
			int _type = T__122;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:107:8: ( 'length' )
			// GosuProg.g:107:10: 'length'
			{
			match("length"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__122"

	// $ANTLR start "T__123"
	public final void mT__123() throws RecognitionException {
		try {
			int _type = T__123;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:108:8: ( 'new' )
			// GosuProg.g:108:10: 'new'
			{
			match("new"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__123"

	// $ANTLR start "T__124"
	public final void mT__124() throws RecognitionException {
		try {
			int _type = T__124;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:109:8: ( 'not' )
			// GosuProg.g:109:10: 'not'
			{
			match("not"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__124"

	// $ANTLR start "T__125"
	public final void mT__125() throws RecognitionException {
		try {
			int _type = T__125;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:110:8: ( 'null' )
			// GosuProg.g:110:10: 'null'
			{
			match("null"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__125"

	// $ANTLR start "T__126"
	public final void mT__126() throws RecognitionException {
		try {
			int _type = T__126;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:111:8: ( 'or' )
			// GosuProg.g:111:10: 'or'
			{
			match("or"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__126"

	// $ANTLR start "T__127"
	public final void mT__127() throws RecognitionException {
		try {
			int _type = T__127;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:112:8: ( 'outer' )
			// GosuProg.g:112:10: 'outer'
			{
			match("outer"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__127"

	// $ANTLR start "T__128"
	public final void mT__128() throws RecognitionException {
		try {
			int _type = T__128;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:113:8: ( 'override' )
			// GosuProg.g:113:10: 'override'
			{
			match("override"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__128"

	// $ANTLR start "T__129"
	public final void mT__129() throws RecognitionException {
		try {
			int _type = T__129;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:114:8: ( 'package' )
			// GosuProg.g:114:10: 'package'
			{
			match("package"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__129"

	// $ANTLR start "T__130"
	public final void mT__130() throws RecognitionException {
		try {
			int _type = T__130;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:115:8: ( 'private' )
			// GosuProg.g:115:10: 'private'
			{
			match("private"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__130"

	// $ANTLR start "T__131"
	public final void mT__131() throws RecognitionException {
		try {
			int _type = T__131;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:116:8: ( 'property' )
			// GosuProg.g:116:10: 'property'
			{
			match("property"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__131"

	// $ANTLR start "T__132"
	public final void mT__132() throws RecognitionException {
		try {
			int _type = T__132;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:117:8: ( 'protected' )
			// GosuProg.g:117:10: 'protected'
			{
			match("protected"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__132"

	// $ANTLR start "T__133"
	public final void mT__133() throws RecognitionException {
		try {
			int _type = T__133;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:118:8: ( 'public' )
			// GosuProg.g:118:10: 'public'
			{
			match("public"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__133"

	// $ANTLR start "T__134"
	public final void mT__134() throws RecognitionException {
		try {
			int _type = T__134;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:119:8: ( 'readonly' )
			// GosuProg.g:119:10: 'readonly'
			{
			match("readonly"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__134"

	// $ANTLR start "T__135"
	public final void mT__135() throws RecognitionException {
		try {
			int _type = T__135;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:120:8: ( 'represents' )
			// GosuProg.g:120:10: 'represents'
			{
			match("represents"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__135"

	// $ANTLR start "T__136"
	public final void mT__136() throws RecognitionException {
		try {
			int _type = T__136;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:121:8: ( 'request' )
			// GosuProg.g:121:10: 'request'
			{
			match("request"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__136"

	// $ANTLR start "T__137"
	public final void mT__137() throws RecognitionException {
		try {
			int _type = T__137;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:122:8: ( 'return' )
			// GosuProg.g:122:10: 'return'
			{
			match("return"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__137"

	// $ANTLR start "T__138"
	public final void mT__138() throws RecognitionException {
		try {
			int _type = T__138;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:123:8: ( 'session' )
			// GosuProg.g:123:10: 'session'
			{
			match("session"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__138"

	// $ANTLR start "T__139"
	public final void mT__139() throws RecognitionException {
		try {
			int _type = T__139;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:124:8: ( 'set' )
			// GosuProg.g:124:10: 'set'
			{
			match("set"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__139"

	// $ANTLR start "T__140"
	public final void mT__140() throws RecognitionException {
		try {
			int _type = T__140;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:125:8: ( 'startswith' )
			// GosuProg.g:125:10: 'startswith'
			{
			match("startswith"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__140"

	// $ANTLR start "T__141"
	public final void mT__141() throws RecognitionException {
		try {
			int _type = T__141;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:126:8: ( 'static' )
			// GosuProg.g:126:10: 'static'
			{
			match("static"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__141"

	// $ANTLR start "T__142"
	public final void mT__142() throws RecognitionException {
		try {
			int _type = T__142;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:127:8: ( 'statictypeof' )
			// GosuProg.g:127:10: 'statictypeof'
			{
			match("statictypeof"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__142"

	// $ANTLR start "T__143"
	public final void mT__143() throws RecognitionException {
		try {
			int _type = T__143;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:128:8: ( 'structure' )
			// GosuProg.g:128:10: 'structure'
			{
			match("structure"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__143"

	// $ANTLR start "T__144"
	public final void mT__144() throws RecognitionException {
		try {
			int _type = T__144;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:129:8: ( 'super' )
			// GosuProg.g:129:10: 'super'
			{
			match("super"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__144"

	// $ANTLR start "T__145"
	public final void mT__145() throws RecognitionException {
		try {
			int _type = T__145;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:130:8: ( 'switch' )
			// GosuProg.g:130:10: 'switch'
			{
			match("switch"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__145"

	// $ANTLR start "T__146"
	public final void mT__146() throws RecognitionException {
		try {
			int _type = T__146;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:131:8: ( 'this' )
			// GosuProg.g:131:10: 'this'
			{
			match("this"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__146"

	// $ANTLR start "T__147"
	public final void mT__147() throws RecognitionException {
		try {
			int _type = T__147;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:132:8: ( 'throw' )
			// GosuProg.g:132:10: 'throw'
			{
			match("throw"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__147"

	// $ANTLR start "T__148"
	public final void mT__148() throws RecognitionException {
		try {
			int _type = T__148;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:133:8: ( 'transient' )
			// GosuProg.g:133:10: 'transient'
			{
			match("transient"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__148"

	// $ANTLR start "T__149"
	public final void mT__149() throws RecognitionException {
		try {
			int _type = T__149;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:134:8: ( 'true' )
			// GosuProg.g:134:10: 'true'
			{
			match("true"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__149"

	// $ANTLR start "T__150"
	public final void mT__150() throws RecognitionException {
		try {
			int _type = T__150;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:135:8: ( 'try' )
			// GosuProg.g:135:10: 'try'
			{
			match("try"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__150"

	// $ANTLR start "T__151"
	public final void mT__151() throws RecognitionException {
		try {
			int _type = T__151;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:136:8: ( 'typeas' )
			// GosuProg.g:136:10: 'typeas'
			{
			match("typeas"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__151"

	// $ANTLR start "T__152"
	public final void mT__152() throws RecognitionException {
		try {
			int _type = T__152;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:137:8: ( 'typeis' )
			// GosuProg.g:137:10: 'typeis'
			{
			match("typeis"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__152"

	// $ANTLR start "T__153"
	public final void mT__153() throws RecognitionException {
		try {
			int _type = T__153;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:138:8: ( 'typeloader' )
			// GosuProg.g:138:10: 'typeloader'
			{
			match("typeloader"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__153"

	// $ANTLR start "T__154"
	public final void mT__154() throws RecognitionException {
		try {
			int _type = T__154;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:139:8: ( 'typeof' )
			// GosuProg.g:139:10: 'typeof'
			{
			match("typeof"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__154"

	// $ANTLR start "T__155"
	public final void mT__155() throws RecognitionException {
		try {
			int _type = T__155;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:140:8: ( 'unless' )
			// GosuProg.g:140:10: 'unless'
			{
			match("unless"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__155"

	// $ANTLR start "T__156"
	public final void mT__156() throws RecognitionException {
		try {
			int _type = T__156;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:141:8: ( 'uses' )
			// GosuProg.g:141:10: 'uses'
			{
			match("uses"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__156"

	// $ANTLR start "T__157"
	public final void mT__157() throws RecognitionException {
		try {
			int _type = T__157;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:142:8: ( 'using' )
			// GosuProg.g:142:10: 'using'
			{
			match("using"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__157"

	// $ANTLR start "T__158"
	public final void mT__158() throws RecognitionException {
		try {
			int _type = T__158;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:143:8: ( 'var' )
			// GosuProg.g:143:10: 'var'
			{
			match("var"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__158"

	// $ANTLR start "T__159"
	public final void mT__159() throws RecognitionException {
		try {
			int _type = T__159;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:144:8: ( 'void' )
			// GosuProg.g:144:10: 'void'
			{
			match("void"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__159"

	// $ANTLR start "T__160"
	public final void mT__160() throws RecognitionException {
		try {
			int _type = T__160;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:145:8: ( 'where' )
			// GosuProg.g:145:10: 'where'
			{
			match("where"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__160"

	// $ANTLR start "T__161"
	public final void mT__161() throws RecognitionException {
		try {
			int _type = T__161;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:146:8: ( 'while' )
			// GosuProg.g:146:10: 'while'
			{
			match("while"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__161"

	// $ANTLR start "T__162"
	public final void mT__162() throws RecognitionException {
		try {
			int _type = T__162;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:147:8: ( '{' )
			// GosuProg.g:147:10: '{'
			{
			match('{'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__162"

	// $ANTLR start "T__163"
	public final void mT__163() throws RecognitionException {
		try {
			int _type = T__163;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:148:8: ( '|' )
			// GosuProg.g:148:10: '|'
			{
			match('|'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__163"

	// $ANTLR start "T__164"
	public final void mT__164() throws RecognitionException {
		try {
			int _type = T__164;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:149:8: ( '|..' )
			// GosuProg.g:149:10: '|..'
			{
			match("|.."); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__164"

	// $ANTLR start "T__165"
	public final void mT__165() throws RecognitionException {
		try {
			int _type = T__165;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:150:8: ( '|..|' )
			// GosuProg.g:150:10: '|..|'
			{
			match("|..|"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__165"

	// $ANTLR start "T__166"
	public final void mT__166() throws RecognitionException {
		try {
			int _type = T__166;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:151:8: ( '|=' )
			// GosuProg.g:151:10: '|='
			{
			match("|="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__166"

	// $ANTLR start "T__167"
	public final void mT__167() throws RecognitionException {
		try {
			int _type = T__167;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:152:8: ( '||' )
			// GosuProg.g:152:10: '||'
			{
			match("||"); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__167"

	// $ANTLR start "T__168"
	public final void mT__168() throws RecognitionException {
		try {
			int _type = T__168;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:153:8: ( '||=' )
			// GosuProg.g:153:10: '||='
			{
			match("||="); if (state.failed) return;

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__168"

	// $ANTLR start "T__169"
	public final void mT__169() throws RecognitionException {
		try {
			int _type = T__169;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:154:8: ( '}' )
			// GosuProg.g:154:10: '}'
			{
			match('}'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__169"

	// $ANTLR start "T__170"
	public final void mT__170() throws RecognitionException {
		try {
			int _type = T__170;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:155:8: ( '~' )
			// GosuProg.g:155:10: '~'
			{
			match('~'); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__170"

	// $ANTLR start "Ident"
	public final void mIdent() throws RecognitionException {
		try {
			int _type = Ident;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:583:7: ( Letter ( Digit | Letter )* )
			// GosuProg.g:583:9: Letter ( Digit | Letter )*
			{
			mLetter(); if (state.failed) return;

			// GosuProg.g:583:16: ( Digit | Letter )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0=='$'||(LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// GosuProg.g:
					{
					if ( input.LA(1)=='$'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Ident"

	// $ANTLR start "NumberLiteral"
	public final void mNumberLiteral() throws RecognitionException {
		try {
			int _type = NumberLiteral;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:585:15: ( 'NaN' | 'Infinity' | HexLiteral | BinLiteral | IntOrFloatPointLiteral )
			int alt2=5;
			switch ( input.LA(1) ) {
			case 'N':
				{
				alt2=1;
				}
				break;
			case 'I':
				{
				alt2=2;
				}
				break;
			case '0':
				{
				switch ( input.LA(2) ) {
				case 'X':
				case 'x':
					{
					alt2=3;
					}
					break;
				case 'b':
					{
					int LA2_6 = input.LA(3);
					if ( ((LA2_6 >= '0' && LA2_6 <= '1')) ) {
						alt2=4;
					}

					else {
						alt2=5;
					}

					}
					break;
				case 'B':
					{
					int LA2_7 = input.LA(3);
					if ( ((LA2_7 >= '0' && LA2_7 <= '1')) ) {
						alt2=4;
					}

					else {
						alt2=5;
					}

					}
					break;
				default:
					alt2=5;
				}
				}
				break;
			case '.':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				{
				alt2=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// GosuProg.g:585:18: 'NaN'
					{
					match("NaN"); if (state.failed) return;

					}
					break;
				case 2 :
					// GosuProg.g:586:18: 'Infinity'
					{
					match("Infinity"); if (state.failed) return;

					}
					break;
				case 3 :
					// GosuProg.g:587:18: HexLiteral
					{
					mHexLiteral(); if (state.failed) return;

					}
					break;
				case 4 :
					// GosuProg.g:588:18: BinLiteral
					{
					mBinLiteral(); if (state.failed) return;

					}
					break;
				case 5 :
					// GosuProg.g:589:18: IntOrFloatPointLiteral
					{
					mIntOrFloatPointLiteral(); if (state.failed) return;

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NumberLiteral"

	// $ANTLR start "BinLiteral"
	public final void mBinLiteral() throws RecognitionException {
		try {
			// GosuProg.g:594:12: ( ( '0b' | '0B' ) ( '0' | '1' )+ ( IntegerTypeSuffix )? )
			// GosuProg.g:594:14: ( '0b' | '0B' ) ( '0' | '1' )+ ( IntegerTypeSuffix )?
			{
			// GosuProg.g:594:14: ( '0b' | '0B' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='0') ) {
				int LA3_1 = input.LA(2);
				if ( (LA3_1=='b') ) {
					alt3=1;
				}
				else if ( (LA3_1=='B') ) {
					alt3=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// GosuProg.g:594:15: '0b'
					{
					match("0b"); if (state.failed) return;

					}
					break;
				case 2 :
					// GosuProg.g:594:20: '0B'
					{
					match("0B"); if (state.failed) return;

					}
					break;

			}

			// GosuProg.g:594:26: ( '0' | '1' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '0' && LA4_0 <= '1')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '1') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			// GosuProg.g:594:39: ( IntegerTypeSuffix )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0=='B'||LA5_0=='L'||LA5_0=='S'||LA5_0=='b'||LA5_0=='l'||LA5_0=='s') ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// GosuProg.g:594:39: IntegerTypeSuffix
					{
					mIntegerTypeSuffix(); if (state.failed) return;

					}
					break;

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BinLiteral"

	// $ANTLR start "HexLiteral"
	public final void mHexLiteral() throws RecognitionException {
		try {
			// GosuProg.g:597:12: ( ( '0x' | '0X' ) ( HexDigit )+ ( 's' | 'S' | 'l' | 'L' )? )
			// GosuProg.g:597:14: ( '0x' | '0X' ) ( HexDigit )+ ( 's' | 'S' | 'l' | 'L' )?
			{
			// GosuProg.g:597:14: ( '0x' | '0X' )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0=='0') ) {
				int LA6_1 = input.LA(2);
				if ( (LA6_1=='x') ) {
					alt6=1;
				}
				else if ( (LA6_1=='X') ) {
					alt6=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// GosuProg.g:597:15: '0x'
					{
					match("0x"); if (state.failed) return;

					}
					break;
				case 2 :
					// GosuProg.g:597:20: '0X'
					{
					match("0X"); if (state.failed) return;

					}
					break;

			}

			// GosuProg.g:597:26: ( HexDigit )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( ((LA7_0 >= '0' && LA7_0 <= '9')||(LA7_0 >= 'A' && LA7_0 <= 'F')||(LA7_0 >= 'a' && LA7_0 <= 'f')) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}

			// GosuProg.g:597:36: ( 's' | 'S' | 'l' | 'L' )?
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0=='L'||LA8_0=='S'||LA8_0=='l'||LA8_0=='s') ) {
				alt8=1;
			}
			switch (alt8) {
				case 1 :
					// GosuProg.g:
					{
					if ( input.LA(1)=='L'||input.LA(1)=='S'||input.LA(1)=='l'||input.LA(1)=='s' ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HexLiteral"

	// $ANTLR start "IntOrFloatPointLiteral"
	public final void mIntOrFloatPointLiteral() throws RecognitionException {
		try {
			// GosuProg.g:600:24: ( '.' ( Digit )+ ( FloatTypeSuffix )? | ( Digit )+ ( ( '.' ~ '.' )=> '.' ( Digit )* ( Exponent )? ( FloatTypeSuffix )? | Exponent ( FloatTypeSuffix )? | FloatTypeSuffix | IntegerTypeSuffix |) )
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0=='.') ) {
				alt17=1;
			}
			else if ( ((LA17_0 >= '0' && LA17_0 <= '9')) ) {
				alt17=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}

			switch (alt17) {
				case 1 :
					// GosuProg.g:600:26: '.' ( Digit )+ ( FloatTypeSuffix )?
					{
					match('.'); if (state.failed) return;
					// GosuProg.g:600:30: ( Digit )+
					int cnt9=0;
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// GosuProg.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt9 >= 1 ) break loop9;
							if (state.backtracking>0) {state.failed=true; return;}
							EarlyExitException eee = new EarlyExitException(9, input);
							throw eee;
						}
						cnt9++;
					}

					// GosuProg.g:600:37: ( FloatTypeSuffix )?
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0=='B'||LA10_0=='D'||LA10_0=='F'||LA10_0=='b'||LA10_0=='d'||LA10_0=='f') ) {
						alt10=1;
					}
					switch (alt10) {
						case 1 :
							// GosuProg.g:600:37: FloatTypeSuffix
							{
							mFloatTypeSuffix(); if (state.failed) return;

							}
							break;

					}

					}
					break;
				case 2 :
					// GosuProg.g:601:26: ( Digit )+ ( ( '.' ~ '.' )=> '.' ( Digit )* ( Exponent )? ( FloatTypeSuffix )? | Exponent ( FloatTypeSuffix )? | FloatTypeSuffix | IntegerTypeSuffix |)
					{
					// GosuProg.g:601:26: ( Digit )+
					int cnt11=0;
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( ((LA11_0 >= '0' && LA11_0 <= '9')) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// GosuProg.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt11 >= 1 ) break loop11;
							if (state.backtracking>0) {state.failed=true; return;}
							EarlyExitException eee = new EarlyExitException(11, input);
							throw eee;
						}
						cnt11++;
					}

					// GosuProg.g:601:33: ( ( '.' ~ '.' )=> '.' ( Digit )* ( Exponent )? ( FloatTypeSuffix )? | Exponent ( FloatTypeSuffix )? | FloatTypeSuffix | IntegerTypeSuffix |)
					int alt16=5;
					int LA16_0 = input.LA(1);
					if ( (LA16_0=='.') && (synpred1_GosuProg())) {
						alt16=1;
					}
					else if ( (LA16_0=='E'||LA16_0=='e') ) {
						alt16=2;
					}
					else if ( (LA16_0=='D'||LA16_0=='F'||LA16_0=='d'||LA16_0=='f') ) {
						alt16=3;
					}
					else if ( (LA16_0=='b') ) {
						int LA16_4 = input.LA(2);
						if ( (LA16_4=='d') ) {
							alt16=3;
						}

						else {
							alt16=4;
						}

					}
					else if ( (LA16_0=='B') ) {
						int LA16_5 = input.LA(2);
						if ( (LA16_5=='D') ) {
							alt16=3;
						}

						else {
							alt16=4;
						}

					}
					else if ( (LA16_0=='L'||LA16_0=='S'||LA16_0=='l'||LA16_0=='s') ) {
						alt16=4;
					}

					else {
						alt16=5;
					}

					switch (alt16) {
						case 1 :
							// GosuProg.g:601:35: ( '.' ~ '.' )=> '.' ( Digit )* ( Exponent )? ( FloatTypeSuffix )?
							{
							match('.'); if (state.failed) return;
							// GosuProg.g:601:52: ( Digit )*
							loop12:
							while (true) {
								int alt12=2;
								int LA12_0 = input.LA(1);
								if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
									alt12=1;
								}

								switch (alt12) {
								case 1 :
									// GosuProg.g:
									{
									if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
										input.consume();
										state.failed=false;
									}
									else {
										if (state.backtracking>0) {state.failed=true; return;}
										MismatchedSetException mse = new MismatchedSetException(null,input);
										recover(mse);
										throw mse;
									}
									}
									break;

								default :
									break loop12;
								}
							}

							// GosuProg.g:601:59: ( Exponent )?
							int alt13=2;
							int LA13_0 = input.LA(1);
							if ( (LA13_0=='E'||LA13_0=='e') ) {
								alt13=1;
							}
							switch (alt13) {
								case 1 :
									// GosuProg.g:601:59: Exponent
									{
									mExponent(); if (state.failed) return;

									}
									break;

							}

							// GosuProg.g:601:69: ( FloatTypeSuffix )?
							int alt14=2;
							int LA14_0 = input.LA(1);
							if ( (LA14_0=='B'||LA14_0=='D'||LA14_0=='F'||LA14_0=='b'||LA14_0=='d'||LA14_0=='f') ) {
								alt14=1;
							}
							switch (alt14) {
								case 1 :
									// GosuProg.g:601:69: FloatTypeSuffix
									{
									mFloatTypeSuffix(); if (state.failed) return;

									}
									break;

							}

							}
							break;
						case 2 :
							// GosuProg.g:602:35: Exponent ( FloatTypeSuffix )?
							{
							mExponent(); if (state.failed) return;

							// GosuProg.g:602:44: ( FloatTypeSuffix )?
							int alt15=2;
							int LA15_0 = input.LA(1);
							if ( (LA15_0=='B'||LA15_0=='D'||LA15_0=='F'||LA15_0=='b'||LA15_0=='d'||LA15_0=='f') ) {
								alt15=1;
							}
							switch (alt15) {
								case 1 :
									// GosuProg.g:602:44: FloatTypeSuffix
									{
									mFloatTypeSuffix(); if (state.failed) return;

									}
									break;

							}

							}
							break;
						case 3 :
							// GosuProg.g:603:35: FloatTypeSuffix
							{
							mFloatTypeSuffix(); if (state.failed) return;

							}
							break;
						case 4 :
							// GosuProg.g:604:35: IntegerTypeSuffix
							{
							mIntegerTypeSuffix(); if (state.failed) return;

							}
							break;
						case 5 :
							// GosuProg.g:606:33: 
							{
							}
							break;

					}

					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IntOrFloatPointLiteral"

	// $ANTLR start "CharLiteral"
	public final void mCharLiteral() throws RecognitionException {
		try {
			int _type = CharLiteral;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:608:13: ( '\\'' ( EscapeSequence |~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) ) '\\'' )
			// GosuProg.g:608:16: '\\'' ( EscapeSequence |~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) ) '\\''
			{
			match('\''); if (state.failed) return;
			// GosuProg.g:608:21: ( EscapeSequence |~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) )
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0=='\\') ) {
				alt18=1;
			}
			else if ( ((LA18_0 >= '\u0000' && LA18_0 <= '\t')||(LA18_0 >= '\u000B' && LA18_0 <= '\f')||(LA18_0 >= '\u000E' && LA18_0 <= '&')||(LA18_0 >= '(' && LA18_0 <= '[')||(LA18_0 >= ']' && LA18_0 <= '\uFFFF')) ) {
				alt18=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 18, 0, input);
				throw nvae;
			}

			switch (alt18) {
				case 1 :
					// GosuProg.g:608:23: EscapeSequence
					{
					mEscapeSequence(); if (state.failed) return;

					}
					break;
				case 2 :
					// GosuProg.g:608:40: ~ ( '\\'' | '\\\\' | '\\r' | '\\n' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			match('\''); if (state.failed) return;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CharLiteral"

	// $ANTLR start "StringLiteral"
	public final void mStringLiteral() throws RecognitionException {
		try {
			int _type = StringLiteral;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:610:15: ( '\"' ( EscapeSequence |~ ( '\\\\' | '\"' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )* ( '\"' | '$\"' ) | '\\'' ( EscapeSequence |~ ( '\\\\' | '\\'' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )* ( '\\'' | '$\\'' ) )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0=='\"') ) {
				alt27=1;
			}
			else if ( (LA27_0=='\'') ) {
				alt27=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// GosuProg.g:610:17: '\"' ( EscapeSequence |~ ( '\\\\' | '\"' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )* ( '\"' | '$\"' )
					{
					match('\"'); if (state.failed) return;
					// GosuProg.g:610:21: ( EscapeSequence |~ ( '\\\\' | '\"' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )*
					loop21:
					while (true) {
						int alt21=4;
						int LA21_0 = input.LA(1);
						if ( (LA21_0=='$') ) {
							int LA21_2 = input.LA(2);
							if ( ((LA21_2 >= '\u0000' && LA21_2 <= '\t')||(LA21_2 >= '\u000B' && LA21_2 <= '\f')||(LA21_2 >= '\u000E' && LA21_2 <= '!')||(LA21_2 >= '#' && LA21_2 <= '\uFFFF')) ) {
								alt21=3;
							}

						}
						else if ( (LA21_0=='\\') ) {
							alt21=1;
						}
						else if ( ((LA21_0 >= '\u0000' && LA21_0 <= '\t')||(LA21_0 >= '\u000B' && LA21_0 <= '\f')||(LA21_0 >= '\u000E' && LA21_0 <= '!')||LA21_0=='#'||(LA21_0 >= '%' && LA21_0 <= '[')||(LA21_0 >= ']' && LA21_0 <= '\uFFFF')) ) {
							alt21=2;
						}

						switch (alt21) {
						case 1 :
							// GosuProg.g:610:23: EscapeSequence
							{
							mEscapeSequence(); if (state.failed) return;

							}
							break;
						case 2 :
							// GosuProg.g:610:40: ~ ( '\\\\' | '\"' | '$' | '\\r' | '\\n' )
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '!')||input.LA(1)=='#'||(input.LA(1) >= '%' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;
						case 3 :
							// GosuProg.g:610:68: '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) )
							{
							match('$'); if (state.failed) return;
							// GosuProg.g:610:71: ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) )
							int alt20=2;
							int LA20_0 = input.LA(1);
							if ( (LA20_0=='{') ) {
								alt20=1;
							}
							else if ( ((LA20_0 >= '\u0000' && LA20_0 <= '\t')||(LA20_0 >= '\u000B' && LA20_0 <= '\f')||(LA20_0 >= '\u000E' && LA20_0 <= '!')||(LA20_0 >= '#' && LA20_0 <= 'z')||(LA20_0 >= '|' && LA20_0 <= '\uFFFF')) ) {
								alt20=2;
							}

							else {
								if (state.backtracking>0) {state.failed=true; return;}
								NoViableAltException nvae =
									new NoViableAltException("", 20, 0, input);
								throw nvae;
							}

							switch (alt20) {
								case 1 :
									// GosuProg.g:610:72: '{' (~ ( '}' ) )* '}'
									{
									match('{'); if (state.failed) return;
									// GosuProg.g:610:76: (~ ( '}' ) )*
									loop19:
									while (true) {
										int alt19=2;
										int LA19_0 = input.LA(1);
										if ( ((LA19_0 >= '\u0000' && LA19_0 <= '|')||(LA19_0 >= '~' && LA19_0 <= '\uFFFF')) ) {
											alt19=1;
										}

										switch (alt19) {
										case 1 :
											// GosuProg.g:
											{
											if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '|')||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
												input.consume();
												state.failed=false;
											}
											else {
												if (state.backtracking>0) {state.failed=true; return;}
												MismatchedSetException mse = new MismatchedSetException(null,input);
												recover(mse);
												throw mse;
											}
											}
											break;

										default :
											break loop19;
										}
									}

									match('}'); if (state.failed) return;
									}
									break;
								case 2 :
									// GosuProg.g:610:89: ~ ( '{' | '\"' | '\\r' | '\\n' )
									{
									if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= 'z')||(input.LA(1) >= '|' && input.LA(1) <= '\uFFFF') ) {
										input.consume();
										state.failed=false;
									}
									else {
										if (state.backtracking>0) {state.failed=true; return;}
										MismatchedSetException mse = new MismatchedSetException(null,input);
										recover(mse);
										throw mse;
									}
									}
									break;

							}

							}
							break;

						default :
							break loop21;
						}
					}

					// GosuProg.g:610:115: ( '\"' | '$\"' )
					int alt22=2;
					int LA22_0 = input.LA(1);
					if ( (LA22_0=='\"') ) {
						alt22=1;
					}
					else if ( (LA22_0=='$') ) {
						alt22=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 22, 0, input);
						throw nvae;
					}

					switch (alt22) {
						case 1 :
							// GosuProg.g:610:116: '\"'
							{
							match('\"'); if (state.failed) return;
							}
							break;
						case 2 :
							// GosuProg.g:610:120: '$\"'
							{
							match("$\""); if (state.failed) return;

							}
							break;

					}

					}
					break;
				case 2 :
					// GosuProg.g:611:17: '\\'' ( EscapeSequence |~ ( '\\\\' | '\\'' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )* ( '\\'' | '$\\'' )
					{
					match('\''); if (state.failed) return;
					// GosuProg.g:611:22: ( EscapeSequence |~ ( '\\\\' | '\\'' | '$' | '\\r' | '\\n' ) | '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) ) )*
					loop25:
					while (true) {
						int alt25=4;
						int LA25_0 = input.LA(1);
						if ( (LA25_0=='$') ) {
							int LA25_2 = input.LA(2);
							if ( (LA25_2=='\'') ) {
								int LA25_5 = input.LA(3);
								if ( ((LA25_5 >= '\u0000' && LA25_5 <= '\t')||(LA25_5 >= '\u000B' && LA25_5 <= '\f')||(LA25_5 >= '\u000E' && LA25_5 <= '\uFFFF')) ) {
									alt25=3;
								}

							}
							else if ( ((LA25_2 >= '\u0000' && LA25_2 <= '\t')||(LA25_2 >= '\u000B' && LA25_2 <= '\f')||(LA25_2 >= '\u000E' && LA25_2 <= '!')||(LA25_2 >= '#' && LA25_2 <= '&')||(LA25_2 >= '(' && LA25_2 <= '\uFFFF')) ) {
								alt25=3;
							}

						}
						else if ( (LA25_0=='\\') ) {
							alt25=1;
						}
						else if ( ((LA25_0 >= '\u0000' && LA25_0 <= '\t')||(LA25_0 >= '\u000B' && LA25_0 <= '\f')||(LA25_0 >= '\u000E' && LA25_0 <= '#')||(LA25_0 >= '%' && LA25_0 <= '&')||(LA25_0 >= '(' && LA25_0 <= '[')||(LA25_0 >= ']' && LA25_0 <= '\uFFFF')) ) {
							alt25=2;
						}

						switch (alt25) {
						case 1 :
							// GosuProg.g:611:24: EscapeSequence
							{
							mEscapeSequence(); if (state.failed) return;

							}
							break;
						case 2 :
							// GosuProg.g:611:41: ~ ( '\\\\' | '\\'' | '$' | '\\r' | '\\n' )
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '#')||(input.LA(1) >= '%' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;
						case 3 :
							// GosuProg.g:611:69: '$' ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) )
							{
							match('$'); if (state.failed) return;
							// GosuProg.g:611:72: ( '{' (~ ( '}' ) )* '}' |~ ( '{' | '\"' | '\\r' | '\\n' ) )
							int alt24=2;
							int LA24_0 = input.LA(1);
							if ( (LA24_0=='{') ) {
								alt24=1;
							}
							else if ( ((LA24_0 >= '\u0000' && LA24_0 <= '\t')||(LA24_0 >= '\u000B' && LA24_0 <= '\f')||(LA24_0 >= '\u000E' && LA24_0 <= '!')||(LA24_0 >= '#' && LA24_0 <= 'z')||(LA24_0 >= '|' && LA24_0 <= '\uFFFF')) ) {
								alt24=2;
							}

							else {
								if (state.backtracking>0) {state.failed=true; return;}
								NoViableAltException nvae =
									new NoViableAltException("", 24, 0, input);
								throw nvae;
							}

							switch (alt24) {
								case 1 :
									// GosuProg.g:611:73: '{' (~ ( '}' ) )* '}'
									{
									match('{'); if (state.failed) return;
									// GosuProg.g:611:77: (~ ( '}' ) )*
									loop23:
									while (true) {
										int alt23=2;
										int LA23_0 = input.LA(1);
										if ( ((LA23_0 >= '\u0000' && LA23_0 <= '|')||(LA23_0 >= '~' && LA23_0 <= '\uFFFF')) ) {
											alt23=1;
										}

										switch (alt23) {
										case 1 :
											// GosuProg.g:
											{
											if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '|')||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
												input.consume();
												state.failed=false;
											}
											else {
												if (state.backtracking>0) {state.failed=true; return;}
												MismatchedSetException mse = new MismatchedSetException(null,input);
												recover(mse);
												throw mse;
											}
											}
											break;

										default :
											break loop23;
										}
									}

									match('}'); if (state.failed) return;
									}
									break;
								case 2 :
									// GosuProg.g:611:90: ~ ( '{' | '\"' | '\\r' | '\\n' )
									{
									if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= 'z')||(input.LA(1) >= '|' && input.LA(1) <= '\uFFFF') ) {
										input.consume();
										state.failed=false;
									}
									else {
										if (state.backtracking>0) {state.failed=true; return;}
										MismatchedSetException mse = new MismatchedSetException(null,input);
										recover(mse);
										throw mse;
									}
									}
									break;

							}

							}
							break;

						default :
							break loop25;
						}
					}

					// GosuProg.g:611:116: ( '\\'' | '$\\'' )
					int alt26=2;
					int LA26_0 = input.LA(1);
					if ( (LA26_0=='\'') ) {
						alt26=1;
					}
					else if ( (LA26_0=='$') ) {
						alt26=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 26, 0, input);
						throw nvae;
					}

					switch (alt26) {
						case 1 :
							// GosuProg.g:611:117: '\\''
							{
							match('\''); if (state.failed) return;
							}
							break;
						case 2 :
							// GosuProg.g:611:122: '$\\''
							{
							match("$'"); if (state.failed) return;

							}
							break;

					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "StringLiteral"

	// $ANTLR start "HexDigit"
	public final void mHexDigit() throws RecognitionException {
		try {
			// GosuProg.g:616:10: ( Digit | 'a' .. 'f' | 'A' .. 'F' )
			// GosuProg.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HexDigit"

	// $ANTLR start "IntegerTypeSuffix"
	public final void mIntegerTypeSuffix() throws RecognitionException {
		try {
			// GosuProg.g:619:19: ( ( 'l' | 'L' | 's' | 'S' | 'bi' | 'BI' | 'b' | 'B' ) )
			// GosuProg.g:619:21: ( 'l' | 'L' | 's' | 'S' | 'bi' | 'BI' | 'b' | 'B' )
			{
			// GosuProg.g:619:21: ( 'l' | 'L' | 's' | 'S' | 'bi' | 'BI' | 'b' | 'B' )
			int alt28=8;
			switch ( input.LA(1) ) {
			case 'l':
				{
				alt28=1;
				}
				break;
			case 'L':
				{
				alt28=2;
				}
				break;
			case 's':
				{
				alt28=3;
				}
				break;
			case 'S':
				{
				alt28=4;
				}
				break;
			case 'b':
				{
				int LA28_5 = input.LA(2);
				if ( (LA28_5=='i') ) {
					alt28=5;
				}

				else {
					alt28=7;
				}

				}
				break;
			case 'B':
				{
				int LA28_6 = input.LA(2);
				if ( (LA28_6=='I') ) {
					alt28=6;
				}

				else {
					alt28=8;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			switch (alt28) {
				case 1 :
					// GosuProg.g:619:22: 'l'
					{
					match('l'); if (state.failed) return;
					}
					break;
				case 2 :
					// GosuProg.g:619:26: 'L'
					{
					match('L'); if (state.failed) return;
					}
					break;
				case 3 :
					// GosuProg.g:619:30: 's'
					{
					match('s'); if (state.failed) return;
					}
					break;
				case 4 :
					// GosuProg.g:619:34: 'S'
					{
					match('S'); if (state.failed) return;
					}
					break;
				case 5 :
					// GosuProg.g:619:38: 'bi'
					{
					match("bi"); if (state.failed) return;

					}
					break;
				case 6 :
					// GosuProg.g:619:43: 'BI'
					{
					match("BI"); if (state.failed) return;

					}
					break;
				case 7 :
					// GosuProg.g:619:48: 'b'
					{
					match('b'); if (state.failed) return;
					}
					break;
				case 8 :
					// GosuProg.g:619:52: 'B'
					{
					match('B'); if (state.failed) return;
					}
					break;

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IntegerTypeSuffix"

	// $ANTLR start "Letter"
	public final void mLetter() throws RecognitionException {
		try {
			// GosuProg.g:622:8: ( 'A' .. 'Z' | 'a' .. 'z' | '_' | '$' )
			// GosuProg.g:
			{
			if ( input.LA(1)=='$'||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Letter"

	// $ANTLR start "Digit"
	public final void mDigit() throws RecognitionException {
		try {
			// GosuProg.g:625:7: ( '0' .. '9' )
			// GosuProg.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Digit"

	// $ANTLR start "NonZeroDigit"
	public final void mNonZeroDigit() throws RecognitionException {
		try {
			// GosuProg.g:628:14: ( '1' .. '9' )
			// GosuProg.g:
			{
			if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
				input.consume();
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NonZeroDigit"

	// $ANTLR start "Exponent"
	public final void mExponent() throws RecognitionException {
		try {
			// GosuProg.g:631:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( Digit )+ )
			// GosuProg.g:631:12: ( 'e' | 'E' ) ( '+' | '-' )? ( Digit )+
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// GosuProg.g:631:22: ( '+' | '-' )?
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0=='+'||LA29_0=='-') ) {
				alt29=1;
			}
			switch (alt29) {
				case 1 :
					// GosuProg.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// GosuProg.g:631:33: ( Digit )+
			int cnt30=0;
			loop30:
			while (true) {
				int alt30=2;
				int LA30_0 = input.LA(1);
				if ( ((LA30_0 >= '0' && LA30_0 <= '9')) ) {
					alt30=1;
				}

				switch (alt30) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt30 >= 1 ) break loop30;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(30, input);
					throw eee;
				}
				cnt30++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Exponent"

	// $ANTLR start "FloatTypeSuffix"
	public final void mFloatTypeSuffix() throws RecognitionException {
		try {
			// GosuProg.g:634:17: ( ( 'f' | 'F' | 'd' | 'D' | 'bd' | 'BD' ) )
			// GosuProg.g:634:19: ( 'f' | 'F' | 'd' | 'D' | 'bd' | 'BD' )
			{
			// GosuProg.g:634:19: ( 'f' | 'F' | 'd' | 'D' | 'bd' | 'BD' )
			int alt31=6;
			switch ( input.LA(1) ) {
			case 'f':
				{
				alt31=1;
				}
				break;
			case 'F':
				{
				alt31=2;
				}
				break;
			case 'd':
				{
				alt31=3;
				}
				break;
			case 'D':
				{
				alt31=4;
				}
				break;
			case 'b':
				{
				alt31=5;
				}
				break;
			case 'B':
				{
				alt31=6;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 31, 0, input);
				throw nvae;
			}
			switch (alt31) {
				case 1 :
					// GosuProg.g:634:20: 'f'
					{
					match('f'); if (state.failed) return;
					}
					break;
				case 2 :
					// GosuProg.g:634:24: 'F'
					{
					match('F'); if (state.failed) return;
					}
					break;
				case 3 :
					// GosuProg.g:634:28: 'd'
					{
					match('d'); if (state.failed) return;
					}
					break;
				case 4 :
					// GosuProg.g:634:32: 'D'
					{
					match('D'); if (state.failed) return;
					}
					break;
				case 5 :
					// GosuProg.g:634:36: 'bd'
					{
					match("bd"); if (state.failed) return;

					}
					break;
				case 6 :
					// GosuProg.g:634:41: 'BD'
					{
					match("BD"); if (state.failed) return;

					}
					break;

			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FloatTypeSuffix"

	// $ANTLR start "EscapeSequence"
	public final void mEscapeSequence() throws RecognitionException {
		try {
			// GosuProg.g:637:16: ( '\\\\' ( 'v' | 'a' | 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '$' | '<' ) | UnicodeEscape | OctalEscape )
			int alt32=3;
			int LA32_0 = input.LA(1);
			if ( (LA32_0=='\\') ) {
				switch ( input.LA(2) ) {
				case '\"':
				case '$':
				case '\'':
				case '<':
				case '\\':
				case 'a':
				case 'b':
				case 'f':
				case 'n':
				case 'r':
				case 't':
				case 'v':
					{
					alt32=1;
					}
					break;
				case 'u':
					{
					alt32=2;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
					{
					alt32=3;
					}
					break;
				default:
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 32, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 32, 0, input);
				throw nvae;
			}

			switch (alt32) {
				case 1 :
					// GosuProg.g:637:20: '\\\\' ( 'v' | 'a' | 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '$' | '<' )
					{
					match('\\'); if (state.failed) return;
					if ( input.LA(1)=='\"'||input.LA(1)=='$'||input.LA(1)=='\''||input.LA(1)=='<'||input.LA(1)=='\\'||(input.LA(1) >= 'a' && input.LA(1) <= 'b')||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t'||input.LA(1)=='v' ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// GosuProg.g:638:20: UnicodeEscape
					{
					mUnicodeEscape(); if (state.failed) return;

					}
					break;
				case 3 :
					// GosuProg.g:639:20: OctalEscape
					{
					mOctalEscape(); if (state.failed) return;

					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EscapeSequence"

	// $ANTLR start "OctalEscape"
	public final void mOctalEscape() throws RecognitionException {
		try {
			// GosuProg.g:643:13: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
			int alt33=3;
			int LA33_0 = input.LA(1);
			if ( (LA33_0=='\\') ) {
				int LA33_1 = input.LA(2);
				if ( ((LA33_1 >= '0' && LA33_1 <= '3')) ) {
					int LA33_2 = input.LA(3);
					if ( ((LA33_2 >= '0' && LA33_2 <= '7')) ) {
						int LA33_4 = input.LA(4);
						if ( ((LA33_4 >= '0' && LA33_4 <= '7')) ) {
							alt33=1;
						}

						else {
							alt33=2;
						}

					}

					else {
						alt33=3;
					}

				}
				else if ( ((LA33_1 >= '4' && LA33_1 <= '7')) ) {
					int LA33_3 = input.LA(3);
					if ( ((LA33_3 >= '0' && LA33_3 <= '7')) ) {
						alt33=2;
					}

					else {
						alt33=3;
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 33, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 33, 0, input);
				throw nvae;
			}

			switch (alt33) {
				case 1 :
					// GosuProg.g:643:15: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); if (state.failed) return;
					if ( (input.LA(1) >= '0' && input.LA(1) <= '3') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// GosuProg.g:644:15: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); if (state.failed) return;
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// GosuProg.g:645:15: '\\\\' ( '0' .. '7' )
					{
					match('\\'); if (state.failed) return;
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OctalEscape"

	// $ANTLR start "UnicodeEscape"
	public final void mUnicodeEscape() throws RecognitionException {
		try {
			// GosuProg.g:649:15: ( '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit )
			// GosuProg.g:649:17: '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit
			{
			match('\\'); if (state.failed) return;
			match('u'); if (state.failed) return;
			mHexDigit(); if (state.failed) return;

			mHexDigit(); if (state.failed) return;

			mHexDigit(); if (state.failed) return;

			mHexDigit(); if (state.failed) return;

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UnicodeEscape"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:650:4: ( ( ' ' | '\\r' | '\\t' | '\\n' )+ )
			// GosuProg.g:650:8: ( ' ' | '\\r' | '\\t' | '\\n' )+
			{
			// GosuProg.g:650:8: ( ' ' | '\\r' | '\\t' | '\\n' )+
			int cnt34=0;
			loop34:
			while (true) {
				int alt34=2;
				int LA34_0 = input.LA(1);
				if ( ((LA34_0 >= '\t' && LA34_0 <= '\n')||LA34_0=='\r'||LA34_0==' ') ) {
					alt34=1;
				}

				switch (alt34) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt34 >= 1 ) break loop34;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(34, input);
					throw eee;
				}
				cnt34++;
			}

			if ( state.backtracking==0 ) { skip();  }
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:652:9: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// GosuProg.g:652:13: '/*' ( options {greedy=false; } : . )* '*/'
			{
			match("/*"); if (state.failed) return;

			// GosuProg.g:652:18: ( options {greedy=false; } : . )*
			loop35:
			while (true) {
				int alt35=2;
				int LA35_0 = input.LA(1);
				if ( (LA35_0=='*') ) {
					int LA35_1 = input.LA(2);
					if ( (LA35_1=='/') ) {
						alt35=2;
					}
					else if ( ((LA35_1 >= '\u0000' && LA35_1 <= '.')||(LA35_1 >= '0' && LA35_1 <= '\uFFFF')) ) {
						alt35=1;
					}

				}
				else if ( ((LA35_0 >= '\u0000' && LA35_0 <= ')')||(LA35_0 >= '+' && LA35_0 <= '\uFFFF')) ) {
					alt35=1;
				}

				switch (alt35) {
				case 1 :
					// GosuProg.g:652:47: .
					{
					matchAny(); if (state.failed) return;
					}
					break;

				default :
					break loop35;
				}
			}

			match("*/"); if (state.failed) return;

			if ( state.backtracking==0 ) { _channel=HIDDEN; }
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "LINE_COMMENT"
	public final void mLINE_COMMENT() throws RecognitionException {
		try {
			int _type = LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:654:14: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? ( '\\n' )? )
			// GosuProg.g:654:17: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? ( '\\n' )?
			{
			match("//"); if (state.failed) return;

			// GosuProg.g:654:22: (~ ( '\\n' | '\\r' ) )*
			loop36:
			while (true) {
				int alt36=2;
				int LA36_0 = input.LA(1);
				if ( ((LA36_0 >= '\u0000' && LA36_0 <= '\t')||(LA36_0 >= '\u000B' && LA36_0 <= '\f')||(LA36_0 >= '\u000E' && LA36_0 <= '\uFFFF')) ) {
					alt36=1;
				}

				switch (alt36) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop36;
				}
			}

			// GosuProg.g:654:37: ( '\\r' )?
			int alt37=2;
			int LA37_0 = input.LA(1);
			if ( (LA37_0=='\r') ) {
				alt37=1;
			}
			switch (alt37) {
				case 1 :
					// GosuProg.g:654:37: '\\r'
					{
					match('\r'); if (state.failed) return;
					}
					break;

			}

			// GosuProg.g:654:43: ( '\\n' )?
			int alt38=2;
			int LA38_0 = input.LA(1);
			if ( (LA38_0=='\n') ) {
				alt38=1;
			}
			switch (alt38) {
				case 1 :
					// GosuProg.g:654:43: '\\n'
					{
					match('\n'); if (state.failed) return;
					}
					break;

			}

			if ( state.backtracking==0 ) { _channel=HIDDEN; }
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMENT"

	// $ANTLR start "HASHBANG"
	public final void mHASHBANG() throws RecognitionException {
		try {
			int _type = HASHBANG;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// GosuProg.g:656:10: ( '#!' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? ( '\\n' )? )
			// GosuProg.g:656:12: '#!' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? ( '\\n' )?
			{
			match("#!"); if (state.failed) return;

			// GosuProg.g:656:17: (~ ( '\\n' | '\\r' ) )*
			loop39:
			while (true) {
				int alt39=2;
				int LA39_0 = input.LA(1);
				if ( ((LA39_0 >= '\u0000' && LA39_0 <= '\t')||(LA39_0 >= '\u000B' && LA39_0 <= '\f')||(LA39_0 >= '\u000E' && LA39_0 <= '\uFFFF')) ) {
					alt39=1;
				}

				switch (alt39) {
				case 1 :
					// GosuProg.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop39;
				}
			}

			// GosuProg.g:656:32: ( '\\r' )?
			int alt40=2;
			int LA40_0 = input.LA(1);
			if ( (LA40_0=='\r') ) {
				alt40=1;
			}
			switch (alt40) {
				case 1 :
					// GosuProg.g:656:32: '\\r'
					{
					match('\r'); if (state.failed) return;
					}
					break;

			}

			// GosuProg.g:656:38: ( '\\n' )?
			int alt41=2;
			int LA41_0 = input.LA(1);
			if ( (LA41_0=='\n') ) {
				alt41=1;
			}
			switch (alt41) {
				case 1 :
					// GosuProg.g:656:38: '\\n'
					{
					match('\n'); if (state.failed) return;
					}
					break;

			}

			if ( state.backtracking==0 ) { skip(); }
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HASHBANG"

	@Override
	public void mTokens() throws RecognitionException {
		// GosuProg.g:1:8: ( T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | T__140 | T__141 | T__142 | T__143 | T__144 | T__145 | T__146 | T__147 | T__148 | T__149 | T__150 | T__151 | T__152 | T__153 | T__154 | T__155 | T__156 | T__157 | T__158 | T__159 | T__160 | T__161 | T__162 | T__163 | T__164 | T__165 | T__166 | T__167 | T__168 | T__169 | T__170 | Ident | NumberLiteral | CharLiteral | StringLiteral | WS | COMMENT | LINE_COMMENT | HASHBANG )
		int alt42=154;
		alt42 = dfa42.predict(input);
		switch (alt42) {
			case 1 :
				// GosuProg.g:1:10: T__25
				{
				mT__25(); if (state.failed) return;

				}
				break;
			case 2 :
				// GosuProg.g:1:16: T__26
				{
				mT__26(); if (state.failed) return;

				}
				break;
			case 3 :
				// GosuProg.g:1:22: T__27
				{
				mT__27(); if (state.failed) return;

				}
				break;
			case 4 :
				// GosuProg.g:1:28: T__28
				{
				mT__28(); if (state.failed) return;

				}
				break;
			case 5 :
				// GosuProg.g:1:34: T__29
				{
				mT__29(); if (state.failed) return;

				}
				break;
			case 6 :
				// GosuProg.g:1:40: T__30
				{
				mT__30(); if (state.failed) return;

				}
				break;
			case 7 :
				// GosuProg.g:1:46: T__31
				{
				mT__31(); if (state.failed) return;

				}
				break;
			case 8 :
				// GosuProg.g:1:52: T__32
				{
				mT__32(); if (state.failed) return;

				}
				break;
			case 9 :
				// GosuProg.g:1:58: T__33
				{
				mT__33(); if (state.failed) return;

				}
				break;
			case 10 :
				// GosuProg.g:1:64: T__34
				{
				mT__34(); if (state.failed) return;

				}
				break;
			case 11 :
				// GosuProg.g:1:70: T__35
				{
				mT__35(); if (state.failed) return;

				}
				break;
			case 12 :
				// GosuProg.g:1:76: T__36
				{
				mT__36(); if (state.failed) return;

				}
				break;
			case 13 :
				// GosuProg.g:1:82: T__37
				{
				mT__37(); if (state.failed) return;

				}
				break;
			case 14 :
				// GosuProg.g:1:88: T__38
				{
				mT__38(); if (state.failed) return;

				}
				break;
			case 15 :
				// GosuProg.g:1:94: T__39
				{
				mT__39(); if (state.failed) return;

				}
				break;
			case 16 :
				// GosuProg.g:1:100: T__40
				{
				mT__40(); if (state.failed) return;

				}
				break;
			case 17 :
				// GosuProg.g:1:106: T__41
				{
				mT__41(); if (state.failed) return;

				}
				break;
			case 18 :
				// GosuProg.g:1:112: T__42
				{
				mT__42(); if (state.failed) return;

				}
				break;
			case 19 :
				// GosuProg.g:1:118: T__43
				{
				mT__43(); if (state.failed) return;

				}
				break;
			case 20 :
				// GosuProg.g:1:124: T__44
				{
				mT__44(); if (state.failed) return;

				}
				break;
			case 21 :
				// GosuProg.g:1:130: T__45
				{
				mT__45(); if (state.failed) return;

				}
				break;
			case 22 :
				// GosuProg.g:1:136: T__46
				{
				mT__46(); if (state.failed) return;

				}
				break;
			case 23 :
				// GosuProg.g:1:142: T__47
				{
				mT__47(); if (state.failed) return;

				}
				break;
			case 24 :
				// GosuProg.g:1:148: T__48
				{
				mT__48(); if (state.failed) return;

				}
				break;
			case 25 :
				// GosuProg.g:1:154: T__49
				{
				mT__49(); if (state.failed) return;

				}
				break;
			case 26 :
				// GosuProg.g:1:160: T__50
				{
				mT__50(); if (state.failed) return;

				}
				break;
			case 27 :
				// GosuProg.g:1:166: T__51
				{
				mT__51(); if (state.failed) return;

				}
				break;
			case 28 :
				// GosuProg.g:1:172: T__52
				{
				mT__52(); if (state.failed) return;

				}
				break;
			case 29 :
				// GosuProg.g:1:178: T__53
				{
				mT__53(); if (state.failed) return;

				}
				break;
			case 30 :
				// GosuProg.g:1:184: T__54
				{
				mT__54(); if (state.failed) return;

				}
				break;
			case 31 :
				// GosuProg.g:1:190: T__55
				{
				mT__55(); if (state.failed) return;

				}
				break;
			case 32 :
				// GosuProg.g:1:196: T__56
				{
				mT__56(); if (state.failed) return;

				}
				break;
			case 33 :
				// GosuProg.g:1:202: T__57
				{
				mT__57(); if (state.failed) return;

				}
				break;
			case 34 :
				// GosuProg.g:1:208: T__58
				{
				mT__58(); if (state.failed) return;

				}
				break;
			case 35 :
				// GosuProg.g:1:214: T__59
				{
				mT__59(); if (state.failed) return;

				}
				break;
			case 36 :
				// GosuProg.g:1:220: T__60
				{
				mT__60(); if (state.failed) return;

				}
				break;
			case 37 :
				// GosuProg.g:1:226: T__61
				{
				mT__61(); if (state.failed) return;

				}
				break;
			case 38 :
				// GosuProg.g:1:232: T__62
				{
				mT__62(); if (state.failed) return;

				}
				break;
			case 39 :
				// GosuProg.g:1:238: T__63
				{
				mT__63(); if (state.failed) return;

				}
				break;
			case 40 :
				// GosuProg.g:1:244: T__64
				{
				mT__64(); if (state.failed) return;

				}
				break;
			case 41 :
				// GosuProg.g:1:250: T__65
				{
				mT__65(); if (state.failed) return;

				}
				break;
			case 42 :
				// GosuProg.g:1:256: T__66
				{
				mT__66(); if (state.failed) return;

				}
				break;
			case 43 :
				// GosuProg.g:1:262: T__67
				{
				mT__67(); if (state.failed) return;

				}
				break;
			case 44 :
				// GosuProg.g:1:268: T__68
				{
				mT__68(); if (state.failed) return;

				}
				break;
			case 45 :
				// GosuProg.g:1:274: T__69
				{
				mT__69(); if (state.failed) return;

				}
				break;
			case 46 :
				// GosuProg.g:1:280: T__70
				{
				mT__70(); if (state.failed) return;

				}
				break;
			case 47 :
				// GosuProg.g:1:286: T__71
				{
				mT__71(); if (state.failed) return;

				}
				break;
			case 48 :
				// GosuProg.g:1:292: T__72
				{
				mT__72(); if (state.failed) return;

				}
				break;
			case 49 :
				// GosuProg.g:1:298: T__73
				{
				mT__73(); if (state.failed) return;

				}
				break;
			case 50 :
				// GosuProg.g:1:304: T__74
				{
				mT__74(); if (state.failed) return;

				}
				break;
			case 51 :
				// GosuProg.g:1:310: T__75
				{
				mT__75(); if (state.failed) return;

				}
				break;
			case 52 :
				// GosuProg.g:1:316: T__76
				{
				mT__76(); if (state.failed) return;

				}
				break;
			case 53 :
				// GosuProg.g:1:322: T__77
				{
				mT__77(); if (state.failed) return;

				}
				break;
			case 54 :
				// GosuProg.g:1:328: T__78
				{
				mT__78(); if (state.failed) return;

				}
				break;
			case 55 :
				// GosuProg.g:1:334: T__79
				{
				mT__79(); if (state.failed) return;

				}
				break;
			case 56 :
				// GosuProg.g:1:340: T__80
				{
				mT__80(); if (state.failed) return;

				}
				break;
			case 57 :
				// GosuProg.g:1:346: T__81
				{
				mT__81(); if (state.failed) return;

				}
				break;
			case 58 :
				// GosuProg.g:1:352: T__82
				{
				mT__82(); if (state.failed) return;

				}
				break;
			case 59 :
				// GosuProg.g:1:358: T__83
				{
				mT__83(); if (state.failed) return;

				}
				break;
			case 60 :
				// GosuProg.g:1:364: T__84
				{
				mT__84(); if (state.failed) return;

				}
				break;
			case 61 :
				// GosuProg.g:1:370: T__85
				{
				mT__85(); if (state.failed) return;

				}
				break;
			case 62 :
				// GosuProg.g:1:376: T__86
				{
				mT__86(); if (state.failed) return;

				}
				break;
			case 63 :
				// GosuProg.g:1:382: T__87
				{
				mT__87(); if (state.failed) return;

				}
				break;
			case 64 :
				// GosuProg.g:1:388: T__88
				{
				mT__88(); if (state.failed) return;

				}
				break;
			case 65 :
				// GosuProg.g:1:394: T__89
				{
				mT__89(); if (state.failed) return;

				}
				break;
			case 66 :
				// GosuProg.g:1:400: T__90
				{
				mT__90(); if (state.failed) return;

				}
				break;
			case 67 :
				// GosuProg.g:1:406: T__91
				{
				mT__91(); if (state.failed) return;

				}
				break;
			case 68 :
				// GosuProg.g:1:412: T__92
				{
				mT__92(); if (state.failed) return;

				}
				break;
			case 69 :
				// GosuProg.g:1:418: T__93
				{
				mT__93(); if (state.failed) return;

				}
				break;
			case 70 :
				// GosuProg.g:1:424: T__94
				{
				mT__94(); if (state.failed) return;

				}
				break;
			case 71 :
				// GosuProg.g:1:430: T__95
				{
				mT__95(); if (state.failed) return;

				}
				break;
			case 72 :
				// GosuProg.g:1:436: T__96
				{
				mT__96(); if (state.failed) return;

				}
				break;
			case 73 :
				// GosuProg.g:1:442: T__97
				{
				mT__97(); if (state.failed) return;

				}
				break;
			case 74 :
				// GosuProg.g:1:448: T__98
				{
				mT__98(); if (state.failed) return;

				}
				break;
			case 75 :
				// GosuProg.g:1:454: T__99
				{
				mT__99(); if (state.failed) return;

				}
				break;
			case 76 :
				// GosuProg.g:1:460: T__100
				{
				mT__100(); if (state.failed) return;

				}
				break;
			case 77 :
				// GosuProg.g:1:467: T__101
				{
				mT__101(); if (state.failed) return;

				}
				break;
			case 78 :
				// GosuProg.g:1:474: T__102
				{
				mT__102(); if (state.failed) return;

				}
				break;
			case 79 :
				// GosuProg.g:1:481: T__103
				{
				mT__103(); if (state.failed) return;

				}
				break;
			case 80 :
				// GosuProg.g:1:488: T__104
				{
				mT__104(); if (state.failed) return;

				}
				break;
			case 81 :
				// GosuProg.g:1:495: T__105
				{
				mT__105(); if (state.failed) return;

				}
				break;
			case 82 :
				// GosuProg.g:1:502: T__106
				{
				mT__106(); if (state.failed) return;

				}
				break;
			case 83 :
				// GosuProg.g:1:509: T__107
				{
				mT__107(); if (state.failed) return;

				}
				break;
			case 84 :
				// GosuProg.g:1:516: T__108
				{
				mT__108(); if (state.failed) return;

				}
				break;
			case 85 :
				// GosuProg.g:1:523: T__109
				{
				mT__109(); if (state.failed) return;

				}
				break;
			case 86 :
				// GosuProg.g:1:530: T__110
				{
				mT__110(); if (state.failed) return;

				}
				break;
			case 87 :
				// GosuProg.g:1:537: T__111
				{
				mT__111(); if (state.failed) return;

				}
				break;
			case 88 :
				// GosuProg.g:1:544: T__112
				{
				mT__112(); if (state.failed) return;

				}
				break;
			case 89 :
				// GosuProg.g:1:551: T__113
				{
				mT__113(); if (state.failed) return;

				}
				break;
			case 90 :
				// GosuProg.g:1:558: T__114
				{
				mT__114(); if (state.failed) return;

				}
				break;
			case 91 :
				// GosuProg.g:1:565: T__115
				{
				mT__115(); if (state.failed) return;

				}
				break;
			case 92 :
				// GosuProg.g:1:572: T__116
				{
				mT__116(); if (state.failed) return;

				}
				break;
			case 93 :
				// GosuProg.g:1:579: T__117
				{
				mT__117(); if (state.failed) return;

				}
				break;
			case 94 :
				// GosuProg.g:1:586: T__118
				{
				mT__118(); if (state.failed) return;

				}
				break;
			case 95 :
				// GosuProg.g:1:593: T__119
				{
				mT__119(); if (state.failed) return;

				}
				break;
			case 96 :
				// GosuProg.g:1:600: T__120
				{
				mT__120(); if (state.failed) return;

				}
				break;
			case 97 :
				// GosuProg.g:1:607: T__121
				{
				mT__121(); if (state.failed) return;

				}
				break;
			case 98 :
				// GosuProg.g:1:614: T__122
				{
				mT__122(); if (state.failed) return;

				}
				break;
			case 99 :
				// GosuProg.g:1:621: T__123
				{
				mT__123(); if (state.failed) return;

				}
				break;
			case 100 :
				// GosuProg.g:1:628: T__124
				{
				mT__124(); if (state.failed) return;

				}
				break;
			case 101 :
				// GosuProg.g:1:635: T__125
				{
				mT__125(); if (state.failed) return;

				}
				break;
			case 102 :
				// GosuProg.g:1:642: T__126
				{
				mT__126(); if (state.failed) return;

				}
				break;
			case 103 :
				// GosuProg.g:1:649: T__127
				{
				mT__127(); if (state.failed) return;

				}
				break;
			case 104 :
				// GosuProg.g:1:656: T__128
				{
				mT__128(); if (state.failed) return;

				}
				break;
			case 105 :
				// GosuProg.g:1:663: T__129
				{
				mT__129(); if (state.failed) return;

				}
				break;
			case 106 :
				// GosuProg.g:1:670: T__130
				{
				mT__130(); if (state.failed) return;

				}
				break;
			case 107 :
				// GosuProg.g:1:677: T__131
				{
				mT__131(); if (state.failed) return;

				}
				break;
			case 108 :
				// GosuProg.g:1:684: T__132
				{
				mT__132(); if (state.failed) return;

				}
				break;
			case 109 :
				// GosuProg.g:1:691: T__133
				{
				mT__133(); if (state.failed) return;

				}
				break;
			case 110 :
				// GosuProg.g:1:698: T__134
				{
				mT__134(); if (state.failed) return;

				}
				break;
			case 111 :
				// GosuProg.g:1:705: T__135
				{
				mT__135(); if (state.failed) return;

				}
				break;
			case 112 :
				// GosuProg.g:1:712: T__136
				{
				mT__136(); if (state.failed) return;

				}
				break;
			case 113 :
				// GosuProg.g:1:719: T__137
				{
				mT__137(); if (state.failed) return;

				}
				break;
			case 114 :
				// GosuProg.g:1:726: T__138
				{
				mT__138(); if (state.failed) return;

				}
				break;
			case 115 :
				// GosuProg.g:1:733: T__139
				{
				mT__139(); if (state.failed) return;

				}
				break;
			case 116 :
				// GosuProg.g:1:740: T__140
				{
				mT__140(); if (state.failed) return;

				}
				break;
			case 117 :
				// GosuProg.g:1:747: T__141
				{
				mT__141(); if (state.failed) return;

				}
				break;
			case 118 :
				// GosuProg.g:1:754: T__142
				{
				mT__142(); if (state.failed) return;

				}
				break;
			case 119 :
				// GosuProg.g:1:761: T__143
				{
				mT__143(); if (state.failed) return;

				}
				break;
			case 120 :
				// GosuProg.g:1:768: T__144
				{
				mT__144(); if (state.failed) return;

				}
				break;
			case 121 :
				// GosuProg.g:1:775: T__145
				{
				mT__145(); if (state.failed) return;

				}
				break;
			case 122 :
				// GosuProg.g:1:782: T__146
				{
				mT__146(); if (state.failed) return;

				}
				break;
			case 123 :
				// GosuProg.g:1:789: T__147
				{
				mT__147(); if (state.failed) return;

				}
				break;
			case 124 :
				// GosuProg.g:1:796: T__148
				{
				mT__148(); if (state.failed) return;

				}
				break;
			case 125 :
				// GosuProg.g:1:803: T__149
				{
				mT__149(); if (state.failed) return;

				}
				break;
			case 126 :
				// GosuProg.g:1:810: T__150
				{
				mT__150(); if (state.failed) return;

				}
				break;
			case 127 :
				// GosuProg.g:1:817: T__151
				{
				mT__151(); if (state.failed) return;

				}
				break;
			case 128 :
				// GosuProg.g:1:824: T__152
				{
				mT__152(); if (state.failed) return;

				}
				break;
			case 129 :
				// GosuProg.g:1:831: T__153
				{
				mT__153(); if (state.failed) return;

				}
				break;
			case 130 :
				// GosuProg.g:1:838: T__154
				{
				mT__154(); if (state.failed) return;

				}
				break;
			case 131 :
				// GosuProg.g:1:845: T__155
				{
				mT__155(); if (state.failed) return;

				}
				break;
			case 132 :
				// GosuProg.g:1:852: T__156
				{
				mT__156(); if (state.failed) return;

				}
				break;
			case 133 :
				// GosuProg.g:1:859: T__157
				{
				mT__157(); if (state.failed) return;

				}
				break;
			case 134 :
				// GosuProg.g:1:866: T__158
				{
				mT__158(); if (state.failed) return;

				}
				break;
			case 135 :
				// GosuProg.g:1:873: T__159
				{
				mT__159(); if (state.failed) return;

				}
				break;
			case 136 :
				// GosuProg.g:1:880: T__160
				{
				mT__160(); if (state.failed) return;

				}
				break;
			case 137 :
				// GosuProg.g:1:887: T__161
				{
				mT__161(); if (state.failed) return;

				}
				break;
			case 138 :
				// GosuProg.g:1:894: T__162
				{
				mT__162(); if (state.failed) return;

				}
				break;
			case 139 :
				// GosuProg.g:1:901: T__163
				{
				mT__163(); if (state.failed) return;

				}
				break;
			case 140 :
				// GosuProg.g:1:908: T__164
				{
				mT__164(); if (state.failed) return;

				}
				break;
			case 141 :
				// GosuProg.g:1:915: T__165
				{
				mT__165(); if (state.failed) return;

				}
				break;
			case 142 :
				// GosuProg.g:1:922: T__166
				{
				mT__166(); if (state.failed) return;

				}
				break;
			case 143 :
				// GosuProg.g:1:929: T__167
				{
				mT__167(); if (state.failed) return;

				}
				break;
			case 144 :
				// GosuProg.g:1:936: T__168
				{
				mT__168(); if (state.failed) return;

				}
				break;
			case 145 :
				// GosuProg.g:1:943: T__169
				{
				mT__169(); if (state.failed) return;

				}
				break;
			case 146 :
				// GosuProg.g:1:950: T__170
				{
				mT__170(); if (state.failed) return;

				}
				break;
			case 147 :
				// GosuProg.g:1:957: Ident
				{
				mIdent(); if (state.failed) return;

				}
				break;
			case 148 :
				// GosuProg.g:1:963: NumberLiteral
				{
				mNumberLiteral(); if (state.failed) return;

				}
				break;
			case 149 :
				// GosuProg.g:1:977: CharLiteral
				{
				mCharLiteral(); if (state.failed) return;

				}
				break;
			case 150 :
				// GosuProg.g:1:989: StringLiteral
				{
				mStringLiteral(); if (state.failed) return;

				}
				break;
			case 151 :
				// GosuProg.g:1:1003: WS
				{
				mWS(); if (state.failed) return;

				}
				break;
			case 152 :
				// GosuProg.g:1:1006: COMMENT
				{
				mCOMMENT(); if (state.failed) return;

				}
				break;
			case 153 :
				// GosuProg.g:1:1014: LINE_COMMENT
				{
				mLINE_COMMENT(); if (state.failed) return;

				}
				break;
			case 154 :
				// GosuProg.g:1:1027: HASHBANG
				{
				mHASHBANG(); if (state.failed) return;

				}
				break;

		}
	}

	// $ANTLR start synpred1_GosuProg
	public final void synpred1_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:601:35: ( '.' ~ '.' )
		// GosuProg.g:601:36: '.' ~ '.'
		{
		match('.'); if (state.failed) return;
		if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '-')||(input.LA(1) >= '/' && input.LA(1) <= '\uFFFF') ) {
			input.consume();
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			recover(mse);
			throw mse;
		}
		}

	}
	// $ANTLR end synpred1_GosuProg

	public final boolean synpred1_GosuProg() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred1_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}


	protected DFA42 dfa42 = new DFA42(this);
	static final String DFA42_eotS =
		"\1\uffff\1\72\1\74\1\76\1\101\2\uffff\1\104\1\107\1\uffff\1\113\1\115"+
		"\1\121\2\uffff\1\123\1\125\1\uffff\1\136\1\uffff\2\61\3\uffff\1\142\23"+
		"\61\1\uffff\1\u0096\12\uffff\1\u009b\5\uffff\1\u009d\14\uffff\1\u009f"+
		"\7\uffff\1\u00a1\12\uffff\2\61\2\uffff\3\61\1\u00a8\6\61\1\u00b1\12\61"+
		"\1\u00c0\1\61\1\u00c4\5\61\1\u00ca\22\61\2\uffff\1\u00ea\14\uffff\1\61"+
		"\1\u00f2\1\61\1\u00f4\2\61\1\uffff\10\61\1\uffff\12\61\1\u010c\1\61\1"+
		"\u010e\1\61\1\uffff\3\61\1\uffff\2\61\1\u0115\1\u0116\1\61\1\uffff\13"+
		"\61\1\u0124\10\61\1\u012e\4\61\1\u0133\3\61\1\u0138\7\uffff\1\u013c\1"+
		"\61\1\uffff\1\61\1\uffff\4\61\1\u0143\6\61\1\u014b\1\61\1\u014d\1\u014e"+
		"\6\61\1\u0155\1\61\1\uffff\1\61\1\uffff\1\u0158\5\61\2\uffff\1\u015e\14"+
		"\61\1\uffff\5\61\1\u0170\2\61\1\u0173\1\uffff\2\61\1\u0179\1\61\1\uffff"+
		"\1\u017b\2\61\6\uffff\4\61\1\u0184\1\u0185\1\uffff\1\u0186\1\u0188\5\61"+
		"\1\uffff\1\61\2\uffff\4\61\1\u0193\1\u0195\1\uffff\2\61\1\uffff\1\61\1"+
		"\u0199\3\61\1\uffff\1\u019e\16\61\1\u01ad\1\61\1\uffff\1\u01af\1\61\1"+
		"\uffff\5\61\1\uffff\1\u01b6\1\uffff\1\u01b7\1\u01b8\2\uffff\3\61\1\u01bd"+
		"\3\uffff\1\61\1\uffff\6\61\1\u01c5\1\61\1\u01c7\1\61\1\uffff\1\61\1\uffff"+
		"\3\61\1\uffff\3\61\1\u01d0\1\uffff\5\61\1\u01d6\3\61\1\u01da\2\61\1\u01de"+
		"\1\61\1\uffff\1\u01e0\1\uffff\1\61\1\u01e2\1\u01e3\1\61\1\u01e5\1\u01e6"+
		"\4\uffff\3\61\1\uffff\4\61\1\u01ef\2\61\1\uffff\1\61\1\uffff\1\u01f3\1"+
		"\u01f4\1\u01f5\5\61\1\uffff\1\61\1\u01fc\1\u01fd\2\61\1\uffff\2\61\1\u0202"+
		"\1\uffff\1\u0203\2\61\1\uffff\1\61\1\uffff\1\61\2\uffff\1\61\3\uffff\1"+
		"\u0209\1\u020a\3\61\1\u020e\1\u020f\1\uffff\1\u0210\2\61\3\uffff\1\u0213"+
		"\2\61\1\u0216\1\u0217\1\u0218\2\uffff\1\u0219\1\61\1\u021b\1\61\2\uffff"+
		"\5\61\2\uffff\1\61\1\u0223\1\u0224\3\uffff\1\61\1\u0226\1\uffff\1\61\1"+
		"\u0228\4\uffff\1\u0229\1\uffff\3\61\1\u022d\1\u022e\2\61\2\uffff\1\61"+
		"\1\uffff\1\u0232\2\uffff\1\u0233\1\u0234\1\61\2\uffff\1\u0236\1\u0237"+
		"\1\u0238\3\uffff\1\61\3\uffff\1\u023a\1\uffff";
	static final String DFA42_eofS =
		"\u023b\uffff";
	static final String DFA42_minS =
		"\1\11\1\52\1\41\1\75\1\46\2\uffff\1\56\1\53\1\uffff\1\55\1\56\1\52\2\uffff"+
		"\1\76\1\75\1\uffff\1\45\1\uffff\1\156\1\141\3\uffff\1\75\1\142\1\154\1"+
		"\141\1\145\1\154\1\141\1\145\1\151\1\146\2\145\1\162\1\141\2\145\1\150"+
		"\1\156\1\141\1\150\1\uffff\1\56\4\uffff\1\0\5\uffff\1\75\5\uffff\1\75"+
		"\14\uffff\1\174\7\uffff\1\75\12\uffff\1\146\1\116\2\uffff\1\163\1\144"+
		"\1\160\1\44\1\157\1\145\1\163\1\141\1\156\1\146\1\44\1\163\1\150\1\141"+
		"\1\143\1\154\1\156\1\162\1\156\1\164\1\144\1\44\1\160\1\44\1\145\1\156"+
		"\1\167\1\164\1\154\1\44\1\164\1\145\1\143\1\151\1\142\1\141\1\163\1\141"+
		"\1\160\2\151\1\141\1\160\1\154\1\145\1\162\1\151\1\145\1\56\1\uffff\1"+
		"\75\1\uffff\1\42\2\0\10\uffff\1\151\1\44\1\164\1\44\1\154\1\145\1\uffff"+
		"\1\143\1\141\1\145\1\143\2\163\1\141\1\145\1\uffff\1\145\1\141\1\155\1"+
		"\154\1\145\1\143\1\163\1\145\1\163\1\141\1\44\1\143\1\44\1\145\1\uffff"+
		"\1\154\2\145\1\uffff\1\162\1\147\2\44\1\154\1\uffff\1\145\1\162\1\153"+
		"\1\166\1\160\1\154\1\144\1\162\2\165\1\163\1\44\1\162\1\165\1\145\1\164"+
		"\1\163\1\157\1\156\1\145\1\44\2\145\1\163\1\156\1\44\1\144\1\162\1\154"+
		"\1\174\2\uffff\1\0\1\60\2\0\1\uffff\1\0\1\156\1\uffff\1\162\1\uffff\1"+
		"\151\1\162\2\153\1\44\1\150\1\163\1\164\1\141\1\165\1\147\1\44\1\156\2"+
		"\44\1\160\1\165\1\164\1\156\1\145\1\154\1\44\1\141\1\uffff\1\164\1\uffff"+
		"\1\44\1\145\1\170\1\162\1\141\1\164\2\uffff\1\44\2\162\2\141\2\145\1\151"+
		"\1\157\2\145\1\162\1\151\1\uffff\1\164\1\151\1\143\1\162\1\143\1\44\1"+
		"\167\1\163\1\44\1\uffff\1\141\1\163\1\44\1\147\1\uffff\1\44\2\145\2\uffff"+
		"\1\60\2\0\1\uffff\1\151\1\141\1\143\1\164\2\44\1\uffff\2\44\1\162\1\151"+
		"\1\156\1\154\1\141\1\uffff\1\143\2\uffff\2\164\1\163\1\144\2\44\1\uffff"+
		"\1\143\1\151\1\uffff\1\155\1\44\1\146\1\164\1\150\1\uffff\1\44\1\151\1"+
		"\147\1\164\1\162\2\143\1\156\2\163\1\156\1\157\1\163\1\143\1\164\1\44"+
		"\1\150\1\uffff\1\44\1\151\1\uffff\2\163\1\157\1\146\1\163\1\uffff\1\44"+
		"\1\uffff\2\44\1\60\1\0\1\164\1\143\1\141\1\44\3\uffff\1\141\1\uffff\1"+
		"\165\1\156\1\165\2\164\1\145\1\44\1\151\1\44\1\163\1\uffff\1\171\1\uffff"+
		"\1\150\1\157\1\145\1\uffff\2\141\1\157\1\44\1\uffff\1\144\2\145\2\164"+
		"\1\44\1\154\1\145\1\164\1\44\1\156\1\167\1\44\1\165\1\uffff\1\44\1\uffff"+
		"\1\145\2\44\1\141\2\44\3\uffff\1\60\1\171\2\164\1\uffff\1\164\1\143\1"+
		"\163\1\145\1\44\1\145\1\155\1\uffff\1\157\1\uffff\3\44\2\156\1\143\1\154"+
		"\1\162\1\uffff\1\145\2\44\1\171\1\145\1\uffff\1\171\1\156\1\44\1\uffff"+
		"\1\44\1\151\1\171\1\uffff\1\162\1\uffff\1\156\2\uffff\1\144\2\uffff\1"+
		"\0\2\44\1\151\1\150\1\164\2\44\1\uffff\1\44\1\145\1\156\3\uffff\1\44\1"+
		"\164\1\145\3\44\2\uffff\1\44\1\144\1\44\1\164\2\uffff\1\164\1\160\1\145"+
		"\1\164\1\145\2\uffff\1\157\2\44\3\uffff\1\156\1\44\1\uffff\1\163\1\44"+
		"\4\uffff\1\44\1\uffff\1\163\1\150\1\145\2\44\1\162\1\156\2\uffff\1\164"+
		"\1\uffff\1\44\2\uffff\2\44\1\157\2\uffff\3\44\3\uffff\1\146\3\uffff\1"+
		"\44\1\uffff";
	static final String DFA42_maxS =
		"\1\176\1\75\1\41\2\75\2\uffff\2\75\1\uffff\1\76\1\71\1\75\2\uffff\1\76"+
		"\1\75\1\uffff\1\133\1\uffff\1\156\1\141\3\uffff\1\75\1\163\1\162\2\157"+
		"\1\170\1\165\1\145\1\151\1\164\1\145\1\165\1\166\1\165\1\145\1\167\1\171"+
		"\1\163\1\157\1\150\1\uffff\1\174\4\uffff\1\uffff\5\uffff\1\75\5\uffff"+
		"\1\75\14\uffff\1\174\7\uffff\1\75\12\uffff\1\146\1\116\2\uffff\1\163\1"+
		"\144\1\160\1\172\1\157\1\145\1\164\1\141\1\156\1\154\1\172\1\163\1\165"+
		"\1\141\1\164\1\154\1\156\1\162\1\156\1\164\1\144\1\172\1\160\1\172\1\145"+
		"\1\156\1\167\1\164\1\154\1\172\1\164\1\145\1\143\1\157\1\142\2\164\1\162"+
		"\1\160\1\151\1\162\1\171\1\160\1\154\1\151\1\162\2\151\1\56\1\uffff\1"+
		"\75\1\uffff\1\166\2\uffff\10\uffff\1\151\1\172\1\164\1\172\1\154\1\145"+
		"\1\uffff\1\143\1\141\1\145\1\143\1\163\1\164\1\141\1\145\1\uffff\1\145"+
		"\1\141\1\155\1\154\1\145\1\143\1\163\1\145\1\163\1\144\1\172\1\143\1\172"+
		"\1\145\1\uffff\1\154\2\145\1\uffff\1\162\1\147\2\172\1\154\1\uffff\1\145"+
		"\1\162\1\153\1\166\1\164\1\154\1\144\1\162\2\165\1\163\1\172\1\164\1\165"+
		"\1\145\1\164\1\163\1\157\1\156\1\145\1\172\2\145\1\163\1\156\1\172\1\144"+
		"\1\162\1\154\1\174\2\uffff\1\uffff\1\146\2\uffff\1\uffff\1\uffff\1\156"+
		"\1\uffff\1\162\1\uffff\1\151\1\162\2\153\1\172\1\150\1\163\1\164\1\151"+
		"\1\165\1\147\1\172\1\156\2\172\1\160\1\165\1\164\1\156\1\145\1\154\1\172"+
		"\1\141\1\uffff\1\164\1\uffff\1\172\1\145\1\170\1\162\1\141\1\164\2\uffff"+
		"\1\172\2\162\2\141\2\145\1\151\1\157\2\145\1\162\1\151\1\uffff\1\164\1"+
		"\151\1\143\1\162\1\143\1\172\1\167\1\163\1\172\1\uffff\1\157\1\163\1\172"+
		"\1\147\1\uffff\1\172\2\145\2\uffff\1\146\2\uffff\1\uffff\1\151\1\141\1"+
		"\143\1\164\2\172\1\uffff\2\172\1\162\1\151\1\156\1\154\1\141\1\uffff\1"+
		"\143\2\uffff\2\164\1\163\1\144\2\172\1\uffff\1\143\1\151\1\uffff\1\155"+
		"\1\172\1\156\1\164\1\150\1\uffff\1\172\1\151\1\147\1\164\1\162\2\143\1"+
		"\156\2\163\1\156\1\157\1\163\1\143\1\164\1\172\1\150\1\uffff\1\172\1\151"+
		"\1\uffff\2\163\1\157\1\146\1\163\1\uffff\1\172\1\uffff\2\172\1\146\1\uffff"+
		"\1\164\1\143\1\141\1\172\3\uffff\1\141\1\uffff\1\165\1\156\1\165\2\164"+
		"\1\145\1\172\1\151\1\172\1\163\1\uffff\1\171\1\uffff\1\150\1\157\1\145"+
		"\1\uffff\2\141\1\157\1\172\1\uffff\1\144\2\145\2\164\1\172\1\154\1\145"+
		"\1\164\1\172\1\156\1\167\1\172\1\165\1\uffff\1\172\1\uffff\1\145\2\172"+
		"\1\141\2\172\3\uffff\1\146\1\171\2\164\1\uffff\1\164\1\143\1\163\1\145"+
		"\1\172\1\145\1\155\1\uffff\1\157\1\uffff\3\172\2\156\1\143\1\154\1\162"+
		"\1\uffff\1\145\2\172\1\171\1\145\1\uffff\1\171\1\156\1\172\1\uffff\1\172"+
		"\1\151\1\171\1\uffff\1\162\1\uffff\1\156\2\uffff\1\144\2\uffff\1\uffff"+
		"\2\172\1\151\1\150\1\164\2\172\1\uffff\1\172\1\145\1\156\3\uffff\1\172"+
		"\1\164\1\145\3\172\2\uffff\1\172\1\144\1\172\1\164\2\uffff\1\164\1\160"+
		"\1\145\1\164\1\145\2\uffff\1\157\2\172\3\uffff\1\156\1\172\1\uffff\1\163"+
		"\1\172\4\uffff\1\172\1\uffff\1\163\1\150\1\145\2\172\1\162\1\156\2\uffff"+
		"\1\164\1\uffff\1\172\2\uffff\2\172\1\157\2\uffff\3\172\3\uffff\1\146\3"+
		"\uffff\1\172\1\uffff";
	static final String DFA42_acceptS =
		"\5\uffff\1\16\1\17\2\uffff\1\26\3\uffff\1\40\1\41\2\uffff\1\47\1\uffff"+
		"\1\61\2\uffff\1\64\1\65\1\66\24\uffff\1\u008a\1\uffff\1\u0091\1\u0092"+
		"\1\u0093\1\u0094\1\uffff\1\u0096\1\u0097\1\2\1\3\1\4\1\uffff\1\1\1\u009a"+
		"\1\7\1\11\1\10\1\uffff\1\15\1\14\1\21\1\22\1\20\1\24\1\25\1\23\1\30\1"+
		"\31\1\32\1\27\1\uffff\1\33\1\37\1\u0098\1\u0099\1\36\1\43\1\42\1\uffff"+
		"\1\44\1\50\1\52\1\53\1\54\1\55\1\56\1\57\1\60\1\51\2\uffff\1\70\1\67\61"+
		"\uffff\1\u008e\1\uffff\1\u008b\3\uffff\1\6\1\5\1\13\1\12\1\35\1\34\1\46"+
		"\1\45\6\uffff\1\74\10\uffff\1\111\16\uffff\1\133\3\uffff\1\135\5\uffff"+
		"\1\146\36\uffff\1\u0090\1\u008f\4\uffff\1\u0095\2\uffff\1\63\1\uffff\1"+
		"\72\27\uffff\1\126\1\uffff\1\131\6\uffff\1\143\1\144\15\uffff\1\163\11"+
		"\uffff\1\176\4\uffff\1\u0086\3\uffff\1\u008d\1\u008c\3\uffff\1\u0095\6"+
		"\uffff\1\100\7\uffff\1\112\1\uffff\1\114\1\115\6\uffff\1\125\2\uffff\1"+
		"\132\5\uffff\1\145\21\uffff\1\172\2\uffff\1\175\5\uffff\1\u0084\1\uffff"+
		"\1\u0087\10\uffff\1\76\1\77\1\101\1\uffff\1\102\12\uffff\1\122\1\uffff"+
		"\1\123\3\uffff\1\136\4\uffff\1\147\16\uffff\1\170\1\uffff\1\173\6\uffff"+
		"\1\u0085\1\u0088\1\u0089\4\uffff\1\75\7\uffff\1\116\1\uffff\1\120\10\uffff"+
		"\1\142\5\uffff\1\155\3\uffff\1\161\3\uffff\1\165\1\uffff\1\171\1\uffff"+
		"\1\177\1\u0080\1\uffff\1\u0082\1\u0083\10\uffff\1\107\3\uffff\1\121\1"+
		"\124\1\127\6\uffff\1\151\1\152\4\uffff\1\160\1\162\5\uffff\1\62\1\71\3"+
		"\uffff\1\105\1\106\1\110\2\uffff\1\130\2\uffff\1\140\1\141\1\150\1\153"+
		"\1\uffff\1\156\7\uffff\1\103\1\104\1\uffff\1\117\1\uffff\1\137\1\154\3"+
		"\uffff\1\167\1\174\3\uffff\1\134\1\157\1\164\1\uffff\1\u0081\1\73\1\113"+
		"\1\uffff\1\166";
	static final String DFA42_specialS =
		"\63\uffff\1\0\144\uffff\1\4\1\6\121\uffff\1\7\1\uffff\1\2\1\10\1\uffff"+
		"\1\1\111\uffff\1\5\1\12\103\uffff\1\11\147\uffff\1\3\123\uffff}>";
	static final String[] DFA42_transitionS = {
			"\2\65\2\uffff\1\65\22\uffff\1\65\1\1\1\64\1\2\1\61\1\3\1\4\1\63\1\5\1"+
			"\6\1\7\1\10\1\11\1\12\1\13\1\14\12\62\1\15\1\16\1\17\1\20\1\21\1\22\1"+
			"\23\10\61\1\24\4\61\1\25\14\61\1\26\1\27\1\30\1\31\1\61\1\uffff\1\32"+
			"\1\33\1\34\1\35\1\36\1\37\1\40\1\41\1\42\2\61\1\43\1\61\1\44\1\45\1\46"+
			"\1\61\1\47\1\50\1\51\1\52\1\53\1\54\3\61\1\55\1\56\1\57\1\60",
			"\1\66\1\67\1\uffff\1\70\17\uffff\1\71",
			"\1\73",
			"\1\75",
			"\1\77\26\uffff\1\100",
			"",
			"",
			"\1\102\16\uffff\1\103",
			"\1\105\21\uffff\1\106",
			"",
			"\1\110\17\uffff\1\111\1\112",
			"\1\114\1\uffff\12\62",
			"\1\117\4\uffff\1\120\15\uffff\1\116",
			"",
			"",
			"\1\122",
			"\1\124",
			"",
			"\1\126\4\uffff\1\127\1\130\1\uffff\1\131\1\132\1\133\12\uffff\1\134"+
			"\40\uffff\1\135",
			"",
			"\1\137",
			"\1\140",
			"",
			"",
			"",
			"\1\141",
			"\1\143\13\uffff\1\144\1\uffff\1\145\2\uffff\1\146",
			"\1\147\5\uffff\1\150",
			"\1\151\12\uffff\1\152\2\uffff\1\153",
			"\1\154\11\uffff\1\155",
			"\1\156\1\uffff\1\157\7\uffff\1\160\1\uffff\1\161",
			"\1\162\7\uffff\1\163\5\uffff\1\164\5\uffff\1\165",
			"\1\166",
			"\1\167",
			"\1\170\6\uffff\1\171\1\172\5\uffff\1\173",
			"\1\174",
			"\1\175\11\uffff\1\176\5\uffff\1\177",
			"\1\u0080\2\uffff\1\u0081\1\u0082",
			"\1\u0083\20\uffff\1\u0084\2\uffff\1\u0085",
			"\1\u0086",
			"\1\u0087\16\uffff\1\u0088\1\u0089\1\uffff\1\u008a",
			"\1\u008b\11\uffff\1\u008c\6\uffff\1\u008d",
			"\1\u008e\4\uffff\1\u008f",
			"\1\u0090\15\uffff\1\u0091",
			"\1\u0092",
			"",
			"\1\u0093\16\uffff\1\u0094\76\uffff\1\u0095",
			"",
			"",
			"",
			"",
			"\12\u0098\1\uffff\2\u0098\1\uffff\26\u0098\1\u0099\2\u0098\1\64\64\u0098"+
			"\1\u0097\uffa3\u0098",
			"",
			"",
			"",
			"",
			"",
			"\1\u009a",
			"",
			"",
			"",
			"",
			"",
			"\1\u009c",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u009e",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u00a0",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u00a2",
			"\1\u00a3",
			"",
			"",
			"\1\u00a4",
			"\1\u00a5",
			"\1\u00a6",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\22\61\1\u00a7"+
			"\7\61",
			"\1\u00a9",
			"\1\u00aa",
			"\1\u00ab\1\u00ac",
			"\1\u00ad",
			"\1\u00ae",
			"\1\u00af\5\uffff\1\u00b0",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u00b2",
			"\1\u00b3\14\uffff\1\u00b4",
			"\1\u00b5",
			"\1\u00b6\1\uffff\1\u00b7\3\uffff\1\u00b8\12\uffff\1\u00b9",
			"\1\u00ba",
			"\1\u00bb",
			"\1\u00bc",
			"\1\u00bd",
			"\1\u00be",
			"\1\u00bf",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u00c1",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\3\61\1\u00c2"+
			"\17\61\1\u00c3\6\61",
			"\1\u00c5",
			"\1\u00c6",
			"\1\u00c7",
			"\1\u00c8",
			"\1\u00c9",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u00cb",
			"\1\u00cc",
			"\1\u00cd",
			"\1\u00ce\5\uffff\1\u00cf",
			"\1\u00d0",
			"\1\u00d1\16\uffff\1\u00d2\1\u00d3\2\uffff\1\u00d4",
			"\1\u00d5\1\u00d6",
			"\1\u00d7\20\uffff\1\u00d8",
			"\1\u00d9",
			"\1\u00da",
			"\1\u00db\10\uffff\1\u00dc",
			"\1\u00dd\23\uffff\1\u00de\3\uffff\1\u00df",
			"\1\u00e0",
			"\1\u00e1",
			"\1\u00e2\3\uffff\1\u00e3",
			"\1\u00e4",
			"\1\u00e5",
			"\1\u00e6\3\uffff\1\u00e7",
			"\1\u00e8",
			"",
			"\1\u00e9",
			"",
			"\1\u00eb\1\uffff\1\u00eb\2\uffff\1\u00eb\10\uffff\4\u00ed\4\u00ee\4"+
			"\uffff\1\u00eb\37\uffff\1\u00eb\4\uffff\2\u00eb\3\uffff\1\u00eb\7\uffff"+
			"\1\u00eb\3\uffff\1\u00eb\1\uffff\1\u00eb\1\u00ec\1\u00eb",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\uffd8\64",
			"\12\64\1\uffff\2\64\1\uffff\24\64\1\uffff\4\64\1\u00f0\uffd8\64",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u00f1",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u00f3",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u00f5",
			"\1\u00f6",
			"",
			"\1\u00f7",
			"\1\u00f8",
			"\1\u00f9",
			"\1\u00fa",
			"\1\u00fb",
			"\1\u00fc\1\u00fd",
			"\1\u00fe",
			"\1\u00ff",
			"",
			"\1\u0100",
			"\1\u0101",
			"\1\u0102",
			"\1\u0103",
			"\1\u0104",
			"\1\u0105",
			"\1\u0106",
			"\1\u0107",
			"\1\u0108",
			"\1\u0109\2\uffff\1\u010a",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\4\61\1\u010b"+
			"\25\61",
			"\1\u010d",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u010f",
			"",
			"\1\u0110",
			"\1\u0111",
			"\1\u0112",
			"",
			"\1\u0113",
			"\1\u0114",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0117",
			"",
			"\1\u0118",
			"\1\u0119",
			"\1\u011a",
			"\1\u011b",
			"\1\u011c\3\uffff\1\u011d",
			"\1\u011e",
			"\1\u011f",
			"\1\u0120",
			"\1\u0121",
			"\1\u0122",
			"\1\u0123",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0125\1\uffff\1\u0126",
			"\1\u0127",
			"\1\u0128",
			"\1\u0129",
			"\1\u012a",
			"\1\u012b",
			"\1\u012c",
			"\1\u012d",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u012f",
			"\1\u0130",
			"\1\u0131",
			"\1\u0132",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0134",
			"\1\u0135",
			"\1\u0136",
			"\1\u0137",
			"",
			"",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\uffd8\64",
			"\12\u0139\7\uffff\6\u0139\32\uffff\6\u0139",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\10\64\10\u013a\uffc8\64",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\10\64\10\u013b\uffc8\64",
			"",
			"\12\64\1\uffff\2\64\1\uffff\ufff2\64",
			"\1\u013d",
			"",
			"\1\u013e",
			"",
			"\1\u013f",
			"\1\u0140",
			"\1\u0141",
			"\1\u0142",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0144",
			"\1\u0145",
			"\1\u0146",
			"\1\u0147\7\uffff\1\u0148",
			"\1\u0149",
			"\1\u014a",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u014c",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u014f",
			"\1\u0150",
			"\1\u0151",
			"\1\u0152",
			"\1\u0153",
			"\1\u0154",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0156",
			"",
			"\1\u0157",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0159",
			"\1\u015a",
			"\1\u015b",
			"\1\u015c",
			"\1\u015d",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u015f",
			"\1\u0160",
			"\1\u0161",
			"\1\u0162",
			"\1\u0163",
			"\1\u0164",
			"\1\u0165",
			"\1\u0166",
			"\1\u0167",
			"\1\u0168",
			"\1\u0169",
			"\1\u016a",
			"",
			"\1\u016b",
			"\1\u016c",
			"\1\u016d",
			"\1\u016e",
			"\1\u016f",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0171",
			"\1\u0172",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\u0174\7\uffff\1\u0175\2\uffff\1\u0176\2\uffff\1\u0177",
			"\1\u0178",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u017a",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u017c",
			"\1\u017d",
			"",
			"",
			"\12\u017e\7\uffff\6\u017e\32\uffff\6\u017e",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\10\64\10\u017f\uffc8\64",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\uffd8\64",
			"",
			"\1\u0180",
			"\1\u0181",
			"\1\u0182",
			"\1\u0183",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\17\61\1\u0187"+
			"\12\61",
			"\1\u0189",
			"\1\u018a",
			"\1\u018b",
			"\1\u018c",
			"\1\u018d",
			"",
			"\1\u018e",
			"",
			"",
			"\1\u018f",
			"\1\u0190",
			"\1\u0191",
			"\1\u0192",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\13\61\1\u0194"+
			"\16\61",
			"",
			"\1\u0196",
			"\1\u0197",
			"",
			"\1\u0198",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u019a\7\uffff\1\u019b",
			"\1\u019c",
			"\1\u019d",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u019f",
			"\1\u01a0",
			"\1\u01a1",
			"\1\u01a2",
			"\1\u01a3",
			"\1\u01a4",
			"\1\u01a5",
			"\1\u01a6",
			"\1\u01a7",
			"\1\u01a8",
			"\1\u01a9",
			"\1\u01aa",
			"\1\u01ab",
			"\1\u01ac",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01ae",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01b0",
			"",
			"\1\u01b1",
			"\1\u01b2",
			"\1\u01b3",
			"\1\u01b4",
			"\1\u01b5",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\12\u01b9\7\uffff\6\u01b9\32\uffff\6\u01b9",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\uffd8\64",
			"\1\u01ba",
			"\1\u01bb",
			"\1\u01bc",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"",
			"\1\u01be",
			"",
			"\1\u01bf",
			"\1\u01c0",
			"\1\u01c1",
			"\1\u01c2",
			"\1\u01c3",
			"\1\u01c4",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01c6",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01c8",
			"",
			"\1\u01c9",
			"",
			"\1\u01ca",
			"\1\u01cb",
			"\1\u01cc",
			"",
			"\1\u01cd",
			"\1\u01ce",
			"\1\u01cf",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\u01d1",
			"\1\u01d2",
			"\1\u01d3",
			"\1\u01d4",
			"\1\u01d5",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01d7",
			"\1\u01d8",
			"\1\u01d9",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01db",
			"\1\u01dc",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\23\61\1\u01dd"+
			"\6\61",
			"\1\u01df",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\u01e1",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01e4",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"",
			"\12\u01e7\7\uffff\6\u01e7\32\uffff\6\u01e7",
			"\1\u01e8",
			"\1\u01e9",
			"\1\u01ea",
			"",
			"\1\u01eb",
			"\1\u01ec",
			"\1\u01ed",
			"\1\u01ee",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01f0",
			"\1\u01f1",
			"",
			"\1\u01f2",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01f6",
			"\1\u01f7",
			"\1\u01f8",
			"\1\u01f9",
			"\1\u01fa",
			"",
			"\1\u01fb",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u01fe",
			"\1\u01ff",
			"",
			"\1\u0200",
			"\1\u0201",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0204",
			"\1\u0205",
			"",
			"\1\u0206",
			"",
			"\1\u0207",
			"",
			"",
			"\1\u0208",
			"",
			"",
			"\12\64\1\uffff\2\64\1\uffff\31\64\1\u00ef\uffd8\64",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u020b",
			"\1\u020c",
			"\1\u020d",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0211",
			"\1\u0212",
			"",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0214",
			"\1\u0215",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u021a",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u021c",
			"",
			"",
			"\1\u021d",
			"\1\u021e",
			"\1\u021f",
			"\1\u0220",
			"\1\u0221",
			"",
			"",
			"\1\u0222",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"",
			"\1\u0225",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\u0227",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"\1\u022a",
			"\1\u022b",
			"\1\u022c",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u022f",
			"\1\u0230",
			"",
			"",
			"\1\u0231",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\u0235",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			"",
			"",
			"",
			"\1\u0239",
			"",
			"",
			"",
			"\1\61\13\uffff\12\61\7\uffff\32\61\4\uffff\1\61\1\uffff\32\61",
			""
	};

	static final short[] DFA42_eot = DFA.unpackEncodedString(DFA42_eotS);
	static final short[] DFA42_eof = DFA.unpackEncodedString(DFA42_eofS);
	static final char[] DFA42_min = DFA.unpackEncodedStringToUnsignedChars(DFA42_minS);
	static final char[] DFA42_max = DFA.unpackEncodedStringToUnsignedChars(DFA42_maxS);
	static final short[] DFA42_accept = DFA.unpackEncodedString(DFA42_acceptS);
	static final short[] DFA42_special = DFA.unpackEncodedString(DFA42_specialS);
	static final short[][] DFA42_transition;

	static {
		int numStates = DFA42_transitionS.length;
		DFA42_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA42_transition[i] = DFA.unpackEncodedString(DFA42_transitionS[i]);
		}
	}

	protected class DFA42 extends DFA {

		public DFA42(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 42;
			this.eot = DFA42_eot;
			this.eof = DFA42_eof;
			this.min = DFA42_min;
			this.max = DFA42_max;
			this.accept = DFA42_accept;
			this.special = DFA42_special;
			this.transition = DFA42_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | T__140 | T__141 | T__142 | T__143 | T__144 | T__145 | T__146 | T__147 | T__148 | T__149 | T__150 | T__151 | T__152 | T__153 | T__154 | T__155 | T__156 | T__157 | T__158 | T__159 | T__160 | T__161 | T__162 | T__163 | T__164 | T__165 | T__166 | T__167 | T__168 | T__169 | T__170 | Ident | NumberLiteral | CharLiteral | StringLiteral | WS | COMMENT | LINE_COMMENT | HASHBANG );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA42_51 = input.LA(1);
						s = -1;
						if ( (LA42_51=='\\') ) {s = 151;}
						else if ( ((LA42_51 >= '\u0000' && LA42_51 <= '\t')||(LA42_51 >= '\u000B' && LA42_51 <= '\f')||(LA42_51 >= '\u000E' && LA42_51 <= '#')||(LA42_51 >= '%' && LA42_51 <= '&')||(LA42_51 >= '(' && LA42_51 <= '[')||(LA42_51 >= ']' && LA42_51 <= '\uFFFF')) ) {s = 152;}
						else if ( (LA42_51=='$') ) {s = 153;}
						else if ( (LA42_51=='\'') ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA42_240 = input.LA(1);
						s = -1;
						if ( ((LA42_240 >= '\u0000' && LA42_240 <= '\t')||(LA42_240 >= '\u000B' && LA42_240 <= '\f')||(LA42_240 >= '\u000E' && LA42_240 <= '\uFFFF')) ) {s = 52;}
						else s = 316;
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA42_237 = input.LA(1);
						s = -1;
						if ( ((LA42_237 >= '0' && LA42_237 <= '7')) ) {s = 314;}
						else if ( (LA42_237=='\'') ) {s = 239;}
						else if ( ((LA42_237 >= '\u0000' && LA42_237 <= '\t')||(LA42_237 >= '\u000B' && LA42_237 <= '\f')||(LA42_237 >= '\u000E' && LA42_237 <= '&')||(LA42_237 >= '(' && LA42_237 <= '/')||(LA42_237 >= '8' && LA42_237 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA42_487 = input.LA(1);
						s = -1;
						if ( (LA42_487=='\'') ) {s = 239;}
						else if ( ((LA42_487 >= '\u0000' && LA42_487 <= '\t')||(LA42_487 >= '\u000B' && LA42_487 <= '\f')||(LA42_487 >= '\u000E' && LA42_487 <= '&')||(LA42_487 >= '(' && LA42_487 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 4 : 
						int LA42_152 = input.LA(1);
						s = -1;
						if ( (LA42_152=='\'') ) {s = 239;}
						else if ( ((LA42_152 >= '\u0000' && LA42_152 <= '\t')||(LA42_152 >= '\u000B' && LA42_152 <= '\f')||(LA42_152 >= '\u000E' && LA42_152 <= '&')||(LA42_152 >= '(' && LA42_152 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 5 : 
						int LA42_314 = input.LA(1);
						s = -1;
						if ( ((LA42_314 >= '0' && LA42_314 <= '7')) ) {s = 383;}
						else if ( (LA42_314=='\'') ) {s = 239;}
						else if ( ((LA42_314 >= '\u0000' && LA42_314 <= '\t')||(LA42_314 >= '\u000B' && LA42_314 <= '\f')||(LA42_314 >= '\u000E' && LA42_314 <= '&')||(LA42_314 >= '(' && LA42_314 <= '/')||(LA42_314 >= '8' && LA42_314 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 6 : 
						int LA42_153 = input.LA(1);
						s = -1;
						if ( (LA42_153=='\'') ) {s = 240;}
						else if ( ((LA42_153 >= '\u0000' && LA42_153 <= '\t')||(LA42_153 >= '\u000B' && LA42_153 <= '\f')||(LA42_153 >= '\u000E' && LA42_153 <= '!')||(LA42_153 >= '#' && LA42_153 <= '&')||(LA42_153 >= '(' && LA42_153 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 7 : 
						int LA42_235 = input.LA(1);
						s = -1;
						if ( (LA42_235=='\'') ) {s = 239;}
						else if ( ((LA42_235 >= '\u0000' && LA42_235 <= '\t')||(LA42_235 >= '\u000B' && LA42_235 <= '\f')||(LA42_235 >= '\u000E' && LA42_235 <= '&')||(LA42_235 >= '(' && LA42_235 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 8 : 
						int LA42_238 = input.LA(1);
						s = -1;
						if ( ((LA42_238 >= '0' && LA42_238 <= '7')) ) {s = 315;}
						else if ( (LA42_238=='\'') ) {s = 239;}
						else if ( ((LA42_238 >= '\u0000' && LA42_238 <= '\t')||(LA42_238 >= '\u000B' && LA42_238 <= '\f')||(LA42_238 >= '\u000E' && LA42_238 <= '&')||(LA42_238 >= '(' && LA42_238 <= '/')||(LA42_238 >= '8' && LA42_238 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 9 : 
						int LA42_383 = input.LA(1);
						s = -1;
						if ( (LA42_383=='\'') ) {s = 239;}
						else if ( ((LA42_383 >= '\u0000' && LA42_383 <= '\t')||(LA42_383 >= '\u000B' && LA42_383 <= '\f')||(LA42_383 >= '\u000E' && LA42_383 <= '&')||(LA42_383 >= '(' && LA42_383 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;

					case 10 : 
						int LA42_315 = input.LA(1);
						s = -1;
						if ( (LA42_315=='\'') ) {s = 239;}
						else if ( ((LA42_315 >= '\u0000' && LA42_315 <= '\t')||(LA42_315 >= '\u000B' && LA42_315 <= '\f')||(LA42_315 >= '\u000E' && LA42_315 <= '&')||(LA42_315 >= '(' && LA42_315 <= '\uFFFF')) ) {s = 52;}
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 42, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
