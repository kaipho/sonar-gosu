// $ANTLR 3.5.2 GosuProg.g 2017-06-19 20:52:50
package de.kaipho.sonar.grammar;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.debug.*;
import java.io.IOException;
@SuppressWarnings("all")
public class GosuProgParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BinLiteral", "COMMENT", "CharLiteral", 
		"Digit", "EscapeSequence", "Exponent", "FloatTypeSuffix", "HASHBANG", 
		"HexDigit", "HexLiteral", "Ident", "IntOrFloatPointLiteral", "IntegerTypeSuffix", 
		"LINE_COMMENT", "Letter", "NonZeroDigit", "NumberLiteral", "OctalEscape", 
		"StringLiteral", "UnicodeEscape", "WS", "'!'", "'!*'", "'!+'", "'!-'", 
		"'!='", "'!=='", "'#'", "'%'", "'%='", "'&&'", "'&&='", "'&'", "'&='", 
		"'('", "')'", "'*'", "'*.'", "'*='", "'+'", "'++'", "'+='", "','", "'-'", 
		"'--'", "'-='", "'->'", "'.'", "'..'", "'..|'", "'/'", "'/='", "':'", 
		"';'", "'<'", "'<>'", "'='", "'=='", "'==='", "'>'", "'?%'", "'?'", "'?*'", 
		"'?+'", "'?-'", "'?.'", "'?/'", "'?:'", "'?['", "'@'", "'Infinity'", "'NaN'", 
		"'['", "'\\\\'", "']'", "'^'", "'^='", "'abstract'", "'and'", "'application'", 
		"'as'", "'assert'", "'block'", "'break'", "'case'", "'catch'", "'class'", 
		"'classpath'", "'construct'", "'contains'", "'continue'", "'default'", 
		"'delegate'", "'do'", "'else'", "'enhancement'", "'enum'", "'eval'", "'except'", 
		"'execution'", "'exists'", "'extends'", "'false'", "'final'", "'finally'", 
		"'find'", "'for'", "'foreach'", "'function'", "'get'", "'hide'", "'if'", 
		"'implements'", "'in'", "'index'", "'interface'", "'internal'", "'iterator'", 
		"'length'", "'new'", "'not'", "'null'", "'or'", "'outer'", "'override'", 
		"'package'", "'private'", "'property'", "'protected'", "'public'", "'readonly'", 
		"'represents'", "'request'", "'return'", "'session'", "'set'", "'startswith'", 
		"'static'", "'statictypeof'", "'structure'", "'super'", "'switch'", "'this'", 
		"'throw'", "'transient'", "'true'", "'try'", "'typeas'", "'typeis'", "'typeloader'", 
		"'typeof'", "'unless'", "'uses'", "'using'", "'var'", "'void'", "'where'", 
		"'while'", "'{'", "'|'", "'|..'", "'|..|'", "'|='", "'||'", "'||='", "'}'", 
		"'~'"
	};
	public static final int EOF=-1;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int T__98=98;
	public static final int T__99=99;
	public static final int T__100=100;
	public static final int T__101=101;
	public static final int T__102=102;
	public static final int T__103=103;
	public static final int T__104=104;
	public static final int T__105=105;
	public static final int T__106=106;
	public static final int T__107=107;
	public static final int T__108=108;
	public static final int T__109=109;
	public static final int T__110=110;
	public static final int T__111=111;
	public static final int T__112=112;
	public static final int T__113=113;
	public static final int T__114=114;
	public static final int T__115=115;
	public static final int T__116=116;
	public static final int T__117=117;
	public static final int T__118=118;
	public static final int T__119=119;
	public static final int T__120=120;
	public static final int T__121=121;
	public static final int T__122=122;
	public static final int T__123=123;
	public static final int T__124=124;
	public static final int T__125=125;
	public static final int T__126=126;
	public static final int T__127=127;
	public static final int T__128=128;
	public static final int T__129=129;
	public static final int T__130=130;
	public static final int T__131=131;
	public static final int T__132=132;
	public static final int T__133=133;
	public static final int T__134=134;
	public static final int T__135=135;
	public static final int T__136=136;
	public static final int T__137=137;
	public static final int T__138=138;
	public static final int T__139=139;
	public static final int T__140=140;
	public static final int T__141=141;
	public static final int T__142=142;
	public static final int T__143=143;
	public static final int T__144=144;
	public static final int T__145=145;
	public static final int T__146=146;
	public static final int T__147=147;
	public static final int T__148=148;
	public static final int T__149=149;
	public static final int T__150=150;
	public static final int T__151=151;
	public static final int T__152=152;
	public static final int T__153=153;
	public static final int T__154=154;
	public static final int T__155=155;
	public static final int T__156=156;
	public static final int T__157=157;
	public static final int T__158=158;
	public static final int T__159=159;
	public static final int T__160=160;
	public static final int T__161=161;
	public static final int T__162=162;
	public static final int T__163=163;
	public static final int T__164=164;
	public static final int T__165=165;
	public static final int T__166=166;
	public static final int T__167=167;
	public static final int T__168=168;
	public static final int T__169=169;
	public static final int T__170=170;
	public static final int BinLiteral=4;
	public static final int COMMENT=5;
	public static final int CharLiteral=6;
	public static final int Digit=7;
	public static final int EscapeSequence=8;
	public static final int Exponent=9;
	public static final int FloatTypeSuffix=10;
	public static final int HASHBANG=11;
	public static final int HexDigit=12;
	public static final int HexLiteral=13;
	public static final int Ident=14;
	public static final int IntOrFloatPointLiteral=15;
	public static final int IntegerTypeSuffix=16;
	public static final int LINE_COMMENT=17;
	public static final int Letter=18;
	public static final int NonZeroDigit=19;
	public static final int NumberLiteral=20;
	public static final int OctalEscape=21;
	public static final int StringLiteral=22;
	public static final int UnicodeEscape=23;
	public static final int WS=24;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "synpred69_GosuProg", "synpred308_GosuProg", "synpred9_GosuProg", 
		"synpred25_GosuProg", "enumConstants", "literal", "synpred345_GosuProg", 
		"synpred365_GosuProg", "header", "synpred93_GosuProg", "assignmentOrMethodCall", 
		"synpred184_GosuProg", "synpred220_GosuProg", "synpred291_GosuProg", "assertStatement", 
		"synpred52_GosuProg", "synpred187_GosuProg", "enhancementMembers", "typeArgument", 
		"synpred3_GosuProg", "synpred210_GosuProg", "synpred371_GosuProg", "synpred109_GosuProg", 
		"synpred206_GosuProg", "fieldDefn", "synpred218_GosuProg", "blockExpr", 
		"returnStatement", "synpred64_GosuProg", "synpred207_GosuProg", "initializer", 
		"synpred287_GosuProg", "classMembers", "synpred344_GosuProg", "synpred150_GosuProg", 
		"anonymousInnerClass", "gClass", "synpred40_GosuProg", "synpred45_GosuProg", 
		"synpred219_GosuProg", "synpred53_GosuProg", "synpred129_GosuProg", "synpred352_GosuProg", 
		"synpred121_GosuProg", "declaration", "synpred30_GosuProg", "synpred271_GosuProg", 
		"synpred282_GosuProg", "functionDefn", "synpred100_GosuProg", "synpred223_GosuProg", 
		"synpred195_GosuProg", "synpred92_GosuProg", "synpred342_GosuProg", "synpred277_GosuProg", 
		"synpred241_GosuProg", "iteratorVar", "synpred231_GosuProg", "statementBlockBody", 
		"synpred214_GosuProg", "optionalArguments", "synpred171_GosuProg", "synpred37_GosuProg", 
		"synpred255_GosuProg", "synpred76_GosuProg", "enumConstant", "typeAsExpr", 
		"synpred62_GosuProg", "multiplicativeOp", "synpred362_GosuProg", "synpred97_GosuProg", 
		"intervalOp", "synpred63_GosuProg", "synpred281_GosuProg", "synpred294_GosuProg", 
		"synpred376_GosuProg", "additiveOp", "synpred173_GosuProg", "synpred32_GosuProg", 
		"synpred44_GosuProg", "synpred57_GosuProg", "arguments", "synpred301_GosuProg", 
		"synpred110_GosuProg", "synpred284_GosuProg", "synpred70_GosuProg", "synpred215_GosuProg", 
		"synpred351_GosuProg", "synpred237_GosuProg", "idclassOrInterfaceType", 
		"synpred79_GosuProg", "synpred238_GosuProg", "synpred47_GosuProg", "synpred240_GosuProg", 
		"synpred316_GosuProg", "blockLiteral", "synpred59_GosuProg", "synpred269_GosuProg", 
		"synpred292_GosuProg", "synpred313_GosuProg", "indexRest", "synpred41_GosuProg", 
		"synpred108_GosuProg", "synpred379_GosuProg", "synpred213_GosuProg", "synpred355_GosuProg", 
		"synpred152_GosuProg", "synpred155_GosuProg", "constructorDefn", "mapInitializerList", 
		"synpred175_GosuProg", "synpred71_GosuProg", "synpred133_GosuProg", "synpred149_GosuProg", 
		"synpred148_GosuProg", "synpred130_GosuProg", "synpred115_GosuProg", "synpred50_GosuProg", 
		"synpred165_GosuProg", "synpred51_GosuProg", "synpred312_GosuProg", "synpred127_GosuProg", 
		"standAloneDataStructureInitialization", "synpred111_GosuProg", "synpred224_GosuProg", 
		"synpred286_GosuProg", "synpred315_GosuProg", "synpred87_GosuProg", "synpred309_GosuProg", 
		"parameters", "synpred288_GosuProg", "synpred26_GosuProg", "synpred162_GosuProg", 
		"throwStatement", "annotationArguments", "synpred153_GosuProg", "initializerAssignment", 
		"synpred225_GosuProg", "synpred112_GosuProg", "synpred12_GosuProg", "newExpr", 
		"bitshiftExpr", "synpred142_GosuProg", "synpred346_GosuProg", "synpred337_GosuProg", 
		"statementTop", "type", "synpred28_GosuProg", "synpred248_GosuProg", "synpred2_GosuProg", 
		"synpred278_GosuProg", "unaryExpr", "classBody", "enhancementBody", "synpred259_GosuProg", 
		"typeArguments", "synpred144_GosuProg", "synpred151_GosuProg", "synpred18_GosuProg", 
		"parameterDeclaration", "synpred198_GosuProg", "synpred265_GosuProg", 
		"incrementOp", "synpred303_GosuProg", "synpred349_GosuProg", "typeLiteralType", 
		"synpred5_GosuProg", "synpred211_GosuProg", "synpred21_GosuProg", "delegateStatement", 
		"synpred209_GosuProg", "synpred232_GosuProg", "statement", "synpred299_GosuProg", 
		"synpred67_GosuProg", "synpred304_GosuProg", "synpred48_GosuProg", "synpred122_GosuProg", 
		"synpred267_GosuProg", "usingStatement", "synpred340_GosuProg", "synpred324_GosuProg", 
		"synpred361_GosuProg", "typeLoaderStatements", "synpred86_GosuProg", "synpred119_GosuProg", 
		"modifiers", "synpred56_GosuProg", "start", "synpred374_GosuProg", "synpred217_GosuProg", 
		"synpred89_GosuProg", "synpred161_GosuProg", "synpred167_GosuProg", "synpred131_GosuProg", 
		"synpred338_GosuProg", "synpred193_GosuProg", "synpred233_GosuProg", "synpred205_GosuProg", 
		"synpred95_GosuProg", "synpred154_GosuProg", "multiplicativeExpr", "synpred368_GosuProg", 
		"synpred120_GosuProg", "typeLiteralList", "relOp", "synpred82_GosuProg", 
		"synpred96_GosuProg", "idAll", "arrayInitializer", "usesStatementList", 
		"synpred107_GosuProg", "synpred192_GosuProg", "synpred329_GosuProg", "classpathStatements", 
		"synpred268_GosuProg", "synpred137_GosuProg", "switchBlockStatementGroup", 
		"synpred160_GosuProg", "synpred274_GosuProg", "synpred31_GosuProg", "doWhileStatement", 
		"synpred377_GosuProg", "synpred84_GosuProg", "synpred372_GosuProg", "synpred123_GosuProg", 
		"synpred370_GosuProg", "synpred222_GosuProg", "evalExpr", "andOp", "annotation", 
		"synpred348_GosuProg", "synpred49_GosuProg", "blockTypeLiteral", "synpred116_GosuProg", 
		"synpred305_GosuProg", "relationalExpr", "synpred343_GosuProg", "synpred244_GosuProg", 
		"synpred208_GosuProg", "synpred19_GosuProg", "synpred216_GosuProg", "synpred22_GosuProg", 
		"synpred158_GosuProg", "synpred139_GosuProg", "synpred325_GosuProg", "synpred234_GosuProg", 
		"synpred8_GosuProg", "synpred181_GosuProg", "synpred252_GosuProg", "synpred285_GosuProg", 
		"switchStatement", "synpred34_GosuProg", "synpred42_GosuProg", "synpred106_GosuProg", 
		"synpred136_GosuProg", "synpred176_GosuProg", "synpred321_GosuProg", "synpred253_GosuProg", 
		"synpred339_GosuProg", "indexVar", "synpred227_GosuProg", "synpred297_GosuProg", 
		"synpred118_GosuProg", "synpred124_GosuProg", "synpred13_GosuProg", "typeAsOp", 
		"synpred334_GosuProg", "synpred295_GosuProg", "synpred73_GosuProg", "enumBody", 
		"bitwiseOrExpr", "synpred311_GosuProg", "synpred145_GosuProg", "synpred258_GosuProg", 
		"synpred197_GosuProg", "functionBody", "localVarStatement", "synpred65_GosuProg", 
		"synpred77_GosuProg", "synpred358_GosuProg", "additiveExpr", "synpred23_GosuProg", 
		"thisSuperExpr", "synpred318_GosuProg", "synpred302_GosuProg", "synpred251_GosuProg", 
		"parameterDeclarationList", "synpred183_GosuProg", "synpred347_GosuProg", 
		"synpred246_GosuProg", "synpred239_GosuProg", "typeLiteralExpr", "synpred17_GosuProg", 
		"whileStatement", "synpred182_GosuProg", "synpred60_GosuProg", "synpred156_GosuProg", 
		"synpred178_GosuProg", "synpred68_GosuProg", "usesStatement", "synpred185_GosuProg", 
		"synpred226_GosuProg", "synpred35_GosuProg", "synpred101_GosuProg", "synpred174_GosuProg", 
		"synpred61_GosuProg", "synpred373_GosuProg", "synpred104_GosuProg", "synpred6_GosuProg", 
		"synpred353_GosuProg", "synpred323_GosuProg", "synpred375_GosuProg", "forEachStatement", 
		"unaryExprNotPlusMinus", "unaryOp", "catchClause", "synpred283_GosuProg", 
		"synpred366_GosuProg", "synpred117_GosuProg", "bitwiseXorExpr", "synpred163_GosuProg", 
		"synpred310_GosuProg", "synpred356_GosuProg", "synpred199_GosuProg", "synpred140_GosuProg", 
		"classOrInterfaceType", "synpred11_GosuProg", "synpred39_GosuProg", "synpred172_GosuProg", 
		"equalityOp", "typeVariableDefs", "synpred128_GosuProg", "synpred203_GosuProg", 
		"synpred58_GosuProg", "argExpression", "synpred102_GosuProg", "synpred81_GosuProg", 
		"synpred143_GosuProg", "synpred369_GosuProg", "synpred272_GosuProg", "synpred170_GosuProg", 
		"gEnhancement", "synpred14_GosuProg", "synpred254_GosuProg", "synpred298_GosuProg", 
		"synpred273_GosuProg", "synpred180_GosuProg", "synpred245_GosuProg", "synpred1_GosuProg", 
		"synpred293_GosuProg", "synpred200_GosuProg", "synpred327_GosuProg", "synpred196_GosuProg", 
		"synpred264_GosuProg", "synpred164_GosuProg", "synpred190_GosuProg", "synpred202_GosuProg", 
		"bitwiseAndExpr", "synpred328_GosuProg", "synpred204_GosuProg", "synpred66_GosuProg", 
		"synpred367_GosuProg", "synpred236_GosuProg", "synpred306_GosuProg", "interfaceMembers", 
		"synpred249_GosuProg", "synpred230_GosuProg", "synpred341_GosuProg", "synpred113_GosuProg", 
		"intervalExpr", "synpred10_GosuProg", "gEnum", "synpred105_GosuProg", 
		"synpred335_GosuProg", "synpred357_GosuProg", "synpred157_GosuProg", "objectInitializer", 
		"synpred250_GosuProg", "synpred279_GosuProg", "synpred132_GosuProg", "assignmentOp", 
		"dotPathWord", "synpred15_GosuProg", "synpred296_GosuProg", "tryCatchFinallyStatement", 
		"synpred331_GosuProg", "blockLiteralArg", "synpred27_GosuProg", "synpred134_GosuProg", 
		"conditionalAndExpr", "synpred103_GosuProg", "synpred243_GosuProg", "indirectMemberAccess", 
		"synpred326_GosuProg", "synpred166_GosuProg", "synpred229_GosuProg", "synpred270_GosuProg", 
		"synpred80_GosuProg", "synpred314_GosuProg", "synpred72_GosuProg", "synpred169_GosuProg", 
		"synpred29_GosuProg", "synpred99_GosuProg", "synpred114_GosuProg", "synpred54_GosuProg", 
		"synpred280_GosuProg", "synpred350_GosuProg", "namespaceStatement", "synpred194_GosuProg", 
		"synpred20_GosuProg", "synpred364_GosuProg", "synpred125_GosuProg", "synpred201_GosuProg", 
		"statementBlock", "synpred159_GosuProg", "gInterfaceOrStructure", "synpred4_GosuProg", 
		"synpred380_GosuProg", "synpred322_GosuProg", "synpred290_GosuProg", "interfaceBody", 
		"synpred74_GosuProg", "synpred354_GosuProg", "orOp", "synpred141_GosuProg", 
		"synpred126_GosuProg", "synpred94_GosuProg", "synpred307_GosuProg", "synpred147_GosuProg", 
		"synpred88_GosuProg", "synpred275_GosuProg", "synpred212_GosuProg", "synpred7_GosuProg", 
		"synpred381_GosuProg", "propertyDefn", "equalityExpr", "featureLiteral", 
		"synpred257_GosuProg", "synpred24_GosuProg", "synpred85_GosuProg", "synpred135_GosuProg", 
		"synpred91_GosuProg", "primaryExpr", "synpred262_GosuProg", "synpred378_GosuProg", 
		"synpred330_GosuProg", "arrayValueList", "synpred189_GosuProg", "synpred263_GosuProg", 
		"synpred179_GosuProg", "synpred317_GosuProg", "synpred78_GosuProg", "ifStatement", 
		"synpred235_GosuProg", "synpred359_GosuProg", "synpred319_GosuProg", "synpred98_GosuProg", 
		"synpred36_GosuProg", "synpred146_GosuProg", "synpred90_GosuProg", "synpred289_GosuProg", 
		"synpred177_GosuProg", "synpred228_GosuProg", "synpred363_GosuProg", "synpred333_GosuProg", 
		"conditionalExpr", "synpred46_GosuProg", "namedArgumentExpression", "typeLiteral", 
		"synpred83_GosuProg", "synpred168_GosuProg", "synpred320_GosuProg", "synpred332_GosuProg", 
		"synpred16_GosuProg", "synpred55_GosuProg", "synpred336_GosuProg", "synpred186_GosuProg", 
		"synpred276_GosuProg", "delegateDefn", "synpred247_GosuProg", "synpred221_GosuProg", 
		"typeVariableDefinition", "id", "synpred43_GosuProg", "conditionalOrExpr", 
		"synpred360_GosuProg", "synpred188_GosuProg", "synpred260_GosuProg", "parenthExpr", 
		"synpred300_GosuProg", "expression", "synpred75_GosuProg", "optionalType", 
		"synpred33_GosuProg", "synpred38_GosuProg", "synpred266_GosuProg", "synpred256_GosuProg", 
		"synpred138_GosuProg", "synpred191_GosuProg", "initializerExpression", 
		"bitshiftOp", "synpred242_GosuProg", "synpred261_GosuProg"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, true, true, false, true, true, false, false, true, true, false, 
		    false, false, false, false, false, false, false, false, false, true, 
		    true, false, false, false, false, false, false, false, false, false, 
		    false, false, false, false, false, false, false, true, true, false, 
		    false, false, false, true, false, false, true, false, false, false, 
		    false, false, false, false, true, true, false, true, true, false, 
		    false, false, false, false, false, false, false, false, true, false, 
		    false, false, false, false, false, false, false, false, false, false, 
		    false, false, false, true, false, false, false, true, true, false, 
		    false, true, false, true, false, false, true, true, true, true, true, 
		    true, true, true, true, true, true, true, true, false, false, false, 
		    true, false, false, false, false, false, false, false, true, false, 
		    false, false, true, false, true, true, false, true, false, false, 
		    false, false, false, false, true, false, false, false, true, false, 
		    false, false, false, false, false, false, false, false, false, false, 
		    false, false, false, false, false, false, false, false, false, false, 
		    false, false, false, false, false, false, false, false, true, false, 
		    false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public GosuProgParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public GosuProgParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		this.state.ruleMemo = new HashMap[504+1];


		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this, port, null);

		setDebugListener(proxy);
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
	}

	public GosuProgParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg, new RecognizerSharedState());
		this.state.ruleMemo = new HashMap[504+1];


	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

	@Override public String[] getTokenNames() { return GosuProgParser.tokenNames; }
	@Override public String getGrammarFileName() { return "GosuProg.g"; }


	  @Override
	  protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
	    throw new MismatchedTokenException(ttype, input);
	  }
	  @Override
	  public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException {
	    throw e;
	  }



	// $ANTLR start "start"
	// GosuProg.g:36:1: start : header ( modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement ) | statementTop )+ ;
	public final void start() throws RecognitionException {
		int start_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "start");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(36, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return; }

			// GosuProg.g:36:7: ( header ( modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement ) | statementTop )+ )
			dbg.enterAlt(1);

			// GosuProg.g:36:9: header ( modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement ) | statementTop )+
			{
			dbg.location(36,9);
			pushFollow(FOLLOW_header_in_start71);
			header();
			state._fsp--;
			if (state.failed) return;dbg.location(36,16);
			// GosuProg.g:36:16: ( modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement ) | statementTop )+
			int cnt2=0;
			try { dbg.enterSubRule(2);

			loop2:
			while (true) {
				int alt2=3;
				try { dbg.enterDecision(2, decisionCanBacktrack[2]);

				switch ( input.LA(1) ) {
				case 73:
					{
					int LA2_2 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 130:
					{
					int LA2_3 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 120:
					{
					int LA2_4 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 132:
					{
					int LA2_5 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 133:
					{
					int LA2_6 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 141:
					{
					int LA2_7 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 81:
					{
					int LA2_8 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 128:
					{
					int LA2_9 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 107:
					{
					int LA2_10 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 148:
					{
					int LA2_11 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case 90:
				case 100:
				case 119:
				case 143:
					{
					alt2=1;
					}
					break;
				case 99:
					{
					int LA2_15 = input.LA(2);
					if ( (synpred4_GosuProg()) ) {
						alt2=1;
					}
					else if ( (synpred5_GosuProg()) ) {
						alt2=2;
					}

					}
					break;
				case Ident:
				case StringLiteral:
				case 38:
				case 57:
				case 74:
				case 75:
				case 83:
				case 84:
				case 85:
				case 86:
				case 87:
				case 91:
				case 93:
				case 94:
				case 97:
				case 101:
				case 102:
				case 103:
				case 104:
				case 106:
				case 109:
				case 110:
				case 111:
				case 112:
				case 113:
				case 114:
				case 115:
				case 118:
				case 121:
				case 122:
				case 123:
				case 125:
				case 127:
				case 131:
				case 134:
				case 136:
				case 137:
				case 138:
				case 139:
				case 140:
				case 144:
				case 145:
				case 146:
				case 147:
				case 149:
				case 150:
				case 153:
				case 157:
				case 158:
				case 159:
				case 160:
				case 161:
				case 162:
					{
					alt2=2;
					}
					break;
				}
				} finally {dbg.exitDecision(2);}

				switch (alt2) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:36:18: modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement )
					{
					dbg.location(36,18);
					pushFollow(FOLLOW_modifiers_in_start75);
					modifiers();
					state._fsp--;
					if (state.failed) return;dbg.location(36,28);
					// GosuProg.g:36:28: ( gClass | gInterfaceOrStructure | gEnum | gEnhancement )
					int alt1=4;
					try { dbg.enterSubRule(1);
					try { dbg.enterDecision(1, decisionCanBacktrack[1]);

					switch ( input.LA(1) ) {
					case 90:
						{
						alt1=1;
						}
						break;
					case 119:
					case 143:
						{
						alt1=2;
						}
						break;
					case 100:
						{
						alt1=3;
						}
						break;
					case 99:
						{
						alt1=4;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 1, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}
					} finally {dbg.exitDecision(1);}

					switch (alt1) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:36:29: gClass
							{
							dbg.location(36,29);
							pushFollow(FOLLOW_gClass_in_start78);
							gClass();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:36:38: gInterfaceOrStructure
							{
							dbg.location(36,38);
							pushFollow(FOLLOW_gInterfaceOrStructure_in_start82);
							gInterfaceOrStructure();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 3 :
							dbg.enterAlt(3);

							// GosuProg.g:36:62: gEnum
							{
							dbg.location(36,62);
							pushFollow(FOLLOW_gEnum_in_start86);
							gEnum();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 4 :
							dbg.enterAlt(4);

							// GosuProg.g:36:70: gEnhancement
							{
							dbg.location(36,70);
							pushFollow(FOLLOW_gEnhancement_in_start90);
							gEnhancement();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(1);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:36:86: statementTop
					{
					dbg.location(36,86);
					pushFollow(FOLLOW_statementTop_in_start95);
					statementTop();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(2, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt2++;
			}
			} finally {dbg.exitSubRule(2);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

		}
		dbg.location(36, 101);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "start");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "start"



	// $ANTLR start "statementTop"
	// GosuProg.g:38:1: statementTop : ( statement | modifiers functionDefn functionBody | modifiers propertyDefn functionBody );
	public final void statementTop() throws RecognitionException {
		int statementTop_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "statementTop");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(38, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return; }

			// GosuProg.g:38:14: ( statement | modifiers functionDefn functionBody | modifiers propertyDefn functionBody )
			int alt3=3;
			try { dbg.enterDecision(3, decisionCanBacktrack[3]);

			switch ( input.LA(1) ) {
			case Ident:
			case StringLiteral:
			case 38:
			case 57:
			case 74:
			case 75:
			case 83:
			case 84:
			case 85:
			case 86:
			case 87:
			case 91:
			case 93:
			case 94:
			case 97:
			case 99:
			case 101:
			case 102:
			case 103:
			case 104:
			case 106:
			case 109:
			case 110:
			case 111:
			case 113:
			case 114:
			case 115:
			case 118:
			case 121:
			case 122:
			case 123:
			case 125:
			case 127:
			case 134:
			case 136:
			case 137:
			case 138:
			case 139:
			case 140:
			case 144:
			case 145:
			case 146:
			case 147:
			case 149:
			case 150:
			case 153:
			case 157:
			case 158:
			case 159:
			case 160:
			case 161:
			case 162:
				{
				alt3=1;
				}
				break;
			case 107:
				{
				int LA3_13 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 130:
				{
				int LA3_18 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 73:
				{
				int LA3_24 = input.LA(2);
				if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 120:
				{
				int LA3_25 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 132:
				{
				int LA3_26 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 133:
				{
				int LA3_27 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 141:
				{
				int LA3_28 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 81:
				{
				int LA3_29 = input.LA(2);
				if ( (synpred6_GosuProg()) ) {
					alt3=1;
				}
				else if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 128:
				{
				int LA3_31 = input.LA(2);
				if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 148:
				{
				int LA3_32 = input.LA(2);
				if ( (synpred7_GosuProg()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case 112:
				{
				alt3=2;
				}
				break;
			case 131:
				{
				alt3=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(3);}

			switch (alt3) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:38:16: statement
					{
					dbg.location(38,16);
					pushFollow(FOLLOW_statement_in_statementTop107);
					statement();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:39:16: modifiers functionDefn functionBody
					{
					dbg.location(39,16);
					pushFollow(FOLLOW_modifiers_in_statementTop161);
					modifiers();
					state._fsp--;
					if (state.failed) return;dbg.location(39,26);
					pushFollow(FOLLOW_functionDefn_in_statementTop163);
					functionDefn();
					state._fsp--;
					if (state.failed) return;dbg.location(39,39);
					pushFollow(FOLLOW_functionBody_in_statementTop165);
					functionBody();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:40:16: modifiers propertyDefn functionBody
					{
					dbg.location(40,16);
					pushFollow(FOLLOW_modifiers_in_statementTop193);
					modifiers();
					state._fsp--;
					if (state.failed) return;dbg.location(40,26);
					pushFollow(FOLLOW_propertyDefn_in_statementTop195);
					propertyDefn();
					state._fsp--;
					if (state.failed) return;dbg.location(40,39);
					pushFollow(FOLLOW_functionBody_in_statementTop197);
					functionBody();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 2, statementTop_StartIndex); }

		}
		dbg.location(41, 13);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "statementTop");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "statementTop"



	// $ANTLR start "header"
	// GosuProg.g:43:1: header : ( HASHBANG )? ( classpathStatements )? ( typeLoaderStatements )? ( 'package' namespaceStatement )? ( usesStatementList )? ;
	public final void header() throws RecognitionException {
		int header_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "header");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(43, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return; }

			// GosuProg.g:43:8: ( ( HASHBANG )? ( classpathStatements )? ( typeLoaderStatements )? ( 'package' namespaceStatement )? ( usesStatementList )? )
			dbg.enterAlt(1);

			// GosuProg.g:43:10: ( HASHBANG )? ( classpathStatements )? ( typeLoaderStatements )? ( 'package' namespaceStatement )? ( usesStatementList )?
			{
			dbg.location(43,10);
			// GosuProg.g:43:10: ( HASHBANG )?
			int alt4=2;
			try { dbg.enterSubRule(4);
			try { dbg.enterDecision(4, decisionCanBacktrack[4]);

			int LA4_0 = input.LA(1);
			if ( (LA4_0==HASHBANG) ) {
				alt4=1;
			}
			} finally {dbg.exitDecision(4);}

			switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:43:10: HASHBANG
					{
					dbg.location(43,10);
					match(input,HASHBANG,FOLLOW_HASHBANG_in_header219); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(4);}
			dbg.location(43,20);
			// GosuProg.g:43:20: ( classpathStatements )?
			int alt5=2;
			try { dbg.enterSubRule(5);
			try { dbg.enterDecision(5, decisionCanBacktrack[5]);

			int LA5_0 = input.LA(1);
			if ( (LA5_0==91) ) {
				int LA5_1 = input.LA(2);
				if ( (LA5_1==StringLiteral) ) {
					int LA5_3 = input.LA(3);
					if ( (synpred9_GosuProg()) ) {
						alt5=1;
					}
				}
			}
			} finally {dbg.exitDecision(5);}

			switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:43:20: classpathStatements
					{
					dbg.location(43,20);
					pushFollow(FOLLOW_classpathStatements_in_header222);
					classpathStatements();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(5);}
			dbg.location(43,41);
			// GosuProg.g:43:41: ( typeLoaderStatements )?
			int alt6=2;
			try { dbg.enterSubRule(6);
			try { dbg.enterDecision(6, decisionCanBacktrack[6]);

			int LA6_0 = input.LA(1);
			if ( (LA6_0==153) ) {
				int LA6_1 = input.LA(2);
				if ( (synpred10_GosuProg()) ) {
					alt6=1;
				}
			}
			} finally {dbg.exitDecision(6);}

			switch (alt6) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:43:41: typeLoaderStatements
					{
					dbg.location(43,41);
					pushFollow(FOLLOW_typeLoaderStatements_in_header225);
					typeLoaderStatements();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(6);}
			dbg.location(43,63);
			// GosuProg.g:43:63: ( 'package' namespaceStatement )?
			int alt7=2;
			try { dbg.enterSubRule(7);
			try { dbg.enterDecision(7, decisionCanBacktrack[7]);

			int LA7_0 = input.LA(1);
			if ( (LA7_0==129) ) {
				alt7=1;
			}
			} finally {dbg.exitDecision(7);}

			switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:43:64: 'package' namespaceStatement
					{
					dbg.location(43,64);
					match(input,129,FOLLOW_129_in_header229); if (state.failed) return;dbg.location(43,74);
					pushFollow(FOLLOW_namespaceStatement_in_header231);
					namespaceStatement();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(7);}
			dbg.location(43,95);
			// GosuProg.g:43:95: ( usesStatementList )?
			int alt8=2;
			try { dbg.enterSubRule(8);
			try { dbg.enterDecision(8, decisionCanBacktrack[8]);

			int LA8_0 = input.LA(1);
			if ( (LA8_0==156) ) {
				alt8=1;
			}
			} finally {dbg.exitDecision(8);}

			switch (alt8) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:43:95: usesStatementList
					{
					dbg.location(43,95);
					pushFollow(FOLLOW_usesStatementList_in_header235);
					usesStatementList();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(8);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 3, header_StartIndex); }

		}
		dbg.location(43, 113);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "header");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "header"



	// $ANTLR start "classpathStatements"
	// GosuProg.g:45:1: classpathStatements : ( 'classpath' StringLiteral )+ ;
	public final void classpathStatements() throws RecognitionException {
		int classpathStatements_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "classpathStatements");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(45, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return; }

			// GosuProg.g:45:21: ( ( 'classpath' StringLiteral )+ )
			dbg.enterAlt(1);

			// GosuProg.g:45:23: ( 'classpath' StringLiteral )+
			{
			dbg.location(45,23);
			// GosuProg.g:45:23: ( 'classpath' StringLiteral )+
			int cnt9=0;
			try { dbg.enterSubRule(9);

			loop9:
			while (true) {
				int alt9=2;
				try { dbg.enterDecision(9, decisionCanBacktrack[9]);

				int LA9_0 = input.LA(1);
				if ( (LA9_0==91) ) {
					int LA9_2 = input.LA(2);
					if ( (LA9_2==StringLiteral) ) {
						int LA9_3 = input.LA(3);
						if ( (synpred13_GosuProg()) ) {
							alt9=1;
						}

					}

				}

				} finally {dbg.exitDecision(9);}

				switch (alt9) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:45:24: 'classpath' StringLiteral
					{
					dbg.location(45,24);
					match(input,91,FOLLOW_91_in_classpathStatements246); if (state.failed) return;dbg.location(45,36);
					match(input,StringLiteral,FOLLOW_StringLiteral_in_classpathStatements248); if (state.failed) return;
					}
					break;

				default :
					if ( cnt9 >= 1 ) break loop9;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(9, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt9++;
			}
			} finally {dbg.exitSubRule(9);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 4, classpathStatements_StartIndex); }

		}
		dbg.location(45, 51);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "classpathStatements");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "classpathStatements"



	// $ANTLR start "typeLoaderStatements"
	// GosuProg.g:47:1: typeLoaderStatements : ( 'typeloader' typeLiteral )+ ;
	public final void typeLoaderStatements() throws RecognitionException {
		int typeLoaderStatements_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeLoaderStatements");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(47, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return; }

			// GosuProg.g:47:22: ( ( 'typeloader' typeLiteral )+ )
			dbg.enterAlt(1);

			// GosuProg.g:47:24: ( 'typeloader' typeLiteral )+
			{
			dbg.location(47,24);
			// GosuProg.g:47:24: ( 'typeloader' typeLiteral )+
			int cnt10=0;
			try { dbg.enterSubRule(10);

			loop10:
			while (true) {
				int alt10=2;
				try { dbg.enterDecision(10, decisionCanBacktrack[10]);

				int LA10_0 = input.LA(1);
				if ( (LA10_0==153) ) {
					int LA10_33 = input.LA(2);
					if ( (synpred14_GosuProg()) ) {
						alt10=1;
					}

				}

				} finally {dbg.exitDecision(10);}

				switch (alt10) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:47:25: 'typeloader' typeLiteral
					{
					dbg.location(47,25);
					match(input,153,FOLLOW_153_in_typeLoaderStatements260); if (state.failed) return;dbg.location(47,38);
					pushFollow(FOLLOW_typeLiteral_in_typeLoaderStatements262);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					if ( cnt10 >= 1 ) break loop10;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(10, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt10++;
			}
			} finally {dbg.exitSubRule(10);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 5, typeLoaderStatements_StartIndex); }

		}
		dbg.location(47, 51);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeLoaderStatements");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeLoaderStatements"



	// $ANTLR start "annotation"
	// GosuProg.g:49:1: annotation : '@' idAll ( '.' idAll )* ( annotationArguments )? ;
	public final void annotation() throws RecognitionException {
		int annotation_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "annotation");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(49, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return; }

			// GosuProg.g:49:12: ( '@' idAll ( '.' idAll )* ( annotationArguments )? )
			dbg.enterAlt(1);

			// GosuProg.g:49:14: '@' idAll ( '.' idAll )* ( annotationArguments )?
			{
			dbg.location(49,14);
			match(input,73,FOLLOW_73_in_annotation273); if (state.failed) return;dbg.location(49,18);
			pushFollow(FOLLOW_idAll_in_annotation275);
			idAll();
			state._fsp--;
			if (state.failed) return;dbg.location(49,24);
			// GosuProg.g:49:24: ( '.' idAll )*
			try { dbg.enterSubRule(11);

			loop11:
			while (true) {
				int alt11=2;
				try { dbg.enterDecision(11, decisionCanBacktrack[11]);

				int LA11_0 = input.LA(1);
				if ( (LA11_0==51) ) {
					alt11=1;
				}

				} finally {dbg.exitDecision(11);}

				switch (alt11) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:49:25: '.' idAll
					{
					dbg.location(49,25);
					match(input,51,FOLLOW_51_in_annotation278); if (state.failed) return;dbg.location(49,29);
					pushFollow(FOLLOW_idAll_in_annotation280);
					idAll();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop11;
				}
			}
			} finally {dbg.exitSubRule(11);}
			dbg.location(49,37);
			// GosuProg.g:49:37: ( annotationArguments )?
			int alt12=2;
			try { dbg.enterSubRule(12);
			try { dbg.enterDecision(12, decisionCanBacktrack[12]);

			int LA12_0 = input.LA(1);
			if ( (LA12_0==38) ) {
				alt12=1;
			}
			} finally {dbg.exitDecision(12);}

			switch (alt12) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:49:37: annotationArguments
					{
					dbg.location(49,37);
					pushFollow(FOLLOW_annotationArguments_in_annotation284);
					annotationArguments();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(12);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 6, annotation_StartIndex); }

		}
		dbg.location(49, 57);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "annotation");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "annotation"



	// $ANTLR start "gClass"
	// GosuProg.g:51:1: gClass : 'class' id typeVariableDefs ( 'extends' classOrInterfaceType )? ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? classBody ;
	public final void gClass() throws RecognitionException {
		int gClass_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "gClass");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(51, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return; }

			// GosuProg.g:51:8: ( 'class' id typeVariableDefs ( 'extends' classOrInterfaceType )? ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? classBody )
			dbg.enterAlt(1);

			// GosuProg.g:51:10: 'class' id typeVariableDefs ( 'extends' classOrInterfaceType )? ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? classBody
			{
			dbg.location(51,10);
			match(input,90,FOLLOW_90_in_gClass294); if (state.failed) return;dbg.location(51,18);
			pushFollow(FOLLOW_id_in_gClass296);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(51,21);
			pushFollow(FOLLOW_typeVariableDefs_in_gClass298);
			typeVariableDefs();
			state._fsp--;
			if (state.failed) return;dbg.location(51,38);
			// GosuProg.g:51:38: ( 'extends' classOrInterfaceType )?
			int alt13=2;
			try { dbg.enterSubRule(13);
			try { dbg.enterDecision(13, decisionCanBacktrack[13]);

			int LA13_0 = input.LA(1);
			if ( (LA13_0==105) ) {
				alt13=1;
			}
			} finally {dbg.exitDecision(13);}

			switch (alt13) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:51:39: 'extends' classOrInterfaceType
					{
					dbg.location(51,39);
					match(input,105,FOLLOW_105_in_gClass301); if (state.failed) return;dbg.location(51,49);
					pushFollow(FOLLOW_classOrInterfaceType_in_gClass303);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(13);}
			dbg.location(51,72);
			// GosuProg.g:51:72: ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )?
			int alt15=2;
			try { dbg.enterSubRule(15);
			try { dbg.enterDecision(15, decisionCanBacktrack[15]);

			int LA15_0 = input.LA(1);
			if ( (LA15_0==116) ) {
				alt15=1;
			}
			} finally {dbg.exitDecision(15);}

			switch (alt15) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:51:73: 'implements' classOrInterfaceType ( ',' classOrInterfaceType )*
					{
					dbg.location(51,73);
					match(input,116,FOLLOW_116_in_gClass308); if (state.failed) return;dbg.location(51,86);
					pushFollow(FOLLOW_classOrInterfaceType_in_gClass310);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;dbg.location(51,107);
					// GosuProg.g:51:107: ( ',' classOrInterfaceType )*
					try { dbg.enterSubRule(14);

					loop14:
					while (true) {
						int alt14=2;
						try { dbg.enterDecision(14, decisionCanBacktrack[14]);

						int LA14_0 = input.LA(1);
						if ( (LA14_0==46) ) {
							alt14=1;
						}

						} finally {dbg.exitDecision(14);}

						switch (alt14) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:51:108: ',' classOrInterfaceType
							{
							dbg.location(51,108);
							match(input,46,FOLLOW_46_in_gClass313); if (state.failed) return;dbg.location(51,112);
							pushFollow(FOLLOW_classOrInterfaceType_in_gClass315);
							classOrInterfaceType();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop14;
						}
					}
					} finally {dbg.exitSubRule(14);}

					}
					break;

			}
			} finally {dbg.exitSubRule(15);}
			dbg.location(51,137);
			pushFollow(FOLLOW_classBody_in_gClass321);
			classBody();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 7, gClass_StartIndex); }

		}
		dbg.location(51, 146);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "gClass");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "gClass"



	// $ANTLR start "gInterfaceOrStructure"
	// GosuProg.g:53:1: gInterfaceOrStructure : ( 'interface' | 'structure' ) id typeVariableDefs ( 'extends' classOrInterfaceType ( ',' classOrInterfaceType )* )? interfaceBody ;
	public final void gInterfaceOrStructure() throws RecognitionException {
		int gInterfaceOrStructure_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "gInterfaceOrStructure");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(53, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return; }

			// GosuProg.g:53:23: ( ( 'interface' | 'structure' ) id typeVariableDefs ( 'extends' classOrInterfaceType ( ',' classOrInterfaceType )* )? interfaceBody )
			dbg.enterAlt(1);

			// GosuProg.g:53:25: ( 'interface' | 'structure' ) id typeVariableDefs ( 'extends' classOrInterfaceType ( ',' classOrInterfaceType )* )? interfaceBody
			{
			dbg.location(53,25);
			if ( input.LA(1)==119||input.LA(1)==143 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}dbg.location(53,53);
			pushFollow(FOLLOW_id_in_gInterfaceOrStructure338);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(53,56);
			pushFollow(FOLLOW_typeVariableDefs_in_gInterfaceOrStructure340);
			typeVariableDefs();
			state._fsp--;
			if (state.failed) return;dbg.location(53,73);
			// GosuProg.g:53:73: ( 'extends' classOrInterfaceType ( ',' classOrInterfaceType )* )?
			int alt17=2;
			try { dbg.enterSubRule(17);
			try { dbg.enterDecision(17, decisionCanBacktrack[17]);

			int LA17_0 = input.LA(1);
			if ( (LA17_0==105) ) {
				alt17=1;
			}
			} finally {dbg.exitDecision(17);}

			switch (alt17) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:53:74: 'extends' classOrInterfaceType ( ',' classOrInterfaceType )*
					{
					dbg.location(53,74);
					match(input,105,FOLLOW_105_in_gInterfaceOrStructure343); if (state.failed) return;dbg.location(53,84);
					pushFollow(FOLLOW_classOrInterfaceType_in_gInterfaceOrStructure345);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;dbg.location(53,105);
					// GosuProg.g:53:105: ( ',' classOrInterfaceType )*
					try { dbg.enterSubRule(16);

					loop16:
					while (true) {
						int alt16=2;
						try { dbg.enterDecision(16, decisionCanBacktrack[16]);

						int LA16_0 = input.LA(1);
						if ( (LA16_0==46) ) {
							alt16=1;
						}

						} finally {dbg.exitDecision(16);}

						switch (alt16) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:53:106: ',' classOrInterfaceType
							{
							dbg.location(53,106);
							match(input,46,FOLLOW_46_in_gInterfaceOrStructure348); if (state.failed) return;dbg.location(53,110);
							pushFollow(FOLLOW_classOrInterfaceType_in_gInterfaceOrStructure350);
							classOrInterfaceType();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop16;
						}
					}
					} finally {dbg.exitSubRule(16);}

					}
					break;

			}
			} finally {dbg.exitSubRule(17);}
			dbg.location(53,135);
			pushFollow(FOLLOW_interfaceBody_in_gInterfaceOrStructure356);
			interfaceBody();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 8, gInterfaceOrStructure_StartIndex); }

		}
		dbg.location(53, 148);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "gInterfaceOrStructure");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "gInterfaceOrStructure"



	// $ANTLR start "gEnum"
	// GosuProg.g:55:1: gEnum : 'enum' id typeVariableDefs ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? enumBody ;
	public final void gEnum() throws RecognitionException {
		int gEnum_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "gEnum");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(55, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return; }

			// GosuProg.g:55:7: ( 'enum' id typeVariableDefs ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? enumBody )
			dbg.enterAlt(1);

			// GosuProg.g:55:9: 'enum' id typeVariableDefs ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )? enumBody
			{
			dbg.location(55,9);
			match(input,100,FOLLOW_100_in_gEnum365); if (state.failed) return;dbg.location(55,16);
			pushFollow(FOLLOW_id_in_gEnum367);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(55,19);
			pushFollow(FOLLOW_typeVariableDefs_in_gEnum369);
			typeVariableDefs();
			state._fsp--;
			if (state.failed) return;dbg.location(55,36);
			// GosuProg.g:55:36: ( 'implements' classOrInterfaceType ( ',' classOrInterfaceType )* )?
			int alt19=2;
			try { dbg.enterSubRule(19);
			try { dbg.enterDecision(19, decisionCanBacktrack[19]);

			int LA19_0 = input.LA(1);
			if ( (LA19_0==116) ) {
				alt19=1;
			}
			} finally {dbg.exitDecision(19);}

			switch (alt19) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:55:37: 'implements' classOrInterfaceType ( ',' classOrInterfaceType )*
					{
					dbg.location(55,37);
					match(input,116,FOLLOW_116_in_gEnum372); if (state.failed) return;dbg.location(55,50);
					pushFollow(FOLLOW_classOrInterfaceType_in_gEnum374);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;dbg.location(55,71);
					// GosuProg.g:55:71: ( ',' classOrInterfaceType )*
					try { dbg.enterSubRule(18);

					loop18:
					while (true) {
						int alt18=2;
						try { dbg.enterDecision(18, decisionCanBacktrack[18]);

						int LA18_0 = input.LA(1);
						if ( (LA18_0==46) ) {
							alt18=1;
						}

						} finally {dbg.exitDecision(18);}

						switch (alt18) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:55:72: ',' classOrInterfaceType
							{
							dbg.location(55,72);
							match(input,46,FOLLOW_46_in_gEnum377); if (state.failed) return;dbg.location(55,76);
							pushFollow(FOLLOW_classOrInterfaceType_in_gEnum379);
							classOrInterfaceType();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop18;
						}
					}
					} finally {dbg.exitSubRule(18);}

					}
					break;

			}
			} finally {dbg.exitSubRule(19);}
			dbg.location(55,101);
			pushFollow(FOLLOW_enumBody_in_gEnum385);
			enumBody();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 9, gEnum_StartIndex); }

		}
		dbg.location(55, 109);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "gEnum");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "gEnum"



	// $ANTLR start "gEnhancement"
	// GosuProg.g:57:1: gEnhancement : 'enhancement' id typeVariableDefs ':' classOrInterfaceType ( '[' ']' )* enhancementBody ;
	public final void gEnhancement() throws RecognitionException {
		int gEnhancement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "gEnhancement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(57, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return; }

			// GosuProg.g:57:14: ( 'enhancement' id typeVariableDefs ':' classOrInterfaceType ( '[' ']' )* enhancementBody )
			dbg.enterAlt(1);

			// GosuProg.g:57:16: 'enhancement' id typeVariableDefs ':' classOrInterfaceType ( '[' ']' )* enhancementBody
			{
			dbg.location(57,16);
			match(input,99,FOLLOW_99_in_gEnhancement394); if (state.failed) return;dbg.location(57,30);
			pushFollow(FOLLOW_id_in_gEnhancement396);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(57,33);
			pushFollow(FOLLOW_typeVariableDefs_in_gEnhancement398);
			typeVariableDefs();
			state._fsp--;
			if (state.failed) return;dbg.location(57,50);
			match(input,56,FOLLOW_56_in_gEnhancement400); if (state.failed) return;dbg.location(57,54);
			pushFollow(FOLLOW_classOrInterfaceType_in_gEnhancement402);
			classOrInterfaceType();
			state._fsp--;
			if (state.failed) return;dbg.location(57,75);
			// GosuProg.g:57:75: ( '[' ']' )*
			try { dbg.enterSubRule(20);

			loop20:
			while (true) {
				int alt20=2;
				try { dbg.enterDecision(20, decisionCanBacktrack[20]);

				int LA20_0 = input.LA(1);
				if ( (LA20_0==76) ) {
					alt20=1;
				}

				} finally {dbg.exitDecision(20);}

				switch (alt20) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:57:76: '[' ']'
					{
					dbg.location(57,76);
					match(input,76,FOLLOW_76_in_gEnhancement405); if (state.failed) return;dbg.location(57,80);
					match(input,78,FOLLOW_78_in_gEnhancement407); if (state.failed) return;
					}
					break;

				default :
					break loop20;
				}
			}
			} finally {dbg.exitSubRule(20);}
			dbg.location(57,86);
			pushFollow(FOLLOW_enhancementBody_in_gEnhancement411);
			enhancementBody();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 10, gEnhancement_StartIndex); }

		}
		dbg.location(57, 101);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "gEnhancement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "gEnhancement"



	// $ANTLR start "classBody"
	// GosuProg.g:59:1: classBody : '{' classMembers '}' ;
	public final void classBody() throws RecognitionException {
		int classBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "classBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(59, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return; }

			// GosuProg.g:59:11: ( '{' classMembers '}' )
			dbg.enterAlt(1);

			// GosuProg.g:59:13: '{' classMembers '}'
			{
			dbg.location(59,13);
			match(input,162,FOLLOW_162_in_classBody420); if (state.failed) return;dbg.location(59,17);
			pushFollow(FOLLOW_classMembers_in_classBody422);
			classMembers();
			state._fsp--;
			if (state.failed) return;dbg.location(59,30);
			match(input,169,FOLLOW_169_in_classBody424); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 11, classBody_StartIndex); }

		}
		dbg.location(59, 33);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "classBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "classBody"



	// $ANTLR start "enhancementBody"
	// GosuProg.g:61:1: enhancementBody : '{' enhancementMembers '}' ;
	public final void enhancementBody() throws RecognitionException {
		int enhancementBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "enhancementBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(61, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return; }

			// GosuProg.g:61:17: ( '{' enhancementMembers '}' )
			dbg.enterAlt(1);

			// GosuProg.g:61:19: '{' enhancementMembers '}'
			{
			dbg.location(61,19);
			match(input,162,FOLLOW_162_in_enhancementBody433); if (state.failed) return;dbg.location(61,23);
			pushFollow(FOLLOW_enhancementMembers_in_enhancementBody435);
			enhancementMembers();
			state._fsp--;
			if (state.failed) return;dbg.location(61,42);
			match(input,169,FOLLOW_169_in_enhancementBody437); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 12, enhancementBody_StartIndex); }

		}
		dbg.location(61, 45);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "enhancementBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "enhancementBody"



	// $ANTLR start "interfaceBody"
	// GosuProg.g:63:1: interfaceBody : '{' interfaceMembers '}' ;
	public final void interfaceBody() throws RecognitionException {
		int interfaceBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "interfaceBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(63, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return; }

			// GosuProg.g:63:15: ( '{' interfaceMembers '}' )
			dbg.enterAlt(1);

			// GosuProg.g:63:17: '{' interfaceMembers '}'
			{
			dbg.location(63,17);
			match(input,162,FOLLOW_162_in_interfaceBody446); if (state.failed) return;dbg.location(63,21);
			pushFollow(FOLLOW_interfaceMembers_in_interfaceBody448);
			interfaceMembers();
			state._fsp--;
			if (state.failed) return;dbg.location(63,38);
			match(input,169,FOLLOW_169_in_interfaceBody450); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 13, interfaceBody_StartIndex); }

		}
		dbg.location(63, 41);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "interfaceBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "interfaceBody"



	// $ANTLR start "enumBody"
	// GosuProg.g:65:1: enumBody : '{' ( enumConstants )? classMembers '}' ;
	public final void enumBody() throws RecognitionException {
		int enumBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "enumBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(65, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return; }

			// GosuProg.g:65:10: ( '{' ( enumConstants )? classMembers '}' )
			dbg.enterAlt(1);

			// GosuProg.g:65:12: '{' ( enumConstants )? classMembers '}'
			{
			dbg.location(65,12);
			match(input,162,FOLLOW_162_in_enumBody459); if (state.failed) return;dbg.location(65,16);
			// GosuProg.g:65:16: ( enumConstants )?
			int alt21=2;
			try { dbg.enterSubRule(21);
			try { dbg.enterDecision(21, decisionCanBacktrack[21]);

			switch ( input.LA(1) ) {
				case 130:
					{
					int LA21_1 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 120:
					{
					int LA21_3 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 132:
					{
					int LA21_4 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 133:
					{
					int LA21_5 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 141:
					{
					int LA21_6 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 81:
					{
					int LA21_7 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case 107:
					{
					int LA21_8 = input.LA(2);
					if ( (synpred26_GosuProg()) ) {
						alt21=1;
					}
					}
					break;
				case Ident:
				case 74:
				case 75:
				case 83:
				case 84:
				case 85:
				case 86:
				case 91:
				case 93:
				case 99:
				case 102:
				case 103:
				case 104:
				case 106:
				case 109:
				case 113:
				case 114:
				case 118:
				case 121:
				case 122:
				case 125:
				case 127:
				case 134:
				case 136:
				case 138:
				case 139:
				case 140:
				case 149:
				case 153:
				case 159:
				case 160:
					{
					alt21=1;
					}
					break;
			}
			} finally {dbg.exitDecision(21);}

			switch (alt21) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:65:16: enumConstants
					{
					dbg.location(65,16);
					pushFollow(FOLLOW_enumConstants_in_enumBody461);
					enumConstants();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(21);}
			dbg.location(65,31);
			pushFollow(FOLLOW_classMembers_in_enumBody464);
			classMembers();
			state._fsp--;
			if (state.failed) return;dbg.location(65,44);
			match(input,169,FOLLOW_169_in_enumBody466); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 14, enumBody_StartIndex); }

		}
		dbg.location(65, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "enumBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "enumBody"



	// $ANTLR start "enumConstants"
	// GosuProg.g:67:1: enumConstants : enumConstant ( ',' enumConstant )* ( ',' )? ( ';' )? ;
	public final void enumConstants() throws RecognitionException {
		int enumConstants_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "enumConstants");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(67, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return; }

			// GosuProg.g:67:15: ( enumConstant ( ',' enumConstant )* ( ',' )? ( ';' )? )
			dbg.enterAlt(1);

			// GosuProg.g:67:17: enumConstant ( ',' enumConstant )* ( ',' )? ( ';' )?
			{
			dbg.location(67,17);
			pushFollow(FOLLOW_enumConstant_in_enumConstants475);
			enumConstant();
			state._fsp--;
			if (state.failed) return;dbg.location(67,31);
			// GosuProg.g:67:31: ( ',' enumConstant )*
			try { dbg.enterSubRule(22);

			loop22:
			while (true) {
				int alt22=2;
				try { dbg.enterDecision(22, decisionCanBacktrack[22]);

				int LA22_0 = input.LA(1);
				if ( (LA22_0==46) ) {
					switch ( input.LA(2) ) {
					case 130:
						{
						int LA22_3 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 120:
						{
						int LA22_4 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 132:
						{
						int LA22_5 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 133:
						{
						int LA22_6 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 141:
						{
						int LA22_7 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 81:
						{
						int LA22_8 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case 107:
						{
						int LA22_9 = input.LA(3);
						if ( (synpred27_GosuProg()) ) {
							alt22=1;
						}

						}
						break;
					case Ident:
					case 74:
					case 75:
					case 83:
					case 84:
					case 85:
					case 86:
					case 91:
					case 93:
					case 99:
					case 102:
					case 103:
					case 104:
					case 106:
					case 109:
					case 113:
					case 114:
					case 118:
					case 121:
					case 122:
					case 125:
					case 127:
					case 134:
					case 136:
					case 138:
					case 139:
					case 140:
					case 149:
					case 153:
					case 159:
					case 160:
						{
						alt22=1;
						}
						break;
					}
				}

				} finally {dbg.exitDecision(22);}

				switch (alt22) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:67:32: ',' enumConstant
					{
					dbg.location(67,32);
					match(input,46,FOLLOW_46_in_enumConstants479); if (state.failed) return;dbg.location(67,36);
					pushFollow(FOLLOW_enumConstant_in_enumConstants481);
					enumConstant();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop22;
				}
			}
			} finally {dbg.exitSubRule(22);}
			dbg.location(67,52);
			// GosuProg.g:67:52: ( ',' )?
			int alt23=2;
			try { dbg.enterSubRule(23);
			try { dbg.enterDecision(23, decisionCanBacktrack[23]);

			int LA23_0 = input.LA(1);
			if ( (LA23_0==46) ) {
				alt23=1;
			}
			} finally {dbg.exitDecision(23);}

			switch (alt23) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:67:52: ','
					{
					dbg.location(67,52);
					match(input,46,FOLLOW_46_in_enumConstants486); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(23);}
			dbg.location(67,57);
			// GosuProg.g:67:57: ( ';' )?
			int alt24=2;
			try { dbg.enterSubRule(24);
			try { dbg.enterDecision(24, decisionCanBacktrack[24]);

			int LA24_0 = input.LA(1);
			if ( (LA24_0==57) ) {
				alt24=1;
			}
			} finally {dbg.exitDecision(24);}

			switch (alt24) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:67:57: ';'
					{
					dbg.location(67,57);
					match(input,57,FOLLOW_57_in_enumConstants489); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(24);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 15, enumConstants_StartIndex); }

		}
		dbg.location(67, 62);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "enumConstants");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "enumConstants"



	// $ANTLR start "enumConstant"
	// GosuProg.g:69:1: enumConstant : id optionalArguments ;
	public final void enumConstant() throws RecognitionException {
		int enumConstant_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "enumConstant");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(69, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return; }

			// GosuProg.g:69:14: ( id optionalArguments )
			dbg.enterAlt(1);

			// GosuProg.g:69:16: id optionalArguments
			{
			dbg.location(69,16);
			pushFollow(FOLLOW_id_in_enumConstant500);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(69,19);
			pushFollow(FOLLOW_optionalArguments_in_enumConstant502);
			optionalArguments();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 16, enumConstant_StartIndex); }

		}
		dbg.location(69, 36);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "enumConstant");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "enumConstant"



	// $ANTLR start "interfaceMembers"
	// GosuProg.g:71:1: interfaceMembers : ( modifiers ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? )* ;
	public final void interfaceMembers() throws RecognitionException {
		int interfaceMembers_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "interfaceMembers");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(71, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return; }

			// GosuProg.g:71:18: ( ( modifiers ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? )* )
			dbg.enterAlt(1);

			// GosuProg.g:71:20: ( modifiers ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? )*
			{
			dbg.location(71,20);
			// GosuProg.g:71:20: ( modifiers ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? )*
			try { dbg.enterSubRule(27);

			loop27:
			while (true) {
				int alt27=2;
				try { dbg.enterDecision(27, decisionCanBacktrack[27]);

				int LA27_0 = input.LA(1);
				if ( (LA27_0==73||LA27_0==81||LA27_0==90||LA27_0==92||LA27_0==96||(LA27_0 >= 99 && LA27_0 <= 100)||LA27_0==107||LA27_0==112||(LA27_0 >= 119 && LA27_0 <= 120)||LA27_0==128||(LA27_0 >= 130 && LA27_0 <= 133)||LA27_0==141||LA27_0==143||LA27_0==148||LA27_0==158) ) {
					alt27=1;
				}

				} finally {dbg.exitDecision(27);}

				switch (alt27) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:71:22: modifiers ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )?
					{
					dbg.location(71,22);
					pushFollow(FOLLOW_modifiers_in_interfaceMembers513);
					modifiers();
					state._fsp--;
					if (state.failed) return;dbg.location(72,25);
					// GosuProg.g:72:25: ( functionDefn | propertyDefn | fieldDefn | gClass | gInterfaceOrStructure | gEnum )
					int alt25=6;
					try { dbg.enterSubRule(25);
					try { dbg.enterDecision(25, decisionCanBacktrack[25]);

					switch ( input.LA(1) ) {
					case 112:
						{
						alt25=1;
						}
						break;
					case 131:
						{
						alt25=2;
						}
						break;
					case 158:
						{
						alt25=3;
						}
						break;
					case 90:
						{
						alt25=4;
						}
						break;
					case 119:
					case 143:
						{
						alt25=5;
						}
						break;
					case 100:
						{
						alt25=6;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 25, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}
					} finally {dbg.exitDecision(25);}

					switch (alt25) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:73:27: functionDefn
							{
							dbg.location(73,27);
							pushFollow(FOLLOW_functionDefn_in_interfaceMembers567);
							functionDefn();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:74:27: propertyDefn
							{
							dbg.location(74,27);
							pushFollow(FOLLOW_propertyDefn_in_interfaceMembers607);
							propertyDefn();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 3 :
							dbg.enterAlt(3);

							// GosuProg.g:75:27: fieldDefn
							{
							dbg.location(75,27);
							pushFollow(FOLLOW_fieldDefn_in_interfaceMembers647);
							fieldDefn();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 4 :
							dbg.enterAlt(4);

							// GosuProg.g:76:27: gClass
							{
							dbg.location(76,27);
							pushFollow(FOLLOW_gClass_in_interfaceMembers690);
							gClass();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 5 :
							dbg.enterAlt(5);

							// GosuProg.g:77:27: gInterfaceOrStructure
							{
							dbg.location(77,27);
							pushFollow(FOLLOW_gInterfaceOrStructure_in_interfaceMembers736);
							gInterfaceOrStructure();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 6 :
							dbg.enterAlt(6);

							// GosuProg.g:78:27: gEnum
							{
							dbg.location(78,27);
							pushFollow(FOLLOW_gEnum_in_interfaceMembers767);
							gEnum();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(25);}
					dbg.location(79,27);
					// GosuProg.g:79:27: ( ';' )?
					int alt26=2;
					try { dbg.enterSubRule(26);
					try { dbg.enterDecision(26, decisionCanBacktrack[26]);

					int LA26_0 = input.LA(1);
					if ( (LA26_0==57) ) {
						alt26=1;
					}
					} finally {dbg.exitDecision(26);}

					switch (alt26) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:79:27: ';'
							{
							dbg.location(79,27);
							match(input,57,FOLLOW_57_in_interfaceMembers795); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(26);}

					}
					break;

				default :
					break loop27;
				}
			}
			} finally {dbg.exitSubRule(27);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 17, interfaceMembers_StartIndex); }

		}
		dbg.location(80, 21);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "interfaceMembers");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "interfaceMembers"



	// $ANTLR start "classMembers"
	// GosuProg.g:83:1: classMembers : ( declaration )* ;
	public final void classMembers() throws RecognitionException {
		int classMembers_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "classMembers");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(83, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return; }

			// GosuProg.g:83:14: ( ( declaration )* )
			dbg.enterAlt(1);

			// GosuProg.g:83:16: ( declaration )*
			{
			dbg.location(83,16);
			// GosuProg.g:83:16: ( declaration )*
			try { dbg.enterSubRule(28);

			loop28:
			while (true) {
				int alt28=2;
				try { dbg.enterDecision(28, decisionCanBacktrack[28]);

				int LA28_0 = input.LA(1);
				if ( (LA28_0==73||LA28_0==81||LA28_0==90||LA28_0==92||LA28_0==96||(LA28_0 >= 99 && LA28_0 <= 100)||LA28_0==107||LA28_0==112||(LA28_0 >= 119 && LA28_0 <= 120)||LA28_0==128||(LA28_0 >= 130 && LA28_0 <= 133)||LA28_0==141||LA28_0==143||LA28_0==148||LA28_0==158) ) {
					alt28=1;
				}

				} finally {dbg.exitDecision(28);}

				switch (alt28) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:83:16: declaration
					{
					dbg.location(83,16);
					pushFollow(FOLLOW_declaration_in_classMembers827);
					declaration();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop28;
				}
			}
			} finally {dbg.exitSubRule(28);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 18, classMembers_StartIndex); }

		}
		dbg.location(83, 28);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "classMembers");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "classMembers"



	// $ANTLR start "declaration"
	// GosuProg.g:85:1: declaration : modifiers ( functionDefn ( functionBody )? | constructorDefn functionBody | propertyDefn ( functionBody )? | fieldDefn | delegateDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? ;
	public final void declaration() throws RecognitionException {
		int declaration_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "declaration");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(85, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return; }

			// GosuProg.g:85:13: ( modifiers ( functionDefn ( functionBody )? | constructorDefn functionBody | propertyDefn ( functionBody )? | fieldDefn | delegateDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )? )
			dbg.enterAlt(1);

			// GosuProg.g:86:18: modifiers ( functionDefn ( functionBody )? | constructorDefn functionBody | propertyDefn ( functionBody )? | fieldDefn | delegateDefn | gClass | gInterfaceOrStructure | gEnum ) ( ';' )?
			{
			dbg.location(86,18);
			pushFollow(FOLLOW_modifiers_in_declaration854);
			modifiers();
			state._fsp--;
			if (state.failed) return;dbg.location(87,18);
			// GosuProg.g:87:18: ( functionDefn ( functionBody )? | constructorDefn functionBody | propertyDefn ( functionBody )? | fieldDefn | delegateDefn | gClass | gInterfaceOrStructure | gEnum )
			int alt31=8;
			try { dbg.enterSubRule(31);
			try { dbg.enterDecision(31, decisionCanBacktrack[31]);

			switch ( input.LA(1) ) {
			case 112:
				{
				alt31=1;
				}
				break;
			case 92:
				{
				alt31=2;
				}
				break;
			case 131:
				{
				alt31=3;
				}
				break;
			case 158:
				{
				alt31=4;
				}
				break;
			case 96:
				{
				alt31=5;
				}
				break;
			case 90:
				{
				alt31=6;
				}
				break;
			case 119:
			case 143:
				{
				alt31=7;
				}
				break;
			case 100:
				{
				alt31=8;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 31, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(31);}

			switch (alt31) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:88:20: functionDefn ( functionBody )?
					{
					dbg.location(88,20);
					pushFollow(FOLLOW_functionDefn_in_declaration894);
					functionDefn();
					state._fsp--;
					if (state.failed) return;dbg.location(88,33);
					// GosuProg.g:88:33: ( functionBody )?
					int alt29=2;
					try { dbg.enterSubRule(29);
					try { dbg.enterDecision(29, decisionCanBacktrack[29]);

					int LA29_0 = input.LA(1);
					if ( (LA29_0==162) ) {
						alt29=1;
					}
					} finally {dbg.exitDecision(29);}

					switch (alt29) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:88:33: functionBody
							{
							dbg.location(88,33);
							pushFollow(FOLLOW_functionBody_in_declaration896);
							functionBody();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(29);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:89:20: constructorDefn functionBody
					{
					dbg.location(89,20);
					pushFollow(FOLLOW_constructorDefn_in_declaration924);
					constructorDefn();
					state._fsp--;
					if (state.failed) return;dbg.location(89,36);
					pushFollow(FOLLOW_functionBody_in_declaration926);
					functionBody();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:90:20: propertyDefn ( functionBody )?
					{
					dbg.location(90,20);
					pushFollow(FOLLOW_propertyDefn_in_declaration951);
					propertyDefn();
					state._fsp--;
					if (state.failed) return;dbg.location(90,33);
					// GosuProg.g:90:33: ( functionBody )?
					int alt30=2;
					try { dbg.enterSubRule(30);
					try { dbg.enterDecision(30, decisionCanBacktrack[30]);

					int LA30_0 = input.LA(1);
					if ( (LA30_0==162) ) {
						alt30=1;
					}
					} finally {dbg.exitDecision(30);}

					switch (alt30) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:90:33: functionBody
							{
							dbg.location(90,33);
							pushFollow(FOLLOW_functionBody_in_declaration953);
							functionBody();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(30);}

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:91:20: fieldDefn
					{
					dbg.location(91,20);
					pushFollow(FOLLOW_fieldDefn_in_declaration981);
					fieldDefn();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:92:20: delegateDefn
					{
					dbg.location(92,20);
					pushFollow(FOLLOW_delegateDefn_in_declaration1025);
					delegateDefn();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:93:20: gClass
					{
					dbg.location(93,20);
					pushFollow(FOLLOW_gClass_in_declaration1066);
					gClass();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// GosuProg.g:94:20: gInterfaceOrStructure
					{
					dbg.location(94,20);
					pushFollow(FOLLOW_gInterfaceOrStructure_in_declaration1113);
					gInterfaceOrStructure();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 8 :
					dbg.enterAlt(8);

					// GosuProg.g:95:20: gEnum
					{
					dbg.location(95,20);
					pushFollow(FOLLOW_gEnum_in_declaration1145);
					gEnum();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(31);}
			dbg.location(96,20);
			// GosuProg.g:96:20: ( ';' )?
			int alt32=2;
			try { dbg.enterSubRule(32);
			try { dbg.enterDecision(32, decisionCanBacktrack[32]);

			int LA32_0 = input.LA(1);
			if ( (LA32_0==57) ) {
				alt32=1;
			}
			} finally {dbg.exitDecision(32);}

			switch (alt32) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:96:20: ';'
					{
					dbg.location(96,20);
					match(input,57,FOLLOW_57_in_declaration1166); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(32);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 19, declaration_StartIndex); }

		}
		dbg.location(97, 12);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "declaration");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "declaration"



	// $ANTLR start "enhancementMembers"
	// GosuProg.g:99:1: enhancementMembers : ( modifiers ( functionDefn functionBody | propertyDefn functionBody ) ( ';' )? )* ;
	public final void enhancementMembers() throws RecognitionException {
		int enhancementMembers_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "enhancementMembers");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(99, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return; }

			// GosuProg.g:99:20: ( ( modifiers ( functionDefn functionBody | propertyDefn functionBody ) ( ';' )? )* )
			dbg.enterAlt(1);

			// GosuProg.g:99:22: ( modifiers ( functionDefn functionBody | propertyDefn functionBody ) ( ';' )? )*
			{
			dbg.location(99,22);
			// GosuProg.g:99:22: ( modifiers ( functionDefn functionBody | propertyDefn functionBody ) ( ';' )? )*
			try { dbg.enterSubRule(35);

			loop35:
			while (true) {
				int alt35=2;
				try { dbg.enterDecision(35, decisionCanBacktrack[35]);

				int LA35_0 = input.LA(1);
				if ( (LA35_0==73||LA35_0==81||LA35_0==90||LA35_0==92||LA35_0==96||(LA35_0 >= 99 && LA35_0 <= 100)||LA35_0==107||LA35_0==112||(LA35_0 >= 119 && LA35_0 <= 120)||LA35_0==128||(LA35_0 >= 130 && LA35_0 <= 133)||LA35_0==141||LA35_0==143||LA35_0==148||LA35_0==158) ) {
					alt35=1;
				}

				} finally {dbg.exitDecision(35);}

				switch (alt35) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:99:24: modifiers ( functionDefn functionBody | propertyDefn functionBody ) ( ';' )?
					{
					dbg.location(99,24);
					pushFollow(FOLLOW_modifiers_in_enhancementMembers1190);
					modifiers();
					state._fsp--;
					if (state.failed) return;dbg.location(100,25);
					// GosuProg.g:100:25: ( functionDefn functionBody | propertyDefn functionBody )
					int alt33=2;
					try { dbg.enterSubRule(33);
					try { dbg.enterDecision(33, decisionCanBacktrack[33]);

					int LA33_0 = input.LA(1);
					if ( (LA33_0==112) ) {
						alt33=1;
					}
					else if ( (LA33_0==131) ) {
						alt33=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 33, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}

					} finally {dbg.exitDecision(33);}

					switch (alt33) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:101:27: functionDefn functionBody
							{
							dbg.location(101,27);
							pushFollow(FOLLOW_functionDefn_in_enhancementMembers1244);
							functionDefn();
							state._fsp--;
							if (state.failed) return;dbg.location(101,40);
							pushFollow(FOLLOW_functionBody_in_enhancementMembers1246);
							functionBody();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:102:27: propertyDefn functionBody
							{
							dbg.location(102,27);
							pushFollow(FOLLOW_propertyDefn_in_enhancementMembers1277);
							propertyDefn();
							state._fsp--;
							if (state.failed) return;dbg.location(102,40);
							pushFollow(FOLLOW_functionBody_in_enhancementMembers1279);
							functionBody();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(33);}
					dbg.location(103,27);
					// GosuProg.g:103:27: ( ';' )?
					int alt34=2;
					try { dbg.enterSubRule(34);
					try { dbg.enterDecision(34, decisionCanBacktrack[34]);

					int LA34_0 = input.LA(1);
					if ( (LA34_0==57) ) {
						alt34=1;
					}
					} finally {dbg.exitDecision(34);}

					switch (alt34) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:103:27: ';'
							{
							dbg.location(103,27);
							match(input,57,FOLLOW_57_in_enhancementMembers1307); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(34);}

					}
					break;

				default :
					break loop35;
				}
			}
			} finally {dbg.exitSubRule(35);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 20, enhancementMembers_StartIndex); }

		}
		dbg.location(105, 19);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "enhancementMembers");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "enhancementMembers"



	// $ANTLR start "delegateDefn"
	// GosuProg.g:107:1: delegateDefn : 'delegate' id delegateStatement ;
	public final void delegateDefn() throws RecognitionException {
		int delegateDefn_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "delegateDefn");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(107, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return; }

			// GosuProg.g:107:14: ( 'delegate' id delegateStatement )
			dbg.enterAlt(1);

			// GosuProg.g:107:16: 'delegate' id delegateStatement
			{
			dbg.location(107,16);
			match(input,96,FOLLOW_96_in_delegateDefn1360); if (state.failed) return;dbg.location(107,27);
			pushFollow(FOLLOW_id_in_delegateDefn1362);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(107,30);
			pushFollow(FOLLOW_delegateStatement_in_delegateDefn1364);
			delegateStatement();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 21, delegateDefn_StartIndex); }

		}
		dbg.location(107, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "delegateDefn");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "delegateDefn"



	// $ANTLR start "delegateStatement"
	// GosuProg.g:109:1: delegateStatement : ( ':' typeLiteral )? 'represents' typeLiteral ( ',' typeLiteral )* ( '=' expression )? ;
	public final void delegateStatement() throws RecognitionException {
		int delegateStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "delegateStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(109, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return; }

			// GosuProg.g:109:19: ( ( ':' typeLiteral )? 'represents' typeLiteral ( ',' typeLiteral )* ( '=' expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:109:21: ( ':' typeLiteral )? 'represents' typeLiteral ( ',' typeLiteral )* ( '=' expression )?
			{
			dbg.location(109,21);
			// GosuProg.g:109:21: ( ':' typeLiteral )?
			int alt36=2;
			try { dbg.enterSubRule(36);
			try { dbg.enterDecision(36, decisionCanBacktrack[36]);

			int LA36_0 = input.LA(1);
			if ( (LA36_0==56) ) {
				alt36=1;
			}
			} finally {dbg.exitDecision(36);}

			switch (alt36) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:109:22: ':' typeLiteral
					{
					dbg.location(109,22);
					match(input,56,FOLLOW_56_in_delegateStatement1374); if (state.failed) return;dbg.location(109,26);
					pushFollow(FOLLOW_typeLiteral_in_delegateStatement1376);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(36);}
			dbg.location(109,40);
			match(input,135,FOLLOW_135_in_delegateStatement1380); if (state.failed) return;dbg.location(109,53);
			pushFollow(FOLLOW_typeLiteral_in_delegateStatement1382);
			typeLiteral();
			state._fsp--;
			if (state.failed) return;dbg.location(109,65);
			// GosuProg.g:109:65: ( ',' typeLiteral )*
			try { dbg.enterSubRule(37);

			loop37:
			while (true) {
				int alt37=2;
				try { dbg.enterDecision(37, decisionCanBacktrack[37]);

				int LA37_0 = input.LA(1);
				if ( (LA37_0==46) ) {
					alt37=1;
				}

				} finally {dbg.exitDecision(37);}

				switch (alt37) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:109:66: ',' typeLiteral
					{
					dbg.location(109,66);
					match(input,46,FOLLOW_46_in_delegateStatement1385); if (state.failed) return;dbg.location(109,70);
					pushFollow(FOLLOW_typeLiteral_in_delegateStatement1387);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop37;
				}
			}
			} finally {dbg.exitSubRule(37);}
			dbg.location(109,84);
			// GosuProg.g:109:84: ( '=' expression )?
			int alt38=2;
			try { dbg.enterSubRule(38);
			try { dbg.enterDecision(38, decisionCanBacktrack[38]);

			int LA38_0 = input.LA(1);
			if ( (LA38_0==60) ) {
				alt38=1;
			}
			} finally {dbg.exitDecision(38);}

			switch (alt38) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:109:85: '=' expression
					{
					dbg.location(109,85);
					match(input,60,FOLLOW_60_in_delegateStatement1392); if (state.failed) return;dbg.location(109,89);
					pushFollow(FOLLOW_expression_in_delegateStatement1394);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(38);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 22, delegateStatement_StartIndex); }

		}
		dbg.location(109, 101);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "delegateStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "delegateStatement"



	// $ANTLR start "optionalType"
	// GosuProg.g:111:1: optionalType : ( ':' typeLiteral | blockTypeLiteral )? ;
	public final void optionalType() throws RecognitionException {
		int optionalType_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "optionalType");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(111, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return; }

			// GosuProg.g:111:14: ( ( ':' typeLiteral | blockTypeLiteral )? )
			dbg.enterAlt(1);

			// GosuProg.g:111:16: ( ':' typeLiteral | blockTypeLiteral )?
			{
			dbg.location(111,16);
			// GosuProg.g:111:16: ( ':' typeLiteral | blockTypeLiteral )?
			int alt39=3;
			try { dbg.enterSubRule(39);
			try { dbg.enterDecision(39, decisionCanBacktrack[39]);

			int LA39_0 = input.LA(1);
			if ( (LA39_0==56) ) {
				alt39=1;
			}
			else if ( (LA39_0==38) ) {
				int LA39_2 = input.LA(2);
				if ( (synpred55_GosuProg()) ) {
					alt39=2;
				}
			}
			} finally {dbg.exitDecision(39);}

			switch (alt39) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:111:17: ':' typeLiteral
					{
					dbg.location(111,17);
					match(input,56,FOLLOW_56_in_optionalType1406); if (state.failed) return;dbg.location(111,21);
					pushFollow(FOLLOW_typeLiteral_in_optionalType1408);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:111:35: blockTypeLiteral
					{
					dbg.location(111,35);
					pushFollow(FOLLOW_blockTypeLiteral_in_optionalType1412);
					blockTypeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(39);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 23, optionalType_StartIndex); }

		}
		dbg.location(111, 53);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "optionalType");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "optionalType"



	// $ANTLR start "fieldDefn"
	// GosuProg.g:113:1: fieldDefn : 'var' id optionalType ( 'as' ( 'readonly' )? id )? ( '=' expression )? ;
	public final void fieldDefn() throws RecognitionException {
		int fieldDefn_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "fieldDefn");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(113, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return; }

			// GosuProg.g:113:11: ( 'var' id optionalType ( 'as' ( 'readonly' )? id )? ( '=' expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:113:13: 'var' id optionalType ( 'as' ( 'readonly' )? id )? ( '=' expression )?
			{
			dbg.location(113,13);
			match(input,158,FOLLOW_158_in_fieldDefn1423); if (state.failed) return;dbg.location(113,19);
			pushFollow(FOLLOW_id_in_fieldDefn1425);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(113,22);
			pushFollow(FOLLOW_optionalType_in_fieldDefn1427);
			optionalType();
			state._fsp--;
			if (state.failed) return;dbg.location(113,35);
			// GosuProg.g:113:35: ( 'as' ( 'readonly' )? id )?
			int alt41=2;
			try { dbg.enterSubRule(41);
			try { dbg.enterDecision(41, decisionCanBacktrack[41]);

			int LA41_0 = input.LA(1);
			if ( (LA41_0==84) ) {
				alt41=1;
			}
			} finally {dbg.exitDecision(41);}

			switch (alt41) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:113:36: 'as' ( 'readonly' )? id
					{
					dbg.location(113,36);
					match(input,84,FOLLOW_84_in_fieldDefn1430); if (state.failed) return;dbg.location(113,41);
					// GosuProg.g:113:41: ( 'readonly' )?
					int alt40=2;
					try { dbg.enterSubRule(40);
					try { dbg.enterDecision(40, decisionCanBacktrack[40]);

					int LA40_0 = input.LA(1);
					if ( (LA40_0==134) ) {
						switch ( input.LA(2) ) {
							case 130:
								{
								int LA40_3 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 120:
								{
								int LA40_4 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 132:
								{
								int LA40_5 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 133:
								{
								int LA40_6 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 141:
								{
								int LA40_7 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 81:
								{
								int LA40_8 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case 107:
								{
								int LA40_9 = input.LA(3);
								if ( (synpred56_GosuProg()) ) {
									alt40=1;
								}
								}
								break;
							case Ident:
							case 74:
							case 75:
							case 83:
							case 84:
							case 85:
							case 86:
							case 91:
							case 93:
							case 99:
							case 102:
							case 103:
							case 104:
							case 106:
							case 109:
							case 113:
							case 114:
							case 118:
							case 121:
							case 122:
							case 125:
							case 127:
							case 134:
							case 136:
							case 138:
							case 139:
							case 140:
							case 149:
							case 153:
							case 159:
							case 160:
								{
								alt40=1;
								}
								break;
						}
					}
					} finally {dbg.exitDecision(40);}

					switch (alt40) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:113:41: 'readonly'
							{
							dbg.location(113,41);
							match(input,134,FOLLOW_134_in_fieldDefn1432); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(40);}
					dbg.location(113,53);
					pushFollow(FOLLOW_id_in_fieldDefn1435);
					id();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(41);}
			dbg.location(113,58);
			// GosuProg.g:113:58: ( '=' expression )?
			int alt42=2;
			try { dbg.enterSubRule(42);
			try { dbg.enterDecision(42, decisionCanBacktrack[42]);

			int LA42_0 = input.LA(1);
			if ( (LA42_0==60) ) {
				alt42=1;
			}
			} finally {dbg.exitDecision(42);}

			switch (alt42) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:113:59: '=' expression
					{
					dbg.location(113,59);
					match(input,60,FOLLOW_60_in_fieldDefn1440); if (state.failed) return;dbg.location(113,63);
					pushFollow(FOLLOW_expression_in_fieldDefn1442);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(42);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 24, fieldDefn_StartIndex); }

		}
		dbg.location(113, 75);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "fieldDefn");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "fieldDefn"



	// $ANTLR start "propertyDefn"
	// GosuProg.g:115:1: propertyDefn : 'property' ( 'get' | 'set' ) id parameters ( ':' typeLiteral )? ;
	public final void propertyDefn() throws RecognitionException {
		int propertyDefn_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "propertyDefn");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(115, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return; }

			// GosuProg.g:115:14: ( 'property' ( 'get' | 'set' ) id parameters ( ':' typeLiteral )? )
			dbg.enterAlt(1);

			// GosuProg.g:115:16: 'property' ( 'get' | 'set' ) id parameters ( ':' typeLiteral )?
			{
			dbg.location(115,16);
			match(input,131,FOLLOW_131_in_propertyDefn1453); if (state.failed) return;dbg.location(115,27);
			if ( input.LA(1)==113||input.LA(1)==139 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}dbg.location(115,43);
			pushFollow(FOLLOW_id_in_propertyDefn1463);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(115,46);
			pushFollow(FOLLOW_parameters_in_propertyDefn1465);
			parameters();
			state._fsp--;
			if (state.failed) return;dbg.location(115,57);
			// GosuProg.g:115:57: ( ':' typeLiteral )?
			int alt43=2;
			try { dbg.enterSubRule(43);
			try { dbg.enterDecision(43, decisionCanBacktrack[43]);

			int LA43_0 = input.LA(1);
			if ( (LA43_0==56) ) {
				alt43=1;
			}
			} finally {dbg.exitDecision(43);}

			switch (alt43) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:115:58: ':' typeLiteral
					{
					dbg.location(115,58);
					match(input,56,FOLLOW_56_in_propertyDefn1468); if (state.failed) return;dbg.location(115,62);
					pushFollow(FOLLOW_typeLiteral_in_propertyDefn1470);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(43);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 25, propertyDefn_StartIndex); }

		}
		dbg.location(115, 75);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "propertyDefn");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "propertyDefn"



	// $ANTLR start "dotPathWord"
	// GosuProg.g:117:1: dotPathWord : idAll ( '.' idAll )* ;
	public final void dotPathWord() throws RecognitionException {
		int dotPathWord_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "dotPathWord");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(117, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return; }

			// GosuProg.g:117:13: ( idAll ( '.' idAll )* )
			dbg.enterAlt(1);

			// GosuProg.g:117:15: idAll ( '.' idAll )*
			{
			dbg.location(117,15);
			pushFollow(FOLLOW_idAll_in_dotPathWord1481);
			idAll();
			state._fsp--;
			if (state.failed) return;dbg.location(117,21);
			// GosuProg.g:117:21: ( '.' idAll )*
			try { dbg.enterSubRule(44);

			loop44:
			while (true) {
				int alt44=2;
				try { dbg.enterDecision(44, decisionCanBacktrack[44]);

				int LA44_0 = input.LA(1);
				if ( (LA44_0==51) ) {
					int LA44_2 = input.LA(2);
					if ( (LA44_2==Ident||(LA44_2 >= 74 && LA44_2 <= 75)||(LA44_2 >= 81 && LA44_2 <= 143)||LA44_2==145||(LA44_2 >= 147 && LA44_2 <= 161)) ) {
						alt44=1;
					}

				}

				} finally {dbg.exitDecision(44);}

				switch (alt44) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:117:22: '.' idAll
					{
					dbg.location(117,22);
					match(input,51,FOLLOW_51_in_dotPathWord1484); if (state.failed) return;dbg.location(117,26);
					pushFollow(FOLLOW_idAll_in_dotPathWord1486);
					idAll();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop44;
				}
			}
			} finally {dbg.exitSubRule(44);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 26, dotPathWord_StartIndex); }

		}
		dbg.location(117, 32);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "dotPathWord");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "dotPathWord"



	// $ANTLR start "namespaceStatement"
	// GosuProg.g:119:1: namespaceStatement : dotPathWord ( ';' )* ;
	public final void namespaceStatement() throws RecognitionException {
		int namespaceStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "namespaceStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(119, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return; }

			// GosuProg.g:119:20: ( dotPathWord ( ';' )* )
			dbg.enterAlt(1);

			// GosuProg.g:119:23: dotPathWord ( ';' )*
			{
			dbg.location(119,23);
			pushFollow(FOLLOW_dotPathWord_in_namespaceStatement1497);
			dotPathWord();
			state._fsp--;
			if (state.failed) return;dbg.location(119,35);
			// GosuProg.g:119:35: ( ';' )*
			try { dbg.enterSubRule(45);

			loop45:
			while (true) {
				int alt45=2;
				try { dbg.enterDecision(45, decisionCanBacktrack[45]);

				int LA45_0 = input.LA(1);
				if ( (LA45_0==57) ) {
					int LA45_2 = input.LA(2);
					if ( (synpred62_GosuProg()) ) {
						alt45=1;
					}

				}

				} finally {dbg.exitDecision(45);}

				switch (alt45) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:119:36: ';'
					{
					dbg.location(119,36);
					match(input,57,FOLLOW_57_in_namespaceStatement1500); if (state.failed) return;
					}
					break;

				default :
					break loop45;
				}
			}
			} finally {dbg.exitSubRule(45);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 27, namespaceStatement_StartIndex); }

		}
		dbg.location(119, 41);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "namespaceStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "namespaceStatement"



	// $ANTLR start "usesStatementList"
	// GosuProg.g:121:1: usesStatementList : ( 'uses' usesStatement )+ ;
	public final void usesStatementList() throws RecognitionException {
		int usesStatementList_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "usesStatementList");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(121, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return; }

			// GosuProg.g:121:19: ( ( 'uses' usesStatement )+ )
			dbg.enterAlt(1);

			// GosuProg.g:121:21: ( 'uses' usesStatement )+
			{
			dbg.location(121,21);
			// GosuProg.g:121:21: ( 'uses' usesStatement )+
			int cnt46=0;
			try { dbg.enterSubRule(46);

			loop46:
			while (true) {
				int alt46=2;
				try { dbg.enterDecision(46, decisionCanBacktrack[46]);

				int LA46_0 = input.LA(1);
				if ( (LA46_0==156) ) {
					alt46=1;
				}

				} finally {dbg.exitDecision(46);}

				switch (alt46) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:121:22: 'uses' usesStatement
					{
					dbg.location(121,22);
					match(input,156,FOLLOW_156_in_usesStatementList1512); if (state.failed) return;dbg.location(121,29);
					pushFollow(FOLLOW_usesStatement_in_usesStatementList1514);
					usesStatement();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					if ( cnt46 >= 1 ) break loop46;
					if (state.backtracking>0) {state.failed=true; return;}
					EarlyExitException eee = new EarlyExitException(46, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt46++;
			}
			} finally {dbg.exitSubRule(46);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 28, usesStatementList_StartIndex); }

		}
		dbg.location(121, 44);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "usesStatementList");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "usesStatementList"



	// $ANTLR start "usesStatement"
	// GosuProg.g:123:1: usesStatement : dotPathWord ( '.' '*' )? ( ';' )* ;
	public final void usesStatement() throws RecognitionException {
		int usesStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "usesStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(123, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return; }

			// GosuProg.g:123:15: ( dotPathWord ( '.' '*' )? ( ';' )* )
			dbg.enterAlt(1);

			// GosuProg.g:123:17: dotPathWord ( '.' '*' )? ( ';' )*
			{
			dbg.location(123,17);
			pushFollow(FOLLOW_dotPathWord_in_usesStatement1525);
			dotPathWord();
			state._fsp--;
			if (state.failed) return;dbg.location(123,29);
			// GosuProg.g:123:29: ( '.' '*' )?
			int alt47=2;
			try { dbg.enterSubRule(47);
			try { dbg.enterDecision(47, decisionCanBacktrack[47]);

			int LA47_0 = input.LA(1);
			if ( (LA47_0==51) ) {
				alt47=1;
			}
			} finally {dbg.exitDecision(47);}

			switch (alt47) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:123:30: '.' '*'
					{
					dbg.location(123,30);
					match(input,51,FOLLOW_51_in_usesStatement1528); if (state.failed) return;dbg.location(123,34);
					match(input,40,FOLLOW_40_in_usesStatement1530); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(47);}
			dbg.location(123,40);
			// GosuProg.g:123:40: ( ';' )*
			try { dbg.enterSubRule(48);

			loop48:
			while (true) {
				int alt48=2;
				try { dbg.enterDecision(48, decisionCanBacktrack[48]);

				int LA48_0 = input.LA(1);
				if ( (LA48_0==57) ) {
					int LA48_2 = input.LA(2);
					if ( (synpred65_GosuProg()) ) {
						alt48=1;
					}

				}

				} finally {dbg.exitDecision(48);}

				switch (alt48) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:123:41: ';'
					{
					dbg.location(123,41);
					match(input,57,FOLLOW_57_in_usesStatement1535); if (state.failed) return;
					}
					break;

				default :
					break loop48;
				}
			}
			} finally {dbg.exitSubRule(48);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 29, usesStatement_StartIndex); }

		}
		dbg.location(123, 46);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "usesStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "usesStatement"



	// $ANTLR start "typeVariableDefs"
	// GosuProg.g:125:1: typeVariableDefs : ( '<' typeVariableDefinition ( ',' typeVariableDefinition )* '>' )? ;
	public final void typeVariableDefs() throws RecognitionException {
		int typeVariableDefs_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeVariableDefs");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(125, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return; }

			// GosuProg.g:125:18: ( ( '<' typeVariableDefinition ( ',' typeVariableDefinition )* '>' )? )
			dbg.enterAlt(1);

			// GosuProg.g:125:20: ( '<' typeVariableDefinition ( ',' typeVariableDefinition )* '>' )?
			{
			dbg.location(125,20);
			// GosuProg.g:125:20: ( '<' typeVariableDefinition ( ',' typeVariableDefinition )* '>' )?
			int alt50=2;
			try { dbg.enterSubRule(50);
			try { dbg.enterDecision(50, decisionCanBacktrack[50]);

			int LA50_0 = input.LA(1);
			if ( (LA50_0==58) ) {
				alt50=1;
			}
			} finally {dbg.exitDecision(50);}

			switch (alt50) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:125:21: '<' typeVariableDefinition ( ',' typeVariableDefinition )* '>'
					{
					dbg.location(125,21);
					match(input,58,FOLLOW_58_in_typeVariableDefs1547); if (state.failed) return;dbg.location(125,25);
					pushFollow(FOLLOW_typeVariableDefinition_in_typeVariableDefs1549);
					typeVariableDefinition();
					state._fsp--;
					if (state.failed) return;dbg.location(125,48);
					// GosuProg.g:125:48: ( ',' typeVariableDefinition )*
					try { dbg.enterSubRule(49);

					loop49:
					while (true) {
						int alt49=2;
						try { dbg.enterDecision(49, decisionCanBacktrack[49]);

						int LA49_0 = input.LA(1);
						if ( (LA49_0==46) ) {
							alt49=1;
						}

						} finally {dbg.exitDecision(49);}

						switch (alt49) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:125:49: ',' typeVariableDefinition
							{
							dbg.location(125,49);
							match(input,46,FOLLOW_46_in_typeVariableDefs1552); if (state.failed) return;dbg.location(125,53);
							pushFollow(FOLLOW_typeVariableDefinition_in_typeVariableDefs1554);
							typeVariableDefinition();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop49;
						}
					}
					} finally {dbg.exitSubRule(49);}
					dbg.location(125,78);
					match(input,63,FOLLOW_63_in_typeVariableDefs1558); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(50);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 30, typeVariableDefs_StartIndex); }

		}
		dbg.location(125, 82);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeVariableDefs");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeVariableDefs"



	// $ANTLR start "typeVariableDefinition"
	// GosuProg.g:127:1: typeVariableDefinition : id ( 'extends' typeLiteralList )? ;
	public final void typeVariableDefinition() throws RecognitionException {
		int typeVariableDefinition_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeVariableDefinition");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(127, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return; }

			// GosuProg.g:127:24: ( id ( 'extends' typeLiteralList )? )
			dbg.enterAlt(1);

			// GosuProg.g:127:26: id ( 'extends' typeLiteralList )?
			{
			dbg.location(127,26);
			pushFollow(FOLLOW_id_in_typeVariableDefinition1568);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(127,29);
			// GosuProg.g:127:29: ( 'extends' typeLiteralList )?
			int alt51=2;
			try { dbg.enterSubRule(51);
			try { dbg.enterDecision(51, decisionCanBacktrack[51]);

			int LA51_0 = input.LA(1);
			if ( (LA51_0==105) ) {
				alt51=1;
			}
			} finally {dbg.exitDecision(51);}

			switch (alt51) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:127:30: 'extends' typeLiteralList
					{
					dbg.location(127,30);
					match(input,105,FOLLOW_105_in_typeVariableDefinition1571); if (state.failed) return;dbg.location(127,40);
					pushFollow(FOLLOW_typeLiteralList_in_typeVariableDefinition1573);
					typeLiteralList();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(51);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 31, typeVariableDefinition_StartIndex); }

		}
		dbg.location(127, 57);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeVariableDefinition");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeVariableDefinition"



	// $ANTLR start "functionBody"
	// GosuProg.g:129:1: functionBody : statementBlock ;
	public final void functionBody() throws RecognitionException {
		int functionBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "functionBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(129, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return; }

			// GosuProg.g:129:14: ( statementBlock )
			dbg.enterAlt(1);

			// GosuProg.g:129:16: statementBlock
			{
			dbg.location(129,16);
			pushFollow(FOLLOW_statementBlock_in_functionBody1584);
			statementBlock();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 32, functionBody_StartIndex); }

		}
		dbg.location(129, 30);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "functionBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "functionBody"



	// $ANTLR start "parameters"
	// GosuProg.g:131:1: parameters : '(' ( parameterDeclarationList )? ')' ;
	public final void parameters() throws RecognitionException {
		int parameters_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "parameters");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(131, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return; }

			// GosuProg.g:131:12: ( '(' ( parameterDeclarationList )? ')' )
			dbg.enterAlt(1);

			// GosuProg.g:131:14: '(' ( parameterDeclarationList )? ')'
			{
			dbg.location(131,14);
			match(input,38,FOLLOW_38_in_parameters1593); if (state.failed) return;dbg.location(131,18);
			// GosuProg.g:131:18: ( parameterDeclarationList )?
			int alt52=2;
			try { dbg.enterSubRule(52);
			try { dbg.enterDecision(52, decisionCanBacktrack[52]);

			int LA52_0 = input.LA(1);
			if ( (LA52_0==Ident||(LA52_0 >= 73 && LA52_0 <= 75)||LA52_0==81||(LA52_0 >= 83 && LA52_0 <= 86)||LA52_0==91||LA52_0==93||LA52_0==99||(LA52_0 >= 102 && LA52_0 <= 104)||(LA52_0 >= 106 && LA52_0 <= 107)||LA52_0==109||(LA52_0 >= 113 && LA52_0 <= 114)||LA52_0==118||(LA52_0 >= 120 && LA52_0 <= 122)||LA52_0==125||LA52_0==127||LA52_0==130||(LA52_0 >= 132 && LA52_0 <= 134)||LA52_0==136||(LA52_0 >= 138 && LA52_0 <= 141)||LA52_0==149||LA52_0==153||(LA52_0 >= 159 && LA52_0 <= 160)) ) {
				alt52=1;
			}
			} finally {dbg.exitDecision(52);}

			switch (alt52) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:131:18: parameterDeclarationList
					{
					dbg.location(131,18);
					pushFollow(FOLLOW_parameterDeclarationList_in_parameters1595);
					parameterDeclarationList();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(52);}
			dbg.location(131,44);
			match(input,39,FOLLOW_39_in_parameters1598); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 33, parameters_StartIndex); }

		}
		dbg.location(131, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "parameters");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "parameters"



	// $ANTLR start "functionDefn"
	// GosuProg.g:133:1: functionDefn : 'function' id typeVariableDefs parameters ( ':' typeLiteral )? ;
	public final void functionDefn() throws RecognitionException {
		int functionDefn_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "functionDefn");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(133, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return; }

			// GosuProg.g:133:14: ( 'function' id typeVariableDefs parameters ( ':' typeLiteral )? )
			dbg.enterAlt(1);

			// GosuProg.g:133:16: 'function' id typeVariableDefs parameters ( ':' typeLiteral )?
			{
			dbg.location(133,16);
			match(input,112,FOLLOW_112_in_functionDefn1607); if (state.failed) return;dbg.location(133,27);
			pushFollow(FOLLOW_id_in_functionDefn1609);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(133,30);
			pushFollow(FOLLOW_typeVariableDefs_in_functionDefn1611);
			typeVariableDefs();
			state._fsp--;
			if (state.failed) return;dbg.location(133,48);
			pushFollow(FOLLOW_parameters_in_functionDefn1614);
			parameters();
			state._fsp--;
			if (state.failed) return;dbg.location(133,59);
			// GosuProg.g:133:59: ( ':' typeLiteral )?
			int alt53=2;
			try { dbg.enterSubRule(53);
			try { dbg.enterDecision(53, decisionCanBacktrack[53]);

			int LA53_0 = input.LA(1);
			if ( (LA53_0==56) ) {
				alt53=1;
			}
			} finally {dbg.exitDecision(53);}

			switch (alt53) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:133:60: ':' typeLiteral
					{
					dbg.location(133,60);
					match(input,56,FOLLOW_56_in_functionDefn1617); if (state.failed) return;dbg.location(133,64);
					pushFollow(FOLLOW_typeLiteral_in_functionDefn1619);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(53);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 34, functionDefn_StartIndex); }

		}
		dbg.location(133, 77);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "functionDefn");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "functionDefn"



	// $ANTLR start "constructorDefn"
	// GosuProg.g:135:1: constructorDefn : 'construct' parameters ( ':' typeLiteral )? ;
	public final void constructorDefn() throws RecognitionException {
		int constructorDefn_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "constructorDefn");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(135, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return; }

			// GosuProg.g:135:17: ( 'construct' parameters ( ':' typeLiteral )? )
			dbg.enterAlt(1);

			// GosuProg.g:135:19: 'construct' parameters ( ':' typeLiteral )?
			{
			dbg.location(135,19);
			match(input,92,FOLLOW_92_in_constructorDefn1630); if (state.failed) return;dbg.location(135,31);
			pushFollow(FOLLOW_parameters_in_constructorDefn1632);
			parameters();
			state._fsp--;
			if (state.failed) return;dbg.location(135,42);
			// GosuProg.g:135:42: ( ':' typeLiteral )?
			int alt54=2;
			try { dbg.enterSubRule(54);
			try { dbg.enterDecision(54, decisionCanBacktrack[54]);

			int LA54_0 = input.LA(1);
			if ( (LA54_0==56) ) {
				alt54=1;
			}
			} finally {dbg.exitDecision(54);}

			switch (alt54) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:135:43: ':' typeLiteral
					{
					dbg.location(135,43);
					match(input,56,FOLLOW_56_in_constructorDefn1635); if (state.failed) return;dbg.location(135,47);
					pushFollow(FOLLOW_typeLiteral_in_constructorDefn1637);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(54);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 35, constructorDefn_StartIndex); }

		}
		dbg.location(135, 60);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "constructorDefn");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "constructorDefn"



	// $ANTLR start "modifiers"
	// GosuProg.g:137:1: modifiers : ( annotation | 'private' | 'internal' | 'protected' | 'public' | 'static' | 'abstract' | 'override' | 'final' | 'transient' )* ;
	public final void modifiers() throws RecognitionException {
		int modifiers_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "modifiers");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(137, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return; }

			// GosuProg.g:137:11: ( ( annotation | 'private' | 'internal' | 'protected' | 'public' | 'static' | 'abstract' | 'override' | 'final' | 'transient' )* )
			dbg.enterAlt(1);

			// GosuProg.g:137:14: ( annotation | 'private' | 'internal' | 'protected' | 'public' | 'static' | 'abstract' | 'override' | 'final' | 'transient' )*
			{
			dbg.location(137,14);
			// GosuProg.g:137:14: ( annotation | 'private' | 'internal' | 'protected' | 'public' | 'static' | 'abstract' | 'override' | 'final' | 'transient' )*
			try { dbg.enterSubRule(55);

			loop55:
			while (true) {
				int alt55=11;
				try { dbg.enterDecision(55, decisionCanBacktrack[55]);

				switch ( input.LA(1) ) {
				case 73:
					{
					alt55=1;
					}
					break;
				case 130:
					{
					alt55=2;
					}
					break;
				case 120:
					{
					alt55=3;
					}
					break;
				case 132:
					{
					alt55=4;
					}
					break;
				case 133:
					{
					alt55=5;
					}
					break;
				case 141:
					{
					alt55=6;
					}
					break;
				case 81:
					{
					alt55=7;
					}
					break;
				case 128:
					{
					alt55=8;
					}
					break;
				case 107:
					{
					alt55=9;
					}
					break;
				case 148:
					{
					alt55=10;
					}
					break;
				}
				} finally {dbg.exitDecision(55);}

				switch (alt55) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:137:15: annotation
					{
					dbg.location(137,15);
					pushFollow(FOLLOW_annotation_in_modifiers1650);
					annotation();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:138:15: 'private'
					{
					dbg.location(138,15);
					match(input,130,FOLLOW_130_in_modifiers1669); if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:139:15: 'internal'
					{
					dbg.location(139,15);
					match(input,120,FOLLOW_120_in_modifiers1689); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:140:15: 'protected'
					{
					dbg.location(140,15);
					match(input,132,FOLLOW_132_in_modifiers1708); if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:141:15: 'public'
					{
					dbg.location(141,15);
					match(input,133,FOLLOW_133_in_modifiers1726); if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:142:15: 'static'
					{
					dbg.location(142,15);
					match(input,141,FOLLOW_141_in_modifiers1747); if (state.failed) return;
					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// GosuProg.g:143:15: 'abstract'
					{
					dbg.location(143,15);
					match(input,81,FOLLOW_81_in_modifiers1768); if (state.failed) return;
					}
					break;
				case 8 :
					dbg.enterAlt(8);

					// GosuProg.g:144:15: 'override'
					{
					dbg.location(144,15);
					match(input,128,FOLLOW_128_in_modifiers1787); if (state.failed) return;
					}
					break;
				case 9 :
					dbg.enterAlt(9);

					// GosuProg.g:146:15: 'final'
					{
					dbg.location(146,15);
					match(input,107,FOLLOW_107_in_modifiers1821); if (state.failed) return;
					}
					break;
				case 10 :
					dbg.enterAlt(10);

					// GosuProg.g:147:15: 'transient'
					{
					dbg.location(147,15);
					match(input,148,FOLLOW_148_in_modifiers1843); if (state.failed) return;
					}
					break;

				default :
					break loop55;
				}
			}
			} finally {dbg.exitSubRule(55);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 36, modifiers_StartIndex); }

		}
		dbg.location(148, 10);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "modifiers");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "modifiers"



	// $ANTLR start "statement"
	// GosuProg.g:150:1: statement : ( ( ifStatement | tryCatchFinallyStatement | throwStatement | 'continue' | 'break' | returnStatement | forEachStatement | whileStatement | doWhileStatement | switchStatement | usingStatement | assertStatement | 'final' localVarStatement | localVarStatement | evalExpr | assignmentOrMethodCall | statementBlock ) ( ';' )? | ';' );
	public final void statement() throws RecognitionException {
		int statement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "statement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(150, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return; }

			// GosuProg.g:150:11: ( ( ifStatement | tryCatchFinallyStatement | throwStatement | 'continue' | 'break' | returnStatement | forEachStatement | whileStatement | doWhileStatement | switchStatement | usingStatement | assertStatement | 'final' localVarStatement | localVarStatement | evalExpr | assignmentOrMethodCall | statementBlock ) ( ';' )? | ';' )
			int alt58=2;
			try { dbg.enterDecision(58, decisionCanBacktrack[58]);

			int LA58_0 = input.LA(1);
			if ( (LA58_0==Ident||LA58_0==StringLiteral||LA58_0==38||(LA58_0 >= 74 && LA58_0 <= 75)||LA58_0==81||(LA58_0 >= 83 && LA58_0 <= 87)||LA58_0==91||(LA58_0 >= 93 && LA58_0 <= 94)||LA58_0==97||LA58_0==99||(LA58_0 >= 101 && LA58_0 <= 104)||(LA58_0 >= 106 && LA58_0 <= 107)||(LA58_0 >= 109 && LA58_0 <= 111)||(LA58_0 >= 113 && LA58_0 <= 115)||LA58_0==118||(LA58_0 >= 120 && LA58_0 <= 123)||LA58_0==125||LA58_0==127||LA58_0==130||(LA58_0 >= 132 && LA58_0 <= 134)||(LA58_0 >= 136 && LA58_0 <= 141)||(LA58_0 >= 144 && LA58_0 <= 147)||(LA58_0 >= 149 && LA58_0 <= 150)||LA58_0==153||(LA58_0 >= 157 && LA58_0 <= 162)) ) {
				alt58=1;
			}
			else if ( (LA58_0==57) ) {
				alt58=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 58, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(58);}

			switch (alt58) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:150:13: ( ifStatement | tryCatchFinallyStatement | throwStatement | 'continue' | 'break' | returnStatement | forEachStatement | whileStatement | doWhileStatement | switchStatement | usingStatement | assertStatement | 'final' localVarStatement | localVarStatement | evalExpr | assignmentOrMethodCall | statementBlock ) ( ';' )?
					{
					dbg.location(150,13);
					// GosuProg.g:150:13: ( ifStatement | tryCatchFinallyStatement | throwStatement | 'continue' | 'break' | returnStatement | forEachStatement | whileStatement | doWhileStatement | switchStatement | usingStatement | assertStatement | 'final' localVarStatement | localVarStatement | evalExpr | assignmentOrMethodCall | statementBlock )
					int alt56=17;
					try { dbg.enterSubRule(56);
					try { dbg.enterDecision(56, decisionCanBacktrack[56]);

					switch ( input.LA(1) ) {
					case 115:
						{
						alt56=1;
						}
						break;
					case 150:
						{
						alt56=2;
						}
						break;
					case 147:
						{
						alt56=3;
						}
						break;
					case 94:
						{
						alt56=4;
						}
						break;
					case 87:
						{
						alt56=5;
						}
						break;
					case 137:
						{
						alt56=6;
						}
						break;
					case 110:
					case 111:
						{
						alt56=7;
						}
						break;
					case 161:
						{
						alt56=8;
						}
						break;
					case 97:
						{
						alt56=9;
						}
						break;
					case 145:
						{
						alt56=10;
						}
						break;
					case 157:
						{
						alt56=11;
						}
						break;
					case 85:
						{
						int LA56_12 = input.LA(2);
						if ( (synpred93_GosuProg()) ) {
							alt56=12;
						}
						else if ( (synpred97_GosuProg()) ) {
							alt56=16;
						}

						else {
							if (state.backtracking>0) {state.failed=true; return;}
							int nvaeMark = input.mark();
							try {
								input.consume();
								NoViableAltException nvae =
									new NoViableAltException("", 56, 12, input);
								dbg.recognitionException(nvae);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					case 107:
						{
						int LA56_13 = input.LA(2);
						if ( (synpred94_GosuProg()) ) {
							alt56=13;
						}
						else if ( (synpred97_GosuProg()) ) {
							alt56=16;
						}

						else {
							if (state.backtracking>0) {state.failed=true; return;}
							int nvaeMark = input.mark();
							try {
								input.consume();
								NoViableAltException nvae =
									new NoViableAltException("", 56, 13, input);
								dbg.recognitionException(nvae);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					case 158:
						{
						alt56=14;
						}
						break;
					case 101:
						{
						alt56=15;
						}
						break;
					case Ident:
					case StringLiteral:
					case 38:
					case 74:
					case 75:
					case 81:
					case 83:
					case 84:
					case 86:
					case 91:
					case 93:
					case 99:
					case 102:
					case 103:
					case 104:
					case 106:
					case 109:
					case 113:
					case 114:
					case 118:
					case 120:
					case 121:
					case 122:
					case 123:
					case 125:
					case 127:
					case 130:
					case 132:
					case 133:
					case 134:
					case 136:
					case 138:
					case 139:
					case 140:
					case 141:
					case 144:
					case 146:
					case 149:
					case 153:
					case 159:
					case 160:
						{
						alt56=16;
						}
						break;
					case 162:
						{
						alt56=17;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 56, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}
					} finally {dbg.exitDecision(56);}

					switch (alt56) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:151:15: ifStatement
							{
							dbg.location(151,15);
							pushFollow(FOLLOW_ifStatement_in_statement1880);
							ifStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:152:15: tryCatchFinallyStatement
							{
							dbg.location(152,15);
							pushFollow(FOLLOW_tryCatchFinallyStatement_in_statement1916);
							tryCatchFinallyStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 3 :
							dbg.enterAlt(3);

							// GosuProg.g:153:15: throwStatement
							{
							dbg.location(153,15);
							pushFollow(FOLLOW_throwStatement_in_statement1939);
							throwStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 4 :
							dbg.enterAlt(4);

							// GosuProg.g:154:15: 'continue'
							{
							dbg.location(154,15);
							match(input,94,FOLLOW_94_in_statement1972); if (state.failed) return;
							}
							break;
						case 5 :
							dbg.enterAlt(5);

							// GosuProg.g:155:15: 'break'
							{
							dbg.location(155,15);
							match(input,87,FOLLOW_87_in_statement2009); if (state.failed) return;
							}
							break;
						case 6 :
							dbg.enterAlt(6);

							// GosuProg.g:156:15: returnStatement
							{
							dbg.location(156,15);
							pushFollow(FOLLOW_returnStatement_in_statement2049);
							returnStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 7 :
							dbg.enterAlt(7);

							// GosuProg.g:157:15: forEachStatement
							{
							dbg.location(157,15);
							pushFollow(FOLLOW_forEachStatement_in_statement2081);
							forEachStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 8 :
							dbg.enterAlt(8);

							// GosuProg.g:158:15: whileStatement
							{
							dbg.location(158,15);
							pushFollow(FOLLOW_whileStatement_in_statement2112);
							whileStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 9 :
							dbg.enterAlt(9);

							// GosuProg.g:159:15: doWhileStatement
							{
							dbg.location(159,15);
							pushFollow(FOLLOW_doWhileStatement_in_statement2145);
							doWhileStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 10 :
							dbg.enterAlt(10);

							// GosuProg.g:160:15: switchStatement
							{
							dbg.location(160,15);
							pushFollow(FOLLOW_switchStatement_in_statement2176);
							switchStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 11 :
							dbg.enterAlt(11);

							// GosuProg.g:161:15: usingStatement
							{
							dbg.location(161,15);
							pushFollow(FOLLOW_usingStatement_in_statement2208);
							usingStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 12 :
							dbg.enterAlt(12);

							// GosuProg.g:162:15: assertStatement
							{
							dbg.location(162,15);
							pushFollow(FOLLOW_assertStatement_in_statement2241);
							assertStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 13 :
							dbg.enterAlt(13);

							// GosuProg.g:163:15: 'final' localVarStatement
							{
							dbg.location(163,15);
							match(input,107,FOLLOW_107_in_statement2273); if (state.failed) return;dbg.location(163,23);
							pushFollow(FOLLOW_localVarStatement_in_statement2275);
							localVarStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 14 :
							dbg.enterAlt(14);

							// GosuProg.g:164:15: localVarStatement
							{
							dbg.location(164,15);
							pushFollow(FOLLOW_localVarStatement_in_statement2297);
							localVarStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 15 :
							dbg.enterAlt(15);

							// GosuProg.g:165:15: evalExpr
							{
							dbg.location(165,15);
							pushFollow(FOLLOW_evalExpr_in_statement2327);
							evalExpr();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 16 :
							dbg.enterAlt(16);

							// GosuProg.g:166:15: assignmentOrMethodCall
							{
							dbg.location(166,15);
							pushFollow(FOLLOW_assignmentOrMethodCall_in_statement2366);
							assignmentOrMethodCall();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 17 :
							dbg.enterAlt(17);

							// GosuProg.g:167:15: statementBlock
							{
							dbg.location(167,15);
							pushFollow(FOLLOW_statementBlock_in_statement2391);
							statementBlock();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(56);}
					dbg.location(167,31);
					// GosuProg.g:167:31: ( ';' )?
					int alt57=2;
					try { dbg.enterSubRule(57);
					try { dbg.enterDecision(57, decisionCanBacktrack[57]);

					int LA57_0 = input.LA(1);
					if ( (LA57_0==57) ) {
						int LA57_1 = input.LA(2);
						if ( (synpred98_GosuProg()) ) {
							alt57=1;
						}
					}
					} finally {dbg.exitDecision(57);}

					switch (alt57) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:167:31: ';'
							{
							dbg.location(167,31);
							match(input,57,FOLLOW_57_in_statement2394); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(57);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:168:15: ';'
					{
					dbg.location(168,15);
					match(input,57,FOLLOW_57_in_statement2422); if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 37, statement_StartIndex); }

		}
		dbg.location(169, 10);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "statement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "statement"



	// $ANTLR start "ifStatement"
	// GosuProg.g:171:1: ifStatement : 'if' '(' expression ')' statement ( ';' )? ( 'else' statement )? ;
	public final void ifStatement() throws RecognitionException {
		int ifStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "ifStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(171, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return; }

			// GosuProg.g:171:13: ( 'if' '(' expression ')' statement ( ';' )? ( 'else' statement )? )
			dbg.enterAlt(1);

			// GosuProg.g:171:15: 'if' '(' expression ')' statement ( ';' )? ( 'else' statement )?
			{
			dbg.location(171,15);
			match(input,115,FOLLOW_115_in_ifStatement2441); if (state.failed) return;dbg.location(171,20);
			match(input,38,FOLLOW_38_in_ifStatement2443); if (state.failed) return;dbg.location(171,24);
			pushFollow(FOLLOW_expression_in_ifStatement2445);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(171,35);
			match(input,39,FOLLOW_39_in_ifStatement2447); if (state.failed) return;dbg.location(171,39);
			pushFollow(FOLLOW_statement_in_ifStatement2449);
			statement();
			state._fsp--;
			if (state.failed) return;dbg.location(171,50);
			// GosuProg.g:171:50: ( ';' )?
			int alt59=2;
			try { dbg.enterSubRule(59);
			try { dbg.enterDecision(59, decisionCanBacktrack[59]);

			int LA59_0 = input.LA(1);
			if ( (LA59_0==57) ) {
				int LA59_1 = input.LA(2);
				if ( (synpred100_GosuProg()) ) {
					alt59=1;
				}
			}
			} finally {dbg.exitDecision(59);}

			switch (alt59) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:171:50: ';'
					{
					dbg.location(171,50);
					match(input,57,FOLLOW_57_in_ifStatement2452); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(59);}
			dbg.location(171,55);
			// GosuProg.g:171:55: ( 'else' statement )?
			int alt60=2;
			try { dbg.enterSubRule(60);
			try { dbg.enterDecision(60, decisionCanBacktrack[60]);

			int LA60_0 = input.LA(1);
			if ( (LA60_0==98) ) {
				int LA60_1 = input.LA(2);
				if ( (synpred101_GosuProg()) ) {
					alt60=1;
				}
			}
			} finally {dbg.exitDecision(60);}

			switch (alt60) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:171:56: 'else' statement
					{
					dbg.location(171,56);
					match(input,98,FOLLOW_98_in_ifStatement2456); if (state.failed) return;dbg.location(171,63);
					pushFollow(FOLLOW_statement_in_ifStatement2458);
					statement();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(60);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 38, ifStatement_StartIndex); }

		}
		dbg.location(171, 75);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "ifStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "ifStatement"



	// $ANTLR start "tryCatchFinallyStatement"
	// GosuProg.g:173:1: tryCatchFinallyStatement : 'try' statementBlock ( ( catchClause )+ ( 'finally' statementBlock )? | 'finally' statementBlock ) ;
	public final void tryCatchFinallyStatement() throws RecognitionException {
		int tryCatchFinallyStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "tryCatchFinallyStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(173, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return; }

			// GosuProg.g:173:26: ( 'try' statementBlock ( ( catchClause )+ ( 'finally' statementBlock )? | 'finally' statementBlock ) )
			dbg.enterAlt(1);

			// GosuProg.g:173:28: 'try' statementBlock ( ( catchClause )+ ( 'finally' statementBlock )? | 'finally' statementBlock )
			{
			dbg.location(173,28);
			match(input,150,FOLLOW_150_in_tryCatchFinallyStatement2470); if (state.failed) return;dbg.location(173,34);
			pushFollow(FOLLOW_statementBlock_in_tryCatchFinallyStatement2472);
			statementBlock();
			state._fsp--;
			if (state.failed) return;dbg.location(173,49);
			// GosuProg.g:173:49: ( ( catchClause )+ ( 'finally' statementBlock )? | 'finally' statementBlock )
			int alt63=2;
			try { dbg.enterSubRule(63);
			try { dbg.enterDecision(63, decisionCanBacktrack[63]);

			int LA63_0 = input.LA(1);
			if ( (LA63_0==89) ) {
				alt63=1;
			}
			else if ( (LA63_0==108) ) {
				alt63=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 63, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(63);}

			switch (alt63) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:173:51: ( catchClause )+ ( 'finally' statementBlock )?
					{
					dbg.location(173,51);
					// GosuProg.g:173:51: ( catchClause )+
					int cnt61=0;
					try { dbg.enterSubRule(61);

					loop61:
					while (true) {
						int alt61=2;
						try { dbg.enterDecision(61, decisionCanBacktrack[61]);

						int LA61_0 = input.LA(1);
						if ( (LA61_0==89) ) {
							alt61=1;
						}

						} finally {dbg.exitDecision(61);}

						switch (alt61) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:173:51: catchClause
							{
							dbg.location(173,51);
							pushFollow(FOLLOW_catchClause_in_tryCatchFinallyStatement2476);
							catchClause();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							if ( cnt61 >= 1 ) break loop61;
							if (state.backtracking>0) {state.failed=true; return;}
							EarlyExitException eee = new EarlyExitException(61, input);
							dbg.recognitionException(eee);

							throw eee;
						}
						cnt61++;
					}
					} finally {dbg.exitSubRule(61);}
					dbg.location(173,64);
					// GosuProg.g:173:64: ( 'finally' statementBlock )?
					int alt62=2;
					try { dbg.enterSubRule(62);
					try { dbg.enterDecision(62, decisionCanBacktrack[62]);

					int LA62_0 = input.LA(1);
					if ( (LA62_0==108) ) {
						alt62=1;
					}
					} finally {dbg.exitDecision(62);}

					switch (alt62) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:173:65: 'finally' statementBlock
							{
							dbg.location(173,65);
							match(input,108,FOLLOW_108_in_tryCatchFinallyStatement2480); if (state.failed) return;dbg.location(173,75);
							pushFollow(FOLLOW_statementBlock_in_tryCatchFinallyStatement2482);
							statementBlock();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(62);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:173:94: 'finally' statementBlock
					{
					dbg.location(173,94);
					match(input,108,FOLLOW_108_in_tryCatchFinallyStatement2488); if (state.failed) return;dbg.location(173,104);
					pushFollow(FOLLOW_statementBlock_in_tryCatchFinallyStatement2490);
					statementBlock();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(63);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 39, tryCatchFinallyStatement_StartIndex); }

		}
		dbg.location(173, 118);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "tryCatchFinallyStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "tryCatchFinallyStatement"



	// $ANTLR start "catchClause"
	// GosuProg.g:175:1: catchClause : 'catch' '(' ( 'var' )? id ( ':' typeLiteral )? ')' statementBlock ;
	public final void catchClause() throws RecognitionException {
		int catchClause_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "catchClause");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(175, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return; }

			// GosuProg.g:175:13: ( 'catch' '(' ( 'var' )? id ( ':' typeLiteral )? ')' statementBlock )
			dbg.enterAlt(1);

			// GosuProg.g:175:16: 'catch' '(' ( 'var' )? id ( ':' typeLiteral )? ')' statementBlock
			{
			dbg.location(175,16);
			match(input,89,FOLLOW_89_in_catchClause2500); if (state.failed) return;dbg.location(175,24);
			match(input,38,FOLLOW_38_in_catchClause2502); if (state.failed) return;dbg.location(175,28);
			// GosuProg.g:175:28: ( 'var' )?
			int alt64=2;
			try { dbg.enterSubRule(64);
			try { dbg.enterDecision(64, decisionCanBacktrack[64]);

			int LA64_0 = input.LA(1);
			if ( (LA64_0==158) ) {
				alt64=1;
			}
			} finally {dbg.exitDecision(64);}

			switch (alt64) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:175:28: 'var'
					{
					dbg.location(175,28);
					match(input,158,FOLLOW_158_in_catchClause2504); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(64);}
			dbg.location(175,35);
			pushFollow(FOLLOW_id_in_catchClause2507);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(175,38);
			// GosuProg.g:175:38: ( ':' typeLiteral )?
			int alt65=2;
			try { dbg.enterSubRule(65);
			try { dbg.enterDecision(65, decisionCanBacktrack[65]);

			int LA65_0 = input.LA(1);
			if ( (LA65_0==56) ) {
				alt65=1;
			}
			} finally {dbg.exitDecision(65);}

			switch (alt65) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:175:39: ':' typeLiteral
					{
					dbg.location(175,39);
					match(input,56,FOLLOW_56_in_catchClause2510); if (state.failed) return;dbg.location(175,43);
					pushFollow(FOLLOW_typeLiteral_in_catchClause2512);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(65);}
			dbg.location(175,57);
			match(input,39,FOLLOW_39_in_catchClause2516); if (state.failed) return;dbg.location(175,61);
			pushFollow(FOLLOW_statementBlock_in_catchClause2518);
			statementBlock();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 40, catchClause_StartIndex); }

		}
		dbg.location(175, 75);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "catchClause");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "catchClause"



	// $ANTLR start "assertStatement"
	// GosuProg.g:177:1: assertStatement : 'assert' expression ( ':' expression )? ;
	public final void assertStatement() throws RecognitionException {
		int assertStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "assertStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(177, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return; }

			// GosuProg.g:177:17: ( 'assert' expression ( ':' expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:177:19: 'assert' expression ( ':' expression )?
			{
			dbg.location(177,19);
			match(input,85,FOLLOW_85_in_assertStatement2527); if (state.failed) return;dbg.location(177,28);
			pushFollow(FOLLOW_expression_in_assertStatement2529);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(177,39);
			// GosuProg.g:177:39: ( ':' expression )?
			int alt66=2;
			try { dbg.enterSubRule(66);
			try { dbg.enterDecision(66, decisionCanBacktrack[66]);

			int LA66_0 = input.LA(1);
			if ( (LA66_0==56) ) {
				alt66=1;
			}
			} finally {dbg.exitDecision(66);}

			switch (alt66) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:177:40: ':' expression
					{
					dbg.location(177,40);
					match(input,56,FOLLOW_56_in_assertStatement2532); if (state.failed) return;dbg.location(177,44);
					pushFollow(FOLLOW_expression_in_assertStatement2534);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(66);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 41, assertStatement_StartIndex); }

		}
		dbg.location(177, 57);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "assertStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "assertStatement"



	// $ANTLR start "usingStatement"
	// GosuProg.g:179:1: usingStatement : 'using' '(' ( localVarStatement ( ',' localVarStatement )* | expression ) ')' statementBlock ( 'finally' statementBlock )? ;
	public final void usingStatement() throws RecognitionException {
		int usingStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "usingStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(179, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return; }

			// GosuProg.g:179:16: ( 'using' '(' ( localVarStatement ( ',' localVarStatement )* | expression ) ')' statementBlock ( 'finally' statementBlock )? )
			dbg.enterAlt(1);

			// GosuProg.g:179:18: 'using' '(' ( localVarStatement ( ',' localVarStatement )* | expression ) ')' statementBlock ( 'finally' statementBlock )?
			{
			dbg.location(179,18);
			match(input,157,FOLLOW_157_in_usingStatement2546); if (state.failed) return;dbg.location(179,26);
			match(input,38,FOLLOW_38_in_usingStatement2548); if (state.failed) return;dbg.location(179,30);
			// GosuProg.g:179:30: ( localVarStatement ( ',' localVarStatement )* | expression )
			int alt68=2;
			try { dbg.enterSubRule(68);
			try { dbg.enterDecision(68, decisionCanBacktrack[68]);

			int LA68_0 = input.LA(1);
			if ( (LA68_0==158) ) {
				alt68=1;
			}
			else if ( (LA68_0==CharLiteral||LA68_0==Ident||LA68_0==NumberLiteral||LA68_0==StringLiteral||LA68_0==25||LA68_0==28||LA68_0==31||LA68_0==38||LA68_0==43||LA68_0==47||(LA68_0 >= 74 && LA68_0 <= 75)||LA68_0==77||LA68_0==81||(LA68_0 >= 83 && LA68_0 <= 86)||LA68_0==91||LA68_0==93||LA68_0==99||(LA68_0 >= 101 && LA68_0 <= 104)||(LA68_0 >= 106 && LA68_0 <= 107)||LA68_0==109||(LA68_0 >= 113 && LA68_0 <= 114)||LA68_0==118||(LA68_0 >= 120 && LA68_0 <= 125)||LA68_0==127||LA68_0==130||(LA68_0 >= 132 && LA68_0 <= 134)||LA68_0==136||(LA68_0 >= 138 && LA68_0 <= 142)||LA68_0==144||LA68_0==146||LA68_0==149||(LA68_0 >= 153 && LA68_0 <= 154)||(LA68_0 >= 159 && LA68_0 <= 160)||LA68_0==162||LA68_0==170) ) {
				alt68=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 68, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(68);}

			switch (alt68) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:179:31: localVarStatement ( ',' localVarStatement )*
					{
					dbg.location(179,31);
					pushFollow(FOLLOW_localVarStatement_in_usingStatement2551);
					localVarStatement();
					state._fsp--;
					if (state.failed) return;dbg.location(179,49);
					// GosuProg.g:179:49: ( ',' localVarStatement )*
					try { dbg.enterSubRule(67);

					loop67:
					while (true) {
						int alt67=2;
						try { dbg.enterDecision(67, decisionCanBacktrack[67]);

						int LA67_0 = input.LA(1);
						if ( (LA67_0==46) ) {
							alt67=1;
						}

						} finally {dbg.exitDecision(67);}

						switch (alt67) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:179:50: ',' localVarStatement
							{
							dbg.location(179,50);
							match(input,46,FOLLOW_46_in_usingStatement2554); if (state.failed) return;dbg.location(179,54);
							pushFollow(FOLLOW_localVarStatement_in_usingStatement2556);
							localVarStatement();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop67;
						}
					}
					} finally {dbg.exitSubRule(67);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:179:76: expression
					{
					dbg.location(179,76);
					pushFollow(FOLLOW_expression_in_usingStatement2562);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(68);}
			dbg.location(179,88);
			match(input,39,FOLLOW_39_in_usingStatement2565); if (state.failed) return;dbg.location(179,92);
			pushFollow(FOLLOW_statementBlock_in_usingStatement2567);
			statementBlock();
			state._fsp--;
			if (state.failed) return;dbg.location(179,107);
			// GosuProg.g:179:107: ( 'finally' statementBlock )?
			int alt69=2;
			try { dbg.enterSubRule(69);
			try { dbg.enterDecision(69, decisionCanBacktrack[69]);

			int LA69_0 = input.LA(1);
			if ( (LA69_0==108) ) {
				alt69=1;
			}
			} finally {dbg.exitDecision(69);}

			switch (alt69) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:179:108: 'finally' statementBlock
					{
					dbg.location(179,108);
					match(input,108,FOLLOW_108_in_usingStatement2570); if (state.failed) return;dbg.location(179,118);
					pushFollow(FOLLOW_statementBlock_in_usingStatement2572);
					statementBlock();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(69);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 42, usingStatement_StartIndex); }

		}
		dbg.location(179, 134);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "usingStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "usingStatement"



	// $ANTLR start "returnStatement"
	// GosuProg.g:181:1: returnStatement : 'return' ( ( expression ( (~ '=' ) | EOF ) )=> expression )? ;
	public final void returnStatement() throws RecognitionException {
		int returnStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "returnStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(181, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return; }

			// GosuProg.g:181:17: ( 'return' ( ( expression ( (~ '=' ) | EOF ) )=> expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:181:19: 'return' ( ( expression ( (~ '=' ) | EOF ) )=> expression )?
			{
			dbg.location(181,19);
			match(input,137,FOLLOW_137_in_returnStatement2583); if (state.failed) return;dbg.location(181,28);
			// GosuProg.g:181:28: ( ( expression ( (~ '=' ) | EOF ) )=> expression )?
			int alt70=2;
			try { dbg.enterSubRule(70);
			try { dbg.enterDecision(70, decisionCanBacktrack[70]);

			int LA70_0 = input.LA(1);
			if ( (LA70_0==28||LA70_0==43||LA70_0==47) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==25||LA70_0==124||LA70_0==142||LA70_0==154||LA70_0==170) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==77) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==101) ) {
				int LA70_4 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==123) ) {
				int LA70_5 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==144||LA70_0==146) ) {
				int LA70_6 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==NumberLiteral) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==31) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==StringLiteral) ) {
				int LA70_9 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==CharLiteral) && (synpred112_GosuProg())) {
				alt70=1;
			}
			else if ( (LA70_0==149) ) {
				int LA70_11 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==106) ) {
				int LA70_12 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==125) ) {
				int LA70_13 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==130) ) {
				int LA70_14 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==86) ) {
				int LA70_15 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==38) ) {
				int LA70_16 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==162) ) {
				int LA70_17 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==120) ) {
				int LA70_21 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==132) ) {
				int LA70_22 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==133) ) {
				int LA70_23 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==141) ) {
				int LA70_24 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==81) ) {
				int LA70_25 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==107) ) {
				int LA70_26 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==99) ) {
				int LA70_28 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==85) ) {
				int LA70_33 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			else if ( (LA70_0==Ident||(LA70_0 >= 74 && LA70_0 <= 75)||(LA70_0 >= 83 && LA70_0 <= 84)||LA70_0==91||LA70_0==93||(LA70_0 >= 102 && LA70_0 <= 104)||LA70_0==109||(LA70_0 >= 113 && LA70_0 <= 114)||LA70_0==118||(LA70_0 >= 121 && LA70_0 <= 122)||LA70_0==127||LA70_0==134||LA70_0==136||(LA70_0 >= 138 && LA70_0 <= 140)||LA70_0==153||(LA70_0 >= 159 && LA70_0 <= 160)) ) {
				int LA70_45 = input.LA(2);
				if ( (synpred112_GosuProg()) ) {
					alt70=1;
				}
			}
			} finally {dbg.exitDecision(70);}

			switch (alt70) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:181:30: ( expression ( (~ '=' ) | EOF ) )=> expression
					{
					dbg.location(181,59);
					pushFollow(FOLLOW_expression_in_returnStatement2602);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(70);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 43, returnStatement_StartIndex); }

		}
		dbg.location(181, 72);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "returnStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "returnStatement"



	// $ANTLR start "whileStatement"
	// GosuProg.g:183:1: whileStatement : 'while' '(' expression ')' statement ;
	public final void whileStatement() throws RecognitionException {
		int whileStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "whileStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(183, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 44) ) { return; }

			// GosuProg.g:183:16: ( 'while' '(' expression ')' statement )
			dbg.enterAlt(1);

			// GosuProg.g:183:18: 'while' '(' expression ')' statement
			{
			dbg.location(183,18);
			match(input,161,FOLLOW_161_in_whileStatement2614); if (state.failed) return;dbg.location(183,26);
			match(input,38,FOLLOW_38_in_whileStatement2616); if (state.failed) return;dbg.location(183,29);
			pushFollow(FOLLOW_expression_in_whileStatement2617);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(183,40);
			match(input,39,FOLLOW_39_in_whileStatement2619); if (state.failed) return;dbg.location(183,44);
			pushFollow(FOLLOW_statement_in_whileStatement2621);
			statement();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 44, whileStatement_StartIndex); }

		}
		dbg.location(183, 53);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "whileStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "whileStatement"



	// $ANTLR start "doWhileStatement"
	// GosuProg.g:185:1: doWhileStatement : 'do' statement 'while' '(' expression ')' ;
	public final void doWhileStatement() throws RecognitionException {
		int doWhileStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "doWhileStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(185, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 45) ) { return; }

			// GosuProg.g:185:18: ( 'do' statement 'while' '(' expression ')' )
			dbg.enterAlt(1);

			// GosuProg.g:185:20: 'do' statement 'while' '(' expression ')'
			{
			dbg.location(185,20);
			match(input,97,FOLLOW_97_in_doWhileStatement2630); if (state.failed) return;dbg.location(185,25);
			pushFollow(FOLLOW_statement_in_doWhileStatement2632);
			statement();
			state._fsp--;
			if (state.failed) return;dbg.location(185,35);
			match(input,161,FOLLOW_161_in_doWhileStatement2634); if (state.failed) return;dbg.location(185,43);
			match(input,38,FOLLOW_38_in_doWhileStatement2636); if (state.failed) return;dbg.location(185,47);
			pushFollow(FOLLOW_expression_in_doWhileStatement2638);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(185,58);
			match(input,39,FOLLOW_39_in_doWhileStatement2640); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 45, doWhileStatement_StartIndex); }

		}
		dbg.location(185, 61);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "doWhileStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "doWhileStatement"



	// $ANTLR start "switchStatement"
	// GosuProg.g:187:1: switchStatement : 'switch' '(' expression ')' '{' ( switchBlockStatementGroup )* '}' ;
	public final void switchStatement() throws RecognitionException {
		int switchStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "switchStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(187, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 46) ) { return; }

			// GosuProg.g:187:17: ( 'switch' '(' expression ')' '{' ( switchBlockStatementGroup )* '}' )
			dbg.enterAlt(1);

			// GosuProg.g:187:19: 'switch' '(' expression ')' '{' ( switchBlockStatementGroup )* '}'
			{
			dbg.location(187,19);
			match(input,145,FOLLOW_145_in_switchStatement2649); if (state.failed) return;dbg.location(187,28);
			match(input,38,FOLLOW_38_in_switchStatement2651); if (state.failed) return;dbg.location(187,32);
			pushFollow(FOLLOW_expression_in_switchStatement2653);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(187,43);
			match(input,39,FOLLOW_39_in_switchStatement2655); if (state.failed) return;dbg.location(187,47);
			match(input,162,FOLLOW_162_in_switchStatement2657); if (state.failed) return;dbg.location(187,51);
			// GosuProg.g:187:51: ( switchBlockStatementGroup )*
			try { dbg.enterSubRule(71);

			loop71:
			while (true) {
				int alt71=2;
				try { dbg.enterDecision(71, decisionCanBacktrack[71]);

				int LA71_0 = input.LA(1);
				if ( (LA71_0==88||LA71_0==95) ) {
					alt71=1;
				}

				} finally {dbg.exitDecision(71);}

				switch (alt71) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:187:51: switchBlockStatementGroup
					{
					dbg.location(187,51);
					pushFollow(FOLLOW_switchBlockStatementGroup_in_switchStatement2659);
					switchBlockStatementGroup();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop71;
				}
			}
			} finally {dbg.exitSubRule(71);}
			dbg.location(187,78);
			match(input,169,FOLLOW_169_in_switchStatement2662); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 46, switchStatement_StartIndex); }

		}
		dbg.location(187, 81);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "switchStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "switchStatement"



	// $ANTLR start "switchBlockStatementGroup"
	// GosuProg.g:189:1: switchBlockStatementGroup : ( 'case' expression ':' | 'default' ':' ) ( statement )* ;
	public final void switchBlockStatementGroup() throws RecognitionException {
		int switchBlockStatementGroup_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "switchBlockStatementGroup");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(189, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 47) ) { return; }

			// GosuProg.g:189:27: ( ( 'case' expression ':' | 'default' ':' ) ( statement )* )
			dbg.enterAlt(1);

			// GosuProg.g:189:29: ( 'case' expression ':' | 'default' ':' ) ( statement )*
			{
			dbg.location(189,29);
			// GosuProg.g:189:29: ( 'case' expression ':' | 'default' ':' )
			int alt72=2;
			try { dbg.enterSubRule(72);
			try { dbg.enterDecision(72, decisionCanBacktrack[72]);

			int LA72_0 = input.LA(1);
			if ( (LA72_0==88) ) {
				alt72=1;
			}
			else if ( (LA72_0==95) ) {
				alt72=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 72, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(72);}

			switch (alt72) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:189:30: 'case' expression ':'
					{
					dbg.location(189,30);
					match(input,88,FOLLOW_88_in_switchBlockStatementGroup2672); if (state.failed) return;dbg.location(189,37);
					pushFollow(FOLLOW_expression_in_switchBlockStatementGroup2674);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(189,48);
					match(input,56,FOLLOW_56_in_switchBlockStatementGroup2676); if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:189:53: 'default' ':'
					{
					dbg.location(189,53);
					match(input,95,FOLLOW_95_in_switchBlockStatementGroup2679); if (state.failed) return;dbg.location(189,63);
					match(input,56,FOLLOW_56_in_switchBlockStatementGroup2681); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(72);}
			dbg.location(189,68);
			// GosuProg.g:189:68: ( statement )*
			try { dbg.enterSubRule(73);

			loop73:
			while (true) {
				int alt73=2;
				try { dbg.enterDecision(73, decisionCanBacktrack[73]);

				int LA73_0 = input.LA(1);
				if ( (LA73_0==Ident||LA73_0==StringLiteral||LA73_0==38||LA73_0==57||(LA73_0 >= 74 && LA73_0 <= 75)||LA73_0==81||(LA73_0 >= 83 && LA73_0 <= 87)||LA73_0==91||(LA73_0 >= 93 && LA73_0 <= 94)||LA73_0==97||LA73_0==99||(LA73_0 >= 101 && LA73_0 <= 104)||(LA73_0 >= 106 && LA73_0 <= 107)||(LA73_0 >= 109 && LA73_0 <= 111)||(LA73_0 >= 113 && LA73_0 <= 115)||LA73_0==118||(LA73_0 >= 120 && LA73_0 <= 123)||LA73_0==125||LA73_0==127||LA73_0==130||(LA73_0 >= 132 && LA73_0 <= 134)||(LA73_0 >= 136 && LA73_0 <= 141)||(LA73_0 >= 144 && LA73_0 <= 147)||(LA73_0 >= 149 && LA73_0 <= 150)||LA73_0==153||(LA73_0 >= 157 && LA73_0 <= 162)) ) {
					alt73=1;
				}

				} finally {dbg.exitDecision(73);}

				switch (alt73) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:189:68: statement
					{
					dbg.location(189,68);
					pushFollow(FOLLOW_statement_in_switchBlockStatementGroup2684);
					statement();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop73;
				}
			}
			} finally {dbg.exitSubRule(73);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 47, switchBlockStatementGroup_StartIndex); }

		}
		dbg.location(189, 78);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "switchBlockStatementGroup");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "switchBlockStatementGroup"



	// $ANTLR start "throwStatement"
	// GosuProg.g:191:1: throwStatement : 'throw' expression ;
	public final void throwStatement() throws RecognitionException {
		int throwStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "throwStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(191, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 48) ) { return; }

			// GosuProg.g:191:16: ( 'throw' expression )
			dbg.enterAlt(1);

			// GosuProg.g:191:18: 'throw' expression
			{
			dbg.location(191,18);
			match(input,147,FOLLOW_147_in_throwStatement2694); if (state.failed) return;dbg.location(191,26);
			pushFollow(FOLLOW_expression_in_throwStatement2696);
			expression();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 48, throwStatement_StartIndex); }

		}
		dbg.location(191, 36);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "throwStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "throwStatement"



	// $ANTLR start "localVarStatement"
	// GosuProg.g:193:1: localVarStatement : 'var' id optionalType ( '=' expression )? ;
	public final void localVarStatement() throws RecognitionException {
		int localVarStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "localVarStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(193, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 49) ) { return; }

			// GosuProg.g:193:19: ( 'var' id optionalType ( '=' expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:193:21: 'var' id optionalType ( '=' expression )?
			{
			dbg.location(193,21);
			match(input,158,FOLLOW_158_in_localVarStatement2705); if (state.failed) return;dbg.location(193,27);
			pushFollow(FOLLOW_id_in_localVarStatement2707);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(193,30);
			pushFollow(FOLLOW_optionalType_in_localVarStatement2709);
			optionalType();
			state._fsp--;
			if (state.failed) return;dbg.location(193,43);
			// GosuProg.g:193:43: ( '=' expression )?
			int alt74=2;
			try { dbg.enterSubRule(74);
			try { dbg.enterDecision(74, decisionCanBacktrack[74]);

			int LA74_0 = input.LA(1);
			if ( (LA74_0==60) ) {
				alt74=1;
			}
			} finally {dbg.exitDecision(74);}

			switch (alt74) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:193:44: '=' expression
					{
					dbg.location(193,44);
					match(input,60,FOLLOW_60_in_localVarStatement2712); if (state.failed) return;dbg.location(193,48);
					pushFollow(FOLLOW_expression_in_localVarStatement2714);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(74);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 49, localVarStatement_StartIndex); }

		}
		dbg.location(193, 60);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "localVarStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "localVarStatement"



	// $ANTLR start "forEachStatement"
	// GosuProg.g:195:1: forEachStatement : ( 'foreach' | 'for' ) '(' ( expression ( indexVar )? | ( 'var' )? id 'in' expression ( indexRest )? ) ')' statement ;
	public final void forEachStatement() throws RecognitionException {
		int forEachStatement_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "forEachStatement");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(195, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 50) ) { return; }

			// GosuProg.g:195:18: ( ( 'foreach' | 'for' ) '(' ( expression ( indexVar )? | ( 'var' )? id 'in' expression ( indexRest )? ) ')' statement )
			dbg.enterAlt(1);

			// GosuProg.g:195:20: ( 'foreach' | 'for' ) '(' ( expression ( indexVar )? | ( 'var' )? id 'in' expression ( indexRest )? ) ')' statement
			{
			dbg.location(195,20);
			if ( (input.LA(1) >= 110 && input.LA(1) <= 111) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}dbg.location(195,40);
			match(input,38,FOLLOW_38_in_forEachStatement2733); if (state.failed) return;dbg.location(195,44);
			// GosuProg.g:195:44: ( expression ( indexVar )? | ( 'var' )? id 'in' expression ( indexRest )? )
			int alt78=2;
			try { dbg.enterSubRule(78);
			try { dbg.enterDecision(78, decisionCanBacktrack[78]);

			switch ( input.LA(1) ) {
			case CharLiteral:
			case NumberLiteral:
			case StringLiteral:
			case 25:
			case 28:
			case 31:
			case 38:
			case 43:
			case 47:
			case 77:
			case 101:
			case 123:
			case 124:
			case 142:
			case 144:
			case 146:
			case 154:
			case 162:
			case 170:
				{
				alt78=1;
				}
				break;
			case 149:
				{
				int LA78_2 = input.LA(2);
				if ( ((LA78_2 >= 26 && LA78_2 <= 32)||LA78_2==34||LA78_2==36||(LA78_2 >= 38 && LA78_2 <= 41)||LA78_2==43||LA78_2==47||(LA78_2 >= 51 && LA78_2 <= 54)||(LA78_2 >= 58 && LA78_2 <= 59)||(LA78_2 >= 61 && LA78_2 <= 72)||LA78_2==76||LA78_2==79||LA78_2==82||LA78_2==84||LA78_2==118||LA78_2==126||(LA78_2 >= 151 && LA78_2 <= 152)||(LA78_2 >= 163 && LA78_2 <= 165)||LA78_2==167) ) {
					alt78=1;
				}
				else if ( (LA78_2==117) ) {
					alt78=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 78, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 106:
				{
				int LA78_3 = input.LA(2);
				if ( ((LA78_3 >= 26 && LA78_3 <= 32)||LA78_3==34||LA78_3==36||(LA78_3 >= 38 && LA78_3 <= 41)||LA78_3==43||LA78_3==47||(LA78_3 >= 51 && LA78_3 <= 54)||(LA78_3 >= 58 && LA78_3 <= 59)||(LA78_3 >= 61 && LA78_3 <= 72)||LA78_3==76||LA78_3==79||LA78_3==82||LA78_3==84||LA78_3==118||LA78_3==126||(LA78_3 >= 151 && LA78_3 <= 152)||(LA78_3 >= 163 && LA78_3 <= 165)||LA78_3==167) ) {
					alt78=1;
				}
				else if ( (LA78_3==117) ) {
					alt78=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 78, 3, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 125:
				{
				int LA78_4 = input.LA(2);
				if ( ((LA78_4 >= 26 && LA78_4 <= 32)||LA78_4==34||LA78_4==36||(LA78_4 >= 38 && LA78_4 <= 41)||LA78_4==43||LA78_4==47||(LA78_4 >= 51 && LA78_4 <= 54)||(LA78_4 >= 58 && LA78_4 <= 59)||(LA78_4 >= 61 && LA78_4 <= 72)||LA78_4==76||LA78_4==79||LA78_4==82||LA78_4==84||LA78_4==118||LA78_4==126||(LA78_4 >= 151 && LA78_4 <= 152)||(LA78_4 >= 163 && LA78_4 <= 165)||LA78_4==167) ) {
					alt78=1;
				}
				else if ( (LA78_4==117) ) {
					alt78=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 78, 4, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case Ident:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 153:
			case 159:
			case 160:
				{
				int LA78_5 = input.LA(2);
				if ( ((LA78_5 >= 26 && LA78_5 <= 32)||LA78_5==34||LA78_5==36||(LA78_5 >= 38 && LA78_5 <= 41)||LA78_5==43||LA78_5==47||(LA78_5 >= 51 && LA78_5 <= 54)||(LA78_5 >= 58 && LA78_5 <= 59)||(LA78_5 >= 61 && LA78_5 <= 72)||LA78_5==76||LA78_5==79||LA78_5==82||LA78_5==84||LA78_5==118||LA78_5==126||(LA78_5 >= 151 && LA78_5 <= 152)||(LA78_5 >= 163 && LA78_5 <= 165)||LA78_5==167) ) {
					alt78=1;
				}
				else if ( (LA78_5==117) ) {
					alt78=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 78, 5, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 86:
				{
				int LA78_6 = input.LA(2);
				if ( (LA78_6==38) ) {
					alt78=1;
				}
				else if ( (LA78_6==117) ) {
					alt78=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 78, 6, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 158:
				{
				alt78=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 78, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(78);}

			switch (alt78) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:195:45: expression ( indexVar )?
					{
					dbg.location(195,45);
					pushFollow(FOLLOW_expression_in_forEachStatement2736);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(195,56);
					// GosuProg.g:195:56: ( indexVar )?
					int alt75=2;
					try { dbg.enterSubRule(75);
					try { dbg.enterDecision(75, decisionCanBacktrack[75]);

					int LA75_0 = input.LA(1);
					if ( (LA75_0==118) ) {
						alt75=1;
					}
					} finally {dbg.exitDecision(75);}

					switch (alt75) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:195:56: indexVar
							{
							dbg.location(195,56);
							pushFollow(FOLLOW_indexVar_in_forEachStatement2738);
							indexVar();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(75);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:195:68: ( 'var' )? id 'in' expression ( indexRest )?
					{
					dbg.location(195,68);
					// GosuProg.g:195:68: ( 'var' )?
					int alt76=2;
					try { dbg.enterSubRule(76);
					try { dbg.enterDecision(76, decisionCanBacktrack[76]);

					int LA76_0 = input.LA(1);
					if ( (LA76_0==158) ) {
						alt76=1;
					}
					} finally {dbg.exitDecision(76);}

					switch (alt76) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:195:68: 'var'
							{
							dbg.location(195,68);
							match(input,158,FOLLOW_158_in_forEachStatement2743); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(76);}
					dbg.location(195,75);
					pushFollow(FOLLOW_id_in_forEachStatement2746);
					id();
					state._fsp--;
					if (state.failed) return;dbg.location(195,78);
					match(input,117,FOLLOW_117_in_forEachStatement2748); if (state.failed) return;dbg.location(195,83);
					pushFollow(FOLLOW_expression_in_forEachStatement2750);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(195,94);
					// GosuProg.g:195:94: ( indexRest )?
					int alt77=2;
					try { dbg.enterSubRule(77);
					try { dbg.enterDecision(77, decisionCanBacktrack[77]);

					int LA77_0 = input.LA(1);
					if ( (LA77_0==118||LA77_0==121) ) {
						alt77=1;
					}
					} finally {dbg.exitDecision(77);}

					switch (alt77) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:195:94: indexRest
							{
							dbg.location(195,94);
							pushFollow(FOLLOW_indexRest_in_forEachStatement2752);
							indexRest();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(77);}

					}
					break;

			}
			} finally {dbg.exitSubRule(78);}
			dbg.location(195,106);
			match(input,39,FOLLOW_39_in_forEachStatement2756); if (state.failed) return;dbg.location(195,110);
			pushFollow(FOLLOW_statement_in_forEachStatement2758);
			statement();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 50, forEachStatement_StartIndex); }

		}
		dbg.location(195, 119);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "forEachStatement");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "forEachStatement"



	// $ANTLR start "indexRest"
	// GosuProg.g:197:1: indexRest : ( indexVar iteratorVar | iteratorVar indexVar | indexVar | iteratorVar );
	public final void indexRest() throws RecognitionException {
		int indexRest_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "indexRest");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(197, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 51) ) { return; }

			// GosuProg.g:197:11: ( indexVar iteratorVar | iteratorVar indexVar | indexVar | iteratorVar )
			int alt79=4;
			try { dbg.enterDecision(79, decisionCanBacktrack[79]);

			int LA79_0 = input.LA(1);
			if ( (LA79_0==118) ) {
				int LA79_1 = input.LA(2);
				if ( (LA79_1==Ident||(LA79_1 >= 74 && LA79_1 <= 75)||LA79_1==81||(LA79_1 >= 83 && LA79_1 <= 86)||LA79_1==91||LA79_1==93||LA79_1==99||(LA79_1 >= 102 && LA79_1 <= 104)||(LA79_1 >= 106 && LA79_1 <= 107)||LA79_1==109||(LA79_1 >= 113 && LA79_1 <= 114)||LA79_1==118||(LA79_1 >= 120 && LA79_1 <= 122)||LA79_1==125||LA79_1==127||LA79_1==130||(LA79_1 >= 132 && LA79_1 <= 134)||LA79_1==136||(LA79_1 >= 138 && LA79_1 <= 141)||LA79_1==149||LA79_1==153||(LA79_1 >= 159 && LA79_1 <= 160)) ) {
					int LA79_3 = input.LA(3);
					if ( (LA79_3==121) ) {
						alt79=1;
					}
					else if ( (LA79_3==EOF||LA79_3==39) ) {
						alt79=3;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 79, 3, input);
							dbg.recognitionException(nvae);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 79, 1, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA79_0==121) ) {
				int LA79_2 = input.LA(2);
				if ( (LA79_2==Ident||(LA79_2 >= 74 && LA79_2 <= 75)||LA79_2==81||(LA79_2 >= 83 && LA79_2 <= 86)||LA79_2==91||LA79_2==93||LA79_2==99||(LA79_2 >= 102 && LA79_2 <= 104)||(LA79_2 >= 106 && LA79_2 <= 107)||LA79_2==109||(LA79_2 >= 113 && LA79_2 <= 114)||LA79_2==118||(LA79_2 >= 120 && LA79_2 <= 122)||LA79_2==125||LA79_2==127||LA79_2==130||(LA79_2 >= 132 && LA79_2 <= 134)||LA79_2==136||(LA79_2 >= 138 && LA79_2 <= 141)||LA79_2==149||LA79_2==153||(LA79_2 >= 159 && LA79_2 <= 160)) ) {
					int LA79_4 = input.LA(3);
					if ( (LA79_4==118) ) {
						alt79=2;
					}
					else if ( (LA79_4==EOF||LA79_4==39) ) {
						alt79=4;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 79, 4, input);
							dbg.recognitionException(nvae);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 79, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 79, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(79);}

			switch (alt79) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:197:13: indexVar iteratorVar
					{
					dbg.location(197,13);
					pushFollow(FOLLOW_indexVar_in_indexRest2767);
					indexVar();
					state._fsp--;
					if (state.failed) return;dbg.location(197,22);
					pushFollow(FOLLOW_iteratorVar_in_indexRest2769);
					iteratorVar();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:198:13: iteratorVar indexVar
					{
					dbg.location(198,13);
					pushFollow(FOLLOW_iteratorVar_in_indexRest2786);
					iteratorVar();
					state._fsp--;
					if (state.failed) return;dbg.location(198,25);
					pushFollow(FOLLOW_indexVar_in_indexRest2788);
					indexVar();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:199:13: indexVar
					{
					dbg.location(199,13);
					pushFollow(FOLLOW_indexVar_in_indexRest2805);
					indexVar();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:200:13: iteratorVar
					{
					dbg.location(200,13);
					pushFollow(FOLLOW_iteratorVar_in_indexRest2834);
					iteratorVar();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 51, indexRest_StartIndex); }

		}
		dbg.location(201, 10);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "indexRest");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "indexRest"



	// $ANTLR start "indexVar"
	// GosuProg.g:203:1: indexVar : 'index' id ;
	public final void indexVar() throws RecognitionException {
		int indexVar_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "indexVar");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(203, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 52) ) { return; }

			// GosuProg.g:203:10: ( 'index' id )
			dbg.enterAlt(1);

			// GosuProg.g:203:12: 'index' id
			{
			dbg.location(203,12);
			match(input,118,FOLLOW_118_in_indexVar2853); if (state.failed) return;dbg.location(203,20);
			pushFollow(FOLLOW_id_in_indexVar2855);
			id();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 52, indexVar_StartIndex); }

		}
		dbg.location(203, 22);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "indexVar");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "indexVar"



	// $ANTLR start "iteratorVar"
	// GosuProg.g:205:1: iteratorVar : 'iterator' id ;
	public final void iteratorVar() throws RecognitionException {
		int iteratorVar_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "iteratorVar");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(205, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 53) ) { return; }

			// GosuProg.g:205:13: ( 'iterator' id )
			dbg.enterAlt(1);

			// GosuProg.g:205:15: 'iterator' id
			{
			dbg.location(205,15);
			match(input,121,FOLLOW_121_in_iteratorVar2864); if (state.failed) return;dbg.location(205,26);
			pushFollow(FOLLOW_id_in_iteratorVar2866);
			id();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 53, iteratorVar_StartIndex); }

		}
		dbg.location(205, 28);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "iteratorVar");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "iteratorVar"



	// $ANTLR start "thisSuperExpr"
	// GosuProg.g:207:1: thisSuperExpr : ( 'this' | 'super' );
	public final void thisSuperExpr() throws RecognitionException {
		int thisSuperExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "thisSuperExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(207, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 54) ) { return; }

			// GosuProg.g:207:15: ( 'this' | 'super' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(207,15);
			if ( input.LA(1)==144||input.LA(1)==146 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 54, thisSuperExpr_StartIndex); }

		}
		dbg.location(207, 33);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "thisSuperExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "thisSuperExpr"



	// $ANTLR start "assignmentOrMethodCall"
	// GosuProg.g:209:1: assignmentOrMethodCall : ( ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral ) indirectMemberAccess ) ( incrementOp | assignmentOp expression )? ;
	public final void assignmentOrMethodCall() throws RecognitionException {
		int assignmentOrMethodCall_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "assignmentOrMethodCall");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(209, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 55) ) { return; }

			// GosuProg.g:209:24: ( ( ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral ) indirectMemberAccess ) ( incrementOp | assignmentOp expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:209:26: ( ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral ) indirectMemberAccess ) ( incrementOp | assignmentOp expression )?
			{
			dbg.location(209,26);
			// GosuProg.g:209:26: ( ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral ) indirectMemberAccess )
			dbg.enterAlt(1);

			// GosuProg.g:210:28: ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral ) indirectMemberAccess
			{
			dbg.location(210,28);
			// GosuProg.g:210:28: ( newExpr | thisSuperExpr | typeLiteralExpr | parenthExpr | StringLiteral )
			int alt80=5;
			try { dbg.enterSubRule(80);
			try { dbg.enterDecision(80, decisionCanBacktrack[80]);

			switch ( input.LA(1) ) {
			case 123:
				{
				alt80=1;
				}
				break;
			case 144:
			case 146:
				{
				alt80=2;
				}
				break;
			case Ident:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 86:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 106:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 125:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 149:
			case 153:
			case 159:
			case 160:
				{
				alt80=3;
				}
				break;
			case 38:
				{
				alt80=4;
				}
				break;
			case StringLiteral:
				{
				alt80=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 80, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(80);}

			switch (alt80) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:210:29: newExpr
					{
					dbg.location(210,29);
					pushFollow(FOLLOW_newExpr_in_assignmentOrMethodCall2918);
					newExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:210:39: thisSuperExpr
					{
					dbg.location(210,39);
					pushFollow(FOLLOW_thisSuperExpr_in_assignmentOrMethodCall2922);
					thisSuperExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:210:55: typeLiteralExpr
					{
					dbg.location(210,55);
					pushFollow(FOLLOW_typeLiteralExpr_in_assignmentOrMethodCall2926);
					typeLiteralExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:210:73: parenthExpr
					{
					dbg.location(210,73);
					pushFollow(FOLLOW_parenthExpr_in_assignmentOrMethodCall2930);
					parenthExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:210:87: StringLiteral
					{
					dbg.location(210,87);
					match(input,StringLiteral,FOLLOW_StringLiteral_in_assignmentOrMethodCall2934); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(80);}
			dbg.location(211,28);
			pushFollow(FOLLOW_indirectMemberAccess_in_assignmentOrMethodCall2964);
			indirectMemberAccess();
			state._fsp--;
			if (state.failed) return;
			}
			dbg.location(212,28);
			// GosuProg.g:212:28: ( incrementOp | assignmentOp expression )?
			int alt81=3;
			try { dbg.enterSubRule(81);
			try { dbg.enterDecision(81, decisionCanBacktrack[81]);

			int LA81_0 = input.LA(1);
			if ( (LA81_0==44||LA81_0==48) ) {
				alt81=1;
			}
			else if ( (LA81_0==33||LA81_0==35||LA81_0==37||LA81_0==42||LA81_0==45||LA81_0==49||LA81_0==55||LA81_0==58||LA81_0==60||LA81_0==63||LA81_0==80||LA81_0==166||LA81_0==168) ) {
				alt81=2;
			}
			} finally {dbg.exitDecision(81);}

			switch (alt81) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:212:29: incrementOp
					{
					dbg.location(212,29);
					pushFollow(FOLLOW_incrementOp_in_assignmentOrMethodCall2994);
					incrementOp();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:212:43: assignmentOp expression
					{
					dbg.location(212,43);
					pushFollow(FOLLOW_assignmentOp_in_assignmentOrMethodCall2998);
					assignmentOp();
					state._fsp--;
					if (state.failed) return;dbg.location(212,56);
					pushFollow(FOLLOW_expression_in_assignmentOrMethodCall3000);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(81);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 55, assignmentOrMethodCall_StartIndex); }

		}
		dbg.location(214, 23);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "assignmentOrMethodCall");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "assignmentOrMethodCall"



	// $ANTLR start "statementBlock"
	// GosuProg.g:216:1: statementBlock : statementBlockBody ;
	public final void statementBlock() throws RecognitionException {
		int statementBlock_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "statementBlock");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(216, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 56) ) { return; }

			// GosuProg.g:216:16: ( statementBlockBody )
			dbg.enterAlt(1);

			// GosuProg.g:216:18: statementBlockBody
			{
			dbg.location(216,18);
			pushFollow(FOLLOW_statementBlockBody_in_statementBlock3035);
			statementBlockBody();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 56, statementBlock_StartIndex); }

		}
		dbg.location(216, 36);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "statementBlock");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "statementBlock"



	// $ANTLR start "statementBlockBody"
	// GosuProg.g:218:1: statementBlockBody : '{' ( statement )* '}' ;
	public final void statementBlockBody() throws RecognitionException {
		int statementBlockBody_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "statementBlockBody");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(218, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 57) ) { return; }

			// GosuProg.g:218:20: ( '{' ( statement )* '}' )
			dbg.enterAlt(1);

			// GosuProg.g:218:22: '{' ( statement )* '}'
			{
			dbg.location(218,22);
			match(input,162,FOLLOW_162_in_statementBlockBody3044); if (state.failed) return;dbg.location(218,26);
			// GosuProg.g:218:26: ( statement )*
			try { dbg.enterSubRule(82);

			loop82:
			while (true) {
				int alt82=2;
				try { dbg.enterDecision(82, decisionCanBacktrack[82]);

				int LA82_0 = input.LA(1);
				if ( (LA82_0==Ident||LA82_0==StringLiteral||LA82_0==38||LA82_0==57||(LA82_0 >= 74 && LA82_0 <= 75)||LA82_0==81||(LA82_0 >= 83 && LA82_0 <= 87)||LA82_0==91||(LA82_0 >= 93 && LA82_0 <= 94)||LA82_0==97||LA82_0==99||(LA82_0 >= 101 && LA82_0 <= 104)||(LA82_0 >= 106 && LA82_0 <= 107)||(LA82_0 >= 109 && LA82_0 <= 111)||(LA82_0 >= 113 && LA82_0 <= 115)||LA82_0==118||(LA82_0 >= 120 && LA82_0 <= 123)||LA82_0==125||LA82_0==127||LA82_0==130||(LA82_0 >= 132 && LA82_0 <= 134)||(LA82_0 >= 136 && LA82_0 <= 141)||(LA82_0 >= 144 && LA82_0 <= 147)||(LA82_0 >= 149 && LA82_0 <= 150)||LA82_0==153||(LA82_0 >= 157 && LA82_0 <= 162)) ) {
					alt82=1;
				}

				} finally {dbg.exitDecision(82);}

				switch (alt82) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:218:26: statement
					{
					dbg.location(218,26);
					pushFollow(FOLLOW_statement_in_statementBlockBody3046);
					statement();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop82;
				}
			}
			} finally {dbg.exitSubRule(82);}
			dbg.location(218,37);
			match(input,169,FOLLOW_169_in_statementBlockBody3049); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 57, statementBlockBody_StartIndex); }

		}
		dbg.location(218, 40);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "statementBlockBody");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "statementBlockBody"



	// $ANTLR start "blockTypeLiteral"
	// GosuProg.g:220:1: blockTypeLiteral : blockLiteral ;
	public final void blockTypeLiteral() throws RecognitionException {
		int blockTypeLiteral_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "blockTypeLiteral");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(220, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 58) ) { return; }

			// GosuProg.g:220:18: ( blockLiteral )
			dbg.enterAlt(1);

			// GosuProg.g:220:20: blockLiteral
			{
			dbg.location(220,20);
			pushFollow(FOLLOW_blockLiteral_in_blockTypeLiteral3058);
			blockLiteral();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 58, blockTypeLiteral_StartIndex); }

		}
		dbg.location(220, 32);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blockTypeLiteral");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "blockTypeLiteral"



	// $ANTLR start "blockLiteral"
	// GosuProg.g:222:1: blockLiteral : '(' ( blockLiteralArg ( ',' blockLiteralArg )* )? ')' ( ':' typeLiteral )? ;
	public final void blockLiteral() throws RecognitionException {
		int blockLiteral_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "blockLiteral");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(222, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 59) ) { return; }

			// GosuProg.g:222:14: ( '(' ( blockLiteralArg ( ',' blockLiteralArg )* )? ')' ( ':' typeLiteral )? )
			dbg.enterAlt(1);

			// GosuProg.g:222:16: '(' ( blockLiteralArg ( ',' blockLiteralArg )* )? ')' ( ':' typeLiteral )?
			{
			dbg.location(222,16);
			match(input,38,FOLLOW_38_in_blockLiteral3067); if (state.failed) return;dbg.location(222,20);
			// GosuProg.g:222:20: ( blockLiteralArg ( ',' blockLiteralArg )* )?
			int alt84=2;
			try { dbg.enterSubRule(84);
			try { dbg.enterDecision(84, decisionCanBacktrack[84]);

			int LA84_0 = input.LA(1);
			if ( (LA84_0==Ident||(LA84_0 >= 74 && LA84_0 <= 75)||LA84_0==81||(LA84_0 >= 83 && LA84_0 <= 86)||LA84_0==91||LA84_0==93||LA84_0==99||(LA84_0 >= 102 && LA84_0 <= 104)||(LA84_0 >= 106 && LA84_0 <= 107)||LA84_0==109||(LA84_0 >= 113 && LA84_0 <= 114)||LA84_0==118||(LA84_0 >= 120 && LA84_0 <= 122)||LA84_0==125||LA84_0==127||LA84_0==130||(LA84_0 >= 132 && LA84_0 <= 134)||LA84_0==136||(LA84_0 >= 138 && LA84_0 <= 141)||LA84_0==149||LA84_0==153||(LA84_0 >= 159 && LA84_0 <= 160)) ) {
				alt84=1;
			}
			} finally {dbg.exitDecision(84);}

			switch (alt84) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:222:21: blockLiteralArg ( ',' blockLiteralArg )*
					{
					dbg.location(222,21);
					pushFollow(FOLLOW_blockLiteralArg_in_blockLiteral3070);
					blockLiteralArg();
					state._fsp--;
					if (state.failed) return;dbg.location(222,37);
					// GosuProg.g:222:37: ( ',' blockLiteralArg )*
					try { dbg.enterSubRule(83);

					loop83:
					while (true) {
						int alt83=2;
						try { dbg.enterDecision(83, decisionCanBacktrack[83]);

						int LA83_0 = input.LA(1);
						if ( (LA83_0==46) ) {
							alt83=1;
						}

						} finally {dbg.exitDecision(83);}

						switch (alt83) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:222:38: ',' blockLiteralArg
							{
							dbg.location(222,38);
							match(input,46,FOLLOW_46_in_blockLiteral3073); if (state.failed) return;dbg.location(222,42);
							pushFollow(FOLLOW_blockLiteralArg_in_blockLiteral3075);
							blockLiteralArg();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop83;
						}
					}
					} finally {dbg.exitSubRule(83);}

					}
					break;

			}
			} finally {dbg.exitSubRule(84);}
			dbg.location(222,61);
			match(input,39,FOLLOW_39_in_blockLiteral3080); if (state.failed) return;dbg.location(222,65);
			// GosuProg.g:222:65: ( ':' typeLiteral )?
			int alt85=2;
			try { dbg.enterSubRule(85);
			try { dbg.enterDecision(85, decisionCanBacktrack[85]);

			try {
				isCyclicDecision = true;
				alt85 = dfa85.predict(input);
			}
			catch (NoViableAltException nvae) {
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(85);}

			switch (alt85) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:222:66: ':' typeLiteral
					{
					dbg.location(222,66);
					match(input,56,FOLLOW_56_in_blockLiteral3083); if (state.failed) return;dbg.location(222,70);
					pushFollow(FOLLOW_typeLiteral_in_blockLiteral3085);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(85);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 59, blockLiteral_StartIndex); }

		}
		dbg.location(222, 85);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blockLiteral");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "blockLiteral"



	// $ANTLR start "blockLiteralArg"
	// GosuProg.g:225:1: blockLiteralArg : ( id ( '=' expression | blockTypeLiteral ) | ( id ':' )? typeLiteral ( '=' expression )? );
	public final void blockLiteralArg() throws RecognitionException {
		int blockLiteralArg_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "blockLiteralArg");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(225, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 60) ) { return; }

			// GosuProg.g:225:17: ( id ( '=' expression | blockTypeLiteral ) | ( id ':' )? typeLiteral ( '=' expression )? )
			int alt89=2;
			try { dbg.enterDecision(89, decisionCanBacktrack[89]);

			int LA89_0 = input.LA(1);
			if ( (LA89_0==Ident||(LA89_0 >= 74 && LA89_0 <= 75)||LA89_0==81||(LA89_0 >= 83 && LA89_0 <= 85)||LA89_0==91||LA89_0==93||LA89_0==99||(LA89_0 >= 102 && LA89_0 <= 104)||(LA89_0 >= 106 && LA89_0 <= 107)||LA89_0==109||(LA89_0 >= 113 && LA89_0 <= 114)||LA89_0==118||(LA89_0 >= 120 && LA89_0 <= 122)||LA89_0==125||LA89_0==127||LA89_0==130||(LA89_0 >= 132 && LA89_0 <= 134)||LA89_0==136||(LA89_0 >= 138 && LA89_0 <= 141)||LA89_0==149||LA89_0==153||(LA89_0 >= 159 && LA89_0 <= 160)) ) {
				int LA89_1 = input.LA(2);
				if ( (synpred137_GosuProg()) ) {
					alt89=1;
				}
				else if ( (true) ) {
					alt89=2;
				}

			}
			else if ( (LA89_0==86) ) {
				int LA89_2 = input.LA(2);
				if ( (synpred137_GosuProg()) ) {
					alt89=1;
				}
				else if ( (true) ) {
					alt89=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 89, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(89);}

			switch (alt89) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:225:20: id ( '=' expression | blockTypeLiteral )
					{
					dbg.location(225,20);
					pushFollow(FOLLOW_id_in_blockLiteralArg3100);
					id();
					state._fsp--;
					if (state.failed) return;dbg.location(225,23);
					// GosuProg.g:225:23: ( '=' expression | blockTypeLiteral )
					int alt86=2;
					try { dbg.enterSubRule(86);
					try { dbg.enterDecision(86, decisionCanBacktrack[86]);

					int LA86_0 = input.LA(1);
					if ( (LA86_0==60) ) {
						alt86=1;
					}
					else if ( (LA86_0==38) ) {
						alt86=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 86, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}

					} finally {dbg.exitDecision(86);}

					switch (alt86) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:225:24: '=' expression
							{
							dbg.location(225,24);
							match(input,60,FOLLOW_60_in_blockLiteralArg3103); if (state.failed) return;dbg.location(225,28);
							pushFollow(FOLLOW_expression_in_blockLiteralArg3105);
							expression();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:225:41: blockTypeLiteral
							{
							dbg.location(225,41);
							pushFollow(FOLLOW_blockTypeLiteral_in_blockLiteralArg3109);
							blockTypeLiteral();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(86);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:226:20: ( id ':' )? typeLiteral ( '=' expression )?
					{
					dbg.location(226,20);
					// GosuProg.g:226:20: ( id ':' )?
					int alt87=2;
					try { dbg.enterSubRule(87);
					try { dbg.enterDecision(87, decisionCanBacktrack[87]);

					int LA87_0 = input.LA(1);
					if ( (LA87_0==Ident||(LA87_0 >= 74 && LA87_0 <= 75)||LA87_0==81||(LA87_0 >= 83 && LA87_0 <= 85)||LA87_0==91||LA87_0==93||LA87_0==99||(LA87_0 >= 102 && LA87_0 <= 104)||(LA87_0 >= 106 && LA87_0 <= 107)||LA87_0==109||(LA87_0 >= 113 && LA87_0 <= 114)||LA87_0==118||(LA87_0 >= 120 && LA87_0 <= 122)||LA87_0==125||LA87_0==127||LA87_0==130||(LA87_0 >= 132 && LA87_0 <= 134)||LA87_0==136||(LA87_0 >= 138 && LA87_0 <= 141)||LA87_0==149||LA87_0==153||(LA87_0 >= 159 && LA87_0 <= 160)) ) {
						int LA87_1 = input.LA(2);
						if ( (LA87_1==56) ) {
							alt87=1;
						}
					}
					else if ( (LA87_0==86) ) {
						int LA87_2 = input.LA(2);
						if ( (LA87_2==56) ) {
							alt87=1;
						}
					}
					} finally {dbg.exitDecision(87);}

					switch (alt87) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:226:21: id ':'
							{
							dbg.location(226,21);
							pushFollow(FOLLOW_id_in_blockLiteralArg3134);
							id();
							state._fsp--;
							if (state.failed) return;dbg.location(226,24);
							match(input,56,FOLLOW_56_in_blockLiteralArg3136); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(87);}
					dbg.location(226,30);
					pushFollow(FOLLOW_typeLiteral_in_blockLiteralArg3140);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;dbg.location(226,42);
					// GosuProg.g:226:42: ( '=' expression )?
					int alt88=2;
					try { dbg.enterSubRule(88);
					try { dbg.enterDecision(88, decisionCanBacktrack[88]);

					int LA88_0 = input.LA(1);
					if ( (LA88_0==60) ) {
						alt88=1;
					}
					} finally {dbg.exitDecision(88);}

					switch (alt88) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:226:43: '=' expression
							{
							dbg.location(226,43);
							match(input,60,FOLLOW_60_in_blockLiteralArg3143); if (state.failed) return;dbg.location(226,47);
							pushFollow(FOLLOW_expression_in_blockLiteralArg3145);
							expression();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(88);}

					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 60, blockLiteralArg_StartIndex); }

		}
		dbg.location(226, 60);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blockLiteralArg");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "blockLiteralArg"



	// $ANTLR start "typeLiteral"
	// GosuProg.g:228:1: typeLiteral : type ( '&' type )* ;
	public final void typeLiteral() throws RecognitionException {
		int typeLiteral_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeLiteral");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(228, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 61) ) { return; }

			// GosuProg.g:228:13: ( type ( '&' type )* )
			dbg.enterAlt(1);

			// GosuProg.g:228:15: type ( '&' type )*
			{
			dbg.location(228,15);
			pushFollow(FOLLOW_type_in_typeLiteral3157);
			type();
			state._fsp--;
			if (state.failed) return;dbg.location(228,21);
			// GosuProg.g:228:21: ( '&' type )*
			try { dbg.enterSubRule(90);

			loop90:
			while (true) {
				int alt90=2;
				try { dbg.enterDecision(90, decisionCanBacktrack[90]);

				int LA90_0 = input.LA(1);
				if ( (LA90_0==36) ) {
					int LA90_2 = input.LA(2);
					if ( (synpred140_GosuProg()) ) {
						alt90=1;
					}

				}

				} finally {dbg.exitDecision(90);}

				switch (alt90) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:228:22: '&' type
					{
					dbg.location(228,22);
					match(input,36,FOLLOW_36_in_typeLiteral3161); if (state.failed) return;dbg.location(228,26);
					pushFollow(FOLLOW_type_in_typeLiteral3163);
					type();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop90;
				}
			}
			} finally {dbg.exitSubRule(90);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 61, typeLiteral_StartIndex); }

		}
		dbg.location(228, 31);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeLiteral");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeLiteral"



	// $ANTLR start "typeLiteralType"
	// GosuProg.g:230:1: typeLiteralType : typeLiteral ;
	public final void typeLiteralType() throws RecognitionException {
		int typeLiteralType_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeLiteralType");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(230, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 62) ) { return; }

			// GosuProg.g:230:17: ( typeLiteral )
			dbg.enterAlt(1);

			// GosuProg.g:230:19: typeLiteral
			{
			dbg.location(230,19);
			pushFollow(FOLLOW_typeLiteral_in_typeLiteralType3173);
			typeLiteral();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 62, typeLiteralType_StartIndex); }

		}
		dbg.location(230, 30);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeLiteralType");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeLiteralType"



	// $ANTLR start "typeLiteralExpr"
	// GosuProg.g:232:1: typeLiteralExpr : typeLiteral ;
	public final void typeLiteralExpr() throws RecognitionException {
		int typeLiteralExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeLiteralExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(232, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 63) ) { return; }

			// GosuProg.g:232:17: ( typeLiteral )
			dbg.enterAlt(1);

			// GosuProg.g:232:19: typeLiteral
			{
			dbg.location(232,19);
			pushFollow(FOLLOW_typeLiteral_in_typeLiteralExpr3182);
			typeLiteral();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 63, typeLiteralExpr_StartIndex); }

		}
		dbg.location(232, 30);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeLiteralExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeLiteralExpr"



	// $ANTLR start "typeLiteralList"
	// GosuProg.g:234:1: typeLiteralList : typeLiteral ;
	public final void typeLiteralList() throws RecognitionException {
		int typeLiteralList_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeLiteralList");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(234, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 64) ) { return; }

			// GosuProg.g:234:17: ( typeLiteral )
			dbg.enterAlt(1);

			// GosuProg.g:234:19: typeLiteral
			{
			dbg.location(234,19);
			pushFollow(FOLLOW_typeLiteral_in_typeLiteralList3191);
			typeLiteral();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 64, typeLiteralList_StartIndex); }

		}
		dbg.location(234, 30);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeLiteralList");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeLiteralList"



	// $ANTLR start "type"
	// GosuProg.g:236:1: type : ( classOrInterfaceType ( '[' ']' )* | 'block' blockLiteral );
	public final void type() throws RecognitionException {
		int type_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "type");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(236, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 65) ) { return; }

			// GosuProg.g:237:5: ( classOrInterfaceType ( '[' ']' )* | 'block' blockLiteral )
			int alt92=2;
			try { dbg.enterDecision(92, decisionCanBacktrack[92]);

			int LA92_0 = input.LA(1);
			if ( (LA92_0==Ident||(LA92_0 >= 74 && LA92_0 <= 75)||LA92_0==81||(LA92_0 >= 83 && LA92_0 <= 85)||LA92_0==91||LA92_0==93||LA92_0==99||(LA92_0 >= 102 && LA92_0 <= 104)||(LA92_0 >= 106 && LA92_0 <= 107)||LA92_0==109||(LA92_0 >= 113 && LA92_0 <= 114)||LA92_0==118||(LA92_0 >= 120 && LA92_0 <= 122)||LA92_0==125||LA92_0==127||LA92_0==130||(LA92_0 >= 132 && LA92_0 <= 134)||LA92_0==136||(LA92_0 >= 138 && LA92_0 <= 141)||LA92_0==149||LA92_0==153||(LA92_0 >= 159 && LA92_0 <= 160)) ) {
				alt92=1;
			}
			else if ( (LA92_0==86) ) {
				alt92=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 92, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(92);}

			switch (alt92) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:238:9: classOrInterfaceType ( '[' ']' )*
					{
					dbg.location(238,9);
					pushFollow(FOLLOW_classOrInterfaceType_in_type3212);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;dbg.location(238,31);
					// GosuProg.g:238:31: ( '[' ']' )*
					try { dbg.enterSubRule(91);

					loop91:
					while (true) {
						int alt91=2;
						try { dbg.enterDecision(91, decisionCanBacktrack[91]);

						int LA91_0 = input.LA(1);
						if ( (LA91_0==76) ) {
							int LA91_2 = input.LA(2);
							if ( (LA91_2==78) ) {
								alt91=1;
							}

						}

						} finally {dbg.exitDecision(91);}

						switch (alt91) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:238:32: '[' ']'
							{
							dbg.location(238,32);
							match(input,76,FOLLOW_76_in_type3216); if (state.failed) return;dbg.location(238,36);
							match(input,78,FOLLOW_78_in_type3218); if (state.failed) return;
							}
							break;

						default :
							break loop91;
						}
					}
					} finally {dbg.exitSubRule(91);}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:239:8: 'block' blockLiteral
					{
					dbg.location(239,8);
					match(input,86,FOLLOW_86_in_type3231); if (state.failed) return;dbg.location(239,16);
					pushFollow(FOLLOW_blockLiteral_in_type3233);
					blockLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 65, type_StartIndex); }

		}
		dbg.location(240, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "type");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "type"



	// $ANTLR start "classOrInterfaceType"
	// GosuProg.g:242:1: classOrInterfaceType : idclassOrInterfaceType typeArguments ( '.' id typeArguments )* ;
	public final void classOrInterfaceType() throws RecognitionException {
		int classOrInterfaceType_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "classOrInterfaceType");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(242, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 66) ) { return; }

			// GosuProg.g:243:5: ( idclassOrInterfaceType typeArguments ( '.' id typeArguments )* )
			dbg.enterAlt(1);

			// GosuProg.g:243:9: idclassOrInterfaceType typeArguments ( '.' id typeArguments )*
			{
			dbg.location(243,9);
			pushFollow(FOLLOW_idclassOrInterfaceType_in_classOrInterfaceType3252);
			idclassOrInterfaceType();
			state._fsp--;
			if (state.failed) return;dbg.location(243,32);
			pushFollow(FOLLOW_typeArguments_in_classOrInterfaceType3254);
			typeArguments();
			state._fsp--;
			if (state.failed) return;dbg.location(243,46);
			// GosuProg.g:243:46: ( '.' id typeArguments )*
			try { dbg.enterSubRule(93);

			loop93:
			while (true) {
				int alt93=2;
				try { dbg.enterDecision(93, decisionCanBacktrack[93]);

				int LA93_0 = input.LA(1);
				if ( (LA93_0==51) ) {
					int LA93_2 = input.LA(2);
					if ( (LA93_2==Ident||(LA93_2 >= 74 && LA93_2 <= 75)||LA93_2==81||(LA93_2 >= 83 && LA93_2 <= 86)||LA93_2==91||LA93_2==93||LA93_2==99||(LA93_2 >= 102 && LA93_2 <= 104)||(LA93_2 >= 106 && LA93_2 <= 107)||LA93_2==109||(LA93_2 >= 113 && LA93_2 <= 114)||LA93_2==118||(LA93_2 >= 120 && LA93_2 <= 122)||LA93_2==125||LA93_2==127||LA93_2==130||(LA93_2 >= 132 && LA93_2 <= 134)||LA93_2==136||(LA93_2 >= 138 && LA93_2 <= 141)||LA93_2==149||LA93_2==153||(LA93_2 >= 159 && LA93_2 <= 160)) ) {
						int LA93_3 = input.LA(3);
						if ( (synpred143_GosuProg()) ) {
							alt93=1;
						}

					}

				}

				} finally {dbg.exitDecision(93);}

				switch (alt93) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:243:47: '.' id typeArguments
					{
					dbg.location(243,47);
					match(input,51,FOLLOW_51_in_classOrInterfaceType3257); if (state.failed) return;dbg.location(243,51);
					pushFollow(FOLLOW_id_in_classOrInterfaceType3259);
					id();
					state._fsp--;
					if (state.failed) return;dbg.location(243,54);
					pushFollow(FOLLOW_typeArguments_in_classOrInterfaceType3261);
					typeArguments();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop93;
				}
			}
			} finally {dbg.exitSubRule(93);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 66, classOrInterfaceType_StartIndex); }

		}
		dbg.location(244, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "classOrInterfaceType");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "classOrInterfaceType"



	// $ANTLR start "typeArguments"
	// GosuProg.g:246:1: typeArguments : ( '<' typeArgument ( ',' typeArgument )* '>' )? ;
	public final void typeArguments() throws RecognitionException {
		int typeArguments_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeArguments");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(246, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 67) ) { return; }

			// GosuProg.g:247:5: ( ( '<' typeArgument ( ',' typeArgument )* '>' )? )
			dbg.enterAlt(1);

			// GosuProg.g:247:9: ( '<' typeArgument ( ',' typeArgument )* '>' )?
			{
			dbg.location(247,9);
			// GosuProg.g:247:9: ( '<' typeArgument ( ',' typeArgument )* '>' )?
			int alt95=2;
			try { dbg.enterSubRule(95);
			try { dbg.enterDecision(95, decisionCanBacktrack[95]);

			try {
				isCyclicDecision = true;
				alt95 = dfa95.predict(input);
			}
			catch (NoViableAltException nvae) {
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(95);}

			switch (alt95) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:247:10: '<' typeArgument ( ',' typeArgument )* '>'
					{
					dbg.location(247,10);
					match(input,58,FOLLOW_58_in_typeArguments3284); if (state.failed) return;dbg.location(247,14);
					pushFollow(FOLLOW_typeArgument_in_typeArguments3286);
					typeArgument();
					state._fsp--;
					if (state.failed) return;dbg.location(247,27);
					// GosuProg.g:247:27: ( ',' typeArgument )*
					try { dbg.enterSubRule(94);

					loop94:
					while (true) {
						int alt94=2;
						try { dbg.enterDecision(94, decisionCanBacktrack[94]);

						int LA94_0 = input.LA(1);
						if ( (LA94_0==46) ) {
							alt94=1;
						}

						} finally {dbg.exitDecision(94);}

						switch (alt94) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:247:28: ',' typeArgument
							{
							dbg.location(247,28);
							match(input,46,FOLLOW_46_in_typeArguments3289); if (state.failed) return;dbg.location(247,32);
							pushFollow(FOLLOW_typeArgument_in_typeArguments3291);
							typeArgument();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop94;
						}
					}
					} finally {dbg.exitSubRule(94);}
					dbg.location(247,47);
					match(input,63,FOLLOW_63_in_typeArguments3295); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(95);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 67, typeArguments_StartIndex); }

		}
		dbg.location(248, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeArguments");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeArguments"



	// $ANTLR start "typeArgument"
	// GosuProg.g:250:1: typeArgument : ( typeLiteralType | '?' ( ( 'extends' | 'super' ) typeLiteralType )? );
	public final void typeArgument() throws RecognitionException {
		int typeArgument_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeArgument");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(250, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 68) ) { return; }

			// GosuProg.g:251:5: ( typeLiteralType | '?' ( ( 'extends' | 'super' ) typeLiteralType )? )
			int alt97=2;
			try { dbg.enterDecision(97, decisionCanBacktrack[97]);

			int LA97_0 = input.LA(1);
			if ( (LA97_0==Ident||(LA97_0 >= 74 && LA97_0 <= 75)||LA97_0==81||(LA97_0 >= 83 && LA97_0 <= 86)||LA97_0==91||LA97_0==93||LA97_0==99||(LA97_0 >= 102 && LA97_0 <= 104)||(LA97_0 >= 106 && LA97_0 <= 107)||LA97_0==109||(LA97_0 >= 113 && LA97_0 <= 114)||LA97_0==118||(LA97_0 >= 120 && LA97_0 <= 122)||LA97_0==125||LA97_0==127||LA97_0==130||(LA97_0 >= 132 && LA97_0 <= 134)||LA97_0==136||(LA97_0 >= 138 && LA97_0 <= 141)||LA97_0==149||LA97_0==153||(LA97_0 >= 159 && LA97_0 <= 160)) ) {
				alt97=1;
			}
			else if ( (LA97_0==65) ) {
				alt97=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 97, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(97);}

			switch (alt97) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:251:9: typeLiteralType
					{
					dbg.location(251,9);
					pushFollow(FOLLOW_typeLiteralType_in_typeArgument3316);
					typeLiteralType();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:251:27: '?' ( ( 'extends' | 'super' ) typeLiteralType )?
					{
					dbg.location(251,27);
					match(input,65,FOLLOW_65_in_typeArgument3320); if (state.failed) return;dbg.location(251,31);
					// GosuProg.g:251:31: ( ( 'extends' | 'super' ) typeLiteralType )?
					int alt96=2;
					try { dbg.enterSubRule(96);
					try { dbg.enterDecision(96, decisionCanBacktrack[96]);

					int LA96_0 = input.LA(1);
					if ( (LA96_0==105||LA96_0==144) ) {
						alt96=1;
					}
					} finally {dbg.exitDecision(96);}

					switch (alt96) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:251:32: ( 'extends' | 'super' ) typeLiteralType
							{
							dbg.location(251,32);
							if ( input.LA(1)==105||input.LA(1)==144 ) {
								input.consume();
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								dbg.recognitionException(mse);
								throw mse;
							}dbg.location(251,54);
							pushFollow(FOLLOW_typeLiteralType_in_typeArgument3331);
							typeLiteralType();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(96);}

					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 68, typeArgument_StartIndex); }

		}
		dbg.location(252, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeArgument");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeArgument"



	// $ANTLR start "expression"
	// GosuProg.g:254:1: expression : conditionalExpr ;
	public final void expression() throws RecognitionException {
		int expression_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "expression");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(254, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 69) ) { return; }

			// GosuProg.g:254:12: ( conditionalExpr )
			dbg.enterAlt(1);

			// GosuProg.g:254:14: conditionalExpr
			{
			dbg.location(254,14);
			pushFollow(FOLLOW_conditionalExpr_in_expression3346);
			conditionalExpr();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 69, expression_StartIndex); }

		}
		dbg.location(254, 30);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expression");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "expression"



	// $ANTLR start "conditionalExpr"
	// GosuProg.g:256:1: conditionalExpr : conditionalOrExpr ( '?' conditionalExpr ':' conditionalExpr | '?:' conditionalExpr )? ;
	public final void conditionalExpr() throws RecognitionException {
		int conditionalExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "conditionalExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(256, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 70) ) { return; }

			// GosuProg.g:256:17: ( conditionalOrExpr ( '?' conditionalExpr ':' conditionalExpr | '?:' conditionalExpr )? )
			dbg.enterAlt(1);

			// GosuProg.g:256:19: conditionalOrExpr ( '?' conditionalExpr ':' conditionalExpr | '?:' conditionalExpr )?
			{
			dbg.location(256,19);
			pushFollow(FOLLOW_conditionalOrExpr_in_conditionalExpr3356);
			conditionalOrExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(256,37);
			// GosuProg.g:256:37: ( '?' conditionalExpr ':' conditionalExpr | '?:' conditionalExpr )?
			int alt98=3;
			try { dbg.enterSubRule(98);
			try { dbg.enterDecision(98, decisionCanBacktrack[98]);

			int LA98_0 = input.LA(1);
			if ( (LA98_0==65) ) {
				int LA98_1 = input.LA(2);
				if ( (synpred149_GosuProg()) ) {
					alt98=1;
				}
			}
			else if ( (LA98_0==71) ) {
				int LA98_2 = input.LA(2);
				if ( (synpred150_GosuProg()) ) {
					alt98=2;
				}
			}
			} finally {dbg.exitDecision(98);}

			switch (alt98) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:256:38: '?' conditionalExpr ':' conditionalExpr
					{
					dbg.location(256,38);
					match(input,65,FOLLOW_65_in_conditionalExpr3359); if (state.failed) return;dbg.location(256,42);
					pushFollow(FOLLOW_conditionalExpr_in_conditionalExpr3361);
					conditionalExpr();
					state._fsp--;
					if (state.failed) return;dbg.location(256,58);
					match(input,56,FOLLOW_56_in_conditionalExpr3363); if (state.failed) return;dbg.location(256,62);
					pushFollow(FOLLOW_conditionalExpr_in_conditionalExpr3365);
					conditionalExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:256:80: '?:' conditionalExpr
					{
					dbg.location(256,80);
					match(input,71,FOLLOW_71_in_conditionalExpr3369); if (state.failed) return;dbg.location(256,85);
					pushFollow(FOLLOW_conditionalExpr_in_conditionalExpr3371);
					conditionalExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(98);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 70, conditionalExpr_StartIndex); }

		}
		dbg.location(256, 101);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "conditionalExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "conditionalExpr"



	// $ANTLR start "conditionalOrExpr"
	// GosuProg.g:258:1: conditionalOrExpr : conditionalAndExpr ( orOp conditionalAndExpr )* ;
	public final void conditionalOrExpr() throws RecognitionException {
		int conditionalOrExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "conditionalOrExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(258, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 71) ) { return; }

			// GosuProg.g:258:19: ( conditionalAndExpr ( orOp conditionalAndExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:258:21: conditionalAndExpr ( orOp conditionalAndExpr )*
			{
			dbg.location(258,21);
			pushFollow(FOLLOW_conditionalAndExpr_in_conditionalOrExpr3381);
			conditionalAndExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(258,40);
			// GosuProg.g:258:40: ( orOp conditionalAndExpr )*
			try { dbg.enterSubRule(99);

			loop99:
			while (true) {
				int alt99=2;
				try { dbg.enterDecision(99, decisionCanBacktrack[99]);

				int LA99_0 = input.LA(1);
				if ( (LA99_0==126||LA99_0==167) ) {
					int LA99_2 = input.LA(2);
					if ( (synpred151_GosuProg()) ) {
						alt99=1;
					}

				}

				} finally {dbg.exitDecision(99);}

				switch (alt99) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:258:41: orOp conditionalAndExpr
					{
					dbg.location(258,41);
					pushFollow(FOLLOW_orOp_in_conditionalOrExpr3384);
					orOp();
					state._fsp--;
					if (state.failed) return;dbg.location(258,46);
					pushFollow(FOLLOW_conditionalAndExpr_in_conditionalOrExpr3386);
					conditionalAndExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop99;
				}
			}
			} finally {dbg.exitSubRule(99);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 71, conditionalOrExpr_StartIndex); }

		}
		dbg.location(258, 66);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "conditionalOrExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "conditionalOrExpr"



	// $ANTLR start "conditionalAndExpr"
	// GosuProg.g:260:1: conditionalAndExpr : bitwiseOrExpr ( andOp bitwiseOrExpr )* ;
	public final void conditionalAndExpr() throws RecognitionException {
		int conditionalAndExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "conditionalAndExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(260, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 72) ) { return; }

			// GosuProg.g:260:20: ( bitwiseOrExpr ( andOp bitwiseOrExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:260:22: bitwiseOrExpr ( andOp bitwiseOrExpr )*
			{
			dbg.location(260,22);
			pushFollow(FOLLOW_bitwiseOrExpr_in_conditionalAndExpr3397);
			bitwiseOrExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(260,36);
			// GosuProg.g:260:36: ( andOp bitwiseOrExpr )*
			try { dbg.enterSubRule(100);

			loop100:
			while (true) {
				int alt100=2;
				try { dbg.enterDecision(100, decisionCanBacktrack[100]);

				int LA100_0 = input.LA(1);
				if ( (LA100_0==34||LA100_0==82) ) {
					int LA100_2 = input.LA(2);
					if ( (synpred152_GosuProg()) ) {
						alt100=1;
					}

				}

				} finally {dbg.exitDecision(100);}

				switch (alt100) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:260:37: andOp bitwiseOrExpr
					{
					dbg.location(260,37);
					pushFollow(FOLLOW_andOp_in_conditionalAndExpr3400);
					andOp();
					state._fsp--;
					if (state.failed) return;dbg.location(260,43);
					pushFollow(FOLLOW_bitwiseOrExpr_in_conditionalAndExpr3402);
					bitwiseOrExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop100;
				}
			}
			} finally {dbg.exitSubRule(100);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 72, conditionalAndExpr_StartIndex); }

		}
		dbg.location(260, 58);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "conditionalAndExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "conditionalAndExpr"



	// $ANTLR start "bitwiseOrExpr"
	// GosuProg.g:262:1: bitwiseOrExpr : bitwiseXorExpr ( '|' bitwiseXorExpr )* ;
	public final void bitwiseOrExpr() throws RecognitionException {
		int bitwiseOrExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "bitwiseOrExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(262, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 73) ) { return; }

			// GosuProg.g:262:15: ( bitwiseXorExpr ( '|' bitwiseXorExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:262:17: bitwiseXorExpr ( '|' bitwiseXorExpr )*
			{
			dbg.location(262,17);
			pushFollow(FOLLOW_bitwiseXorExpr_in_bitwiseOrExpr3413);
			bitwiseXorExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(262,32);
			// GosuProg.g:262:32: ( '|' bitwiseXorExpr )*
			try { dbg.enterSubRule(101);

			loop101:
			while (true) {
				int alt101=2;
				try { dbg.enterDecision(101, decisionCanBacktrack[101]);

				int LA101_0 = input.LA(1);
				if ( (LA101_0==163) ) {
					int LA101_2 = input.LA(2);
					if ( (synpred153_GosuProg()) ) {
						alt101=1;
					}

				}

				} finally {dbg.exitDecision(101);}

				switch (alt101) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:262:33: '|' bitwiseXorExpr
					{
					dbg.location(262,33);
					match(input,163,FOLLOW_163_in_bitwiseOrExpr3416); if (state.failed) return;dbg.location(262,37);
					pushFollow(FOLLOW_bitwiseXorExpr_in_bitwiseOrExpr3418);
					bitwiseXorExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop101;
				}
			}
			} finally {dbg.exitSubRule(101);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 73, bitwiseOrExpr_StartIndex); }

		}
		dbg.location(262, 53);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitwiseOrExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "bitwiseOrExpr"



	// $ANTLR start "bitwiseXorExpr"
	// GosuProg.g:264:1: bitwiseXorExpr : bitwiseAndExpr ( '^' bitwiseAndExpr )* ;
	public final void bitwiseXorExpr() throws RecognitionException {
		int bitwiseXorExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "bitwiseXorExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(264, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 74) ) { return; }

			// GosuProg.g:264:16: ( bitwiseAndExpr ( '^' bitwiseAndExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:264:18: bitwiseAndExpr ( '^' bitwiseAndExpr )*
			{
			dbg.location(264,18);
			pushFollow(FOLLOW_bitwiseAndExpr_in_bitwiseXorExpr3429);
			bitwiseAndExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(264,33);
			// GosuProg.g:264:33: ( '^' bitwiseAndExpr )*
			try { dbg.enterSubRule(102);

			loop102:
			while (true) {
				int alt102=2;
				try { dbg.enterDecision(102, decisionCanBacktrack[102]);

				int LA102_0 = input.LA(1);
				if ( (LA102_0==79) ) {
					int LA102_2 = input.LA(2);
					if ( (synpred154_GosuProg()) ) {
						alt102=1;
					}

				}

				} finally {dbg.exitDecision(102);}

				switch (alt102) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:264:34: '^' bitwiseAndExpr
					{
					dbg.location(264,34);
					match(input,79,FOLLOW_79_in_bitwiseXorExpr3432); if (state.failed) return;dbg.location(264,38);
					pushFollow(FOLLOW_bitwiseAndExpr_in_bitwiseXorExpr3434);
					bitwiseAndExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop102;
				}
			}
			} finally {dbg.exitSubRule(102);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 74, bitwiseXorExpr_StartIndex); }

		}
		dbg.location(264, 54);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitwiseXorExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "bitwiseXorExpr"



	// $ANTLR start "bitwiseAndExpr"
	// GosuProg.g:266:1: bitwiseAndExpr : equalityExpr ( '&' equalityExpr )* ;
	public final void bitwiseAndExpr() throws RecognitionException {
		int bitwiseAndExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "bitwiseAndExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(266, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 75) ) { return; }

			// GosuProg.g:266:16: ( equalityExpr ( '&' equalityExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:266:18: equalityExpr ( '&' equalityExpr )*
			{
			dbg.location(266,18);
			pushFollow(FOLLOW_equalityExpr_in_bitwiseAndExpr3445);
			equalityExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(266,31);
			// GosuProg.g:266:31: ( '&' equalityExpr )*
			try { dbg.enterSubRule(103);

			loop103:
			while (true) {
				int alt103=2;
				try { dbg.enterDecision(103, decisionCanBacktrack[103]);

				int LA103_0 = input.LA(1);
				if ( (LA103_0==36) ) {
					int LA103_2 = input.LA(2);
					if ( (synpred155_GosuProg()) ) {
						alt103=1;
					}

				}

				} finally {dbg.exitDecision(103);}

				switch (alt103) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:266:32: '&' equalityExpr
					{
					dbg.location(266,32);
					match(input,36,FOLLOW_36_in_bitwiseAndExpr3448); if (state.failed) return;dbg.location(266,36);
					pushFollow(FOLLOW_equalityExpr_in_bitwiseAndExpr3450);
					equalityExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop103;
				}
			}
			} finally {dbg.exitSubRule(103);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 75, bitwiseAndExpr_StartIndex); }

		}
		dbg.location(266, 50);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitwiseAndExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "bitwiseAndExpr"



	// $ANTLR start "equalityExpr"
	// GosuProg.g:268:1: equalityExpr : relationalExpr ( equalityOp relationalExpr )* ;
	public final void equalityExpr() throws RecognitionException {
		int equalityExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "equalityExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(268, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 76) ) { return; }

			// GosuProg.g:268:14: ( relationalExpr ( equalityOp relationalExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:268:16: relationalExpr ( equalityOp relationalExpr )*
			{
			dbg.location(268,16);
			pushFollow(FOLLOW_relationalExpr_in_equalityExpr3461);
			relationalExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(268,31);
			// GosuProg.g:268:31: ( equalityOp relationalExpr )*
			try { dbg.enterSubRule(104);

			loop104:
			while (true) {
				int alt104=2;
				try { dbg.enterDecision(104, decisionCanBacktrack[104]);

				int LA104_0 = input.LA(1);
				if ( ((LA104_0 >= 29 && LA104_0 <= 30)||LA104_0==59||(LA104_0 >= 61 && LA104_0 <= 62)) ) {
					int LA104_2 = input.LA(2);
					if ( (synpred156_GosuProg()) ) {
						alt104=1;
					}

				}

				} finally {dbg.exitDecision(104);}

				switch (alt104) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:268:32: equalityOp relationalExpr
					{
					dbg.location(268,32);
					pushFollow(FOLLOW_equalityOp_in_equalityExpr3464);
					equalityOp();
					state._fsp--;
					if (state.failed) return;dbg.location(268,43);
					pushFollow(FOLLOW_relationalExpr_in_equalityExpr3466);
					relationalExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop104;
				}
			}
			} finally {dbg.exitSubRule(104);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 76, equalityExpr_StartIndex); }

		}
		dbg.location(268, 59);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "equalityExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "equalityExpr"



	// $ANTLR start "relationalExpr"
	// GosuProg.g:270:1: relationalExpr : intervalExpr ( relOp intervalExpr | 'typeis' typeLiteralType )* ;
	public final void relationalExpr() throws RecognitionException {
		int relationalExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "relationalExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(270, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 77) ) { return; }

			// GosuProg.g:270:16: ( intervalExpr ( relOp intervalExpr | 'typeis' typeLiteralType )* )
			dbg.enterAlt(1);

			// GosuProg.g:270:18: intervalExpr ( relOp intervalExpr | 'typeis' typeLiteralType )*
			{
			dbg.location(270,18);
			pushFollow(FOLLOW_intervalExpr_in_relationalExpr3477);
			intervalExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(270,31);
			// GosuProg.g:270:31: ( relOp intervalExpr | 'typeis' typeLiteralType )*
			try { dbg.enterSubRule(105);

			loop105:
			while (true) {
				int alt105=3;
				try { dbg.enterDecision(105, decisionCanBacktrack[105]);

				switch ( input.LA(1) ) {
				case 58:
					{
					int LA105_2 = input.LA(2);
					if ( (synpred157_GosuProg()) ) {
						alt105=1;
					}

					}
					break;
				case 63:
					{
					int LA105_3 = input.LA(2);
					if ( (synpred157_GosuProg()) ) {
						alt105=1;
					}

					}
					break;
				case 152:
					{
					int LA105_4 = input.LA(2);
					if ( (synpred158_GosuProg()) ) {
						alt105=2;
					}

					}
					break;
				}
				} finally {dbg.exitDecision(105);}

				switch (alt105) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:270:32: relOp intervalExpr
					{
					dbg.location(270,32);
					pushFollow(FOLLOW_relOp_in_relationalExpr3480);
					relOp();
					state._fsp--;
					if (state.failed) return;dbg.location(270,38);
					pushFollow(FOLLOW_intervalExpr_in_relationalExpr3482);
					intervalExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:270:53: 'typeis' typeLiteralType
					{
					dbg.location(270,53);
					match(input,152,FOLLOW_152_in_relationalExpr3486); if (state.failed) return;dbg.location(270,62);
					pushFollow(FOLLOW_typeLiteralType_in_relationalExpr3488);
					typeLiteralType();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop105;
				}
			}
			} finally {dbg.exitSubRule(105);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 77, relationalExpr_StartIndex); }

		}
		dbg.location(270, 80);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "relationalExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "relationalExpr"



	// $ANTLR start "intervalExpr"
	// GosuProg.g:272:1: intervalExpr : bitshiftExpr ( intervalOp bitshiftExpr )? ;
	public final void intervalExpr() throws RecognitionException {
		int intervalExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "intervalExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(272, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 78) ) { return; }

			// GosuProg.g:272:14: ( bitshiftExpr ( intervalOp bitshiftExpr )? )
			dbg.enterAlt(1);

			// GosuProg.g:272:16: bitshiftExpr ( intervalOp bitshiftExpr )?
			{
			dbg.location(272,16);
			pushFollow(FOLLOW_bitshiftExpr_in_intervalExpr3500);
			bitshiftExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(272,29);
			// GosuProg.g:272:29: ( intervalOp bitshiftExpr )?
			int alt106=2;
			try { dbg.enterSubRule(106);
			try { dbg.enterDecision(106, decisionCanBacktrack[106]);

			int LA106_0 = input.LA(1);
			if ( ((LA106_0 >= 52 && LA106_0 <= 53)||(LA106_0 >= 164 && LA106_0 <= 165)) ) {
				int LA106_1 = input.LA(2);
				if ( (synpred159_GosuProg()) ) {
					alt106=1;
				}
			}
			} finally {dbg.exitDecision(106);}

			switch (alt106) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:272:30: intervalOp bitshiftExpr
					{
					dbg.location(272,30);
					pushFollow(FOLLOW_intervalOp_in_intervalExpr3503);
					intervalOp();
					state._fsp--;
					if (state.failed) return;dbg.location(272,41);
					pushFollow(FOLLOW_bitshiftExpr_in_intervalExpr3505);
					bitshiftExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(106);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 78, intervalExpr_StartIndex); }

		}
		dbg.location(272, 55);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "intervalExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "intervalExpr"



	// $ANTLR start "bitshiftExpr"
	// GosuProg.g:274:1: bitshiftExpr : additiveExpr ( bitshiftOp additiveExpr )* ;
	public final void bitshiftExpr() throws RecognitionException {
		int bitshiftExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "bitshiftExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(274, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 79) ) { return; }

			// GosuProg.g:274:14: ( additiveExpr ( bitshiftOp additiveExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:274:16: additiveExpr ( bitshiftOp additiveExpr )*
			{
			dbg.location(274,16);
			pushFollow(FOLLOW_additiveExpr_in_bitshiftExpr3516);
			additiveExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(274,29);
			// GosuProg.g:274:29: ( bitshiftOp additiveExpr )*
			try { dbg.enterSubRule(107);

			loop107:
			while (true) {
				int alt107=2;
				try { dbg.enterDecision(107, decisionCanBacktrack[107]);

				int LA107_0 = input.LA(1);
				if ( (LA107_0==58) ) {
					int LA107_2 = input.LA(2);
					if ( (synpred160_GosuProg()) ) {
						alt107=1;
					}

				}
				else if ( (LA107_0==63) ) {
					int LA107_3 = input.LA(2);
					if ( (synpred160_GosuProg()) ) {
						alt107=1;
					}

				}

				} finally {dbg.exitDecision(107);}

				switch (alt107) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:274:30: bitshiftOp additiveExpr
					{
					dbg.location(274,30);
					pushFollow(FOLLOW_bitshiftOp_in_bitshiftExpr3519);
					bitshiftOp();
					state._fsp--;
					if (state.failed) return;dbg.location(274,41);
					pushFollow(FOLLOW_additiveExpr_in_bitshiftExpr3521);
					additiveExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop107;
				}
			}
			} finally {dbg.exitSubRule(107);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 79, bitshiftExpr_StartIndex); }

		}
		dbg.location(274, 56);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitshiftExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "bitshiftExpr"



	// $ANTLR start "additiveExpr"
	// GosuProg.g:276:1: additiveExpr : multiplicativeExpr ( additiveOp multiplicativeExpr )* ;
	public final void additiveExpr() throws RecognitionException {
		int additiveExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "additiveExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(276, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 80) ) { return; }

			// GosuProg.g:276:14: ( multiplicativeExpr ( additiveOp multiplicativeExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:276:16: multiplicativeExpr ( additiveOp multiplicativeExpr )*
			{
			dbg.location(276,16);
			pushFollow(FOLLOW_multiplicativeExpr_in_additiveExpr3533);
			multiplicativeExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(276,35);
			// GosuProg.g:276:35: ( additiveOp multiplicativeExpr )*
			try { dbg.enterSubRule(108);

			loop108:
			while (true) {
				int alt108=2;
				try { dbg.enterDecision(108, decisionCanBacktrack[108]);

				int LA108_0 = input.LA(1);
				if ( ((LA108_0 >= 27 && LA108_0 <= 28)||LA108_0==43||LA108_0==47||(LA108_0 >= 67 && LA108_0 <= 68)) ) {
					int LA108_2 = input.LA(2);
					if ( (synpred161_GosuProg()) ) {
						alt108=1;
					}

				}

				} finally {dbg.exitDecision(108);}

				switch (alt108) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:276:36: additiveOp multiplicativeExpr
					{
					dbg.location(276,36);
					pushFollow(FOLLOW_additiveOp_in_additiveExpr3536);
					additiveOp();
					state._fsp--;
					if (state.failed) return;dbg.location(276,47);
					pushFollow(FOLLOW_multiplicativeExpr_in_additiveExpr3538);
					multiplicativeExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop108;
				}
			}
			} finally {dbg.exitSubRule(108);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 80, additiveExpr_StartIndex); }

		}
		dbg.location(276, 67);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "additiveExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "additiveExpr"



	// $ANTLR start "multiplicativeExpr"
	// GosuProg.g:278:1: multiplicativeExpr : typeAsExpr ( multiplicativeOp typeAsExpr )* ;
	public final void multiplicativeExpr() throws RecognitionException {
		int multiplicativeExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "multiplicativeExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(278, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 81) ) { return; }

			// GosuProg.g:278:20: ( typeAsExpr ( multiplicativeOp typeAsExpr )* )
			dbg.enterAlt(1);

			// GosuProg.g:278:22: typeAsExpr ( multiplicativeOp typeAsExpr )*
			{
			dbg.location(278,22);
			pushFollow(FOLLOW_typeAsExpr_in_multiplicativeExpr3549);
			typeAsExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(278,33);
			// GosuProg.g:278:33: ( multiplicativeOp typeAsExpr )*
			try { dbg.enterSubRule(109);

			loop109:
			while (true) {
				int alt109=2;
				try { dbg.enterDecision(109, decisionCanBacktrack[109]);

				int LA109_0 = input.LA(1);
				if ( (LA109_0==26||LA109_0==32||LA109_0==40||LA109_0==54||LA109_0==64||LA109_0==66||LA109_0==70) ) {
					int LA109_2 = input.LA(2);
					if ( (synpred162_GosuProg()) ) {
						alt109=1;
					}

				}

				} finally {dbg.exitDecision(109);}

				switch (alt109) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:278:34: multiplicativeOp typeAsExpr
					{
					dbg.location(278,34);
					pushFollow(FOLLOW_multiplicativeOp_in_multiplicativeExpr3552);
					multiplicativeOp();
					state._fsp--;
					if (state.failed) return;dbg.location(278,51);
					pushFollow(FOLLOW_typeAsExpr_in_multiplicativeExpr3554);
					typeAsExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop109;
				}
			}
			} finally {dbg.exitSubRule(109);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 81, multiplicativeExpr_StartIndex); }

		}
		dbg.location(278, 63);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multiplicativeExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "multiplicativeExpr"



	// $ANTLR start "typeAsExpr"
	// GosuProg.g:280:1: typeAsExpr : unaryExpr ( typeAsOp typeLiteral )* ;
	public final void typeAsExpr() throws RecognitionException {
		int typeAsExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeAsExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(280, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 82) ) { return; }

			// GosuProg.g:280:12: ( unaryExpr ( typeAsOp typeLiteral )* )
			dbg.enterAlt(1);

			// GosuProg.g:280:14: unaryExpr ( typeAsOp typeLiteral )*
			{
			dbg.location(280,14);
			pushFollow(FOLLOW_unaryExpr_in_typeAsExpr3565);
			unaryExpr();
			state._fsp--;
			if (state.failed) return;dbg.location(280,24);
			// GosuProg.g:280:24: ( typeAsOp typeLiteral )*
			try { dbg.enterSubRule(110);

			loop110:
			while (true) {
				int alt110=2;
				try { dbg.enterDecision(110, decisionCanBacktrack[110]);

				int LA110_0 = input.LA(1);
				if ( (LA110_0==84) ) {
					int LA110_2 = input.LA(2);
					if ( (synpred163_GosuProg()) ) {
						alt110=1;
					}

				}
				else if ( (LA110_0==151) ) {
					int LA110_3 = input.LA(2);
					if ( (synpred163_GosuProg()) ) {
						alt110=1;
					}

				}

				} finally {dbg.exitDecision(110);}

				switch (alt110) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:280:25: typeAsOp typeLiteral
					{
					dbg.location(280,25);
					pushFollow(FOLLOW_typeAsOp_in_typeAsExpr3568);
					typeAsOp();
					state._fsp--;
					if (state.failed) return;dbg.location(280,34);
					pushFollow(FOLLOW_typeLiteral_in_typeAsExpr3570);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop110;
				}
			}
			} finally {dbg.exitSubRule(110);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 82, typeAsExpr_StartIndex); }

		}
		dbg.location(280, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeAsExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeAsExpr"



	// $ANTLR start "unaryExpr"
	// GosuProg.g:282:1: unaryExpr : ( ( '+' | '-' | '!-' ) unaryExprNotPlusMinus | unaryExprNotPlusMinus );
	public final void unaryExpr() throws RecognitionException {
		int unaryExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "unaryExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(282, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 83) ) { return; }

			// GosuProg.g:282:11: ( ( '+' | '-' | '!-' ) unaryExprNotPlusMinus | unaryExprNotPlusMinus )
			int alt111=2;
			try { dbg.enterDecision(111, decisionCanBacktrack[111]);

			int LA111_0 = input.LA(1);
			if ( (LA111_0==28||LA111_0==43||LA111_0==47) ) {
				alt111=1;
			}
			else if ( (LA111_0==CharLiteral||LA111_0==Ident||LA111_0==NumberLiteral||LA111_0==StringLiteral||LA111_0==25||LA111_0==31||LA111_0==38||(LA111_0 >= 74 && LA111_0 <= 75)||LA111_0==77||LA111_0==81||(LA111_0 >= 83 && LA111_0 <= 86)||LA111_0==91||LA111_0==93||LA111_0==99||(LA111_0 >= 101 && LA111_0 <= 104)||(LA111_0 >= 106 && LA111_0 <= 107)||LA111_0==109||(LA111_0 >= 113 && LA111_0 <= 114)||LA111_0==118||(LA111_0 >= 120 && LA111_0 <= 125)||LA111_0==127||LA111_0==130||(LA111_0 >= 132 && LA111_0 <= 134)||LA111_0==136||(LA111_0 >= 138 && LA111_0 <= 142)||LA111_0==144||LA111_0==146||LA111_0==149||(LA111_0 >= 153 && LA111_0 <= 154)||(LA111_0 >= 159 && LA111_0 <= 160)||LA111_0==162||LA111_0==170) ) {
				alt111=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 111, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(111);}

			switch (alt111) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:282:14: ( '+' | '-' | '!-' ) unaryExprNotPlusMinus
					{
					dbg.location(282,14);
					if ( input.LA(1)==28||input.LA(1)==43||input.LA(1)==47 ) {
						input.consume();
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						dbg.recognitionException(mse);
						throw mse;
					}dbg.location(282,33);
					pushFollow(FOLLOW_unaryExprNotPlusMinus_in_unaryExpr3594);
					unaryExprNotPlusMinus();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:282:57: unaryExprNotPlusMinus
					{
					dbg.location(282,57);
					pushFollow(FOLLOW_unaryExprNotPlusMinus_in_unaryExpr3598);
					unaryExprNotPlusMinus();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 83, unaryExpr_StartIndex); }

		}
		dbg.location(282, 78);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "unaryExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "unaryExpr"



	// $ANTLR start "unaryExprNotPlusMinus"
	// GosuProg.g:284:1: unaryExprNotPlusMinus : ( unaryOp unaryExpr | '\\\\' blockExpr | evalExpr | primaryExpr );
	public final void unaryExprNotPlusMinus() throws RecognitionException {
		int unaryExprNotPlusMinus_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "unaryExprNotPlusMinus");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(284, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 84) ) { return; }

			// GosuProg.g:284:23: ( unaryOp unaryExpr | '\\\\' blockExpr | evalExpr | primaryExpr )
			int alt112=4;
			try { dbg.enterDecision(112, decisionCanBacktrack[112]);

			switch ( input.LA(1) ) {
			case 25:
			case 124:
			case 142:
			case 154:
			case 170:
				{
				alt112=1;
				}
				break;
			case 77:
				{
				alt112=2;
				}
				break;
			case 101:
				{
				alt112=3;
				}
				break;
			case CharLiteral:
			case Ident:
			case NumberLiteral:
			case StringLiteral:
			case 31:
			case 38:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 86:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 106:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 123:
			case 125:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 144:
			case 146:
			case 149:
			case 153:
			case 159:
			case 160:
			case 162:
				{
				alt112=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 112, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(112);}

			switch (alt112) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:284:26: unaryOp unaryExpr
					{
					dbg.location(284,26);
					pushFollow(FOLLOW_unaryOp_in_unaryExprNotPlusMinus3608);
					unaryOp();
					state._fsp--;
					if (state.failed) return;dbg.location(284,34);
					pushFollow(FOLLOW_unaryExpr_in_unaryExprNotPlusMinus3610);
					unaryExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:285:26: '\\\\' blockExpr
					{
					dbg.location(285,26);
					match(input,77,FOLLOW_77_in_unaryExprNotPlusMinus3639); if (state.failed) return;dbg.location(285,31);
					pushFollow(FOLLOW_blockExpr_in_unaryExprNotPlusMinus3641);
					blockExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:286:26: evalExpr
					{
					dbg.location(286,26);
					pushFollow(FOLLOW_evalExpr_in_unaryExprNotPlusMinus3673);
					evalExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:287:26: primaryExpr
					{
					dbg.location(287,26);
					pushFollow(FOLLOW_primaryExpr_in_unaryExprNotPlusMinus3711);
					primaryExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 84, unaryExprNotPlusMinus_StartIndex); }

		}
		dbg.location(288, 22);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "unaryExprNotPlusMinus");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "unaryExprNotPlusMinus"



	// $ANTLR start "blockExpr"
	// GosuProg.g:290:1: blockExpr : ( parameterDeclarationList )? '->' ( expression | statementBlock ) ;
	public final void blockExpr() throws RecognitionException {
		int blockExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "blockExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(290, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 85) ) { return; }

			// GosuProg.g:290:11: ( ( parameterDeclarationList )? '->' ( expression | statementBlock ) )
			dbg.enterAlt(1);

			// GosuProg.g:290:13: ( parameterDeclarationList )? '->' ( expression | statementBlock )
			{
			dbg.location(290,13);
			// GosuProg.g:290:13: ( parameterDeclarationList )?
			int alt113=2;
			try { dbg.enterSubRule(113);
			try { dbg.enterDecision(113, decisionCanBacktrack[113]);

			int LA113_0 = input.LA(1);
			if ( (LA113_0==Ident||(LA113_0 >= 73 && LA113_0 <= 75)||LA113_0==81||(LA113_0 >= 83 && LA113_0 <= 86)||LA113_0==91||LA113_0==93||LA113_0==99||(LA113_0 >= 102 && LA113_0 <= 104)||(LA113_0 >= 106 && LA113_0 <= 107)||LA113_0==109||(LA113_0 >= 113 && LA113_0 <= 114)||LA113_0==118||(LA113_0 >= 120 && LA113_0 <= 122)||LA113_0==125||LA113_0==127||LA113_0==130||(LA113_0 >= 132 && LA113_0 <= 134)||LA113_0==136||(LA113_0 >= 138 && LA113_0 <= 141)||LA113_0==149||LA113_0==153||(LA113_0 >= 159 && LA113_0 <= 160)) ) {
				alt113=1;
			}
			} finally {dbg.exitDecision(113);}

			switch (alt113) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:290:13: parameterDeclarationList
					{
					dbg.location(290,13);
					pushFollow(FOLLOW_parameterDeclarationList_in_blockExpr3742);
					parameterDeclarationList();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(113);}
			dbg.location(290,39);
			match(input,50,FOLLOW_50_in_blockExpr3745); if (state.failed) return;dbg.location(290,44);
			// GosuProg.g:290:44: ( expression | statementBlock )
			int alt114=2;
			try { dbg.enterSubRule(114);
			try { dbg.enterDecision(114, decisionCanBacktrack[114]);

			int LA114_0 = input.LA(1);
			if ( (LA114_0==CharLiteral||LA114_0==Ident||LA114_0==NumberLiteral||LA114_0==StringLiteral||LA114_0==25||LA114_0==28||LA114_0==31||LA114_0==38||LA114_0==43||LA114_0==47||(LA114_0 >= 74 && LA114_0 <= 75)||LA114_0==77||LA114_0==81||(LA114_0 >= 83 && LA114_0 <= 86)||LA114_0==91||LA114_0==93||LA114_0==99||(LA114_0 >= 101 && LA114_0 <= 104)||(LA114_0 >= 106 && LA114_0 <= 107)||LA114_0==109||(LA114_0 >= 113 && LA114_0 <= 114)||LA114_0==118||(LA114_0 >= 120 && LA114_0 <= 125)||LA114_0==127||LA114_0==130||(LA114_0 >= 132 && LA114_0 <= 134)||LA114_0==136||(LA114_0 >= 138 && LA114_0 <= 142)||LA114_0==144||LA114_0==146||LA114_0==149||(LA114_0 >= 153 && LA114_0 <= 154)||(LA114_0 >= 159 && LA114_0 <= 160)||LA114_0==170) ) {
				alt114=1;
			}
			else if ( (LA114_0==162) ) {
				int LA114_17 = input.LA(2);
				if ( (synpred171_GosuProg()) ) {
					alt114=1;
				}
				else if ( (true) ) {
					alt114=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 114, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(114);}

			switch (alt114) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:290:45: expression
					{
					dbg.location(290,45);
					pushFollow(FOLLOW_expression_in_blockExpr3748);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:290:58: statementBlock
					{
					dbg.location(290,58);
					pushFollow(FOLLOW_statementBlock_in_blockExpr3752);
					statementBlock();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(114);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 85, blockExpr_StartIndex); }

		}
		dbg.location(290, 74);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "blockExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "blockExpr"



	// $ANTLR start "parameterDeclarationList"
	// GosuProg.g:292:1: parameterDeclarationList : parameterDeclaration ( ',' parameterDeclaration )* ;
	public final void parameterDeclarationList() throws RecognitionException {
		int parameterDeclarationList_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "parameterDeclarationList");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(292, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 86) ) { return; }

			// GosuProg.g:292:26: ( parameterDeclaration ( ',' parameterDeclaration )* )
			dbg.enterAlt(1);

			// GosuProg.g:292:28: parameterDeclaration ( ',' parameterDeclaration )*
			{
			dbg.location(292,28);
			pushFollow(FOLLOW_parameterDeclaration_in_parameterDeclarationList3763);
			parameterDeclaration();
			state._fsp--;
			if (state.failed) return;dbg.location(292,49);
			// GosuProg.g:292:49: ( ',' parameterDeclaration )*
			try { dbg.enterSubRule(115);

			loop115:
			while (true) {
				int alt115=2;
				try { dbg.enterDecision(115, decisionCanBacktrack[115]);

				int LA115_0 = input.LA(1);
				if ( (LA115_0==46) ) {
					alt115=1;
				}

				} finally {dbg.exitDecision(115);}

				switch (alt115) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:292:50: ',' parameterDeclaration
					{
					dbg.location(292,50);
					match(input,46,FOLLOW_46_in_parameterDeclarationList3766); if (state.failed) return;dbg.location(292,54);
					pushFollow(FOLLOW_parameterDeclaration_in_parameterDeclarationList3768);
					parameterDeclaration();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop115;
				}
			}
			} finally {dbg.exitSubRule(115);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 86, parameterDeclarationList_StartIndex); }

		}
		dbg.location(292, 76);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "parameterDeclarationList");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "parameterDeclarationList"



	// $ANTLR start "parameterDeclaration"
	// GosuProg.g:294:1: parameterDeclaration : ( annotation )* ( 'final' )? id ( ( ':' typeLiteral ( '=' expression )? ) | blockTypeLiteral | '=' expression )? ;
	public final void parameterDeclaration() throws RecognitionException {
		int parameterDeclaration_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "parameterDeclaration");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(294, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 87) ) { return; }

			// GosuProg.g:294:22: ( ( annotation )* ( 'final' )? id ( ( ':' typeLiteral ( '=' expression )? ) | blockTypeLiteral | '=' expression )? )
			dbg.enterAlt(1);

			// GosuProg.g:294:24: ( annotation )* ( 'final' )? id ( ( ':' typeLiteral ( '=' expression )? ) | blockTypeLiteral | '=' expression )?
			{
			dbg.location(294,24);
			// GosuProg.g:294:24: ( annotation )*
			try { dbg.enterSubRule(116);

			loop116:
			while (true) {
				int alt116=2;
				try { dbg.enterDecision(116, decisionCanBacktrack[116]);

				int LA116_0 = input.LA(1);
				if ( (LA116_0==73) ) {
					alt116=1;
				}

				} finally {dbg.exitDecision(116);}

				switch (alt116) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:294:24: annotation
					{
					dbg.location(294,24);
					pushFollow(FOLLOW_annotation_in_parameterDeclaration3779);
					annotation();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop116;
				}
			}
			} finally {dbg.exitSubRule(116);}
			dbg.location(294,36);
			// GosuProg.g:294:36: ( 'final' )?
			int alt117=2;
			try { dbg.enterSubRule(117);
			try { dbg.enterDecision(117, decisionCanBacktrack[117]);

			int LA117_0 = input.LA(1);
			if ( (LA117_0==107) ) {
				int LA117_1 = input.LA(2);
				if ( (LA117_1==Ident||(LA117_1 >= 74 && LA117_1 <= 75)||LA117_1==81||(LA117_1 >= 83 && LA117_1 <= 86)||LA117_1==91||LA117_1==93||LA117_1==99||(LA117_1 >= 102 && LA117_1 <= 104)||(LA117_1 >= 106 && LA117_1 <= 107)||LA117_1==109||(LA117_1 >= 113 && LA117_1 <= 114)||LA117_1==118||(LA117_1 >= 120 && LA117_1 <= 122)||LA117_1==125||LA117_1==127||LA117_1==130||(LA117_1 >= 132 && LA117_1 <= 134)||LA117_1==136||(LA117_1 >= 138 && LA117_1 <= 141)||LA117_1==149||LA117_1==153||(LA117_1 >= 159 && LA117_1 <= 160)) ) {
					alt117=1;
				}
			}
			} finally {dbg.exitDecision(117);}

			switch (alt117) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:294:36: 'final'
					{
					dbg.location(294,36);
					match(input,107,FOLLOW_107_in_parameterDeclaration3782); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(117);}
			dbg.location(294,45);
			pushFollow(FOLLOW_id_in_parameterDeclaration3785);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(294,48);
			// GosuProg.g:294:48: ( ( ':' typeLiteral ( '=' expression )? ) | blockTypeLiteral | '=' expression )?
			int alt119=4;
			try { dbg.enterSubRule(119);
			try { dbg.enterDecision(119, decisionCanBacktrack[119]);

			switch ( input.LA(1) ) {
				case 56:
					{
					alt119=1;
					}
					break;
				case 38:
					{
					alt119=2;
					}
					break;
				case 60:
					{
					alt119=3;
					}
					break;
			}
			} finally {dbg.exitDecision(119);}

			switch (alt119) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:294:49: ( ':' typeLiteral ( '=' expression )? )
					{
					dbg.location(294,49);
					// GosuProg.g:294:49: ( ':' typeLiteral ( '=' expression )? )
					dbg.enterAlt(1);

					// GosuProg.g:294:50: ':' typeLiteral ( '=' expression )?
					{
					dbg.location(294,50);
					match(input,56,FOLLOW_56_in_parameterDeclaration3789); if (state.failed) return;dbg.location(294,54);
					pushFollow(FOLLOW_typeLiteral_in_parameterDeclaration3791);
					typeLiteral();
					state._fsp--;
					if (state.failed) return;dbg.location(294,66);
					// GosuProg.g:294:66: ( '=' expression )?
					int alt118=2;
					try { dbg.enterSubRule(118);
					try { dbg.enterDecision(118, decisionCanBacktrack[118]);

					int LA118_0 = input.LA(1);
					if ( (LA118_0==60) ) {
						alt118=1;
					}
					} finally {dbg.exitDecision(118);}

					switch (alt118) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:294:67: '=' expression
							{
							dbg.location(294,67);
							match(input,60,FOLLOW_60_in_parameterDeclaration3794); if (state.failed) return;dbg.location(294,71);
							pushFollow(FOLLOW_expression_in_parameterDeclaration3796);
							expression();
							state._fsp--;
							if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(118);}

					}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:294:87: blockTypeLiteral
					{
					dbg.location(294,87);
					pushFollow(FOLLOW_blockTypeLiteral_in_parameterDeclaration3803);
					blockTypeLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:294:106: '=' expression
					{
					dbg.location(294,106);
					match(input,60,FOLLOW_60_in_parameterDeclaration3807); if (state.failed) return;dbg.location(294,110);
					pushFollow(FOLLOW_expression_in_parameterDeclaration3809);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(119);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 87, parameterDeclaration_StartIndex); }

		}
		dbg.location(294, 123);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "parameterDeclaration");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "parameterDeclaration"



	// $ANTLR start "annotationArguments"
	// GosuProg.g:296:1: annotationArguments : arguments ;
	public final void annotationArguments() throws RecognitionException {
		int annotationArguments_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "annotationArguments");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(296, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 88) ) { return; }

			// GosuProg.g:296:21: ( arguments )
			dbg.enterAlt(1);

			// GosuProg.g:296:23: arguments
			{
			dbg.location(296,23);
			pushFollow(FOLLOW_arguments_in_annotationArguments3821);
			arguments();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 88, annotationArguments_StartIndex); }

		}
		dbg.location(296, 32);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "annotationArguments");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "annotationArguments"



	// $ANTLR start "arguments"
	// GosuProg.g:298:1: arguments : '(' ( argExpression ( ',' argExpression )* )? ')' ;
	public final void arguments() throws RecognitionException {
		int arguments_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "arguments");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(298, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 89) ) { return; }

			// GosuProg.g:298:11: ( '(' ( argExpression ( ',' argExpression )* )? ')' )
			dbg.enterAlt(1);

			// GosuProg.g:298:13: '(' ( argExpression ( ',' argExpression )* )? ')'
			{
			dbg.location(298,13);
			match(input,38,FOLLOW_38_in_arguments3830); if (state.failed) return;dbg.location(298,17);
			// GosuProg.g:298:17: ( argExpression ( ',' argExpression )* )?
			int alt121=2;
			try { dbg.enterSubRule(121);
			try { dbg.enterDecision(121, decisionCanBacktrack[121]);

			int LA121_0 = input.LA(1);
			if ( (LA121_0==CharLiteral||LA121_0==Ident||LA121_0==NumberLiteral||LA121_0==StringLiteral||LA121_0==25||LA121_0==28||LA121_0==31||LA121_0==38||LA121_0==43||LA121_0==47||LA121_0==56||(LA121_0 >= 74 && LA121_0 <= 75)||LA121_0==77||LA121_0==81||(LA121_0 >= 83 && LA121_0 <= 86)||LA121_0==91||LA121_0==93||LA121_0==99||(LA121_0 >= 101 && LA121_0 <= 104)||(LA121_0 >= 106 && LA121_0 <= 107)||LA121_0==109||(LA121_0 >= 113 && LA121_0 <= 114)||LA121_0==118||(LA121_0 >= 120 && LA121_0 <= 125)||LA121_0==127||LA121_0==130||(LA121_0 >= 132 && LA121_0 <= 134)||LA121_0==136||(LA121_0 >= 138 && LA121_0 <= 142)||LA121_0==144||LA121_0==146||LA121_0==149||(LA121_0 >= 153 && LA121_0 <= 154)||(LA121_0 >= 159 && LA121_0 <= 160)||LA121_0==162||LA121_0==170) ) {
				alt121=1;
			}
			} finally {dbg.exitDecision(121);}

			switch (alt121) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:298:18: argExpression ( ',' argExpression )*
					{
					dbg.location(298,18);
					pushFollow(FOLLOW_argExpression_in_arguments3833);
					argExpression();
					state._fsp--;
					if (state.failed) return;dbg.location(298,32);
					// GosuProg.g:298:32: ( ',' argExpression )*
					try { dbg.enterSubRule(120);

					loop120:
					while (true) {
						int alt120=2;
						try { dbg.enterDecision(120, decisionCanBacktrack[120]);

						int LA120_0 = input.LA(1);
						if ( (LA120_0==46) ) {
							alt120=1;
						}

						} finally {dbg.exitDecision(120);}

						switch (alt120) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:298:33: ',' argExpression
							{
							dbg.location(298,33);
							match(input,46,FOLLOW_46_in_arguments3836); if (state.failed) return;dbg.location(298,37);
							pushFollow(FOLLOW_argExpression_in_arguments3838);
							argExpression();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop120;
						}
					}
					} finally {dbg.exitSubRule(120);}

					}
					break;

			}
			} finally {dbg.exitSubRule(121);}
			dbg.location(298,55);
			match(input,39,FOLLOW_39_in_arguments3844); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 89, arguments_StartIndex); }

		}
		dbg.location(298, 58);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "arguments");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "arguments"



	// $ANTLR start "optionalArguments"
	// GosuProg.g:300:1: optionalArguments : ( arguments )? ;
	public final void optionalArguments() throws RecognitionException {
		int optionalArguments_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "optionalArguments");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(300, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 90) ) { return; }

			// GosuProg.g:300:19: ( ( arguments )? )
			dbg.enterAlt(1);

			// GosuProg.g:300:21: ( arguments )?
			{
			dbg.location(300,21);
			// GosuProg.g:300:21: ( arguments )?
			int alt122=2;
			try { dbg.enterSubRule(122);
			try { dbg.enterDecision(122, decisionCanBacktrack[122]);

			try {
				isCyclicDecision = true;
				alt122 = dfa122.predict(input);
			}
			catch (NoViableAltException nvae) {
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(122);}

			switch (alt122) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:300:21: arguments
					{
					dbg.location(300,21);
					pushFollow(FOLLOW_arguments_in_optionalArguments3853);
					arguments();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(122);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 90, optionalArguments_StartIndex); }

		}
		dbg.location(300, 31);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "optionalArguments");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "optionalArguments"



	// $ANTLR start "argExpression"
	// GosuProg.g:302:1: argExpression : ( namedArgumentExpression | expression );
	public final void argExpression() throws RecognitionException {
		int argExpression_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "argExpression");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(302, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 91) ) { return; }

			// GosuProg.g:302:15: ( namedArgumentExpression | expression )
			int alt123=2;
			try { dbg.enterDecision(123, decisionCanBacktrack[123]);

			int LA123_0 = input.LA(1);
			if ( (LA123_0==56) ) {
				alt123=1;
			}
			else if ( (LA123_0==CharLiteral||LA123_0==Ident||LA123_0==NumberLiteral||LA123_0==StringLiteral||LA123_0==25||LA123_0==28||LA123_0==31||LA123_0==38||LA123_0==43||LA123_0==47||(LA123_0 >= 74 && LA123_0 <= 75)||LA123_0==77||LA123_0==81||(LA123_0 >= 83 && LA123_0 <= 86)||LA123_0==91||LA123_0==93||LA123_0==99||(LA123_0 >= 101 && LA123_0 <= 104)||(LA123_0 >= 106 && LA123_0 <= 107)||LA123_0==109||(LA123_0 >= 113 && LA123_0 <= 114)||LA123_0==118||(LA123_0 >= 120 && LA123_0 <= 125)||LA123_0==127||LA123_0==130||(LA123_0 >= 132 && LA123_0 <= 134)||LA123_0==136||(LA123_0 >= 138 && LA123_0 <= 142)||LA123_0==144||LA123_0==146||LA123_0==149||(LA123_0 >= 153 && LA123_0 <= 154)||(LA123_0 >= 159 && LA123_0 <= 160)||LA123_0==162||LA123_0==170) ) {
				alt123=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 123, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(123);}

			switch (alt123) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:302:17: namedArgumentExpression
					{
					dbg.location(302,17);
					pushFollow(FOLLOW_namedArgumentExpression_in_argExpression3863);
					namedArgumentExpression();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:302:43: expression
					{
					dbg.location(302,43);
					pushFollow(FOLLOW_expression_in_argExpression3867);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 91, argExpression_StartIndex); }

		}
		dbg.location(302, 53);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "argExpression");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "argExpression"



	// $ANTLR start "namedArgumentExpression"
	// GosuProg.g:304:1: namedArgumentExpression : ':' id '=' expression ;
	public final void namedArgumentExpression() throws RecognitionException {
		int namedArgumentExpression_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "namedArgumentExpression");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(304, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 92) ) { return; }

			// GosuProg.g:304:25: ( ':' id '=' expression )
			dbg.enterAlt(1);

			// GosuProg.g:304:27: ':' id '=' expression
			{
			dbg.location(304,27);
			match(input,56,FOLLOW_56_in_namedArgumentExpression3876); if (state.failed) return;dbg.location(304,31);
			pushFollow(FOLLOW_id_in_namedArgumentExpression3878);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(304,34);
			match(input,60,FOLLOW_60_in_namedArgumentExpression3880); if (state.failed) return;dbg.location(304,38);
			pushFollow(FOLLOW_expression_in_namedArgumentExpression3882);
			expression();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 92, namedArgumentExpression_StartIndex); }

		}
		dbg.location(304, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "namedArgumentExpression");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "namedArgumentExpression"



	// $ANTLR start "evalExpr"
	// GosuProg.g:306:1: evalExpr : 'eval' '(' expression ')' ;
	public final void evalExpr() throws RecognitionException {
		int evalExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "evalExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(306, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 93) ) { return; }

			// GosuProg.g:306:10: ( 'eval' '(' expression ')' )
			dbg.enterAlt(1);

			// GosuProg.g:306:12: 'eval' '(' expression ')'
			{
			dbg.location(306,12);
			match(input,101,FOLLOW_101_in_evalExpr3890); if (state.failed) return;dbg.location(306,19);
			match(input,38,FOLLOW_38_in_evalExpr3892); if (state.failed) return;dbg.location(306,23);
			pushFollow(FOLLOW_expression_in_evalExpr3894);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(306,34);
			match(input,39,FOLLOW_39_in_evalExpr3896); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 93, evalExpr_StartIndex); }

		}
		dbg.location(306, 37);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "evalExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "evalExpr"



	// $ANTLR start "featureLiteral"
	// GosuProg.g:308:1: featureLiteral : '#' ( id | 'construct' ) typeArguments optionalArguments ;
	public final void featureLiteral() throws RecognitionException {
		int featureLiteral_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "featureLiteral");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(308, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 94) ) { return; }

			// GosuProg.g:308:16: ( '#' ( id | 'construct' ) typeArguments optionalArguments )
			dbg.enterAlt(1);

			// GosuProg.g:308:18: '#' ( id | 'construct' ) typeArguments optionalArguments
			{
			dbg.location(308,18);
			match(input,31,FOLLOW_31_in_featureLiteral3905); if (state.failed) return;dbg.location(308,22);
			// GosuProg.g:308:22: ( id | 'construct' )
			int alt124=2;
			try { dbg.enterSubRule(124);
			try { dbg.enterDecision(124, decisionCanBacktrack[124]);

			int LA124_0 = input.LA(1);
			if ( (LA124_0==Ident||(LA124_0 >= 74 && LA124_0 <= 75)||LA124_0==81||(LA124_0 >= 83 && LA124_0 <= 86)||LA124_0==91||LA124_0==93||LA124_0==99||(LA124_0 >= 102 && LA124_0 <= 104)||(LA124_0 >= 106 && LA124_0 <= 107)||LA124_0==109||(LA124_0 >= 113 && LA124_0 <= 114)||LA124_0==118||(LA124_0 >= 120 && LA124_0 <= 122)||LA124_0==125||LA124_0==127||LA124_0==130||(LA124_0 >= 132 && LA124_0 <= 134)||LA124_0==136||(LA124_0 >= 138 && LA124_0 <= 141)||LA124_0==149||LA124_0==153||(LA124_0 >= 159 && LA124_0 <= 160)) ) {
				alt124=1;
			}
			else if ( (LA124_0==92) ) {
				alt124=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 124, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(124);}

			switch (alt124) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:308:23: id
					{
					dbg.location(308,23);
					pushFollow(FOLLOW_id_in_featureLiteral3908);
					id();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:308:28: 'construct'
					{
					dbg.location(308,28);
					match(input,92,FOLLOW_92_in_featureLiteral3912); if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(124);}
			dbg.location(308,42);
			pushFollow(FOLLOW_typeArguments_in_featureLiteral3916);
			typeArguments();
			state._fsp--;
			if (state.failed) return;dbg.location(308,57);
			pushFollow(FOLLOW_optionalArguments_in_featureLiteral3919);
			optionalArguments();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 94, featureLiteral_StartIndex); }

		}
		dbg.location(308, 74);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "featureLiteral");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "featureLiteral"



	// $ANTLR start "standAloneDataStructureInitialization"
	// GosuProg.g:310:1: standAloneDataStructureInitialization : '{' ( initializerExpression )? '}' ;
	public final void standAloneDataStructureInitialization() throws RecognitionException {
		int standAloneDataStructureInitialization_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "standAloneDataStructureInitialization");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(310, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 95) ) { return; }

			// GosuProg.g:310:39: ( '{' ( initializerExpression )? '}' )
			dbg.enterAlt(1);

			// GosuProg.g:310:41: '{' ( initializerExpression )? '}'
			{
			dbg.location(310,41);
			match(input,162,FOLLOW_162_in_standAloneDataStructureInitialization3928); if (state.failed) return;dbg.location(310,45);
			// GosuProg.g:310:45: ( initializerExpression )?
			int alt125=2;
			try { dbg.enterSubRule(125);
			try { dbg.enterDecision(125, decisionCanBacktrack[125]);

			int LA125_0 = input.LA(1);
			if ( (LA125_0==CharLiteral||LA125_0==Ident||LA125_0==NumberLiteral||LA125_0==StringLiteral||LA125_0==25||LA125_0==28||LA125_0==31||LA125_0==38||LA125_0==43||LA125_0==47||(LA125_0 >= 74 && LA125_0 <= 75)||LA125_0==77||LA125_0==81||(LA125_0 >= 83 && LA125_0 <= 86)||LA125_0==91||LA125_0==93||LA125_0==99||(LA125_0 >= 101 && LA125_0 <= 104)||(LA125_0 >= 106 && LA125_0 <= 107)||LA125_0==109||(LA125_0 >= 113 && LA125_0 <= 114)||LA125_0==118||(LA125_0 >= 120 && LA125_0 <= 125)||LA125_0==127||LA125_0==130||(LA125_0 >= 132 && LA125_0 <= 134)||LA125_0==136||(LA125_0 >= 138 && LA125_0 <= 142)||LA125_0==144||LA125_0==146||LA125_0==149||(LA125_0 >= 153 && LA125_0 <= 154)||(LA125_0 >= 159 && LA125_0 <= 160)||LA125_0==162||LA125_0==170) ) {
				alt125=1;
			}
			} finally {dbg.exitDecision(125);}

			switch (alt125) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:310:46: initializerExpression
					{
					dbg.location(310,46);
					pushFollow(FOLLOW_initializerExpression_in_standAloneDataStructureInitialization3931);
					initializerExpression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(125);}
			dbg.location(310,70);
			match(input,169,FOLLOW_169_in_standAloneDataStructureInitialization3935); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 95, standAloneDataStructureInitialization_StartIndex); }

		}
		dbg.location(310, 73);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "standAloneDataStructureInitialization");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "standAloneDataStructureInitialization"



	// $ANTLR start "primaryExpr"
	// GosuProg.g:312:1: primaryExpr : ( newExpr | thisSuperExpr | literal | typeLiteralExpr | parenthExpr | standAloneDataStructureInitialization ) indirectMemberAccess ;
	public final void primaryExpr() throws RecognitionException {
		int primaryExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "primaryExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(312, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 96) ) { return; }

			// GosuProg.g:312:13: ( ( newExpr | thisSuperExpr | literal | typeLiteralExpr | parenthExpr | standAloneDataStructureInitialization ) indirectMemberAccess )
			dbg.enterAlt(1);

			// GosuProg.g:313:17: ( newExpr | thisSuperExpr | literal | typeLiteralExpr | parenthExpr | standAloneDataStructureInitialization ) indirectMemberAccess
			{
			dbg.location(313,17);
			// GosuProg.g:313:17: ( newExpr | thisSuperExpr | literal | typeLiteralExpr | parenthExpr | standAloneDataStructureInitialization )
			int alt126=6;
			try { dbg.enterSubRule(126);
			try { dbg.enterDecision(126, decisionCanBacktrack[126]);

			switch ( input.LA(1) ) {
			case 123:
				{
				alt126=1;
				}
				break;
			case 144:
			case 146:
				{
				alt126=2;
				}
				break;
			case CharLiteral:
			case NumberLiteral:
			case StringLiteral:
			case 31:
				{
				alt126=3;
				}
				break;
			case 149:
				{
				int LA126_4 = input.LA(2);
				if ( (synpred187_GosuProg()) ) {
					alt126=3;
				}
				else if ( (synpred188_GosuProg()) ) {
					alt126=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 126, 4, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 106:
				{
				int LA126_5 = input.LA(2);
				if ( (synpred187_GosuProg()) ) {
					alt126=3;
				}
				else if ( (synpred188_GosuProg()) ) {
					alt126=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 126, 5, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 125:
				{
				int LA126_6 = input.LA(2);
				if ( (synpred187_GosuProg()) ) {
					alt126=3;
				}
				else if ( (synpred188_GosuProg()) ) {
					alt126=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 126, 6, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case Ident:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 86:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 153:
			case 159:
			case 160:
				{
				alt126=4;
				}
				break;
			case 38:
				{
				alt126=5;
				}
				break;
			case 162:
				{
				alt126=6;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 126, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(126);}

			switch (alt126) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:313:18: newExpr
					{
					dbg.location(313,18);
					pushFollow(FOLLOW_newExpr_in_primaryExpr3961);
					newExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:314:18: thisSuperExpr
					{
					dbg.location(314,18);
					pushFollow(FOLLOW_thisSuperExpr_in_primaryExpr4017);
					thisSuperExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:317:18: literal
					{
					dbg.location(317,18);
					pushFollow(FOLLOW_literal_in_primaryExpr4103);
					literal();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:318:18: typeLiteralExpr
					{
					dbg.location(318,18);
					pushFollow(FOLLOW_typeLiteralExpr_in_primaryExpr4159);
					typeLiteralExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:319:18: parenthExpr
					{
					dbg.location(319,18);
					pushFollow(FOLLOW_parenthExpr_in_primaryExpr4207);
					parenthExpr();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:320:18: standAloneDataStructureInitialization
					{
					dbg.location(320,18);
					pushFollow(FOLLOW_standAloneDataStructureInitialization_in_primaryExpr4259);
					standAloneDataStructureInitialization();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(126);}
			dbg.location(321,17);
			pushFollow(FOLLOW_indirectMemberAccess_in_primaryExpr4278);
			indirectMemberAccess();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 96, primaryExpr_StartIndex); }

		}
		dbg.location(323, 12);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "primaryExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "primaryExpr"



	// $ANTLR start "parenthExpr"
	// GosuProg.g:325:1: parenthExpr : '(' expression ')' ;
	public final void parenthExpr() throws RecognitionException {
		int parenthExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "parenthExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(325, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 97) ) { return; }

			// GosuProg.g:325:13: ( '(' expression ')' )
			dbg.enterAlt(1);

			// GosuProg.g:325:15: '(' expression ')'
			{
			dbg.location(325,15);
			match(input,38,FOLLOW_38_in_parenthExpr4300); if (state.failed) return;dbg.location(325,19);
			pushFollow(FOLLOW_expression_in_parenthExpr4302);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(325,30);
			match(input,39,FOLLOW_39_in_parenthExpr4304); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 97, parenthExpr_StartIndex); }

		}
		dbg.location(325, 33);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "parenthExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "parenthExpr"



	// $ANTLR start "newExpr"
	// GosuProg.g:333:1: newExpr : 'new' ( classOrInterfaceType )? ( ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? ) | ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) ) ) ;
	public final void newExpr() throws RecognitionException {
		int newExpr_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "newExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(333, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 98) ) { return; }

			// GosuProg.g:333:9: ( 'new' ( classOrInterfaceType )? ( ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? ) | ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) ) ) )
			dbg.enterAlt(1);

			// GosuProg.g:333:12: 'new' ( classOrInterfaceType )? ( ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? ) | ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) ) )
			{
			dbg.location(333,12);
			match(input,123,FOLLOW_123_in_newExpr4323); if (state.failed) return;dbg.location(333,18);
			// GosuProg.g:333:18: ( classOrInterfaceType )?
			int alt127=2;
			try { dbg.enterSubRule(127);
			try { dbg.enterDecision(127, decisionCanBacktrack[127]);

			int LA127_0 = input.LA(1);
			if ( (LA127_0==Ident||(LA127_0 >= 74 && LA127_0 <= 75)||LA127_0==81||(LA127_0 >= 83 && LA127_0 <= 85)||LA127_0==91||LA127_0==93||LA127_0==99||(LA127_0 >= 102 && LA127_0 <= 104)||(LA127_0 >= 106 && LA127_0 <= 107)||LA127_0==109||(LA127_0 >= 113 && LA127_0 <= 114)||LA127_0==118||(LA127_0 >= 120 && LA127_0 <= 122)||LA127_0==125||LA127_0==127||LA127_0==130||(LA127_0 >= 132 && LA127_0 <= 134)||LA127_0==136||(LA127_0 >= 138 && LA127_0 <= 141)||LA127_0==149||LA127_0==153||(LA127_0 >= 159 && LA127_0 <= 160)) ) {
				alt127=1;
			}
			} finally {dbg.exitDecision(127);}

			switch (alt127) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:333:18: classOrInterfaceType
					{
					dbg.location(333,18);
					pushFollow(FOLLOW_classOrInterfaceType_in_newExpr4325);
					classOrInterfaceType();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(127);}
			dbg.location(333,41);
			// GosuProg.g:333:41: ( ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? ) | ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) ) )
			int alt134=2;
			try { dbg.enterSubRule(134);
			try { dbg.enterDecision(134, decisionCanBacktrack[134]);

			int LA134_0 = input.LA(1);
			if ( (LA134_0==38) ) {
				alt134=1;
			}
			else if ( (LA134_0==76) ) {
				alt134=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 134, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(134);}

			switch (alt134) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:334:45: ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? )
					{
					dbg.location(334,45);
					// GosuProg.g:334:45: ( arguments ( '{' ( initializer | anonymousInnerClass ) '}' )? )
					dbg.enterAlt(1);

					// GosuProg.g:334:46: arguments ( '{' ( initializer | anonymousInnerClass ) '}' )?
					{
					dbg.location(334,46);
					pushFollow(FOLLOW_arguments_in_newExpr4376);
					arguments();
					state._fsp--;
					if (state.failed) return;dbg.location(334,56);
					// GosuProg.g:334:56: ( '{' ( initializer | anonymousInnerClass ) '}' )?
					int alt129=2;
					try { dbg.enterSubRule(129);
					try { dbg.enterDecision(129, decisionCanBacktrack[129]);

					try {
						isCyclicDecision = true;
						alt129 = dfa129.predict(input);
					}
					catch (NoViableAltException nvae) {
						dbg.recognitionException(nvae);
						throw nvae;
					}
					} finally {dbg.exitDecision(129);}

					switch (alt129) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:334:58: '{' ( initializer | anonymousInnerClass ) '}'
							{
							dbg.location(334,58);
							match(input,162,FOLLOW_162_in_newExpr4380); if (state.failed) return;dbg.location(334,62);
							// GosuProg.g:334:62: ( initializer | anonymousInnerClass )
							int alt128=2;
							try { dbg.enterSubRule(128);
							try { dbg.enterDecision(128, decisionCanBacktrack[128]);

							switch ( input.LA(1) ) {
							case CharLiteral:
							case Ident:
							case NumberLiteral:
							case StringLiteral:
							case 25:
							case 28:
							case 31:
							case 38:
							case 43:
							case 47:
							case 56:
							case 74:
							case 75:
							case 77:
							case 83:
							case 84:
							case 85:
							case 86:
							case 91:
							case 93:
							case 99:
							case 101:
							case 102:
							case 103:
							case 104:
							case 106:
							case 109:
							case 113:
							case 114:
							case 118:
							case 121:
							case 122:
							case 123:
							case 124:
							case 125:
							case 127:
							case 134:
							case 136:
							case 138:
							case 139:
							case 140:
							case 142:
							case 144:
							case 146:
							case 149:
							case 153:
							case 154:
							case 159:
							case 160:
							case 162:
							case 170:
								{
								alt128=1;
								}
								break;
							case 130:
								{
								int LA128_2 = input.LA(2);
								if ( ((LA128_2 >= 26 && LA128_2 <= 32)||LA128_2==34||LA128_2==36||LA128_2==38||(LA128_2 >= 40 && LA128_2 <= 41)||LA128_2==43||(LA128_2 >= 46 && LA128_2 <= 47)||(LA128_2 >= 50 && LA128_2 <= 54)||(LA128_2 >= 58 && LA128_2 <= 59)||(LA128_2 >= 61 && LA128_2 <= 72)||LA128_2==76||LA128_2==79||LA128_2==82||LA128_2==84||LA128_2==126||(LA128_2 >= 151 && LA128_2 <= 152)||(LA128_2 >= 163 && LA128_2 <= 165)||LA128_2==167||LA128_2==169) ) {
									alt128=1;
								}
								else if ( (LA128_2==73||LA128_2==81||LA128_2==90||LA128_2==92||LA128_2==96||LA128_2==100||LA128_2==107||LA128_2==112||(LA128_2 >= 119 && LA128_2 <= 120)||LA128_2==128||(LA128_2 >= 130 && LA128_2 <= 133)||LA128_2==141||LA128_2==143||LA128_2==148||LA128_2==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 2, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 169:
								{
								int LA128_3 = input.LA(2);
								if ( (synpred191_GosuProg()) ) {
									alt128=1;
								}
								else if ( (true) ) {
									alt128=2;
								}

								}
								break;
							case 73:
							case 90:
							case 92:
							case 96:
							case 100:
							case 112:
							case 119:
							case 128:
							case 131:
							case 143:
							case 148:
							case 158:
								{
								alt128=2;
								}
								break;
							case 120:
								{
								int LA128_5 = input.LA(2);
								if ( ((LA128_5 >= 26 && LA128_5 <= 32)||LA128_5==34||LA128_5==36||LA128_5==38||(LA128_5 >= 40 && LA128_5 <= 41)||LA128_5==43||(LA128_5 >= 46 && LA128_5 <= 47)||(LA128_5 >= 50 && LA128_5 <= 54)||(LA128_5 >= 58 && LA128_5 <= 59)||(LA128_5 >= 61 && LA128_5 <= 72)||LA128_5==76||LA128_5==79||LA128_5==82||LA128_5==84||LA128_5==126||(LA128_5 >= 151 && LA128_5 <= 152)||(LA128_5 >= 163 && LA128_5 <= 165)||LA128_5==167||LA128_5==169) ) {
									alt128=1;
								}
								else if ( (LA128_5==73||LA128_5==81||LA128_5==90||LA128_5==92||LA128_5==96||LA128_5==100||LA128_5==107||LA128_5==112||(LA128_5 >= 119 && LA128_5 <= 120)||LA128_5==128||(LA128_5 >= 130 && LA128_5 <= 133)||LA128_5==141||LA128_5==143||LA128_5==148||LA128_5==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 5, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 132:
								{
								int LA128_6 = input.LA(2);
								if ( ((LA128_6 >= 26 && LA128_6 <= 32)||LA128_6==34||LA128_6==36||LA128_6==38||(LA128_6 >= 40 && LA128_6 <= 41)||LA128_6==43||(LA128_6 >= 46 && LA128_6 <= 47)||(LA128_6 >= 50 && LA128_6 <= 54)||(LA128_6 >= 58 && LA128_6 <= 59)||(LA128_6 >= 61 && LA128_6 <= 72)||LA128_6==76||LA128_6==79||LA128_6==82||LA128_6==84||LA128_6==126||(LA128_6 >= 151 && LA128_6 <= 152)||(LA128_6 >= 163 && LA128_6 <= 165)||LA128_6==167||LA128_6==169) ) {
									alt128=1;
								}
								else if ( (LA128_6==73||LA128_6==81||LA128_6==90||LA128_6==92||LA128_6==96||LA128_6==100||LA128_6==107||LA128_6==112||(LA128_6 >= 119 && LA128_6 <= 120)||LA128_6==128||(LA128_6 >= 130 && LA128_6 <= 133)||LA128_6==141||LA128_6==143||LA128_6==148||LA128_6==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 6, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 133:
								{
								int LA128_7 = input.LA(2);
								if ( ((LA128_7 >= 26 && LA128_7 <= 32)||LA128_7==34||LA128_7==36||LA128_7==38||(LA128_7 >= 40 && LA128_7 <= 41)||LA128_7==43||(LA128_7 >= 46 && LA128_7 <= 47)||(LA128_7 >= 50 && LA128_7 <= 54)||(LA128_7 >= 58 && LA128_7 <= 59)||(LA128_7 >= 61 && LA128_7 <= 72)||LA128_7==76||LA128_7==79||LA128_7==82||LA128_7==84||LA128_7==126||(LA128_7 >= 151 && LA128_7 <= 152)||(LA128_7 >= 163 && LA128_7 <= 165)||LA128_7==167||LA128_7==169) ) {
									alt128=1;
								}
								else if ( (LA128_7==73||LA128_7==81||LA128_7==90||LA128_7==92||LA128_7==96||LA128_7==100||LA128_7==107||LA128_7==112||(LA128_7 >= 119 && LA128_7 <= 120)||LA128_7==128||(LA128_7 >= 130 && LA128_7 <= 133)||LA128_7==141||LA128_7==143||LA128_7==148||LA128_7==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 7, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 141:
								{
								int LA128_8 = input.LA(2);
								if ( ((LA128_8 >= 26 && LA128_8 <= 32)||LA128_8==34||LA128_8==36||LA128_8==38||(LA128_8 >= 40 && LA128_8 <= 41)||LA128_8==43||(LA128_8 >= 46 && LA128_8 <= 47)||(LA128_8 >= 50 && LA128_8 <= 54)||(LA128_8 >= 58 && LA128_8 <= 59)||(LA128_8 >= 61 && LA128_8 <= 72)||LA128_8==76||LA128_8==79||LA128_8==82||LA128_8==84||LA128_8==126||(LA128_8 >= 151 && LA128_8 <= 152)||(LA128_8 >= 163 && LA128_8 <= 165)||LA128_8==167||LA128_8==169) ) {
									alt128=1;
								}
								else if ( (LA128_8==73||LA128_8==81||LA128_8==90||LA128_8==92||LA128_8==96||LA128_8==100||LA128_8==107||LA128_8==112||(LA128_8 >= 119 && LA128_8 <= 120)||LA128_8==128||(LA128_8 >= 130 && LA128_8 <= 133)||LA128_8==141||LA128_8==143||LA128_8==148||LA128_8==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 8, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 81:
								{
								int LA128_9 = input.LA(2);
								if ( ((LA128_9 >= 26 && LA128_9 <= 32)||LA128_9==34||LA128_9==36||LA128_9==38||(LA128_9 >= 40 && LA128_9 <= 41)||LA128_9==43||(LA128_9 >= 46 && LA128_9 <= 47)||(LA128_9 >= 50 && LA128_9 <= 54)||(LA128_9 >= 58 && LA128_9 <= 59)||(LA128_9 >= 61 && LA128_9 <= 72)||LA128_9==76||LA128_9==79||LA128_9==82||LA128_9==84||LA128_9==126||(LA128_9 >= 151 && LA128_9 <= 152)||(LA128_9 >= 163 && LA128_9 <= 165)||LA128_9==167||LA128_9==169) ) {
									alt128=1;
								}
								else if ( (LA128_9==73||LA128_9==81||LA128_9==90||LA128_9==92||LA128_9==96||LA128_9==100||LA128_9==107||LA128_9==112||(LA128_9 >= 119 && LA128_9 <= 120)||LA128_9==128||(LA128_9 >= 130 && LA128_9 <= 133)||LA128_9==141||LA128_9==143||LA128_9==148||LA128_9==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 9, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							case 107:
								{
								int LA128_10 = input.LA(2);
								if ( ((LA128_10 >= 26 && LA128_10 <= 32)||LA128_10==34||LA128_10==36||LA128_10==38||(LA128_10 >= 40 && LA128_10 <= 41)||LA128_10==43||(LA128_10 >= 46 && LA128_10 <= 47)||(LA128_10 >= 50 && LA128_10 <= 54)||(LA128_10 >= 58 && LA128_10 <= 59)||(LA128_10 >= 61 && LA128_10 <= 72)||LA128_10==76||LA128_10==79||LA128_10==82||LA128_10==84||LA128_10==126||(LA128_10 >= 151 && LA128_10 <= 152)||(LA128_10 >= 163 && LA128_10 <= 165)||LA128_10==167||LA128_10==169) ) {
									alt128=1;
								}
								else if ( (LA128_10==73||LA128_10==81||LA128_10==90||LA128_10==92||LA128_10==96||LA128_10==100||LA128_10==107||LA128_10==112||(LA128_10 >= 119 && LA128_10 <= 120)||LA128_10==128||(LA128_10 >= 130 && LA128_10 <= 133)||LA128_10==141||LA128_10==143||LA128_10==148||LA128_10==158) ) {
									alt128=2;
								}

								else {
									if (state.backtracking>0) {state.failed=true; return;}
									int nvaeMark = input.mark();
									try {
										input.consume();
										NoViableAltException nvae =
											new NoViableAltException("", 128, 10, input);
										dbg.recognitionException(nvae);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}

								}
								break;
							default:
								if (state.backtracking>0) {state.failed=true; return;}
								NoViableAltException nvae =
									new NoViableAltException("", 128, 0, input);
								dbg.recognitionException(nvae);
								throw nvae;
							}
							} finally {dbg.exitDecision(128);}

							switch (alt128) {
								case 1 :
									dbg.enterAlt(1);

									// GosuProg.g:334:63: initializer
									{
									dbg.location(334,63);
									pushFollow(FOLLOW_initializer_in_newExpr4383);
									initializer();
									state._fsp--;
									if (state.failed) return;
									}
									break;
								case 2 :
									dbg.enterAlt(2);

									// GosuProg.g:334:77: anonymousInnerClass
									{
									dbg.location(334,77);
									pushFollow(FOLLOW_anonymousInnerClass_in_newExpr4387);
									anonymousInnerClass();
									state._fsp--;
									if (state.failed) return;
									}
									break;

							}
							} finally {dbg.exitSubRule(128);}
							dbg.location(334,98);
							match(input,169,FOLLOW_169_in_newExpr4390); if (state.failed) return;
							}
							break;

					}
					} finally {dbg.exitSubRule(129);}

					}

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:335:37: ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) )
					{
					dbg.location(335,37);
					// GosuProg.g:335:37: ( '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* ) )
					dbg.enterAlt(1);

					// GosuProg.g:336:39: '[' ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* )
					{
					dbg.location(336,39);
					match(input,76,FOLLOW_76_in_newExpr4479); if (state.failed) return;dbg.location(337,43);
					// GosuProg.g:337:43: ( ']' ( '[' ']' )* arrayInitializer | expression ']' ( '[' expression ']' )* ( '[' ']' )* )
					int alt133=2;
					try { dbg.enterSubRule(133);
					try { dbg.enterDecision(133, decisionCanBacktrack[133]);

					int LA133_0 = input.LA(1);
					if ( (LA133_0==78) ) {
						alt133=1;
					}
					else if ( (LA133_0==CharLiteral||LA133_0==Ident||LA133_0==NumberLiteral||LA133_0==StringLiteral||LA133_0==25||LA133_0==28||LA133_0==31||LA133_0==38||LA133_0==43||LA133_0==47||(LA133_0 >= 74 && LA133_0 <= 75)||LA133_0==77||LA133_0==81||(LA133_0 >= 83 && LA133_0 <= 86)||LA133_0==91||LA133_0==93||LA133_0==99||(LA133_0 >= 101 && LA133_0 <= 104)||(LA133_0 >= 106 && LA133_0 <= 107)||LA133_0==109||(LA133_0 >= 113 && LA133_0 <= 114)||LA133_0==118||(LA133_0 >= 120 && LA133_0 <= 125)||LA133_0==127||LA133_0==130||(LA133_0 >= 132 && LA133_0 <= 134)||LA133_0==136||(LA133_0 >= 138 && LA133_0 <= 142)||LA133_0==144||LA133_0==146||LA133_0==149||(LA133_0 >= 153 && LA133_0 <= 154)||(LA133_0 >= 159 && LA133_0 <= 160)||LA133_0==162||LA133_0==170) ) {
						alt133=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return;}
						NoViableAltException nvae =
							new NoViableAltException("", 133, 0, input);
						dbg.recognitionException(nvae);
						throw nvae;
					}

					} finally {dbg.exitDecision(133);}

					switch (alt133) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:338:46: ']' ( '[' ']' )* arrayInitializer
							{
							dbg.location(338,46);
							match(input,78,FOLLOW_78_in_newExpr4570); if (state.failed) return;dbg.location(338,50);
							// GosuProg.g:338:50: ( '[' ']' )*
							try { dbg.enterSubRule(130);

							loop130:
							while (true) {
								int alt130=2;
								try { dbg.enterDecision(130, decisionCanBacktrack[130]);

								int LA130_0 = input.LA(1);
								if ( (LA130_0==76) ) {
									alt130=1;
								}

								} finally {dbg.exitDecision(130);}

								switch (alt130) {
								case 1 :
									dbg.enterAlt(1);

									// GosuProg.g:338:51: '[' ']'
									{
									dbg.location(338,51);
									match(input,76,FOLLOW_76_in_newExpr4573); if (state.failed) return;dbg.location(338,55);
									match(input,78,FOLLOW_78_in_newExpr4575); if (state.failed) return;
									}
									break;

								default :
									break loop130;
								}
							}
							} finally {dbg.exitSubRule(130);}
							dbg.location(338,61);
							pushFollow(FOLLOW_arrayInitializer_in_newExpr4579);
							arrayInitializer();
							state._fsp--;
							if (state.failed) return;
							}
							break;
						case 2 :
							dbg.enterAlt(2);

							// GosuProg.g:339:46: expression ']' ( '[' expression ']' )* ( '[' ']' )*
							{
							dbg.location(339,46);
							pushFollow(FOLLOW_expression_in_newExpr4644);
							expression();
							state._fsp--;
							if (state.failed) return;dbg.location(339,57);
							match(input,78,FOLLOW_78_in_newExpr4646); if (state.failed) return;dbg.location(339,61);
							// GosuProg.g:339:61: ( '[' expression ']' )*
							try { dbg.enterSubRule(131);

							loop131:
							while (true) {
								int alt131=2;
								try { dbg.enterDecision(131, decisionCanBacktrack[131]);

								try {
									isCyclicDecision = true;
									alt131 = dfa131.predict(input);
								}
								catch (NoViableAltException nvae) {
									dbg.recognitionException(nvae);
									throw nvae;
								}
								} finally {dbg.exitDecision(131);}

								switch (alt131) {
								case 1 :
									dbg.enterAlt(1);

									// GosuProg.g:339:62: '[' expression ']'
									{
									dbg.location(339,62);
									match(input,76,FOLLOW_76_in_newExpr4649); if (state.failed) return;dbg.location(339,66);
									pushFollow(FOLLOW_expression_in_newExpr4651);
									expression();
									state._fsp--;
									if (state.failed) return;dbg.location(339,77);
									match(input,78,FOLLOW_78_in_newExpr4653); if (state.failed) return;
									}
									break;

								default :
									break loop131;
								}
							}
							} finally {dbg.exitSubRule(131);}
							dbg.location(339,83);
							// GosuProg.g:339:83: ( '[' ']' )*
							try { dbg.enterSubRule(132);

							loop132:
							while (true) {
								int alt132=2;
								try { dbg.enterDecision(132, decisionCanBacktrack[132]);

								int LA132_0 = input.LA(1);
								if ( (LA132_0==76) ) {
									int LA132_2 = input.LA(2);
									if ( (LA132_2==78) ) {
										alt132=1;
									}

								}

								} finally {dbg.exitDecision(132);}

								switch (alt132) {
								case 1 :
									dbg.enterAlt(1);

									// GosuProg.g:339:84: '[' ']'
									{
									dbg.location(339,84);
									match(input,76,FOLLOW_76_in_newExpr4658); if (state.failed) return;dbg.location(339,88);
									match(input,78,FOLLOW_78_in_newExpr4660); if (state.failed) return;
									}
									break;

								default :
									break loop132;
								}
							}
							} finally {dbg.exitSubRule(132);}

							}
							break;

					}
					} finally {dbg.exitSubRule(133);}

					}

					}
					break;

			}
			} finally {dbg.exitSubRule(134);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 98, newExpr_StartIndex); }

		}
		dbg.location(343, 8);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "newExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "newExpr"



	// $ANTLR start "anonymousInnerClass"
	// GosuProg.g:345:1: anonymousInnerClass : classMembers ;
	public final void anonymousInnerClass() throws RecognitionException {
		int anonymousInnerClass_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "anonymousInnerClass");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(345, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 99) ) { return; }

			// GosuProg.g:345:21: ( classMembers )
			dbg.enterAlt(1);

			// GosuProg.g:345:24: classMembers
			{
			dbg.location(345,24);
			pushFollow(FOLLOW_classMembers_in_anonymousInnerClass4798);
			classMembers();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 99, anonymousInnerClass_StartIndex); }

		}
		dbg.location(345, 35);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "anonymousInnerClass");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "anonymousInnerClass"



	// $ANTLR start "arrayInitializer"
	// GosuProg.g:347:1: arrayInitializer : '{' ( expression ( ',' expression )* )? '}' ;
	public final void arrayInitializer() throws RecognitionException {
		int arrayInitializer_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "arrayInitializer");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(347, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 100) ) { return; }

			// GosuProg.g:347:18: ( '{' ( expression ( ',' expression )* )? '}' )
			dbg.enterAlt(1);

			// GosuProg.g:347:20: '{' ( expression ( ',' expression )* )? '}'
			{
			dbg.location(347,20);
			match(input,162,FOLLOW_162_in_arrayInitializer4806); if (state.failed) return;dbg.location(347,24);
			// GosuProg.g:347:24: ( expression ( ',' expression )* )?
			int alt136=2;
			try { dbg.enterSubRule(136);
			try { dbg.enterDecision(136, decisionCanBacktrack[136]);

			int LA136_0 = input.LA(1);
			if ( (LA136_0==CharLiteral||LA136_0==Ident||LA136_0==NumberLiteral||LA136_0==StringLiteral||LA136_0==25||LA136_0==28||LA136_0==31||LA136_0==38||LA136_0==43||LA136_0==47||(LA136_0 >= 74 && LA136_0 <= 75)||LA136_0==77||LA136_0==81||(LA136_0 >= 83 && LA136_0 <= 86)||LA136_0==91||LA136_0==93||LA136_0==99||(LA136_0 >= 101 && LA136_0 <= 104)||(LA136_0 >= 106 && LA136_0 <= 107)||LA136_0==109||(LA136_0 >= 113 && LA136_0 <= 114)||LA136_0==118||(LA136_0 >= 120 && LA136_0 <= 125)||LA136_0==127||LA136_0==130||(LA136_0 >= 132 && LA136_0 <= 134)||LA136_0==136||(LA136_0 >= 138 && LA136_0 <= 142)||LA136_0==144||LA136_0==146||LA136_0==149||(LA136_0 >= 153 && LA136_0 <= 154)||(LA136_0 >= 159 && LA136_0 <= 160)||LA136_0==162||LA136_0==170) ) {
				alt136=1;
			}
			} finally {dbg.exitDecision(136);}

			switch (alt136) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:347:26: expression ( ',' expression )*
					{
					dbg.location(347,26);
					pushFollow(FOLLOW_expression_in_arrayInitializer4810);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(347,37);
					// GosuProg.g:347:37: ( ',' expression )*
					try { dbg.enterSubRule(135);

					loop135:
					while (true) {
						int alt135=2;
						try { dbg.enterDecision(135, decisionCanBacktrack[135]);

						int LA135_0 = input.LA(1);
						if ( (LA135_0==46) ) {
							alt135=1;
						}

						} finally {dbg.exitDecision(135);}

						switch (alt135) {
						case 1 :
							dbg.enterAlt(1);

							// GosuProg.g:347:38: ',' expression
							{
							dbg.location(347,38);
							match(input,46,FOLLOW_46_in_arrayInitializer4813); if (state.failed) return;dbg.location(347,43);
							pushFollow(FOLLOW_expression_in_arrayInitializer4816);
							expression();
							state._fsp--;
							if (state.failed) return;
							}
							break;

						default :
							break loop135;
						}
					}
					} finally {dbg.exitSubRule(135);}

					}
					break;

			}
			} finally {dbg.exitSubRule(136);}
			dbg.location(347,58);
			match(input,169,FOLLOW_169_in_arrayInitializer4822); if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 100, arrayInitializer_StartIndex); }

		}
		dbg.location(347, 60);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "arrayInitializer");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "arrayInitializer"



	// $ANTLR start "initializer"
	// GosuProg.g:349:1: initializer : ( initializerExpression | objectInitializer )? ;
	public final void initializer() throws RecognitionException {
		int initializer_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "initializer");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(349, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 101) ) { return; }

			// GosuProg.g:349:13: ( ( initializerExpression | objectInitializer )? )
			dbg.enterAlt(1);

			// GosuProg.g:349:15: ( initializerExpression | objectInitializer )?
			{
			dbg.location(349,15);
			// GosuProg.g:349:15: ( initializerExpression | objectInitializer )?
			int alt137=3;
			try { dbg.enterSubRule(137);
			try { dbg.enterDecision(137, decisionCanBacktrack[137]);

			int LA137_0 = input.LA(1);
			if ( (LA137_0==CharLiteral||LA137_0==Ident||LA137_0==NumberLiteral||LA137_0==StringLiteral||LA137_0==25||LA137_0==28||LA137_0==31||LA137_0==38||LA137_0==43||LA137_0==47||(LA137_0 >= 74 && LA137_0 <= 75)||LA137_0==77||LA137_0==81||(LA137_0 >= 83 && LA137_0 <= 86)||LA137_0==91||LA137_0==93||LA137_0==99||(LA137_0 >= 101 && LA137_0 <= 104)||(LA137_0 >= 106 && LA137_0 <= 107)||LA137_0==109||(LA137_0 >= 113 && LA137_0 <= 114)||LA137_0==118||(LA137_0 >= 120 && LA137_0 <= 125)||LA137_0==127||LA137_0==130||(LA137_0 >= 132 && LA137_0 <= 134)||LA137_0==136||(LA137_0 >= 138 && LA137_0 <= 142)||LA137_0==144||LA137_0==146||LA137_0==149||(LA137_0 >= 153 && LA137_0 <= 154)||(LA137_0 >= 159 && LA137_0 <= 160)||LA137_0==162||LA137_0==170) ) {
				alt137=1;
			}
			else if ( (LA137_0==56) ) {
				alt137=2;
			}
			} finally {dbg.exitDecision(137);}

			switch (alt137) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:349:16: initializerExpression
					{
					dbg.location(349,16);
					pushFollow(FOLLOW_initializerExpression_in_initializer4831);
					initializerExpression();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:349:40: objectInitializer
					{
					dbg.location(349,40);
					pushFollow(FOLLOW_objectInitializer_in_initializer4835);
					objectInitializer();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
			} finally {dbg.exitSubRule(137);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 101, initializer_StartIndex); }

		}
		dbg.location(349, 59);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "initializer");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "initializer"



	// $ANTLR start "initializerExpression"
	// GosuProg.g:351:1: initializerExpression : ( mapInitializerList | arrayValueList );
	public final void initializerExpression() throws RecognitionException {
		int initializerExpression_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "initializerExpression");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(351, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 102) ) { return; }

			// GosuProg.g:351:23: ( mapInitializerList | arrayValueList )
			int alt138=2;
			try { dbg.enterDecision(138, decisionCanBacktrack[138]);

			switch ( input.LA(1) ) {
			case 28:
			case 43:
			case 47:
				{
				int LA138_1 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 25:
			case 124:
			case 142:
			case 154:
			case 170:
				{
				int LA138_2 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 77:
				{
				int LA138_3 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 101:
				{
				int LA138_4 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 123:
				{
				int LA138_5 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 144:
			case 146:
				{
				int LA138_6 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case NumberLiteral:
				{
				int LA138_7 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 31:
				{
				int LA138_8 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case StringLiteral:
				{
				int LA138_9 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case CharLiteral:
				{
				int LA138_10 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 149:
				{
				int LA138_11 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 106:
				{
				int LA138_12 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 125:
				{
				int LA138_13 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case Ident:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 153:
			case 159:
			case 160:
				{
				int LA138_14 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 86:
				{
				int LA138_15 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 38:
				{
				int LA138_16 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			case 162:
				{
				int LA138_17 = input.LA(2);
				if ( (synpred202_GosuProg()) ) {
					alt138=1;
				}
				else if ( (true) ) {
					alt138=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 138, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(138);}

			switch (alt138) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:351:25: mapInitializerList
					{
					dbg.location(351,25);
					pushFollow(FOLLOW_mapInitializerList_in_initializerExpression4846);
					mapInitializerList();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:351:46: arrayValueList
					{
					dbg.location(351,46);
					pushFollow(FOLLOW_arrayValueList_in_initializerExpression4850);
					arrayValueList();
					state._fsp--;
					if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 102, initializerExpression_StartIndex); }

		}
		dbg.location(351, 61);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "initializerExpression");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "initializerExpression"



	// $ANTLR start "arrayValueList"
	// GosuProg.g:353:1: arrayValueList : expression ( ',' expression )* ;
	public final void arrayValueList() throws RecognitionException {
		int arrayValueList_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "arrayValueList");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(353, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 103) ) { return; }

			// GosuProg.g:353:16: ( expression ( ',' expression )* )
			dbg.enterAlt(1);

			// GosuProg.g:353:18: expression ( ',' expression )*
			{
			dbg.location(353,18);
			pushFollow(FOLLOW_expression_in_arrayValueList4860);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(353,29);
			// GosuProg.g:353:29: ( ',' expression )*
			try { dbg.enterSubRule(139);

			loop139:
			while (true) {
				int alt139=2;
				try { dbg.enterDecision(139, decisionCanBacktrack[139]);

				int LA139_0 = input.LA(1);
				if ( (LA139_0==46) ) {
					alt139=1;
				}

				} finally {dbg.exitDecision(139);}

				switch (alt139) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:353:30: ',' expression
					{
					dbg.location(353,30);
					match(input,46,FOLLOW_46_in_arrayValueList4863); if (state.failed) return;dbg.location(353,34);
					pushFollow(FOLLOW_expression_in_arrayValueList4865);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop139;
				}
			}
			} finally {dbg.exitSubRule(139);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 103, arrayValueList_StartIndex); }

		}
		dbg.location(353, 46);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "arrayValueList");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "arrayValueList"



	// $ANTLR start "mapInitializerList"
	// GosuProg.g:355:1: mapInitializerList : expression '->' expression ( ',' expression '->' expression )* ;
	public final void mapInitializerList() throws RecognitionException {
		int mapInitializerList_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "mapInitializerList");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(355, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 104) ) { return; }

			// GosuProg.g:355:20: ( expression '->' expression ( ',' expression '->' expression )* )
			dbg.enterAlt(1);

			// GosuProg.g:355:22: expression '->' expression ( ',' expression '->' expression )*
			{
			dbg.location(355,22);
			pushFollow(FOLLOW_expression_in_mapInitializerList4876);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(355,33);
			match(input,50,FOLLOW_50_in_mapInitializerList4878); if (state.failed) return;dbg.location(355,38);
			pushFollow(FOLLOW_expression_in_mapInitializerList4880);
			expression();
			state._fsp--;
			if (state.failed) return;dbg.location(355,49);
			// GosuProg.g:355:49: ( ',' expression '->' expression )*
			try { dbg.enterSubRule(140);

			loop140:
			while (true) {
				int alt140=2;
				try { dbg.enterDecision(140, decisionCanBacktrack[140]);

				int LA140_0 = input.LA(1);
				if ( (LA140_0==46) ) {
					alt140=1;
				}

				} finally {dbg.exitDecision(140);}

				switch (alt140) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:355:50: ',' expression '->' expression
					{
					dbg.location(355,50);
					match(input,46,FOLLOW_46_in_mapInitializerList4883); if (state.failed) return;dbg.location(355,54);
					pushFollow(FOLLOW_expression_in_mapInitializerList4885);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(355,65);
					match(input,50,FOLLOW_50_in_mapInitializerList4887); if (state.failed) return;dbg.location(355,70);
					pushFollow(FOLLOW_expression_in_mapInitializerList4889);
					expression();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop140;
				}
			}
			} finally {dbg.exitSubRule(140);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 104, mapInitializerList_StartIndex); }

		}
		dbg.location(355, 84);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "mapInitializerList");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "mapInitializerList"



	// $ANTLR start "objectInitializer"
	// GosuProg.g:357:1: objectInitializer : initializerAssignment ( ',' initializerAssignment )* ;
	public final void objectInitializer() throws RecognitionException {
		int objectInitializer_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "objectInitializer");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(357, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 105) ) { return; }

			// GosuProg.g:357:19: ( initializerAssignment ( ',' initializerAssignment )* )
			dbg.enterAlt(1);

			// GosuProg.g:357:21: initializerAssignment ( ',' initializerAssignment )*
			{
			dbg.location(357,21);
			pushFollow(FOLLOW_initializerAssignment_in_objectInitializer4902);
			initializerAssignment();
			state._fsp--;
			if (state.failed) return;dbg.location(357,43);
			// GosuProg.g:357:43: ( ',' initializerAssignment )*
			try { dbg.enterSubRule(141);

			loop141:
			while (true) {
				int alt141=2;
				try { dbg.enterDecision(141, decisionCanBacktrack[141]);

				int LA141_0 = input.LA(1);
				if ( (LA141_0==46) ) {
					alt141=1;
				}

				} finally {dbg.exitDecision(141);}

				switch (alt141) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:357:44: ',' initializerAssignment
					{
					dbg.location(357,44);
					match(input,46,FOLLOW_46_in_objectInitializer4905); if (state.failed) return;dbg.location(357,48);
					pushFollow(FOLLOW_initializerAssignment_in_objectInitializer4907);
					initializerAssignment();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop141;
				}
			}
			} finally {dbg.exitSubRule(141);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 105, objectInitializer_StartIndex); }

		}
		dbg.location(357, 71);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "objectInitializer");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "objectInitializer"



	// $ANTLR start "initializerAssignment"
	// GosuProg.g:359:1: initializerAssignment : ':' id '=' expression ;
	public final void initializerAssignment() throws RecognitionException {
		int initializerAssignment_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "initializerAssignment");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(359, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 106) ) { return; }

			// GosuProg.g:359:23: ( ':' id '=' expression )
			dbg.enterAlt(1);

			// GosuProg.g:359:25: ':' id '=' expression
			{
			dbg.location(359,25);
			match(input,56,FOLLOW_56_in_initializerAssignment4918); if (state.failed) return;dbg.location(359,29);
			pushFollow(FOLLOW_id_in_initializerAssignment4920);
			id();
			state._fsp--;
			if (state.failed) return;dbg.location(359,32);
			match(input,60,FOLLOW_60_in_initializerAssignment4922); if (state.failed) return;dbg.location(359,36);
			pushFollow(FOLLOW_expression_in_initializerAssignment4924);
			expression();
			state._fsp--;
			if (state.failed) return;
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 106, initializerAssignment_StartIndex); }

		}
		dbg.location(359, 47);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "initializerAssignment");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "initializerAssignment"



	// $ANTLR start "indirectMemberAccess"
	// GosuProg.g:361:1: indirectMemberAccess : ( ( '.' | '?.' | '*.' ) idAll typeArguments | featureLiteral | ( '[' | '?[' ) expression ']' |{...}? arguments )* ;
	public final void indirectMemberAccess() throws RecognitionException {
		int indirectMemberAccess_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "indirectMemberAccess");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(361, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 107) ) { return; }

			// GosuProg.g:361:22: ( ( ( '.' | '?.' | '*.' ) idAll typeArguments | featureLiteral | ( '[' | '?[' ) expression ']' |{...}? arguments )* )
			dbg.enterAlt(1);

			// GosuProg.g:362:24: ( ( '.' | '?.' | '*.' ) idAll typeArguments | featureLiteral | ( '[' | '?[' ) expression ']' |{...}? arguments )*
			{
			dbg.location(362,24);
			// GosuProg.g:362:24: ( ( '.' | '?.' | '*.' ) idAll typeArguments | featureLiteral | ( '[' | '?[' ) expression ']' |{...}? arguments )*
			try { dbg.enterSubRule(142);

			loop142:
			while (true) {
				int alt142=5;
				try { dbg.enterDecision(142, decisionCanBacktrack[142]);

				try {
					isCyclicDecision = true;
					alt142 = dfa142.predict(input);
				}
				catch (NoViableAltException nvae) {
					dbg.recognitionException(nvae);
					throw nvae;
				}
				} finally {dbg.exitDecision(142);}

				switch (alt142) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:363:27: ( '.' | '?.' | '*.' ) idAll typeArguments
					{
					dbg.location(363,27);
					if ( input.LA(1)==41||input.LA(1)==51||input.LA(1)==69 ) {
						input.consume();
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						dbg.recognitionException(mse);
						throw mse;
					}dbg.location(363,47);
					pushFollow(FOLLOW_idAll_in_indirectMemberAccess4997);
					idAll();
					state._fsp--;
					if (state.failed) return;dbg.location(363,53);
					pushFollow(FOLLOW_typeArguments_in_indirectMemberAccess4999);
					typeArguments();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:364:27: featureLiteral
					{
					dbg.location(364,27);
					pushFollow(FOLLOW_featureLiteral_in_indirectMemberAccess5030);
					featureLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:365:27: ( '[' | '?[' ) expression ']'
					{
					dbg.location(365,27);
					if ( input.LA(1)==72||input.LA(1)==76 ) {
						input.consume();
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						dbg.recognitionException(mse);
						throw mse;
					}dbg.location(365,38);
					pushFollow(FOLLOW_expression_in_indirectMemberAccess5092);
					expression();
					state._fsp--;
					if (state.failed) return;dbg.location(365,50);
					match(input,78,FOLLOW_78_in_indirectMemberAccess5095); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:366:27: {...}? arguments
					{
					dbg.location(366,27);
					if ( !(evalPredicate(input.LT(-1).getLine() == input.LT(1).getLine()  ,"input.LT(-1).getLine() == input.LT(1).getLine()  ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "indirectMemberAccess", "input.LT(-1).getLine() == input.LT(1).getLine()  ");
					}dbg.location(366,80);
					pushFollow(FOLLOW_arguments_in_indirectMemberAccess5141);
					arguments();
					state._fsp--;
					if (state.failed) return;
					}
					break;

				default :
					break loop142;
				}
			}
			} finally {dbg.exitSubRule(142);}

			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 107, indirectMemberAccess_StartIndex); }

		}
		dbg.location(370, 21);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "indirectMemberAccess");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "indirectMemberAccess"



	// $ANTLR start "literal"
	// GosuProg.g:372:1: literal : ( NumberLiteral | featureLiteral | StringLiteral | CharLiteral | 'true' | 'false' | 'null' );
	public final void literal() throws RecognitionException {
		int literal_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "literal");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(372, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 108) ) { return; }

			// GosuProg.g:372:9: ( NumberLiteral | featureLiteral | StringLiteral | CharLiteral | 'true' | 'false' | 'null' )
			int alt143=7;
			try { dbg.enterDecision(143, decisionCanBacktrack[143]);

			switch ( input.LA(1) ) {
			case NumberLiteral:
				{
				alt143=1;
				}
				break;
			case 31:
				{
				alt143=2;
				}
				break;
			case StringLiteral:
				{
				alt143=3;
				}
				break;
			case CharLiteral:
				{
				alt143=4;
				}
				break;
			case 149:
				{
				alt143=5;
				}
				break;
			case 106:
				{
				alt143=6;
				}
				break;
			case 125:
				{
				alt143=7;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 143, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(143);}

			switch (alt143) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:372:11: NumberLiteral
					{
					dbg.location(372,11);
					match(input,NumberLiteral,FOLLOW_NumberLiteral_in_literal5200); if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:373:11: featureLiteral
					{
					dbg.location(373,11);
					pushFollow(FOLLOW_featureLiteral_in_literal5223);
					featureLiteral();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:374:11: StringLiteral
					{
					dbg.location(374,11);
					match(input,StringLiteral,FOLLOW_StringLiteral_in_literal5245); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:375:11: CharLiteral
					{
					dbg.location(375,11);
					match(input,CharLiteral,FOLLOW_CharLiteral_in_literal5268); if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:376:11: 'true'
					{
					dbg.location(376,11);
					match(input,149,FOLLOW_149_in_literal5293); if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:377:11: 'false'
					{
					dbg.location(377,11);
					match(input,106,FOLLOW_106_in_literal5323); if (state.failed) return;
					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// GosuProg.g:378:11: 'null'
					{
					dbg.location(378,11);
					match(input,125,FOLLOW_125_in_literal5352); if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 108, literal_StartIndex); }

		}
		dbg.location(379, 8);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "literal");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "literal"



	// $ANTLR start "orOp"
	// GosuProg.g:381:1: orOp : ( '||' | 'or' );
	public final void orOp() throws RecognitionException {
		int orOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "orOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(381, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 109) ) { return; }

			// GosuProg.g:381:6: ( '||' | 'or' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(381,6);
			if ( input.LA(1)==126||input.LA(1)==167 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 109, orOp_StartIndex); }

		}
		dbg.location(381, 19);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "orOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "orOp"



	// $ANTLR start "andOp"
	// GosuProg.g:383:1: andOp : ( '&&' | 'and' );
	public final void andOp() throws RecognitionException {
		int andOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "andOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(383, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 110) ) { return; }

			// GosuProg.g:383:7: ( '&&' | 'and' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(383,7);
			if ( input.LA(1)==34||input.LA(1)==82 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 110, andOp_StartIndex); }

		}
		dbg.location(383, 21);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "andOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "andOp"



	// $ANTLR start "assignmentOp"
	// GosuProg.g:385:1: assignmentOp : ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '&&=' | '|=' | '||=' | '^=' | '%=' | ( '<' '<' '=' )=>t1= '<' t2= '<' t3= '=' {...}?| ( '>' '>' '>' '=' )=>t1= '>' t2= '>' t3= '>' t4= '=' {...}?| ( '>' '>' '=' )=>t1= '>' t2= '>' t3= '=' {...}?);
	public final void assignmentOp() throws RecognitionException {
		int assignmentOp_StartIndex = input.index();

		Token t1=null;
		Token t2=null;
		Token t3=null;
		Token t4=null;

		try { dbg.enterRule(getGrammarFileName(), "assignmentOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(385, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 111) ) { return; }

			// GosuProg.g:385:14: ( '=' | '+=' | '-=' | '*=' | '/=' | '&=' | '&&=' | '|=' | '||=' | '^=' | '%=' | ( '<' '<' '=' )=>t1= '<' t2= '<' t3= '=' {...}?| ( '>' '>' '>' '=' )=>t1= '>' t2= '>' t3= '>' t4= '=' {...}?| ( '>' '>' '=' )=>t1= '>' t2= '>' t3= '=' {...}?)
			int alt144=14;
			try { dbg.enterDecision(144, decisionCanBacktrack[144]);

			int LA144_0 = input.LA(1);
			if ( (LA144_0==60) ) {
				alt144=1;
			}
			else if ( (LA144_0==45) ) {
				alt144=2;
			}
			else if ( (LA144_0==49) ) {
				alt144=3;
			}
			else if ( (LA144_0==42) ) {
				alt144=4;
			}
			else if ( (LA144_0==55) ) {
				alt144=5;
			}
			else if ( (LA144_0==37) ) {
				alt144=6;
			}
			else if ( (LA144_0==35) ) {
				alt144=7;
			}
			else if ( (LA144_0==166) ) {
				alt144=8;
			}
			else if ( (LA144_0==168) ) {
				alt144=9;
			}
			else if ( (LA144_0==80) ) {
				alt144=10;
			}
			else if ( (LA144_0==33) ) {
				alt144=11;
			}
			else if ( (LA144_0==58) && (synpred232_GosuProg())) {
				alt144=12;
			}
			else if ( (LA144_0==63) ) {
				int LA144_13 = input.LA(2);
				if ( (LA144_13==63) ) {
					int LA144_14 = input.LA(3);
					if ( (LA144_14==63) && (synpred233_GosuProg())) {
						alt144=13;
					}
					else if ( (LA144_14==60) && (synpred234_GosuProg())) {
						alt144=14;
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 144, 13, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 144, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(144);}

			switch (alt144) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:386:9: '='
					{
					dbg.location(386,9);
					match(input,60,FOLLOW_60_in_assignmentOp5403); if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:387:9: '+='
					{
					dbg.location(387,9);
					match(input,45,FOLLOW_45_in_assignmentOp5413); if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:388:9: '-='
					{
					dbg.location(388,9);
					match(input,49,FOLLOW_49_in_assignmentOp5423); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:389:9: '*='
					{
					dbg.location(389,9);
					match(input,42,FOLLOW_42_in_assignmentOp5433); if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:390:9: '/='
					{
					dbg.location(390,9);
					match(input,55,FOLLOW_55_in_assignmentOp5443); if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:391:9: '&='
					{
					dbg.location(391,9);
					match(input,37,FOLLOW_37_in_assignmentOp5453); if (state.failed) return;
					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// GosuProg.g:392:9: '&&='
					{
					dbg.location(392,9);
					match(input,35,FOLLOW_35_in_assignmentOp5463); if (state.failed) return;
					}
					break;
				case 8 :
					dbg.enterAlt(8);

					// GosuProg.g:393:9: '|='
					{
					dbg.location(393,9);
					match(input,166,FOLLOW_166_in_assignmentOp5473); if (state.failed) return;
					}
					break;
				case 9 :
					dbg.enterAlt(9);

					// GosuProg.g:394:9: '||='
					{
					dbg.location(394,9);
					match(input,168,FOLLOW_168_in_assignmentOp5483); if (state.failed) return;
					}
					break;
				case 10 :
					dbg.enterAlt(10);

					// GosuProg.g:395:9: '^='
					{
					dbg.location(395,9);
					match(input,80,FOLLOW_80_in_assignmentOp5493); if (state.failed) return;
					}
					break;
				case 11 :
					dbg.enterAlt(11);

					// GosuProg.g:396:9: '%='
					{
					dbg.location(396,9);
					match(input,33,FOLLOW_33_in_assignmentOp5503); if (state.failed) return;
					}
					break;
				case 12 :
					dbg.enterAlt(12);

					// GosuProg.g:397:9: ( '<' '<' '=' )=>t1= '<' t2= '<' t3= '=' {...}?
					{
					dbg.location(397,27);
					t1=(Token)match(input,58,FOLLOW_58_in_assignmentOp5524); if (state.failed) return;dbg.location(397,34);
					t2=(Token)match(input,58,FOLLOW_58_in_assignmentOp5528); if (state.failed) return;dbg.location(397,41);
					t3=(Token)match(input,60,FOLLOW_60_in_assignmentOp5532); if (state.failed) return;dbg.location(398,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() &&
					          t2.getLine() == t3.getLine() &&
					          t2.getCharPositionInLine() + 1 == t3.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "assignmentOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ");
					}
					}
					break;
				case 13 :
					dbg.enterAlt(13);

					// GosuProg.g:402:9: ( '>' '>' '>' '=' )=>t1= '>' t2= '>' t3= '>' t4= '=' {...}?
					{
					dbg.location(402,31);
					t1=(Token)match(input,63,FOLLOW_63_in_assignmentOp5565); if (state.failed) return;dbg.location(402,38);
					t2=(Token)match(input,63,FOLLOW_63_in_assignmentOp5569); if (state.failed) return;dbg.location(402,45);
					t3=(Token)match(input,63,FOLLOW_63_in_assignmentOp5573); if (state.failed) return;dbg.location(402,52);
					t4=(Token)match(input,60,FOLLOW_60_in_assignmentOp5577); if (state.failed) return;dbg.location(403,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() &&
					          t2.getLine() == t3.getLine() &&
					          t2.getCharPositionInLine() + 1 == t3.getCharPositionInLine() &&
					          t3.getLine() == t4.getLine() &&
					          t3.getCharPositionInLine() + 1 == t4.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() &&\n          $t3.getLine() == $t4.getLine() &&\n          $t3.getCharPositionInLine() + 1 == $t4.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "assignmentOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() &&\n          $t3.getLine() == $t4.getLine() &&\n          $t3.getCharPositionInLine() + 1 == $t4.getCharPositionInLine() ");
					}
					}
					break;
				case 14 :
					dbg.enterAlt(14);

					// GosuProg.g:409:9: ( '>' '>' '=' )=>t1= '>' t2= '>' t3= '=' {...}?
					{
					dbg.location(409,27);
					t1=(Token)match(input,63,FOLLOW_63_in_assignmentOp5608); if (state.failed) return;dbg.location(409,34);
					t2=(Token)match(input,63,FOLLOW_63_in_assignmentOp5612); if (state.failed) return;dbg.location(409,41);
					t3=(Token)match(input,60,FOLLOW_60_in_assignmentOp5616); if (state.failed) return;dbg.location(410,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() &&
					          t2.getLine() == t3.getLine() &&
					          t2.getCharPositionInLine() + 1 == t3.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "assignmentOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ");
					}
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 111, assignmentOp_StartIndex); }

		}
		dbg.location(414, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "assignmentOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "assignmentOp"



	// $ANTLR start "incrementOp"
	// GosuProg.g:417:1: incrementOp : ( '++' | '--' );
	public final void incrementOp() throws RecognitionException {
		int incrementOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "incrementOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(417, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 112) ) { return; }

			// GosuProg.g:417:13: ( '++' | '--' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(417,13);
			if ( input.LA(1)==44||input.LA(1)==48 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 112, incrementOp_StartIndex); }

		}
		dbg.location(417, 26);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "incrementOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "incrementOp"



	// $ANTLR start "equalityOp"
	// GosuProg.g:419:1: equalityOp : ( '===' | '!==' | '==' | '!=' | '<>' );
	public final void equalityOp() throws RecognitionException {
		int equalityOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "equalityOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(419, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 113) ) { return; }

			// GosuProg.g:419:12: ( '===' | '!==' | '==' | '!=' | '<>' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(419,12);
			if ( (input.LA(1) >= 29 && input.LA(1) <= 30)||input.LA(1)==59||(input.LA(1) >= 61 && input.LA(1) <= 62) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 113, equalityOp_StartIndex); }

		}
		dbg.location(419, 48);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "equalityOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "equalityOp"



	// $ANTLR start "intervalOp"
	// GosuProg.g:421:1: intervalOp : ( '..' | '|..' | '..|' | '|..|' );
	public final void intervalOp() throws RecognitionException {
		int intervalOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "intervalOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(421, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 114) ) { return; }

			// GosuProg.g:421:12: ( '..' | '|..' | '..|' | '|..|' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(421,12);
			if ( (input.LA(1) >= 52 && input.LA(1) <= 53)||(input.LA(1) >= 164 && input.LA(1) <= 165) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 114, intervalOp_StartIndex); }

		}
		dbg.location(421, 43);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "intervalOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "intervalOp"



	// $ANTLR start "relOp"
	// GosuProg.g:423:1: relOp : ( ( '<' '=' )=>t1= '<' t2= '=' {...}?| ( '>' '=' )=>t1= '>' t2= '=' {...}?| '<' | '>' );
	public final void relOp() throws RecognitionException {
		int relOp_StartIndex = input.index();

		Token t1=null;
		Token t2=null;

		try { dbg.enterRule(getGrammarFileName(), "relOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(423, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 115) ) { return; }

			// GosuProg.g:423:7: ( ( '<' '=' )=>t1= '<' t2= '=' {...}?| ( '>' '=' )=>t1= '>' t2= '=' {...}?| '<' | '>' )
			int alt145=4;
			try { dbg.enterDecision(145, decisionCanBacktrack[145]);

			int LA145_0 = input.LA(1);
			if ( (LA145_0==58) ) {
				int LA145_1 = input.LA(2);
				if ( (LA145_1==60) && (synpred243_GosuProg())) {
					alt145=1;
				}
				else if ( (LA145_1==CharLiteral||LA145_1==Ident||LA145_1==NumberLiteral||LA145_1==StringLiteral||LA145_1==25||LA145_1==28||LA145_1==31||LA145_1==38||LA145_1==43||LA145_1==47||(LA145_1 >= 74 && LA145_1 <= 75)||LA145_1==77||LA145_1==81||(LA145_1 >= 83 && LA145_1 <= 86)||LA145_1==91||LA145_1==93||LA145_1==99||(LA145_1 >= 101 && LA145_1 <= 104)||(LA145_1 >= 106 && LA145_1 <= 107)||LA145_1==109||(LA145_1 >= 113 && LA145_1 <= 114)||LA145_1==118||(LA145_1 >= 120 && LA145_1 <= 125)||LA145_1==127||LA145_1==130||(LA145_1 >= 132 && LA145_1 <= 134)||LA145_1==136||(LA145_1 >= 138 && LA145_1 <= 142)||LA145_1==144||LA145_1==146||LA145_1==149||(LA145_1 >= 153 && LA145_1 <= 154)||(LA145_1 >= 159 && LA145_1 <= 160)||LA145_1==162||LA145_1==170) ) {
					alt145=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 145, 1, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA145_0==63) ) {
				int LA145_2 = input.LA(2);
				if ( (LA145_2==60) && (synpred244_GosuProg())) {
					alt145=2;
				}
				else if ( (LA145_2==CharLiteral||LA145_2==Ident||LA145_2==NumberLiteral||LA145_2==StringLiteral||LA145_2==25||LA145_2==28||LA145_2==31||LA145_2==38||LA145_2==43||LA145_2==47||(LA145_2 >= 74 && LA145_2 <= 75)||LA145_2==77||LA145_2==81||(LA145_2 >= 83 && LA145_2 <= 86)||LA145_2==91||LA145_2==93||LA145_2==99||(LA145_2 >= 101 && LA145_2 <= 104)||(LA145_2 >= 106 && LA145_2 <= 107)||LA145_2==109||(LA145_2 >= 113 && LA145_2 <= 114)||LA145_2==118||(LA145_2 >= 120 && LA145_2 <= 125)||LA145_2==127||LA145_2==130||(LA145_2 >= 132 && LA145_2 <= 134)||LA145_2==136||(LA145_2 >= 138 && LA145_2 <= 142)||LA145_2==144||LA145_2==146||LA145_2==149||(LA145_2 >= 153 && LA145_2 <= 154)||(LA145_2 >= 159 && LA145_2 <= 160)||LA145_2==162||LA145_2==170) ) {
					alt145=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 145, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 145, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(145);}

			switch (alt145) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:424:9: ( '<' '=' )=>t1= '<' t2= '=' {...}?
					{
					dbg.location(424,23);
					t1=(Token)match(input,58,FOLLOW_58_in_relOp5716); if (state.failed) return;dbg.location(424,30);
					t2=(Token)match(input,60,FOLLOW_60_in_relOp5720); if (state.failed) return;dbg.location(425,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "relOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ");
					}
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:427:9: ( '>' '=' )=>t1= '>' t2= '=' {...}?
					{
					dbg.location(427,23);
					t1=(Token)match(input,63,FOLLOW_63_in_relOp5749); if (state.failed) return;dbg.location(427,30);
					t2=(Token)match(input,60,FOLLOW_60_in_relOp5753); if (state.failed) return;dbg.location(428,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "relOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ");
					}
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:430:9: '<'
					{
					dbg.location(430,9);
					match(input,58,FOLLOW_58_in_relOp5773); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:431:9: '>'
					{
					dbg.location(431,9);
					match(input,63,FOLLOW_63_in_relOp5783); if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 115, relOp_StartIndex); }

		}
		dbg.location(432, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "relOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "relOp"



	// $ANTLR start "bitshiftOp"
	// GosuProg.g:434:1: bitshiftOp : ( ( '<' '<' )=>t1= '<' t2= '<' {...}?| ( '>' '>' '>' )=>t1= '>' t2= '>' t3= '>' {...}?| ( '>' '>' )=>t1= '>' t2= '>' {...}?);
	public final void bitshiftOp() throws RecognitionException {
		int bitshiftOp_StartIndex = input.index();

		Token t1=null;
		Token t2=null;
		Token t3=null;

		try { dbg.enterRule(getGrammarFileName(), "bitshiftOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(434, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 116) ) { return; }

			// GosuProg.g:434:12: ( ( '<' '<' )=>t1= '<' t2= '<' {...}?| ( '>' '>' '>' )=>t1= '>' t2= '>' t3= '>' {...}?| ( '>' '>' )=>t1= '>' t2= '>' {...}?)
			int alt146=3;
			try { dbg.enterDecision(146, decisionCanBacktrack[146]);

			int LA146_0 = input.LA(1);
			if ( (LA146_0==58) && (synpred246_GosuProg())) {
				alt146=1;
			}
			else if ( (LA146_0==63) ) {
				int LA146_2 = input.LA(2);
				if ( (LA146_2==63) ) {
					int LA146_3 = input.LA(3);
					if ( (LA146_3==63) && (synpred247_GosuProg())) {
						alt146=2;
					}
					else if ( (LA146_3==28||LA146_3==43||LA146_3==47) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==25||LA146_3==124||LA146_3==142||LA146_3==154||LA146_3==170) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==77) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==101) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==123) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==144||LA146_3==146) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==NumberLiteral) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==31) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==StringLiteral) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==CharLiteral) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==149) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==106) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==125) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==Ident||(LA146_3 >= 74 && LA146_3 <= 75)||LA146_3==81||(LA146_3 >= 83 && LA146_3 <= 85)||LA146_3==91||LA146_3==93||LA146_3==99||(LA146_3 >= 102 && LA146_3 <= 104)||LA146_3==107||LA146_3==109||(LA146_3 >= 113 && LA146_3 <= 114)||LA146_3==118||(LA146_3 >= 120 && LA146_3 <= 122)||LA146_3==127||LA146_3==130||(LA146_3 >= 132 && LA146_3 <= 134)||LA146_3==136||(LA146_3 >= 138 && LA146_3 <= 141)||LA146_3==153||(LA146_3 >= 159 && LA146_3 <= 160)) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==86) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==38) && (synpred248_GosuProg())) {
						alt146=3;
					}
					else if ( (LA146_3==162) && (synpred248_GosuProg())) {
						alt146=3;
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 146, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 146, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}

			} finally {dbg.exitDecision(146);}

			switch (alt146) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:435:9: ( '<' '<' )=>t1= '<' t2= '<' {...}?
					{
					dbg.location(435,23);
					t1=(Token)match(input,58,FOLLOW_58_in_bitshiftOp5813); if (state.failed) return;dbg.location(435,30);
					t2=(Token)match(input,58,FOLLOW_58_in_bitshiftOp5817); if (state.failed) return;dbg.location(436,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "bitshiftOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ");
					}
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:438:9: ( '>' '>' '>' )=>t1= '>' t2= '>' t3= '>' {...}?
					{
					dbg.location(438,27);
					t1=(Token)match(input,63,FOLLOW_63_in_bitshiftOp5848); if (state.failed) return;dbg.location(438,34);
					t2=(Token)match(input,63,FOLLOW_63_in_bitshiftOp5852); if (state.failed) return;dbg.location(438,41);
					t3=(Token)match(input,63,FOLLOW_63_in_bitshiftOp5856); if (state.failed) return;dbg.location(439,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() &&
					          t2.getLine() == t3.getLine() &&
					          t2.getCharPositionInLine() + 1 == t3.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "bitshiftOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() &&\n          $t2.getLine() == $t3.getLine() &&\n          $t2.getCharPositionInLine() + 1 == $t3.getCharPositionInLine() ");
					}
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:443:9: ( '>' '>' )=>t1= '>' t2= '>' {...}?
					{
					dbg.location(443,23);
					t1=(Token)match(input,63,FOLLOW_63_in_bitshiftOp5885); if (state.failed) return;dbg.location(443,30);
					t2=(Token)match(input,63,FOLLOW_63_in_bitshiftOp5889); if (state.failed) return;dbg.location(444,9);
					if ( !(evalPredicate( t1.getLine() == t2.getLine() &&
					          t1.getCharPositionInLine() + 1 == t2.getCharPositionInLine() ," $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ")) ) {
						if (state.backtracking>0) {state.failed=true; return;}
						throw new FailedPredicateException(input, "bitshiftOp", " $t1.getLine() == $t2.getLine() &&\n          $t1.getCharPositionInLine() + 1 == $t2.getCharPositionInLine() ");
					}
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 116, bitshiftOp_StartIndex); }

		}
		dbg.location(446, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitshiftOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "bitshiftOp"



	// $ANTLR start "additiveOp"
	// GosuProg.g:448:1: additiveOp : ( '+' | '-' | '?+' | '?-' | '!+' | '!-' );
	public final void additiveOp() throws RecognitionException {
		int additiveOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "additiveOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(448, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 117) ) { return; }

			// GosuProg.g:448:12: ( '+' | '-' | '?+' | '?-' | '!+' | '!-' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(448,12);
			if ( (input.LA(1) >= 27 && input.LA(1) <= 28)||input.LA(1)==43||input.LA(1)==47||(input.LA(1) >= 67 && input.LA(1) <= 68) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 117, additiveOp_StartIndex); }

		}
		dbg.location(448, 50);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "additiveOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "additiveOp"



	// $ANTLR start "multiplicativeOp"
	// GosuProg.g:450:1: multiplicativeOp : ( '*' | '/' | '%' | '?*' | '!*' | '?/' | '?%' );
	public final void multiplicativeOp() throws RecognitionException {
		int multiplicativeOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "multiplicativeOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(450, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 118) ) { return; }

			// GosuProg.g:450:18: ( '*' | '/' | '%' | '?*' | '!*' | '?/' | '?%' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(450,18);
			if ( input.LA(1)==26||input.LA(1)==32||input.LA(1)==40||input.LA(1)==54||input.LA(1)==64||input.LA(1)==66||input.LA(1)==70 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 118, multiplicativeOp_StartIndex); }

		}
		dbg.location(450, 63);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multiplicativeOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "multiplicativeOp"



	// $ANTLR start "typeAsOp"
	// GosuProg.g:452:1: typeAsOp : ( 'typeas' | 'as' );
	public final void typeAsOp() throws RecognitionException {
		int typeAsOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "typeAsOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(452, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 119) ) { return; }

			// GosuProg.g:452:10: ( 'typeas' | 'as' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(452,10);
			if ( input.LA(1)==84||input.LA(1)==151 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 119, typeAsOp_StartIndex); }

		}
		dbg.location(452, 27);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "typeAsOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "typeAsOp"



	// $ANTLR start "unaryOp"
	// GosuProg.g:454:1: unaryOp : ( '~' | '!' | 'not' | 'typeof' | 'statictypeof' );
	public final void unaryOp() throws RecognitionException {
		int unaryOp_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "unaryOp");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(454, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 120) ) { return; }

			// GosuProg.g:454:9: ( '~' | '!' | 'not' | 'typeof' | 'statictypeof' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(454,9);
			if ( input.LA(1)==25||input.LA(1)==124||input.LA(1)==142||input.LA(1)==154||input.LA(1)==170 ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 120, unaryOp_StartIndex); }

		}
		dbg.location(454, 55);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "unaryOp");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "unaryOp"



	// $ANTLR start "id"
	// GosuProg.g:456:1: id : ( Ident | 'true' | 'false' | 'NaN' | 'Infinity' | 'null' | 'length' | 'exists' | 'startswith' | 'contains' | 'where' | 'find' | 'as' | 'except' | 'index' | 'iterator' | 'get' | 'set' | 'assert' | 'private' | 'internal' | 'protected' | 'public' | 'abstract' | 'hide' | 'final' | 'static' | 'readonly' | 'outer' | 'execution' | 'request' | 'session' | 'application' | 'void' | 'block' | 'enhancement' | 'classpath' | 'typeloader' );
	public final void id() throws RecognitionException {
		int id_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "id");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(456, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 121) ) { return; }

			// GosuProg.g:456:4: ( Ident | 'true' | 'false' | 'NaN' | 'Infinity' | 'null' | 'length' | 'exists' | 'startswith' | 'contains' | 'where' | 'find' | 'as' | 'except' | 'index' | 'iterator' | 'get' | 'set' | 'assert' | 'private' | 'internal' | 'protected' | 'public' | 'abstract' | 'hide' | 'final' | 'static' | 'readonly' | 'outer' | 'execution' | 'request' | 'session' | 'application' | 'void' | 'block' | 'enhancement' | 'classpath' | 'typeloader' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(456,4);
			if ( input.LA(1)==Ident||(input.LA(1) >= 74 && input.LA(1) <= 75)||input.LA(1)==81||(input.LA(1) >= 83 && input.LA(1) <= 86)||input.LA(1)==91||input.LA(1)==93||input.LA(1)==99||(input.LA(1) >= 102 && input.LA(1) <= 104)||(input.LA(1) >= 106 && input.LA(1) <= 107)||input.LA(1)==109||(input.LA(1) >= 113 && input.LA(1) <= 114)||input.LA(1)==118||(input.LA(1) >= 120 && input.LA(1) <= 122)||input.LA(1)==125||input.LA(1)==127||input.LA(1)==130||(input.LA(1) >= 132 && input.LA(1) <= 134)||input.LA(1)==136||(input.LA(1) >= 138 && input.LA(1) <= 141)||input.LA(1)==149||input.LA(1)==153||(input.LA(1) >= 159 && input.LA(1) <= 160) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 121, id_StartIndex); }

		}
		dbg.location(494, 3);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "id");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "id"



	// $ANTLR start "idclassOrInterfaceType"
	// GosuProg.g:497:1: idclassOrInterfaceType : ( Ident | 'true' | 'false' | 'NaN' | 'Infinity' | 'null' | 'length' | 'exists' | 'startswith' | 'contains' | 'where' | 'find' | 'as' | 'except' | 'index' | 'iterator' | 'get' | 'set' | 'assert' | 'private' | 'internal' | 'protected' | 'public' | 'abstract' | 'hide' | 'final' | 'static' | 'readonly' | 'outer' | 'execution' | 'request' | 'session' | 'application' | 'void' | 'enhancement' | 'classpath' | 'typeloader' );
	public final void idclassOrInterfaceType() throws RecognitionException {
		int idclassOrInterfaceType_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "idclassOrInterfaceType");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(497, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 122) ) { return; }

			// GosuProg.g:497:24: ( Ident | 'true' | 'false' | 'NaN' | 'Infinity' | 'null' | 'length' | 'exists' | 'startswith' | 'contains' | 'where' | 'find' | 'as' | 'except' | 'index' | 'iterator' | 'get' | 'set' | 'assert' | 'private' | 'internal' | 'protected' | 'public' | 'abstract' | 'hide' | 'final' | 'static' | 'readonly' | 'outer' | 'execution' | 'request' | 'session' | 'application' | 'void' | 'enhancement' | 'classpath' | 'typeloader' )
			dbg.enterAlt(1);

			// GosuProg.g:
			{
			dbg.location(497,24);
			if ( input.LA(1)==Ident||(input.LA(1) >= 74 && input.LA(1) <= 75)||input.LA(1)==81||(input.LA(1) >= 83 && input.LA(1) <= 85)||input.LA(1)==91||input.LA(1)==93||input.LA(1)==99||(input.LA(1) >= 102 && input.LA(1) <= 104)||(input.LA(1) >= 106 && input.LA(1) <= 107)||input.LA(1)==109||(input.LA(1) >= 113 && input.LA(1) <= 114)||input.LA(1)==118||(input.LA(1) >= 120 && input.LA(1) <= 122)||input.LA(1)==125||input.LA(1)==127||input.LA(1)==130||(input.LA(1) >= 132 && input.LA(1) <= 134)||input.LA(1)==136||(input.LA(1) >= 138 && input.LA(1) <= 141)||input.LA(1)==149||input.LA(1)==153||(input.LA(1) >= 159 && input.LA(1) <= 160) ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				dbg.recognitionException(mse);
				throw mse;
			}
			}

		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 122, idclassOrInterfaceType_StartIndex); }

		}
		dbg.location(534, 3);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "idclassOrInterfaceType");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "idclassOrInterfaceType"



	// $ANTLR start "idAll"
	// GosuProg.g:536:1: idAll : ( id | 'and' | 'or' | 'not' | 'in' | 'var' | 'delegate' | 'represents' | 'typeof' | 'statictypeof' | 'typeis' | 'typeas' | 'package' | 'uses' | 'if' | 'else' | 'unless' | 'foreach' | 'for' | 'while' | 'do' | 'continue' | 'break' | 'return' | 'construct' | 'function' | 'property' | 'try' | 'catch' | 'finally' | 'throw' | 'new' | 'switch' | 'case' | 'default' | 'eval' | 'override' | 'extends' | 'transient' | 'implements' | 'class' | 'interface' | 'structure' | 'enum' | 'using' );
	public final void idAll() throws RecognitionException {
		int idAll_StartIndex = input.index();

		try { dbg.enterRule(getGrammarFileName(), "idAll");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(536, 0);

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 123) ) { return; }

			// GosuProg.g:536:7: ( id | 'and' | 'or' | 'not' | 'in' | 'var' | 'delegate' | 'represents' | 'typeof' | 'statictypeof' | 'typeis' | 'typeas' | 'package' | 'uses' | 'if' | 'else' | 'unless' | 'foreach' | 'for' | 'while' | 'do' | 'continue' | 'break' | 'return' | 'construct' | 'function' | 'property' | 'try' | 'catch' | 'finally' | 'throw' | 'new' | 'switch' | 'case' | 'default' | 'eval' | 'override' | 'extends' | 'transient' | 'implements' | 'class' | 'interface' | 'structure' | 'enum' | 'using' )
			int alt147=45;
			try { dbg.enterDecision(147, decisionCanBacktrack[147]);

			switch ( input.LA(1) ) {
			case Ident:
			case 74:
			case 75:
			case 81:
			case 83:
			case 84:
			case 85:
			case 86:
			case 91:
			case 93:
			case 99:
			case 102:
			case 103:
			case 104:
			case 106:
			case 107:
			case 109:
			case 113:
			case 114:
			case 118:
			case 120:
			case 121:
			case 122:
			case 125:
			case 127:
			case 130:
			case 132:
			case 133:
			case 134:
			case 136:
			case 138:
			case 139:
			case 140:
			case 141:
			case 149:
			case 153:
			case 159:
			case 160:
				{
				alt147=1;
				}
				break;
			case 82:
				{
				alt147=2;
				}
				break;
			case 126:
				{
				alt147=3;
				}
				break;
			case 124:
				{
				alt147=4;
				}
				break;
			case 117:
				{
				alt147=5;
				}
				break;
			case 158:
				{
				alt147=6;
				}
				break;
			case 96:
				{
				alt147=7;
				}
				break;
			case 135:
				{
				alt147=8;
				}
				break;
			case 154:
				{
				alt147=9;
				}
				break;
			case 142:
				{
				alt147=10;
				}
				break;
			case 152:
				{
				alt147=11;
				}
				break;
			case 151:
				{
				alt147=12;
				}
				break;
			case 129:
				{
				alt147=13;
				}
				break;
			case 156:
				{
				alt147=14;
				}
				break;
			case 115:
				{
				alt147=15;
				}
				break;
			case 98:
				{
				alt147=16;
				}
				break;
			case 155:
				{
				alt147=17;
				}
				break;
			case 111:
				{
				alt147=18;
				}
				break;
			case 110:
				{
				alt147=19;
				}
				break;
			case 161:
				{
				alt147=20;
				}
				break;
			case 97:
				{
				alt147=21;
				}
				break;
			case 94:
				{
				alt147=22;
				}
				break;
			case 87:
				{
				alt147=23;
				}
				break;
			case 137:
				{
				alt147=24;
				}
				break;
			case 92:
				{
				alt147=25;
				}
				break;
			case 112:
				{
				alt147=26;
				}
				break;
			case 131:
				{
				alt147=27;
				}
				break;
			case 150:
				{
				alt147=28;
				}
				break;
			case 89:
				{
				alt147=29;
				}
				break;
			case 108:
				{
				alt147=30;
				}
				break;
			case 147:
				{
				alt147=31;
				}
				break;
			case 123:
				{
				alt147=32;
				}
				break;
			case 145:
				{
				alt147=33;
				}
				break;
			case 88:
				{
				alt147=34;
				}
				break;
			case 95:
				{
				alt147=35;
				}
				break;
			case 101:
				{
				alt147=36;
				}
				break;
			case 128:
				{
				alt147=37;
				}
				break;
			case 105:
				{
				alt147=38;
				}
				break;
			case 148:
				{
				alt147=39;
				}
				break;
			case 116:
				{
				alt147=40;
				}
				break;
			case 90:
				{
				alt147=41;
				}
				break;
			case 119:
				{
				alt147=42;
				}
				break;
			case 143:
				{
				alt147=43;
				}
				break;
			case 100:
				{
				alt147=44;
				}
				break;
			case 157:
				{
				alt147=45;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return;}
				NoViableAltException nvae =
					new NoViableAltException("", 147, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(147);}

			switch (alt147) {
				case 1 :
					dbg.enterAlt(1);

					// GosuProg.g:536:10: id
					{
					dbg.location(536,10);
					pushFollow(FOLLOW_id_in_idAll8203);
					id();
					state._fsp--;
					if (state.failed) return;
					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// GosuProg.g:537:10: 'and'
					{
					dbg.location(537,10);
					match(input,82,FOLLOW_82_in_idAll8233); if (state.failed) return;
					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// GosuProg.g:538:10: 'or'
					{
					dbg.location(538,10);
					match(input,126,FOLLOW_126_in_idAll8260); if (state.failed) return;
					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// GosuProg.g:539:10: 'not'
					{
					dbg.location(539,10);
					match(input,124,FOLLOW_124_in_idAll8288); if (state.failed) return;
					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// GosuProg.g:540:10: 'in'
					{
					dbg.location(540,10);
					match(input,117,FOLLOW_117_in_idAll8315); if (state.failed) return;
					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// GosuProg.g:541:10: 'var'
					{
					dbg.location(541,10);
					match(input,158,FOLLOW_158_in_idAll8343); if (state.failed) return;
					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// GosuProg.g:542:10: 'delegate'
					{
					dbg.location(542,10);
					match(input,96,FOLLOW_96_in_idAll8370); if (state.failed) return;
					}
					break;
				case 8 :
					dbg.enterAlt(8);

					// GosuProg.g:543:10: 'represents'
					{
					dbg.location(543,10);
					match(input,135,FOLLOW_135_in_idAll8392); if (state.failed) return;
					}
					break;
				case 9 :
					dbg.enterAlt(9);

					// GosuProg.g:544:10: 'typeof'
					{
					dbg.location(544,10);
					match(input,154,FOLLOW_154_in_idAll8412); if (state.failed) return;
					}
					break;
				case 10 :
					dbg.enterAlt(10);

					// GosuProg.g:545:10: 'statictypeof'
					{
					dbg.location(545,10);
					match(input,142,FOLLOW_142_in_idAll8436); if (state.failed) return;
					}
					break;
				case 11 :
					dbg.enterAlt(11);

					// GosuProg.g:546:10: 'typeis'
					{
					dbg.location(546,10);
					match(input,152,FOLLOW_152_in_idAll8454); if (state.failed) return;
					}
					break;
				case 12 :
					dbg.enterAlt(12);

					// GosuProg.g:547:10: 'typeas'
					{
					dbg.location(547,10);
					match(input,151,FOLLOW_151_in_idAll8478); if (state.failed) return;
					}
					break;
				case 13 :
					dbg.enterAlt(13);

					// GosuProg.g:548:10: 'package'
					{
					dbg.location(548,10);
					match(input,129,FOLLOW_129_in_idAll8502); if (state.failed) return;
					}
					break;
				case 14 :
					dbg.enterAlt(14);

					// GosuProg.g:549:10: 'uses'
					{
					dbg.location(549,10);
					match(input,156,FOLLOW_156_in_idAll8525); if (state.failed) return;
					}
					break;
				case 15 :
					dbg.enterAlt(15);

					// GosuProg.g:550:10: 'if'
					{
					dbg.location(550,10);
					match(input,115,FOLLOW_115_in_idAll8551); if (state.failed) return;
					}
					break;
				case 16 :
					dbg.enterAlt(16);

					// GosuProg.g:551:10: 'else'
					{
					dbg.location(551,10);
					match(input,98,FOLLOW_98_in_idAll8579); if (state.failed) return;
					}
					break;
				case 17 :
					dbg.enterAlt(17);

					// GosuProg.g:552:10: 'unless'
					{
					dbg.location(552,10);
					match(input,155,FOLLOW_155_in_idAll8605); if (state.failed) return;
					}
					break;
				case 18 :
					dbg.enterAlt(18);

					// GosuProg.g:553:10: 'foreach'
					{
					dbg.location(553,10);
					match(input,111,FOLLOW_111_in_idAll8629); if (state.failed) return;
					}
					break;
				case 19 :
					dbg.enterAlt(19);

					// GosuProg.g:554:10: 'for'
					{
					dbg.location(554,10);
					match(input,110,FOLLOW_110_in_idAll8652); if (state.failed) return;
					}
					break;
				case 20 :
					dbg.enterAlt(20);

					// GosuProg.g:555:10: 'while'
					{
					dbg.location(555,10);
					match(input,161,FOLLOW_161_in_idAll8679); if (state.failed) return;
					}
					break;
				case 21 :
					dbg.enterAlt(21);

					// GosuProg.g:556:10: 'do'
					{
					dbg.location(556,10);
					match(input,97,FOLLOW_97_in_idAll8704); if (state.failed) return;
					}
					break;
				case 22 :
					dbg.enterAlt(22);

					// GosuProg.g:557:10: 'continue'
					{
					dbg.location(557,10);
					match(input,94,FOLLOW_94_in_idAll8732); if (state.failed) return;
					}
					break;
				case 23 :
					dbg.enterAlt(23);

					// GosuProg.g:558:10: 'break'
					{
					dbg.location(558,10);
					match(input,87,FOLLOW_87_in_idAll8754); if (state.failed) return;
					}
					break;
				case 24 :
					dbg.enterAlt(24);

					// GosuProg.g:559:10: 'return'
					{
					dbg.location(559,10);
					match(input,137,FOLLOW_137_in_idAll8779); if (state.failed) return;
					}
					break;
				case 25 :
					dbg.enterAlt(25);

					// GosuProg.g:560:10: 'construct'
					{
					dbg.location(560,10);
					match(input,92,FOLLOW_92_in_idAll8803); if (state.failed) return;
					}
					break;
				case 26 :
					dbg.enterAlt(26);

					// GosuProg.g:561:10: 'function'
					{
					dbg.location(561,10);
					match(input,112,FOLLOW_112_in_idAll8824); if (state.failed) return;
					}
					break;
				case 27 :
					dbg.enterAlt(27);

					// GosuProg.g:562:10: 'property'
					{
					dbg.location(562,10);
					match(input,131,FOLLOW_131_in_idAll8846); if (state.failed) return;
					}
					break;
				case 28 :
					dbg.enterAlt(28);

					// GosuProg.g:563:10: 'try'
					{
					dbg.location(563,10);
					match(input,150,FOLLOW_150_in_idAll8868); if (state.failed) return;
					}
					break;
				case 29 :
					dbg.enterAlt(29);

					// GosuProg.g:564:10: 'catch'
					{
					dbg.location(564,10);
					match(input,89,FOLLOW_89_in_idAll8895); if (state.failed) return;
					}
					break;
				case 30 :
					dbg.enterAlt(30);

					// GosuProg.g:565:10: 'finally'
					{
					dbg.location(565,10);
					match(input,108,FOLLOW_108_in_idAll8920); if (state.failed) return;
					}
					break;
				case 31 :
					dbg.enterAlt(31);

					// GosuProg.g:566:10: 'throw'
					{
					dbg.location(566,10);
					match(input,147,FOLLOW_147_in_idAll8943); if (state.failed) return;
					}
					break;
				case 32 :
					dbg.enterAlt(32);

					// GosuProg.g:567:10: 'new'
					{
					dbg.location(567,10);
					match(input,123,FOLLOW_123_in_idAll8968); if (state.failed) return;
					}
					break;
				case 33 :
					dbg.enterAlt(33);

					// GosuProg.g:568:10: 'switch'
					{
					dbg.location(568,10);
					match(input,145,FOLLOW_145_in_idAll8995); if (state.failed) return;
					}
					break;
				case 34 :
					dbg.enterAlt(34);

					// GosuProg.g:569:10: 'case'
					{
					dbg.location(569,10);
					match(input,88,FOLLOW_88_in_idAll9019); if (state.failed) return;
					}
					break;
				case 35 :
					dbg.enterAlt(35);

					// GosuProg.g:570:10: 'default'
					{
					dbg.location(570,10);
					match(input,95,FOLLOW_95_in_idAll9045); if (state.failed) return;
					}
					break;
				case 36 :
					dbg.enterAlt(36);

					// GosuProg.g:571:10: 'eval'
					{
					dbg.location(571,10);
					match(input,101,FOLLOW_101_in_idAll9068); if (state.failed) return;
					}
					break;
				case 37 :
					dbg.enterAlt(37);

					// GosuProg.g:572:10: 'override'
					{
					dbg.location(572,10);
					match(input,128,FOLLOW_128_in_idAll9094); if (state.failed) return;
					}
					break;
				case 38 :
					dbg.enterAlt(38);

					// GosuProg.g:573:10: 'extends'
					{
					dbg.location(573,10);
					match(input,105,FOLLOW_105_in_idAll9116); if (state.failed) return;
					}
					break;
				case 39 :
					dbg.enterAlt(39);

					// GosuProg.g:574:10: 'transient'
					{
					dbg.location(574,10);
					match(input,148,FOLLOW_148_in_idAll9139); if (state.failed) return;
					}
					break;
				case 40 :
					dbg.enterAlt(40);

					// GosuProg.g:575:10: 'implements'
					{
					dbg.location(575,10);
					match(input,116,FOLLOW_116_in_idAll9160); if (state.failed) return;
					}
					break;
				case 41 :
					dbg.enterAlt(41);

					// GosuProg.g:576:10: 'class'
					{
					dbg.location(576,10);
					match(input,90,FOLLOW_90_in_idAll9180); if (state.failed) return;
					}
					break;
				case 42 :
					dbg.enterAlt(42);

					// GosuProg.g:577:10: 'interface'
					{
					dbg.location(577,10);
					match(input,119,FOLLOW_119_in_idAll9205); if (state.failed) return;
					}
					break;
				case 43 :
					dbg.enterAlt(43);

					// GosuProg.g:578:10: 'structure'
					{
					dbg.location(578,10);
					match(input,143,FOLLOW_143_in_idAll9226); if (state.failed) return;
					}
					break;
				case 44 :
					dbg.enterAlt(44);

					// GosuProg.g:579:10: 'enum'
					{
					dbg.location(579,10);
					match(input,100,FOLLOW_100_in_idAll9247); if (state.failed) return;
					}
					break;
				case 45 :
					dbg.enterAlt(45);

					// GosuProg.g:580:10: 'using'
					{
					dbg.location(580,10);
					match(input,157,FOLLOW_157_in_idAll9273); if (state.failed) return;
					}
					break;

			}
		}

		  catch (RecognitionException e) {
		    throw e;
		  }

		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 123, idAll_StartIndex); }

		}
		dbg.location(581, 7);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "idAll");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

	}
	// $ANTLR end "idAll"

	// $ANTLR start synpred4_GosuProg
	public final void synpred4_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:36:18: ( modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement ) )
		dbg.enterAlt(1);

		// GosuProg.g:36:18: modifiers ( gClass | gInterfaceOrStructure | gEnum | gEnhancement )
		{
		dbg.location(36,18);
		pushFollow(FOLLOW_modifiers_in_synpred4_GosuProg75);
		modifiers();
		state._fsp--;
		if (state.failed) return;dbg.location(36,28);
		// GosuProg.g:36:28: ( gClass | gInterfaceOrStructure | gEnum | gEnhancement )
		int alt148=4;
		try { dbg.enterSubRule(148);
		try { dbg.enterDecision(148, decisionCanBacktrack[148]);

		switch ( input.LA(1) ) {
		case 90:
			{
			alt148=1;
			}
			break;
		case 119:
		case 143:
			{
			alt148=2;
			}
			break;
		case 100:
			{
			alt148=3;
			}
			break;
		case 99:
			{
			alt148=4;
			}
			break;
		default:
			if (state.backtracking>0) {state.failed=true; return;}
			NoViableAltException nvae =
				new NoViableAltException("", 148, 0, input);
			dbg.recognitionException(nvae);
			throw nvae;
		}
		} finally {dbg.exitDecision(148);}

		switch (alt148) {
			case 1 :
				dbg.enterAlt(1);

				// GosuProg.g:36:29: gClass
				{
				dbg.location(36,29);
				pushFollow(FOLLOW_gClass_in_synpred4_GosuProg78);
				gClass();
				state._fsp--;
				if (state.failed) return;
				}
				break;
			case 2 :
				dbg.enterAlt(2);

				// GosuProg.g:36:38: gInterfaceOrStructure
				{
				dbg.location(36,38);
				pushFollow(FOLLOW_gInterfaceOrStructure_in_synpred4_GosuProg82);
				gInterfaceOrStructure();
				state._fsp--;
				if (state.failed) return;
				}
				break;
			case 3 :
				dbg.enterAlt(3);

				// GosuProg.g:36:62: gEnum
				{
				dbg.location(36,62);
				pushFollow(FOLLOW_gEnum_in_synpred4_GosuProg86);
				gEnum();
				state._fsp--;
				if (state.failed) return;
				}
				break;
			case 4 :
				dbg.enterAlt(4);

				// GosuProg.g:36:70: gEnhancement
				{
				dbg.location(36,70);
				pushFollow(FOLLOW_gEnhancement_in_synpred4_GosuProg90);
				gEnhancement();
				state._fsp--;
				if (state.failed) return;
				}
				break;

		}
		} finally {dbg.exitSubRule(148);}

		}

	}
	// $ANTLR end synpred4_GosuProg

	// $ANTLR start synpred5_GosuProg
	public final void synpred5_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:36:86: ( statementTop )
		dbg.enterAlt(1);

		// GosuProg.g:36:86: statementTop
		{
		dbg.location(36,86);
		pushFollow(FOLLOW_statementTop_in_synpred5_GosuProg95);
		statementTop();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred5_GosuProg

	// $ANTLR start synpred6_GosuProg
	public final void synpred6_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:38:16: ( statement )
		dbg.enterAlt(1);

		// GosuProg.g:38:16: statement
		{
		dbg.location(38,16);
		pushFollow(FOLLOW_statement_in_synpred6_GosuProg107);
		statement();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred6_GosuProg

	// $ANTLR start synpred7_GosuProg
	public final void synpred7_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:39:16: ( modifiers functionDefn functionBody )
		dbg.enterAlt(1);

		// GosuProg.g:39:16: modifiers functionDefn functionBody
		{
		dbg.location(39,16);
		pushFollow(FOLLOW_modifiers_in_synpred7_GosuProg161);
		modifiers();
		state._fsp--;
		if (state.failed) return;dbg.location(39,26);
		pushFollow(FOLLOW_functionDefn_in_synpred7_GosuProg163);
		functionDefn();
		state._fsp--;
		if (state.failed) return;dbg.location(39,39);
		pushFollow(FOLLOW_functionBody_in_synpred7_GosuProg165);
		functionBody();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred7_GosuProg

	// $ANTLR start synpred9_GosuProg
	public final void synpred9_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:43:20: ( classpathStatements )
		dbg.enterAlt(1);

		// GosuProg.g:43:20: classpathStatements
		{
		dbg.location(43,20);
		pushFollow(FOLLOW_classpathStatements_in_synpred9_GosuProg222);
		classpathStatements();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred9_GosuProg

	// $ANTLR start synpred10_GosuProg
	public final void synpred10_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:43:41: ( typeLoaderStatements )
		dbg.enterAlt(1);

		// GosuProg.g:43:41: typeLoaderStatements
		{
		dbg.location(43,41);
		pushFollow(FOLLOW_typeLoaderStatements_in_synpred10_GosuProg225);
		typeLoaderStatements();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred10_GosuProg

	// $ANTLR start synpred13_GosuProg
	public final void synpred13_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:45:24: ( 'classpath' StringLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:45:24: 'classpath' StringLiteral
		{
		dbg.location(45,24);
		match(input,91,FOLLOW_91_in_synpred13_GosuProg246); if (state.failed) return;dbg.location(45,36);
		match(input,StringLiteral,FOLLOW_StringLiteral_in_synpred13_GosuProg248); if (state.failed) return;
		}

	}
	// $ANTLR end synpred13_GosuProg

	// $ANTLR start synpred14_GosuProg
	public final void synpred14_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:47:25: ( 'typeloader' typeLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:47:25: 'typeloader' typeLiteral
		{
		dbg.location(47,25);
		match(input,153,FOLLOW_153_in_synpred14_GosuProg260); if (state.failed) return;dbg.location(47,38);
		pushFollow(FOLLOW_typeLiteral_in_synpred14_GosuProg262);
		typeLiteral();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred14_GosuProg

	// $ANTLR start synpred26_GosuProg
	public final void synpred26_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:65:16: ( enumConstants )
		dbg.enterAlt(1);

		// GosuProg.g:65:16: enumConstants
		{
		dbg.location(65,16);
		pushFollow(FOLLOW_enumConstants_in_synpred26_GosuProg461);
		enumConstants();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred26_GosuProg

	// $ANTLR start synpred27_GosuProg
	public final void synpred27_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:67:32: ( ',' enumConstant )
		dbg.enterAlt(1);

		// GosuProg.g:67:32: ',' enumConstant
		{
		dbg.location(67,32);
		match(input,46,FOLLOW_46_in_synpred27_GosuProg479); if (state.failed) return;dbg.location(67,36);
		pushFollow(FOLLOW_enumConstant_in_synpred27_GosuProg481);
		enumConstant();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred27_GosuProg

	// $ANTLR start synpred55_GosuProg
	public final void synpred55_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:111:35: ( blockTypeLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:111:35: blockTypeLiteral
		{
		dbg.location(111,35);
		pushFollow(FOLLOW_blockTypeLiteral_in_synpred55_GosuProg1412);
		blockTypeLiteral();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred55_GosuProg

	// $ANTLR start synpred56_GosuProg
	public final void synpred56_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:113:41: ( 'readonly' )
		dbg.enterAlt(1);

		// GosuProg.g:113:41: 'readonly'
		{
		dbg.location(113,41);
		match(input,134,FOLLOW_134_in_synpred56_GosuProg1432); if (state.failed) return;
		}

	}
	// $ANTLR end synpred56_GosuProg

	// $ANTLR start synpred62_GosuProg
	public final void synpred62_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:119:36: ( ';' )
		dbg.enterAlt(1);

		// GosuProg.g:119:36: ';'
		{
		dbg.location(119,36);
		match(input,57,FOLLOW_57_in_synpred62_GosuProg1500); if (state.failed) return;
		}

	}
	// $ANTLR end synpred62_GosuProg

	// $ANTLR start synpred65_GosuProg
	public final void synpred65_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:123:41: ( ';' )
		dbg.enterAlt(1);

		// GosuProg.g:123:41: ';'
		{
		dbg.location(123,41);
		match(input,57,FOLLOW_57_in_synpred65_GosuProg1535); if (state.failed) return;
		}

	}
	// $ANTLR end synpred65_GosuProg

	// $ANTLR start synpred93_GosuProg
	public final void synpred93_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:162:15: ( assertStatement )
		dbg.enterAlt(1);

		// GosuProg.g:162:15: assertStatement
		{
		dbg.location(162,15);
		pushFollow(FOLLOW_assertStatement_in_synpred93_GosuProg2241);
		assertStatement();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred93_GosuProg

	// $ANTLR start synpred94_GosuProg
	public final void synpred94_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:163:15: ( 'final' localVarStatement )
		dbg.enterAlt(1);

		// GosuProg.g:163:15: 'final' localVarStatement
		{
		dbg.location(163,15);
		match(input,107,FOLLOW_107_in_synpred94_GosuProg2273); if (state.failed) return;dbg.location(163,23);
		pushFollow(FOLLOW_localVarStatement_in_synpred94_GosuProg2275);
		localVarStatement();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred94_GosuProg

	// $ANTLR start synpred97_GosuProg
	public final void synpred97_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:166:15: ( assignmentOrMethodCall )
		dbg.enterAlt(1);

		// GosuProg.g:166:15: assignmentOrMethodCall
		{
		dbg.location(166,15);
		pushFollow(FOLLOW_assignmentOrMethodCall_in_synpred97_GosuProg2366);
		assignmentOrMethodCall();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred97_GosuProg

	// $ANTLR start synpred98_GosuProg
	public final void synpred98_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:167:31: ( ';' )
		dbg.enterAlt(1);

		// GosuProg.g:167:31: ';'
		{
		dbg.location(167,31);
		match(input,57,FOLLOW_57_in_synpred98_GosuProg2394); if (state.failed) return;
		}

	}
	// $ANTLR end synpred98_GosuProg

	// $ANTLR start synpred100_GosuProg
	public final void synpred100_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:171:50: ( ';' )
		dbg.enterAlt(1);

		// GosuProg.g:171:50: ';'
		{
		dbg.location(171,50);
		match(input,57,FOLLOW_57_in_synpred100_GosuProg2452); if (state.failed) return;
		}

	}
	// $ANTLR end synpred100_GosuProg

	// $ANTLR start synpred101_GosuProg
	public final void synpred101_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:171:56: ( 'else' statement )
		dbg.enterAlt(1);

		// GosuProg.g:171:56: 'else' statement
		{
		dbg.location(171,56);
		match(input,98,FOLLOW_98_in_synpred101_GosuProg2456); if (state.failed) return;dbg.location(171,63);
		pushFollow(FOLLOW_statement_in_synpred101_GosuProg2458);
		statement();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred101_GosuProg

	// $ANTLR start synpred112_GosuProg
	public final void synpred112_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:181:30: ( expression ( (~ '=' ) | EOF ) )
		dbg.enterAlt(1);

		// GosuProg.g:181:31: expression ( (~ '=' ) | EOF )
		{
		dbg.location(181,31);
		pushFollow(FOLLOW_expression_in_synpred112_GosuProg2588);
		expression();
		state._fsp--;
		if (state.failed) return;dbg.location(181,42);
		if ( input.LA(1)==EOF||(input.LA(1) >= BinLiteral && input.LA(1) <= 59)||(input.LA(1) >= 61 && input.LA(1) <= 170) ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			dbg.recognitionException(mse);
			throw mse;
		}
		}

	}
	// $ANTLR end synpred112_GosuProg

	// $ANTLR start synpred135_GosuProg
	public final void synpred135_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:222:66: ( ':' typeLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:222:66: ':' typeLiteral
		{
		dbg.location(222,66);
		match(input,56,FOLLOW_56_in_synpred135_GosuProg3083); if (state.failed) return;dbg.location(222,70);
		pushFollow(FOLLOW_typeLiteral_in_synpred135_GosuProg3085);
		typeLiteral();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred135_GosuProg

	// $ANTLR start synpred137_GosuProg
	public final void synpred137_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:225:20: ( id ( '=' expression | blockTypeLiteral ) )
		dbg.enterAlt(1);

		// GosuProg.g:225:20: id ( '=' expression | blockTypeLiteral )
		{
		dbg.location(225,20);
		pushFollow(FOLLOW_id_in_synpred137_GosuProg3100);
		id();
		state._fsp--;
		if (state.failed) return;dbg.location(225,23);
		// GosuProg.g:225:23: ( '=' expression | blockTypeLiteral )
		int alt167=2;
		try { dbg.enterSubRule(167);
		try { dbg.enterDecision(167, decisionCanBacktrack[167]);

		int LA167_0 = input.LA(1);
		if ( (LA167_0==60) ) {
			alt167=1;
		}
		else if ( (LA167_0==38) ) {
			alt167=2;
		}

		else {
			if (state.backtracking>0) {state.failed=true; return;}
			NoViableAltException nvae =
				new NoViableAltException("", 167, 0, input);
			dbg.recognitionException(nvae);
			throw nvae;
		}

		} finally {dbg.exitDecision(167);}

		switch (alt167) {
			case 1 :
				dbg.enterAlt(1);

				// GosuProg.g:225:24: '=' expression
				{
				dbg.location(225,24);
				match(input,60,FOLLOW_60_in_synpred137_GosuProg3103); if (state.failed) return;dbg.location(225,28);
				pushFollow(FOLLOW_expression_in_synpred137_GosuProg3105);
				expression();
				state._fsp--;
				if (state.failed) return;
				}
				break;
			case 2 :
				dbg.enterAlt(2);

				// GosuProg.g:225:41: blockTypeLiteral
				{
				dbg.location(225,41);
				pushFollow(FOLLOW_blockTypeLiteral_in_synpred137_GosuProg3109);
				blockTypeLiteral();
				state._fsp--;
				if (state.failed) return;
				}
				break;

		}
		} finally {dbg.exitSubRule(167);}

		}

	}
	// $ANTLR end synpred137_GosuProg

	// $ANTLR start synpred140_GosuProg
	public final void synpred140_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:228:22: ( '&' type )
		dbg.enterAlt(1);

		// GosuProg.g:228:22: '&' type
		{
		dbg.location(228,22);
		match(input,36,FOLLOW_36_in_synpred140_GosuProg3161); if (state.failed) return;dbg.location(228,26);
		pushFollow(FOLLOW_type_in_synpred140_GosuProg3163);
		type();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred140_GosuProg

	// $ANTLR start synpred143_GosuProg
	public final void synpred143_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:243:47: ( '.' id typeArguments )
		dbg.enterAlt(1);

		// GosuProg.g:243:47: '.' id typeArguments
		{
		dbg.location(243,47);
		match(input,51,FOLLOW_51_in_synpred143_GosuProg3257); if (state.failed) return;dbg.location(243,51);
		pushFollow(FOLLOW_id_in_synpred143_GosuProg3259);
		id();
		state._fsp--;
		if (state.failed) return;dbg.location(243,54);
		pushFollow(FOLLOW_typeArguments_in_synpred143_GosuProg3261);
		typeArguments();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred143_GosuProg

	// $ANTLR start synpred145_GosuProg
	public final void synpred145_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:247:10: ( '<' typeArgument ( ',' typeArgument )* '>' )
		dbg.enterAlt(1);

		// GosuProg.g:247:10: '<' typeArgument ( ',' typeArgument )* '>'
		{
		dbg.location(247,10);
		match(input,58,FOLLOW_58_in_synpred145_GosuProg3284); if (state.failed) return;dbg.location(247,14);
		pushFollow(FOLLOW_typeArgument_in_synpred145_GosuProg3286);
		typeArgument();
		state._fsp--;
		if (state.failed) return;dbg.location(247,27);
		// GosuProg.g:247:27: ( ',' typeArgument )*
		try { dbg.enterSubRule(169);

		loop169:
		while (true) {
			int alt169=2;
			try { dbg.enterDecision(169, decisionCanBacktrack[169]);

			int LA169_0 = input.LA(1);
			if ( (LA169_0==46) ) {
				alt169=1;
			}

			} finally {dbg.exitDecision(169);}

			switch (alt169) {
			case 1 :
				dbg.enterAlt(1);

				// GosuProg.g:247:28: ',' typeArgument
				{
				dbg.location(247,28);
				match(input,46,FOLLOW_46_in_synpred145_GosuProg3289); if (state.failed) return;dbg.location(247,32);
				pushFollow(FOLLOW_typeArgument_in_synpred145_GosuProg3291);
				typeArgument();
				state._fsp--;
				if (state.failed) return;
				}
				break;

			default :
				break loop169;
			}
		}
		} finally {dbg.exitSubRule(169);}
		dbg.location(247,47);
		match(input,63,FOLLOW_63_in_synpred145_GosuProg3295); if (state.failed) return;
		}

	}
	// $ANTLR end synpred145_GosuProg

	// $ANTLR start synpred149_GosuProg
	public final void synpred149_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:256:38: ( '?' conditionalExpr ':' conditionalExpr )
		dbg.enterAlt(1);

		// GosuProg.g:256:38: '?' conditionalExpr ':' conditionalExpr
		{
		dbg.location(256,38);
		match(input,65,FOLLOW_65_in_synpred149_GosuProg3359); if (state.failed) return;dbg.location(256,42);
		pushFollow(FOLLOW_conditionalExpr_in_synpred149_GosuProg3361);
		conditionalExpr();
		state._fsp--;
		if (state.failed) return;dbg.location(256,58);
		match(input,56,FOLLOW_56_in_synpred149_GosuProg3363); if (state.failed) return;dbg.location(256,62);
		pushFollow(FOLLOW_conditionalExpr_in_synpred149_GosuProg3365);
		conditionalExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred149_GosuProg

	// $ANTLR start synpred150_GosuProg
	public final void synpred150_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:256:80: ( '?:' conditionalExpr )
		dbg.enterAlt(1);

		// GosuProg.g:256:80: '?:' conditionalExpr
		{
		dbg.location(256,80);
		match(input,71,FOLLOW_71_in_synpred150_GosuProg3369); if (state.failed) return;dbg.location(256,85);
		pushFollow(FOLLOW_conditionalExpr_in_synpred150_GosuProg3371);
		conditionalExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred150_GosuProg

	// $ANTLR start synpred151_GosuProg
	public final void synpred151_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:258:41: ( orOp conditionalAndExpr )
		dbg.enterAlt(1);

		// GosuProg.g:258:41: orOp conditionalAndExpr
		{
		dbg.location(258,41);
		pushFollow(FOLLOW_orOp_in_synpred151_GosuProg3384);
		orOp();
		state._fsp--;
		if (state.failed) return;dbg.location(258,46);
		pushFollow(FOLLOW_conditionalAndExpr_in_synpred151_GosuProg3386);
		conditionalAndExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred151_GosuProg

	// $ANTLR start synpred152_GosuProg
	public final void synpred152_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:260:37: ( andOp bitwiseOrExpr )
		dbg.enterAlt(1);

		// GosuProg.g:260:37: andOp bitwiseOrExpr
		{
		dbg.location(260,37);
		pushFollow(FOLLOW_andOp_in_synpred152_GosuProg3400);
		andOp();
		state._fsp--;
		if (state.failed) return;dbg.location(260,43);
		pushFollow(FOLLOW_bitwiseOrExpr_in_synpred152_GosuProg3402);
		bitwiseOrExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred152_GosuProg

	// $ANTLR start synpred153_GosuProg
	public final void synpred153_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:262:33: ( '|' bitwiseXorExpr )
		dbg.enterAlt(1);

		// GosuProg.g:262:33: '|' bitwiseXorExpr
		{
		dbg.location(262,33);
		match(input,163,FOLLOW_163_in_synpred153_GosuProg3416); if (state.failed) return;dbg.location(262,37);
		pushFollow(FOLLOW_bitwiseXorExpr_in_synpred153_GosuProg3418);
		bitwiseXorExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred153_GosuProg

	// $ANTLR start synpred154_GosuProg
	public final void synpred154_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:264:34: ( '^' bitwiseAndExpr )
		dbg.enterAlt(1);

		// GosuProg.g:264:34: '^' bitwiseAndExpr
		{
		dbg.location(264,34);
		match(input,79,FOLLOW_79_in_synpred154_GosuProg3432); if (state.failed) return;dbg.location(264,38);
		pushFollow(FOLLOW_bitwiseAndExpr_in_synpred154_GosuProg3434);
		bitwiseAndExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred154_GosuProg

	// $ANTLR start synpred155_GosuProg
	public final void synpred155_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:266:32: ( '&' equalityExpr )
		dbg.enterAlt(1);

		// GosuProg.g:266:32: '&' equalityExpr
		{
		dbg.location(266,32);
		match(input,36,FOLLOW_36_in_synpred155_GosuProg3448); if (state.failed) return;dbg.location(266,36);
		pushFollow(FOLLOW_equalityExpr_in_synpred155_GosuProg3450);
		equalityExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred155_GosuProg

	// $ANTLR start synpred156_GosuProg
	public final void synpred156_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:268:32: ( equalityOp relationalExpr )
		dbg.enterAlt(1);

		// GosuProg.g:268:32: equalityOp relationalExpr
		{
		dbg.location(268,32);
		pushFollow(FOLLOW_equalityOp_in_synpred156_GosuProg3464);
		equalityOp();
		state._fsp--;
		if (state.failed) return;dbg.location(268,43);
		pushFollow(FOLLOW_relationalExpr_in_synpred156_GosuProg3466);
		relationalExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred156_GosuProg

	// $ANTLR start synpred157_GosuProg
	public final void synpred157_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:270:32: ( relOp intervalExpr )
		dbg.enterAlt(1);

		// GosuProg.g:270:32: relOp intervalExpr
		{
		dbg.location(270,32);
		pushFollow(FOLLOW_relOp_in_synpred157_GosuProg3480);
		relOp();
		state._fsp--;
		if (state.failed) return;dbg.location(270,38);
		pushFollow(FOLLOW_intervalExpr_in_synpred157_GosuProg3482);
		intervalExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred157_GosuProg

	// $ANTLR start synpred158_GosuProg
	public final void synpred158_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:270:53: ( 'typeis' typeLiteralType )
		dbg.enterAlt(1);

		// GosuProg.g:270:53: 'typeis' typeLiteralType
		{
		dbg.location(270,53);
		match(input,152,FOLLOW_152_in_synpred158_GosuProg3486); if (state.failed) return;dbg.location(270,62);
		pushFollow(FOLLOW_typeLiteralType_in_synpred158_GosuProg3488);
		typeLiteralType();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred158_GosuProg

	// $ANTLR start synpred159_GosuProg
	public final void synpred159_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:272:30: ( intervalOp bitshiftExpr )
		dbg.enterAlt(1);

		// GosuProg.g:272:30: intervalOp bitshiftExpr
		{
		dbg.location(272,30);
		pushFollow(FOLLOW_intervalOp_in_synpred159_GosuProg3503);
		intervalOp();
		state._fsp--;
		if (state.failed) return;dbg.location(272,41);
		pushFollow(FOLLOW_bitshiftExpr_in_synpred159_GosuProg3505);
		bitshiftExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred159_GosuProg

	// $ANTLR start synpred160_GosuProg
	public final void synpred160_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:274:30: ( bitshiftOp additiveExpr )
		dbg.enterAlt(1);

		// GosuProg.g:274:30: bitshiftOp additiveExpr
		{
		dbg.location(274,30);
		pushFollow(FOLLOW_bitshiftOp_in_synpred160_GosuProg3519);
		bitshiftOp();
		state._fsp--;
		if (state.failed) return;dbg.location(274,41);
		pushFollow(FOLLOW_additiveExpr_in_synpred160_GosuProg3521);
		additiveExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred160_GosuProg

	// $ANTLR start synpred161_GosuProg
	public final void synpred161_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:276:36: ( additiveOp multiplicativeExpr )
		dbg.enterAlt(1);

		// GosuProg.g:276:36: additiveOp multiplicativeExpr
		{
		dbg.location(276,36);
		pushFollow(FOLLOW_additiveOp_in_synpred161_GosuProg3536);
		additiveOp();
		state._fsp--;
		if (state.failed) return;dbg.location(276,47);
		pushFollow(FOLLOW_multiplicativeExpr_in_synpred161_GosuProg3538);
		multiplicativeExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred161_GosuProg

	// $ANTLR start synpred162_GosuProg
	public final void synpred162_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:278:34: ( multiplicativeOp typeAsExpr )
		dbg.enterAlt(1);

		// GosuProg.g:278:34: multiplicativeOp typeAsExpr
		{
		dbg.location(278,34);
		pushFollow(FOLLOW_multiplicativeOp_in_synpred162_GosuProg3552);
		multiplicativeOp();
		state._fsp--;
		if (state.failed) return;dbg.location(278,51);
		pushFollow(FOLLOW_typeAsExpr_in_synpred162_GosuProg3554);
		typeAsExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred162_GosuProg

	// $ANTLR start synpred163_GosuProg
	public final void synpred163_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:280:25: ( typeAsOp typeLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:280:25: typeAsOp typeLiteral
		{
		dbg.location(280,25);
		pushFollow(FOLLOW_typeAsOp_in_synpred163_GosuProg3568);
		typeAsOp();
		state._fsp--;
		if (state.failed) return;dbg.location(280,34);
		pushFollow(FOLLOW_typeLiteral_in_synpred163_GosuProg3570);
		typeLiteral();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred163_GosuProg

	// $ANTLR start synpred171_GosuProg
	public final void synpred171_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:290:45: ( expression )
		dbg.enterAlt(1);

		// GosuProg.g:290:45: expression
		{
		dbg.location(290,45);
		pushFollow(FOLLOW_expression_in_synpred171_GosuProg3748);
		expression();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred171_GosuProg

	// $ANTLR start synpred181_GosuProg
	public final void synpred181_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:300:21: ( arguments )
		dbg.enterAlt(1);

		// GosuProg.g:300:21: arguments
		{
		dbg.location(300,21);
		pushFollow(FOLLOW_arguments_in_synpred181_GosuProg3853);
		arguments();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred181_GosuProg

	// $ANTLR start synpred187_GosuProg
	public final void synpred187_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:317:18: ( literal )
		dbg.enterAlt(1);

		// GosuProg.g:317:18: literal
		{
		dbg.location(317,18);
		pushFollow(FOLLOW_literal_in_synpred187_GosuProg4103);
		literal();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred187_GosuProg

	// $ANTLR start synpred188_GosuProg
	public final void synpred188_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:318:18: ( typeLiteralExpr )
		dbg.enterAlt(1);

		// GosuProg.g:318:18: typeLiteralExpr
		{
		dbg.location(318,18);
		pushFollow(FOLLOW_typeLiteralExpr_in_synpred188_GosuProg4159);
		typeLiteralExpr();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred188_GosuProg

	// $ANTLR start synpred191_GosuProg
	public final void synpred191_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:334:63: ( initializer )
		dbg.enterAlt(1);

		// GosuProg.g:334:63: initializer
		{
		dbg.location(334,63);
		pushFollow(FOLLOW_initializer_in_synpred191_GosuProg4383);
		initializer();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred191_GosuProg

	// $ANTLR start synpred192_GosuProg
	public final void synpred192_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:334:58: ( '{' ( initializer | anonymousInnerClass ) '}' )
		dbg.enterAlt(1);

		// GosuProg.g:334:58: '{' ( initializer | anonymousInnerClass ) '}'
		{
		dbg.location(334,58);
		match(input,162,FOLLOW_162_in_synpred192_GosuProg4380); if (state.failed) return;dbg.location(334,62);
		// GosuProg.g:334:62: ( initializer | anonymousInnerClass )
		int alt172=2;
		try { dbg.enterSubRule(172);
		try { dbg.enterDecision(172, decisionCanBacktrack[172]);

		switch ( input.LA(1) ) {
		case CharLiteral:
		case Ident:
		case NumberLiteral:
		case StringLiteral:
		case 25:
		case 28:
		case 31:
		case 38:
		case 43:
		case 47:
		case 56:
		case 74:
		case 75:
		case 77:
		case 83:
		case 84:
		case 85:
		case 86:
		case 91:
		case 93:
		case 99:
		case 101:
		case 102:
		case 103:
		case 104:
		case 106:
		case 109:
		case 113:
		case 114:
		case 118:
		case 121:
		case 122:
		case 123:
		case 124:
		case 125:
		case 127:
		case 134:
		case 136:
		case 138:
		case 139:
		case 140:
		case 142:
		case 144:
		case 146:
		case 149:
		case 153:
		case 154:
		case 159:
		case 160:
		case 162:
		case 170:
			{
			alt172=1;
			}
			break;
		case 130:
			{
			int LA172_2 = input.LA(2);
			if ( ((LA172_2 >= 26 && LA172_2 <= 32)||LA172_2==34||LA172_2==36||LA172_2==38||(LA172_2 >= 40 && LA172_2 <= 41)||LA172_2==43||(LA172_2 >= 46 && LA172_2 <= 47)||(LA172_2 >= 50 && LA172_2 <= 54)||(LA172_2 >= 58 && LA172_2 <= 59)||(LA172_2 >= 61 && LA172_2 <= 72)||LA172_2==76||LA172_2==79||LA172_2==82||LA172_2==84||LA172_2==126||(LA172_2 >= 151 && LA172_2 <= 152)||(LA172_2 >= 163 && LA172_2 <= 165)||LA172_2==167||LA172_2==169) ) {
				alt172=1;
			}
			else if ( (LA172_2==73||LA172_2==81||LA172_2==90||LA172_2==92||LA172_2==96||LA172_2==100||LA172_2==107||LA172_2==112||(LA172_2 >= 119 && LA172_2 <= 120)||LA172_2==128||(LA172_2 >= 130 && LA172_2 <= 133)||LA172_2==141||LA172_2==143||LA172_2==148||LA172_2==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 2, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 169:
			{
			int LA172_3 = input.LA(2);
			if ( (synpred191_GosuProg()) ) {
				alt172=1;
			}
			else if ( (true) ) {
				alt172=2;
			}

			}
			break;
		case 73:
		case 90:
		case 92:
		case 96:
		case 100:
		case 112:
		case 119:
		case 128:
		case 131:
		case 143:
		case 148:
		case 158:
			{
			alt172=2;
			}
			break;
		case 120:
			{
			int LA172_5 = input.LA(2);
			if ( ((LA172_5 >= 26 && LA172_5 <= 32)||LA172_5==34||LA172_5==36||LA172_5==38||(LA172_5 >= 40 && LA172_5 <= 41)||LA172_5==43||(LA172_5 >= 46 && LA172_5 <= 47)||(LA172_5 >= 50 && LA172_5 <= 54)||(LA172_5 >= 58 && LA172_5 <= 59)||(LA172_5 >= 61 && LA172_5 <= 72)||LA172_5==76||LA172_5==79||LA172_5==82||LA172_5==84||LA172_5==126||(LA172_5 >= 151 && LA172_5 <= 152)||(LA172_5 >= 163 && LA172_5 <= 165)||LA172_5==167||LA172_5==169) ) {
				alt172=1;
			}
			else if ( (LA172_5==73||LA172_5==81||LA172_5==90||LA172_5==92||LA172_5==96||LA172_5==100||LA172_5==107||LA172_5==112||(LA172_5 >= 119 && LA172_5 <= 120)||LA172_5==128||(LA172_5 >= 130 && LA172_5 <= 133)||LA172_5==141||LA172_5==143||LA172_5==148||LA172_5==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 5, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 132:
			{
			int LA172_6 = input.LA(2);
			if ( ((LA172_6 >= 26 && LA172_6 <= 32)||LA172_6==34||LA172_6==36||LA172_6==38||(LA172_6 >= 40 && LA172_6 <= 41)||LA172_6==43||(LA172_6 >= 46 && LA172_6 <= 47)||(LA172_6 >= 50 && LA172_6 <= 54)||(LA172_6 >= 58 && LA172_6 <= 59)||(LA172_6 >= 61 && LA172_6 <= 72)||LA172_6==76||LA172_6==79||LA172_6==82||LA172_6==84||LA172_6==126||(LA172_6 >= 151 && LA172_6 <= 152)||(LA172_6 >= 163 && LA172_6 <= 165)||LA172_6==167||LA172_6==169) ) {
				alt172=1;
			}
			else if ( (LA172_6==73||LA172_6==81||LA172_6==90||LA172_6==92||LA172_6==96||LA172_6==100||LA172_6==107||LA172_6==112||(LA172_6 >= 119 && LA172_6 <= 120)||LA172_6==128||(LA172_6 >= 130 && LA172_6 <= 133)||LA172_6==141||LA172_6==143||LA172_6==148||LA172_6==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 6, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 133:
			{
			int LA172_7 = input.LA(2);
			if ( ((LA172_7 >= 26 && LA172_7 <= 32)||LA172_7==34||LA172_7==36||LA172_7==38||(LA172_7 >= 40 && LA172_7 <= 41)||LA172_7==43||(LA172_7 >= 46 && LA172_7 <= 47)||(LA172_7 >= 50 && LA172_7 <= 54)||(LA172_7 >= 58 && LA172_7 <= 59)||(LA172_7 >= 61 && LA172_7 <= 72)||LA172_7==76||LA172_7==79||LA172_7==82||LA172_7==84||LA172_7==126||(LA172_7 >= 151 && LA172_7 <= 152)||(LA172_7 >= 163 && LA172_7 <= 165)||LA172_7==167||LA172_7==169) ) {
				alt172=1;
			}
			else if ( (LA172_7==73||LA172_7==81||LA172_7==90||LA172_7==92||LA172_7==96||LA172_7==100||LA172_7==107||LA172_7==112||(LA172_7 >= 119 && LA172_7 <= 120)||LA172_7==128||(LA172_7 >= 130 && LA172_7 <= 133)||LA172_7==141||LA172_7==143||LA172_7==148||LA172_7==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 7, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 141:
			{
			int LA172_8 = input.LA(2);
			if ( ((LA172_8 >= 26 && LA172_8 <= 32)||LA172_8==34||LA172_8==36||LA172_8==38||(LA172_8 >= 40 && LA172_8 <= 41)||LA172_8==43||(LA172_8 >= 46 && LA172_8 <= 47)||(LA172_8 >= 50 && LA172_8 <= 54)||(LA172_8 >= 58 && LA172_8 <= 59)||(LA172_8 >= 61 && LA172_8 <= 72)||LA172_8==76||LA172_8==79||LA172_8==82||LA172_8==84||LA172_8==126||(LA172_8 >= 151 && LA172_8 <= 152)||(LA172_8 >= 163 && LA172_8 <= 165)||LA172_8==167||LA172_8==169) ) {
				alt172=1;
			}
			else if ( (LA172_8==73||LA172_8==81||LA172_8==90||LA172_8==92||LA172_8==96||LA172_8==100||LA172_8==107||LA172_8==112||(LA172_8 >= 119 && LA172_8 <= 120)||LA172_8==128||(LA172_8 >= 130 && LA172_8 <= 133)||LA172_8==141||LA172_8==143||LA172_8==148||LA172_8==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 8, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 81:
			{
			int LA172_9 = input.LA(2);
			if ( ((LA172_9 >= 26 && LA172_9 <= 32)||LA172_9==34||LA172_9==36||LA172_9==38||(LA172_9 >= 40 && LA172_9 <= 41)||LA172_9==43||(LA172_9 >= 46 && LA172_9 <= 47)||(LA172_9 >= 50 && LA172_9 <= 54)||(LA172_9 >= 58 && LA172_9 <= 59)||(LA172_9 >= 61 && LA172_9 <= 72)||LA172_9==76||LA172_9==79||LA172_9==82||LA172_9==84||LA172_9==126||(LA172_9 >= 151 && LA172_9 <= 152)||(LA172_9 >= 163 && LA172_9 <= 165)||LA172_9==167||LA172_9==169) ) {
				alt172=1;
			}
			else if ( (LA172_9==73||LA172_9==81||LA172_9==90||LA172_9==92||LA172_9==96||LA172_9==100||LA172_9==107||LA172_9==112||(LA172_9 >= 119 && LA172_9 <= 120)||LA172_9==128||(LA172_9 >= 130 && LA172_9 <= 133)||LA172_9==141||LA172_9==143||LA172_9==148||LA172_9==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 9, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		case 107:
			{
			int LA172_10 = input.LA(2);
			if ( ((LA172_10 >= 26 && LA172_10 <= 32)||LA172_10==34||LA172_10==36||LA172_10==38||(LA172_10 >= 40 && LA172_10 <= 41)||LA172_10==43||(LA172_10 >= 46 && LA172_10 <= 47)||(LA172_10 >= 50 && LA172_10 <= 54)||(LA172_10 >= 58 && LA172_10 <= 59)||(LA172_10 >= 61 && LA172_10 <= 72)||LA172_10==76||LA172_10==79||LA172_10==82||LA172_10==84||LA172_10==126||(LA172_10 >= 151 && LA172_10 <= 152)||(LA172_10 >= 163 && LA172_10 <= 165)||LA172_10==167||LA172_10==169) ) {
				alt172=1;
			}
			else if ( (LA172_10==73||LA172_10==81||LA172_10==90||LA172_10==92||LA172_10==96||LA172_10==100||LA172_10==107||LA172_10==112||(LA172_10 >= 119 && LA172_10 <= 120)||LA172_10==128||(LA172_10 >= 130 && LA172_10 <= 133)||LA172_10==141||LA172_10==143||LA172_10==148||LA172_10==158) ) {
				alt172=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return;}
				int nvaeMark = input.mark();
				try {
					input.consume();
					NoViableAltException nvae =
						new NoViableAltException("", 172, 10, input);
					dbg.recognitionException(nvae);
					throw nvae;
				} finally {
					input.rewind(nvaeMark);
				}
			}

			}
			break;
		default:
			if (state.backtracking>0) {state.failed=true; return;}
			NoViableAltException nvae =
				new NoViableAltException("", 172, 0, input);
			dbg.recognitionException(nvae);
			throw nvae;
		}
		} finally {dbg.exitDecision(172);}

		switch (alt172) {
			case 1 :
				dbg.enterAlt(1);

				// GosuProg.g:334:63: initializer
				{
				dbg.location(334,63);
				pushFollow(FOLLOW_initializer_in_synpred192_GosuProg4383);
				initializer();
				state._fsp--;
				if (state.failed) return;
				}
				break;
			case 2 :
				dbg.enterAlt(2);

				// GosuProg.g:334:77: anonymousInnerClass
				{
				dbg.location(334,77);
				pushFollow(FOLLOW_anonymousInnerClass_in_synpred192_GosuProg4387);
				anonymousInnerClass();
				state._fsp--;
				if (state.failed) return;
				}
				break;

		}
		} finally {dbg.exitSubRule(172);}
		dbg.location(334,98);
		match(input,169,FOLLOW_169_in_synpred192_GosuProg4390); if (state.failed) return;
		}

	}
	// $ANTLR end synpred192_GosuProg

	// $ANTLR start synpred196_GosuProg
	public final void synpred196_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:339:62: ( '[' expression ']' )
		dbg.enterAlt(1);

		// GosuProg.g:339:62: '[' expression ']'
		{
		dbg.location(339,62);
		match(input,76,FOLLOW_76_in_synpred196_GosuProg4649); if (state.failed) return;dbg.location(339,66);
		pushFollow(FOLLOW_expression_in_synpred196_GosuProg4651);
		expression();
		state._fsp--;
		if (state.failed) return;dbg.location(339,77);
		match(input,78,FOLLOW_78_in_synpred196_GosuProg4653); if (state.failed) return;
		}

	}
	// $ANTLR end synpred196_GosuProg

	// $ANTLR start synpred202_GosuProg
	public final void synpred202_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:351:25: ( mapInitializerList )
		dbg.enterAlt(1);

		// GosuProg.g:351:25: mapInitializerList
		{
		dbg.location(351,25);
		pushFollow(FOLLOW_mapInitializerList_in_synpred202_GosuProg4846);
		mapInitializerList();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred202_GosuProg

	// $ANTLR start synpred208_GosuProg
	public final void synpred208_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:363:27: ( ( '.' | '?.' | '*.' ) idAll typeArguments )
		dbg.enterAlt(1);

		// GosuProg.g:363:27: ( '.' | '?.' | '*.' ) idAll typeArguments
		{
		dbg.location(363,27);
		if ( input.LA(1)==41||input.LA(1)==51||input.LA(1)==69 ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			dbg.recognitionException(mse);
			throw mse;
		}dbg.location(363,47);
		pushFollow(FOLLOW_idAll_in_synpred208_GosuProg4997);
		idAll();
		state._fsp--;
		if (state.failed) return;dbg.location(363,53);
		pushFollow(FOLLOW_typeArguments_in_synpred208_GosuProg4999);
		typeArguments();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred208_GosuProg

	// $ANTLR start synpred209_GosuProg
	public final void synpred209_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:364:27: ( featureLiteral )
		dbg.enterAlt(1);

		// GosuProg.g:364:27: featureLiteral
		{
		dbg.location(364,27);
		pushFollow(FOLLOW_featureLiteral_in_synpred209_GosuProg5030);
		featureLiteral();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred209_GosuProg

	// $ANTLR start synpred211_GosuProg
	public final void synpred211_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:365:27: ( ( '[' | '?[' ) expression ']' )
		dbg.enterAlt(1);

		// GosuProg.g:365:27: ( '[' | '?[' ) expression ']'
		{
		dbg.location(365,27);
		if ( input.LA(1)==72||input.LA(1)==76 ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			dbg.recognitionException(mse);
			throw mse;
		}dbg.location(365,38);
		pushFollow(FOLLOW_expression_in_synpred211_GosuProg5092);
		expression();
		state._fsp--;
		if (state.failed) return;dbg.location(365,50);
		match(input,78,FOLLOW_78_in_synpred211_GosuProg5095); if (state.failed) return;
		}

	}
	// $ANTLR end synpred211_GosuProg

	// $ANTLR start synpred212_GosuProg
	public final void synpred212_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:366:27: ({...}? arguments )
		dbg.enterAlt(1);

		// GosuProg.g:366:27: {...}? arguments
		{
		dbg.location(366,27);
		if ( !(evalPredicate(input.LT(-1).getLine() == input.LT(1).getLine()  ,"input.LT(-1).getLine() == input.LT(1).getLine()  ")) ) {
			if (state.backtracking>0) {state.failed=true; return;}
			throw new FailedPredicateException(input, "synpred212_GosuProg", "input.LT(-1).getLine() == input.LT(1).getLine()  ");
		}dbg.location(366,80);
		pushFollow(FOLLOW_arguments_in_synpred212_GosuProg5141);
		arguments();
		state._fsp--;
		if (state.failed) return;
		}

	}
	// $ANTLR end synpred212_GosuProg

	// $ANTLR start synpred232_GosuProg
	public final void synpred232_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:397:9: ( '<' '<' '=' )
		dbg.enterAlt(1);

		// GosuProg.g:397:10: '<' '<' '='
		{
		dbg.location(397,10);
		match(input,58,FOLLOW_58_in_synpred232_GosuProg5514); if (state.failed) return;dbg.location(397,14);
		match(input,58,FOLLOW_58_in_synpred232_GosuProg5516); if (state.failed) return;dbg.location(397,18);
		match(input,60,FOLLOW_60_in_synpred232_GosuProg5518); if (state.failed) return;
		}

	}
	// $ANTLR end synpred232_GosuProg

	// $ANTLR start synpred233_GosuProg
	public final void synpred233_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:402:9: ( '>' '>' '>' '=' )
		dbg.enterAlt(1);

		// GosuProg.g:402:10: '>' '>' '>' '='
		{
		dbg.location(402,10);
		match(input,63,FOLLOW_63_in_synpred233_GosuProg5553); if (state.failed) return;dbg.location(402,14);
		match(input,63,FOLLOW_63_in_synpred233_GosuProg5555); if (state.failed) return;dbg.location(402,18);
		match(input,63,FOLLOW_63_in_synpred233_GosuProg5557); if (state.failed) return;dbg.location(402,22);
		match(input,60,FOLLOW_60_in_synpred233_GosuProg5559); if (state.failed) return;
		}

	}
	// $ANTLR end synpred233_GosuProg

	// $ANTLR start synpred234_GosuProg
	public final void synpred234_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:409:9: ( '>' '>' '=' )
		dbg.enterAlt(1);

		// GosuProg.g:409:10: '>' '>' '='
		{
		dbg.location(409,10);
		match(input,63,FOLLOW_63_in_synpred234_GosuProg5598); if (state.failed) return;dbg.location(409,14);
		match(input,63,FOLLOW_63_in_synpred234_GosuProg5600); if (state.failed) return;dbg.location(409,18);
		match(input,60,FOLLOW_60_in_synpred234_GosuProg5602); if (state.failed) return;
		}

	}
	// $ANTLR end synpred234_GosuProg

	// $ANTLR start synpred243_GosuProg
	public final void synpred243_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:424:9: ( '<' '=' )
		dbg.enterAlt(1);

		// GosuProg.g:424:10: '<' '='
		{
		dbg.location(424,10);
		match(input,58,FOLLOW_58_in_synpred243_GosuProg5708); if (state.failed) return;dbg.location(424,14);
		match(input,60,FOLLOW_60_in_synpred243_GosuProg5710); if (state.failed) return;
		}

	}
	// $ANTLR end synpred243_GosuProg

	// $ANTLR start synpred244_GosuProg
	public final void synpred244_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:427:9: ( '>' '=' )
		dbg.enterAlt(1);

		// GosuProg.g:427:10: '>' '='
		{
		dbg.location(427,10);
		match(input,63,FOLLOW_63_in_synpred244_GosuProg5741); if (state.failed) return;dbg.location(427,14);
		match(input,60,FOLLOW_60_in_synpred244_GosuProg5743); if (state.failed) return;
		}

	}
	// $ANTLR end synpred244_GosuProg

	// $ANTLR start synpred246_GosuProg
	public final void synpred246_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:435:9: ( '<' '<' )
		dbg.enterAlt(1);

		// GosuProg.g:435:10: '<' '<'
		{
		dbg.location(435,10);
		match(input,58,FOLLOW_58_in_synpred246_GosuProg5805); if (state.failed) return;dbg.location(435,14);
		match(input,58,FOLLOW_58_in_synpred246_GosuProg5807); if (state.failed) return;
		}

	}
	// $ANTLR end synpred246_GosuProg

	// $ANTLR start synpred247_GosuProg
	public final void synpred247_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:438:9: ( '>' '>' '>' )
		dbg.enterAlt(1);

		// GosuProg.g:438:10: '>' '>' '>'
		{
		dbg.location(438,10);
		match(input,63,FOLLOW_63_in_synpred247_GosuProg5838); if (state.failed) return;dbg.location(438,14);
		match(input,63,FOLLOW_63_in_synpred247_GosuProg5840); if (state.failed) return;dbg.location(438,18);
		match(input,63,FOLLOW_63_in_synpred247_GosuProg5842); if (state.failed) return;
		}

	}
	// $ANTLR end synpred247_GosuProg

	// $ANTLR start synpred248_GosuProg
	public final void synpred248_GosuProg_fragment() throws RecognitionException {
		// GosuProg.g:443:9: ( '>' '>' )
		dbg.enterAlt(1);

		// GosuProg.g:443:10: '>' '>'
		{
		dbg.location(443,10);
		match(input,63,FOLLOW_63_in_synpred248_GosuProg5877); if (state.failed) return;dbg.location(443,14);
		match(input,63,FOLLOW_63_in_synpred248_GosuProg5879); if (state.failed) return;
		}

	}
	// $ANTLR end synpred248_GosuProg

	// Delegated rules

	public final boolean synpred100_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred100_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred158_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred158_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred155_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred155_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred93_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred93_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred7_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred7_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred247_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred247_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred112_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred112_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred62_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred62_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred65_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred65_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred188_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred188_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred56_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred56_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred192_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred192_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred13_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred13_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred10_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred10_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred145_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred145_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred211_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred211_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred171_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred171_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred6_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred6_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred151_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred151_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred234_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred234_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred157_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred157_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred246_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred246_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred98_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred98_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred163_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred163_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred140_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred140_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred212_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred212_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred152_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred152_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred187_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred187_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred181_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred181_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred196_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred196_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred135_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred135_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred209_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred209_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred233_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred233_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred94_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred94_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred153_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred153_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred156_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred156_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred159_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred159_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred160_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred160_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred97_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred97_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred162_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred162_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred101_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred101_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred5_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred5_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred248_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred248_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred27_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred27_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred243_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred243_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred9_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred9_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred202_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred202_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred208_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred208_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred154_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred154_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred150_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred150_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred191_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred191_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred232_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred232_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred149_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred149_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred26_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred26_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred4_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred4_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred244_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred244_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred143_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred143_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred55_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred55_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred14_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred14_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred161_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred161_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred137_GosuProg() {
		state.backtracking++;
		dbg.beginBacktrack(state.backtracking);
		int start = input.mark();
		try {
			synpred137_GosuProg_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		dbg.endBacktrack(state.backtracking, success);
		state.backtracking--;
		state.failed=false;
		return success;
	}


	protected DFA85 dfa85 = new DFA85(this);
	protected DFA95 dfa95 = new DFA95(this);
	protected DFA122 dfa122 = new DFA122(this);
	protected DFA129 dfa129 = new DFA129(this);
	protected DFA131 dfa131 = new DFA131(this);
	protected DFA142 dfa142 = new DFA142(this);
	static final String DFA85_eotS =
		"\133\uffff";
	static final String DFA85_eofS =
		"\1\2\132\uffff";
	static final String DFA85_minS =
		"\1\4\1\0\131\uffff";
	static final String DFA85_maxS =
		"\1\u00aa\1\0\131\uffff";
	static final String DFA85_acceptS =
		"\2\uffff\1\2\127\uffff\1\1";
	static final String DFA85_specialS =
		"\1\uffff\1\0\131\uffff}>";
	static final String[] DFA85_transitionS = {
			"\64\2\1\1\162\2",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA85_eot = DFA.unpackEncodedString(DFA85_eotS);
	static final short[] DFA85_eof = DFA.unpackEncodedString(DFA85_eofS);
	static final char[] DFA85_min = DFA.unpackEncodedStringToUnsignedChars(DFA85_minS);
	static final char[] DFA85_max = DFA.unpackEncodedStringToUnsignedChars(DFA85_maxS);
	static final short[] DFA85_accept = DFA.unpackEncodedString(DFA85_acceptS);
	static final short[] DFA85_special = DFA.unpackEncodedString(DFA85_specialS);
	static final short[][] DFA85_transition;

	static {
		int numStates = DFA85_transitionS.length;
		DFA85_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA85_transition[i] = DFA.unpackEncodedString(DFA85_transitionS[i]);
		}
	}

	protected class DFA85 extends DFA {

		public DFA85(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 85;
			this.eot = DFA85_eot;
			this.eof = DFA85_eof;
			this.min = DFA85_min;
			this.max = DFA85_max;
			this.accept = DFA85_accept;
			this.special = DFA85_special;
			this.transition = DFA85_transition;
		}
		@Override
		public String getDescription() {
			return "222:65: ( ':' typeLiteral )?";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA85_1 = input.LA(1);
						 
						int index85_1 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred135_GosuProg()) ) {s = 90;}
						else if ( (true) ) {s = 2;}
						 
						input.seek(index85_1);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 85, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	static final String DFA95_eotS =
		"\136\uffff";
	static final String DFA95_eofS =
		"\1\2\135\uffff";
	static final String DFA95_minS =
		"\1\4\1\0\134\uffff";
	static final String DFA95_maxS =
		"\1\u00aa\1\0\134\uffff";
	static final String DFA95_acceptS =
		"\2\uffff\1\2\132\uffff\1\1";
	static final String DFA95_specialS =
		"\1\uffff\1\0\134\uffff}>";
	static final String[] DFA95_transitionS = {
			"\66\2\1\1\160\2",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA95_eot = DFA.unpackEncodedString(DFA95_eotS);
	static final short[] DFA95_eof = DFA.unpackEncodedString(DFA95_eofS);
	static final char[] DFA95_min = DFA.unpackEncodedStringToUnsignedChars(DFA95_minS);
	static final char[] DFA95_max = DFA.unpackEncodedStringToUnsignedChars(DFA95_maxS);
	static final short[] DFA95_accept = DFA.unpackEncodedString(DFA95_acceptS);
	static final short[] DFA95_special = DFA.unpackEncodedString(DFA95_specialS);
	static final short[][] DFA95_transition;

	static {
		int numStates = DFA95_transitionS.length;
		DFA95_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA95_transition[i] = DFA.unpackEncodedString(DFA95_transitionS[i]);
		}
	}

	protected class DFA95 extends DFA {

		public DFA95(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 95;
			this.eot = DFA95_eot;
			this.eof = DFA95_eof;
			this.min = DFA95_min;
			this.max = DFA95_max;
			this.accept = DFA95_accept;
			this.special = DFA95_special;
			this.transition = DFA95_transition;
		}
		@Override
		public String getDescription() {
			return "247:9: ( '<' typeArgument ( ',' typeArgument )* '>' )?";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA95_1 = input.LA(1);
						 
						int index95_1 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred145_GosuProg()) ) {s = 93;}
						else if ( (true) ) {s = 2;}
						 
						input.seek(index95_1);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 95, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	static final String DFA122_eotS =
		"\127\uffff";
	static final String DFA122_eofS =
		"\1\2\126\uffff";
	static final String DFA122_minS =
		"\1\4\1\0\125\uffff";
	static final String DFA122_maxS =
		"\1\u00aa\1\0\125\uffff";
	static final String DFA122_acceptS =
		"\2\uffff\1\2\123\uffff\1\1";
	static final String DFA122_specialS =
		"\1\uffff\1\0\125\uffff}>";
	static final String[] DFA122_transitionS = {
			"\42\2\1\1\u0084\2",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA122_eot = DFA.unpackEncodedString(DFA122_eotS);
	static final short[] DFA122_eof = DFA.unpackEncodedString(DFA122_eofS);
	static final char[] DFA122_min = DFA.unpackEncodedStringToUnsignedChars(DFA122_minS);
	static final char[] DFA122_max = DFA.unpackEncodedStringToUnsignedChars(DFA122_maxS);
	static final short[] DFA122_accept = DFA.unpackEncodedString(DFA122_acceptS);
	static final short[] DFA122_special = DFA.unpackEncodedString(DFA122_specialS);
	static final short[][] DFA122_transition;

	static {
		int numStates = DFA122_transitionS.length;
		DFA122_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA122_transition[i] = DFA.unpackEncodedString(DFA122_transitionS[i]);
		}
	}

	protected class DFA122 extends DFA {

		public DFA122(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 122;
			this.eot = DFA122_eot;
			this.eof = DFA122_eof;
			this.min = DFA122_min;
			this.max = DFA122_max;
			this.accept = DFA122_accept;
			this.special = DFA122_special;
			this.transition = DFA122_transition;
		}
		@Override
		public String getDescription() {
			return "300:21: ( arguments )?";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA122_1 = input.LA(1);
						 
						int index122_1 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred181_GosuProg()) ) {s = 86;}
						else if ( (true) ) {s = 2;}
						 
						input.seek(index122_1);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 122, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	static final String DFA129_eotS =
		"\127\uffff";
	static final String DFA129_eofS =
		"\1\2\126\uffff";
	static final String DFA129_minS =
		"\1\4\1\0\125\uffff";
	static final String DFA129_maxS =
		"\1\u00aa\1\0\125\uffff";
	static final String DFA129_acceptS =
		"\2\uffff\1\2\123\uffff\1\1";
	static final String DFA129_specialS =
		"\1\uffff\1\0\125\uffff}>";
	static final String[] DFA129_transitionS = {
			"\u009e\2\1\1\10\2",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA129_eot = DFA.unpackEncodedString(DFA129_eotS);
	static final short[] DFA129_eof = DFA.unpackEncodedString(DFA129_eofS);
	static final char[] DFA129_min = DFA.unpackEncodedStringToUnsignedChars(DFA129_minS);
	static final char[] DFA129_max = DFA.unpackEncodedStringToUnsignedChars(DFA129_maxS);
	static final short[] DFA129_accept = DFA.unpackEncodedString(DFA129_acceptS);
	static final short[] DFA129_special = DFA.unpackEncodedString(DFA129_specialS);
	static final short[][] DFA129_transition;

	static {
		int numStates = DFA129_transitionS.length;
		DFA129_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA129_transition[i] = DFA.unpackEncodedString(DFA129_transitionS[i]);
		}
	}

	protected class DFA129 extends DFA {

		public DFA129(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 129;
			this.eot = DFA129_eot;
			this.eof = DFA129_eof;
			this.min = DFA129_min;
			this.max = DFA129_max;
			this.accept = DFA129_accept;
			this.special = DFA129_special;
			this.transition = DFA129_transition;
		}
		@Override
		public String getDescription() {
			return "334:56: ( '{' ( initializer | anonymousInnerClass ) '}' )?";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA129_1 = input.LA(1);
						 
						int index129_1 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred192_GosuProg()) ) {s = 86;}
						else if ( (true) ) {s = 2;}
						 
						input.seek(index129_1);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 129, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	static final String DFA131_eotS =
		"\130\uffff";
	static final String DFA131_eofS =
		"\1\2\127\uffff";
	static final String DFA131_minS =
		"\1\4\1\0\126\uffff";
	static final String DFA131_maxS =
		"\1\u00aa\1\0\126\uffff";
	static final String DFA131_acceptS =
		"\2\uffff\1\2\124\uffff\1\1";
	static final String DFA131_specialS =
		"\1\uffff\1\0\126\uffff}>";
	static final String[] DFA131_transitionS = {
			"\110\2\1\1\136\2",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA131_eot = DFA.unpackEncodedString(DFA131_eotS);
	static final short[] DFA131_eof = DFA.unpackEncodedString(DFA131_eofS);
	static final char[] DFA131_min = DFA.unpackEncodedStringToUnsignedChars(DFA131_minS);
	static final char[] DFA131_max = DFA.unpackEncodedStringToUnsignedChars(DFA131_maxS);
	static final short[] DFA131_accept = DFA.unpackEncodedString(DFA131_acceptS);
	static final short[] DFA131_special = DFA.unpackEncodedString(DFA131_specialS);
	static final short[][] DFA131_transition;

	static {
		int numStates = DFA131_transitionS.length;
		DFA131_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA131_transition[i] = DFA.unpackEncodedString(DFA131_transitionS[i]);
		}
	}

	protected class DFA131 extends DFA {

		public DFA131(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 131;
			this.eot = DFA131_eot;
			this.eof = DFA131_eof;
			this.min = DFA131_min;
			this.max = DFA131_max;
			this.accept = DFA131_accept;
			this.special = DFA131_special;
			this.transition = DFA131_transition;
		}
		@Override
		public String getDescription() {
			return "()* loopback of 339:61: ( '[' expression ']' )*";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA131_1 = input.LA(1);
						 
						int index131_1 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred196_GosuProg()) ) {s = 87;}
						else if ( (true) ) {s = 2;}
						 
						input.seek(index131_1);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 131, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	static final String DFA142_eotS =
		"\132\uffff";
	static final String DFA142_eofS =
		"\1\1\131\uffff";
	static final String DFA142_minS =
		"\1\4\60\uffff\1\0\37\uffff\1\0\1\uffff\2\0\5\uffff";
	static final String DFA142_maxS =
		"\1\u00aa\60\uffff\1\0\37\uffff\1\0\1\uffff\2\0\5\uffff";
	static final String DFA142_acceptS =
		"\1\uffff\1\5\124\uffff\1\4\1\1\1\2\1\3";
	static final String DFA142_specialS =
		"\61\uffff\1\0\37\uffff\1\1\1\uffff\1\2\1\3\5\uffff}>";
	static final String[] DFA142_transitionS = {
			"\33\1\1\123\6\1\1\61\2\1\1\121\11\1\1\121\21\1\1\121\2\1\1\124\3\1\1"+
			"\124\136\1",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\uffff",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\uffff",
			"",
			"\1\uffff",
			"\1\uffff",
			"",
			"",
			"",
			"",
			""
	};

	static final short[] DFA142_eot = DFA.unpackEncodedString(DFA142_eotS);
	static final short[] DFA142_eof = DFA.unpackEncodedString(DFA142_eofS);
	static final char[] DFA142_min = DFA.unpackEncodedStringToUnsignedChars(DFA142_minS);
	static final char[] DFA142_max = DFA.unpackEncodedStringToUnsignedChars(DFA142_maxS);
	static final short[] DFA142_accept = DFA.unpackEncodedString(DFA142_acceptS);
	static final short[] DFA142_special = DFA.unpackEncodedString(DFA142_specialS);
	static final short[][] DFA142_transition;

	static {
		int numStates = DFA142_transitionS.length;
		DFA142_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA142_transition[i] = DFA.unpackEncodedString(DFA142_transitionS[i]);
		}
	}

	protected class DFA142 extends DFA {

		public DFA142(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 142;
			this.eot = DFA142_eot;
			this.eof = DFA142_eof;
			this.min = DFA142_min;
			this.max = DFA142_max;
			this.accept = DFA142_accept;
			this.special = DFA142_special;
			this.transition = DFA142_transition;
		}
		@Override
		public String getDescription() {
			return "()* loopback of 362:24: ( ( '.' | '?.' | '*.' ) idAll typeArguments | featureLiteral | ( '[' | '?[' ) expression ']' |{...}? arguments )*";
		}
		public void error(NoViableAltException nvae) {
			dbg.recognitionException(nvae);
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			TokenStream input = (TokenStream)_input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA142_49 = input.LA(1);
						 
						int index142_49 = input.index();
						input.rewind();
						s = -1;
						if ( ((evalPredicate(input.LT(-1).getLine() == input.LT(1).getLine()  ,"input.LT(-1).getLine() == input.LT(1).getLine()  ")&&synpred212_GosuProg())) ) {s = 86;}
						else if ( (true) ) {s = 1;}
						 
						input.seek(index142_49);
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA142_81 = input.LA(1);
						 
						int index142_81 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred208_GosuProg()) ) {s = 87;}
						else if ( (true) ) {s = 1;}
						 
						input.seek(index142_81);
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA142_83 = input.LA(1);
						 
						int index142_83 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred209_GosuProg()) ) {s = 88;}
						else if ( (true) ) {s = 1;}
						 
						input.seek(index142_83);
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA142_84 = input.LA(1);
						 
						int index142_84 = input.index();
						input.rewind();
						s = -1;
						if ( (synpred211_GosuProg()) ) {s = 89;}
						else if ( (true) ) {s = 1;}
						 
						input.seek(index142_84);
						if ( s>=0 ) return s;
						break;
			}
			if (state.backtracking>0) {state.failed=true; return -1;}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 142, _s, input);
			error(nvae);
			throw nvae;
		}
	}

	public static final BitSet FOLLOW_header_in_start71 = new BitSet(new long[]{0x0200004000404000L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_modifiers_in_start75 = new BitSet(new long[]{0x0000000000000000L,0x0080001804000000L,0x0000000000008000L});
	public static final BitSet FOLLOW_gClass_in_start78 = new BitSet(new long[]{0x0200004000404002L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_gInterfaceOrStructure_in_start82 = new BitSet(new long[]{0x0200004000404002L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_gEnum_in_start86 = new BitSet(new long[]{0x0200004000404002L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_gEnhancement_in_start90 = new BitSet(new long[]{0x0200004000404002L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_statementTop_in_start95 = new BitSet(new long[]{0x0200004000404002L,0xAFCFEDFA6CFA0E00L,0x00000007E27FBF7DL});
	public static final BitSet FOLLOW_statement_in_statementTop107 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_statementTop161 = new BitSet(new long[]{0x0000000000000000L,0x0001000000000000L});
	public static final BitSet FOLLOW_functionDefn_in_statementTop163 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_statementTop165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_statementTop193 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_propertyDefn_in_statementTop195 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_statementTop197 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_HASHBANG_in_header219 = new BitSet(new long[]{0x0000000000000002L,0x0000000008000000L,0x0000000012000002L});
	public static final BitSet FOLLOW_classpathStatements_in_header222 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000012000002L});
	public static final BitSet FOLLOW_typeLoaderStatements_in_header225 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000010000002L});
	public static final BitSet FOLLOW_129_in_header229 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_namespaceStatement_in_header231 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000010000000L});
	public static final BitSet FOLLOW_usesStatementList_in_header235 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_91_in_classpathStatements246 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_StringLiteral_in_classpathStatements248 = new BitSet(new long[]{0x0000000000000002L,0x0000000008000000L});
	public static final BitSet FOLLOW_153_in_typeLoaderStatements260 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_typeLoaderStatements262 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000002000000L});
	public static final BitSet FOLLOW_73_in_annotation273 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_idAll_in_annotation275 = new BitSet(new long[]{0x0008004000000002L});
	public static final BitSet FOLLOW_51_in_annotation278 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_idAll_in_annotation280 = new BitSet(new long[]{0x0008004000000002L});
	public static final BitSet FOLLOW_annotationArguments_in_annotation284 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_90_in_gClass294 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_gClass296 = new BitSet(new long[]{0x0400000000000000L,0x0010020000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_typeVariableDefs_in_gClass298 = new BitSet(new long[]{0x0000000000000000L,0x0010020000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_105_in_gClass301 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gClass303 = new BitSet(new long[]{0x0000000000000000L,0x0010000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_116_in_gClass308 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gClass310 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_46_in_gClass313 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gClass315 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_classBody_in_gClass321 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_gInterfaceOrStructure330 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_gInterfaceOrStructure338 = new BitSet(new long[]{0x0400000000000000L,0x0000020000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_typeVariableDefs_in_gInterfaceOrStructure340 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_105_in_gInterfaceOrStructure343 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gInterfaceOrStructure345 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_46_in_gInterfaceOrStructure348 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gInterfaceOrStructure350 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_interfaceBody_in_gInterfaceOrStructure356 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_100_in_gEnum365 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_gEnum367 = new BitSet(new long[]{0x0400000000000000L,0x0010000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_typeVariableDefs_in_gEnum369 = new BitSet(new long[]{0x0000000000000000L,0x0010000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_116_in_gEnum372 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gEnum374 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_46_in_gEnum377 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gEnum379 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_enumBody_in_gEnum385 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_99_in_gEnhancement394 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_gEnhancement396 = new BitSet(new long[]{0x0500000000000000L});
	public static final BitSet FOLLOW_typeVariableDefs_in_gEnhancement398 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_gEnhancement400 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8283A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_gEnhancement402 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L,0x0000000400000000L});
	public static final BitSet FOLLOW_76_in_gEnhancement405 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_gEnhancement407 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L,0x0000000400000000L});
	public static final BitSet FOLLOW_enhancementBody_in_gEnhancement411 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_classBody420 = new BitSet(new long[]{0x0000000000000000L,0x0181081114020200L,0x000002004010A03DL});
	public static final BitSet FOLLOW_classMembers_in_classBody422 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_classBody424 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_enhancementBody433 = new BitSet(new long[]{0x0000000000000000L,0x0101080000020200L,0x000002000010203DL});
	public static final BitSet FOLLOW_enhancementMembers_in_enhancementBody435 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_enhancementBody437 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_interfaceBody446 = new BitSet(new long[]{0x0000000000000000L,0x0181081004020200L,0x000002004010A03DL});
	public static final BitSet FOLLOW_interfaceMembers_in_interfaceBody448 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_interfaceBody450 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_enumBody459 = new BitSet(new long[]{0x0000000000004000L,0xA7C72DD93C7A0E00L,0x00000201C230BD7DL});
	public static final BitSet FOLLOW_enumConstants_in_enumBody461 = new BitSet(new long[]{0x0000000000000000L,0x0181081114020200L,0x000002004010A03DL});
	public static final BitSet FOLLOW_classMembers_in_enumBody464 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_enumBody466 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumConstant_in_enumConstants475 = new BitSet(new long[]{0x0200400000000002L});
	public static final BitSet FOLLOW_46_in_enumConstants479 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_enumConstant_in_enumConstants481 = new BitSet(new long[]{0x0200400000000002L});
	public static final BitSet FOLLOW_46_in_enumConstants486 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_57_in_enumConstants489 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_enumConstant500 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_optionalArguments_in_enumConstant502 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_interfaceMembers513 = new BitSet(new long[]{0x0000000000000000L,0x0081001004000000L,0x0000000040008008L});
	public static final BitSet FOLLOW_functionDefn_in_interfaceMembers567 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_propertyDefn_in_interfaceMembers607 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_fieldDefn_in_interfaceMembers647 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_gClass_in_interfaceMembers690 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_gInterfaceOrStructure_in_interfaceMembers736 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_gEnum_in_interfaceMembers767 = new BitSet(new long[]{0x0200000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_57_in_interfaceMembers795 = new BitSet(new long[]{0x0000000000000002L,0x0181081004020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_declaration_in_classMembers827 = new BitSet(new long[]{0x0000000000000002L,0x0181081114020200L,0x000000004010A03DL});
	public static final BitSet FOLLOW_modifiers_in_declaration854 = new BitSet(new long[]{0x0000000000000000L,0x0081001114000000L,0x0000000040008008L});
	public static final BitSet FOLLOW_functionDefn_in_declaration894 = new BitSet(new long[]{0x0200000000000002L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_declaration896 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_constructorDefn_in_declaration924 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_declaration926 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_propertyDefn_in_declaration951 = new BitSet(new long[]{0x0200000000000002L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_declaration953 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_fieldDefn_in_declaration981 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_delegateDefn_in_declaration1025 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_gClass_in_declaration1066 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_gInterfaceOrStructure_in_declaration1113 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_gEnum_in_declaration1145 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_57_in_declaration1166 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_enhancementMembers1190 = new BitSet(new long[]{0x0000000000000000L,0x0001000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_functionDefn_in_enhancementMembers1244 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_enhancementMembers1246 = new BitSet(new long[]{0x0200000000000002L,0x0101080000020200L,0x000000000010203DL});
	public static final BitSet FOLLOW_propertyDefn_in_enhancementMembers1277 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_enhancementMembers1279 = new BitSet(new long[]{0x0200000000000002L,0x0101080000020200L,0x000000000010203DL});
	public static final BitSet FOLLOW_57_in_enhancementMembers1307 = new BitSet(new long[]{0x0000000000000002L,0x0101080000020200L,0x000000000010203DL});
	public static final BitSet FOLLOW_96_in_delegateDefn1360 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_delegateDefn1362 = new BitSet(new long[]{0x0100000000000000L,0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_delegateStatement_in_delegateDefn1364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_delegateStatement1374 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_delegateStatement1376 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_135_in_delegateStatement1380 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_delegateStatement1382 = new BitSet(new long[]{0x1000400000000002L});
	public static final BitSet FOLLOW_46_in_delegateStatement1385 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_delegateStatement1387 = new BitSet(new long[]{0x1000400000000002L});
	public static final BitSet FOLLOW_60_in_delegateStatement1392 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_delegateStatement1394 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_optionalType1406 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_optionalType1408 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockTypeLiteral_in_optionalType1412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_158_in_fieldDefn1423 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_fieldDefn1425 = new BitSet(new long[]{0x1100004000000000L,0x0000000000100000L});
	public static final BitSet FOLLOW_optionalType_in_fieldDefn1427 = new BitSet(new long[]{0x1000000000000002L,0x0000000000100000L});
	public static final BitSet FOLLOW_84_in_fieldDefn1430 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_134_in_fieldDefn1432 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_fieldDefn1435 = new BitSet(new long[]{0x1000000000000002L});
	public static final BitSet FOLLOW_60_in_fieldDefn1440 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_fieldDefn1442 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_131_in_propertyDefn1453 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_set_in_propertyDefn1455 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_propertyDefn1463 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_parameters_in_propertyDefn1465 = new BitSet(new long[]{0x0100000000000002L});
	public static final BitSet FOLLOW_56_in_propertyDefn1468 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_propertyDefn1470 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_idAll_in_dotPathWord1481 = new BitSet(new long[]{0x0008000000000002L});
	public static final BitSet FOLLOW_51_in_dotPathWord1484 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_idAll_in_dotPathWord1486 = new BitSet(new long[]{0x0008000000000002L});
	public static final BitSet FOLLOW_dotPathWord_in_namespaceStatement1497 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_57_in_namespaceStatement1500 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_156_in_usesStatementList1512 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_usesStatement_in_usesStatementList1514 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000010000000L});
	public static final BitSet FOLLOW_dotPathWord_in_usesStatement1525 = new BitSet(new long[]{0x0208000000000002L});
	public static final BitSet FOLLOW_51_in_usesStatement1528 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_40_in_usesStatement1530 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_57_in_usesStatement1535 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_58_in_typeVariableDefs1547 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeVariableDefinition_in_typeVariableDefs1549 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_46_in_typeVariableDefs1552 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeVariableDefinition_in_typeVariableDefs1554 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_63_in_typeVariableDefs1558 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_typeVariableDefinition1568 = new BitSet(new long[]{0x0000000000000002L,0x0000020000000000L});
	public static final BitSet FOLLOW_105_in_typeVariableDefinition1571 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteralList_in_typeVariableDefinition1573 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statementBlock_in_functionBody1584 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_38_in_parameters1593 = new BitSet(new long[]{0x0000008000004000L,0xA7462DC8287A0E00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_parameterDeclarationList_in_parameters1595 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_parameters1598 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_112_in_functionDefn1607 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_functionDefn1609 = new BitSet(new long[]{0x0400004000000000L});
	public static final BitSet FOLLOW_typeVariableDefs_in_functionDefn1611 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_parameters_in_functionDefn1614 = new BitSet(new long[]{0x0100000000000002L});
	public static final BitSet FOLLOW_56_in_functionDefn1617 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_functionDefn1619 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_92_in_constructorDefn1630 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_parameters_in_constructorDefn1632 = new BitSet(new long[]{0x0100000000000002L});
	public static final BitSet FOLLOW_56_in_constructorDefn1635 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_constructorDefn1637 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotation_in_modifiers1650 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_130_in_modifiers1669 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_120_in_modifiers1689 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_132_in_modifiers1708 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_133_in_modifiers1726 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_141_in_modifiers1747 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_81_in_modifiers1768 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_128_in_modifiers1787 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_107_in_modifiers1821 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_148_in_modifiers1843 = new BitSet(new long[]{0x0000000000000002L,0x0100080000020200L,0x0000000000102035L});
	public static final BitSet FOLLOW_ifStatement_in_statement1880 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_tryCatchFinallyStatement_in_statement1916 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_throwStatement_in_statement1939 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_94_in_statement1972 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_87_in_statement2009 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_returnStatement_in_statement2049 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_forEachStatement_in_statement2081 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_whileStatement_in_statement2112 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_doWhileStatement_in_statement2145 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_switchStatement_in_statement2176 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_usingStatement_in_statement2208 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_assertStatement_in_statement2241 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_107_in_statement2273 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000040000000L});
	public static final BitSet FOLLOW_localVarStatement_in_statement2275 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_localVarStatement_in_statement2297 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_evalExpr_in_statement2327 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_assignmentOrMethodCall_in_statement2366 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_statementBlock_in_statement2391 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_57_in_statement2394 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_statement2422 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_115_in_ifStatement2441 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_ifStatement2443 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_ifStatement2445 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_ifStatement2447 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_ifStatement2449 = new BitSet(new long[]{0x0200000000000002L,0x0000000400000000L});
	public static final BitSet FOLLOW_57_in_ifStatement2452 = new BitSet(new long[]{0x0000000000000002L,0x0000000400000000L});
	public static final BitSet FOLLOW_98_in_ifStatement2456 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_ifStatement2458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_150_in_tryCatchFinallyStatement2470 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_tryCatchFinallyStatement2472 = new BitSet(new long[]{0x0000000000000000L,0x0000100002000000L});
	public static final BitSet FOLLOW_catchClause_in_tryCatchFinallyStatement2476 = new BitSet(new long[]{0x0000000000000002L,0x0000100002000000L});
	public static final BitSet FOLLOW_108_in_tryCatchFinallyStatement2480 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_tryCatchFinallyStatement2482 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_108_in_tryCatchFinallyStatement2488 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_tryCatchFinallyStatement2490 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_89_in_catchClause2500 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_catchClause2502 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x00000001C2203D74L});
	public static final BitSet FOLLOW_158_in_catchClause2504 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_catchClause2507 = new BitSet(new long[]{0x0100008000000000L});
	public static final BitSet FOLLOW_56_in_catchClause2510 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_catchClause2512 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_catchClause2516 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_catchClause2518 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_85_in_assertStatement2527 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_assertStatement2529 = new BitSet(new long[]{0x0100000000000002L});
	public static final BitSet FOLLOW_56_in_assertStatement2532 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_assertStatement2534 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_157_in_usingStatement2546 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_usingStatement2548 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x00000405C6257D74L});
	public static final BitSet FOLLOW_localVarStatement_in_usingStatement2551 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_46_in_usingStatement2554 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000040000000L});
	public static final BitSet FOLLOW_localVarStatement_in_usingStatement2556 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_expression_in_usingStatement2562 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_usingStatement2565 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_usingStatement2567 = new BitSet(new long[]{0x0000000000000002L,0x0000100000000000L});
	public static final BitSet FOLLOW_108_in_usingStatement2570 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_statementBlock_in_usingStatement2572 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_137_in_returnStatement2583 = new BitSet(new long[]{0x0000884092504042L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_returnStatement2602 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_161_in_whileStatement2614 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_whileStatement2616 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_whileStatement2617 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_whileStatement2619 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_whileStatement2621 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_97_in_doWhileStatement2630 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_doWhileStatement2632 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_161_in_doWhileStatement2634 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_doWhileStatement2636 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_doWhileStatement2638 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_doWhileStatement2640 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_145_in_switchStatement2649 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_switchStatement2651 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_switchStatement2653 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_switchStatement2655 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_162_in_switchStatement2657 = new BitSet(new long[]{0x0000000000000000L,0x0000000081000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_switchBlockStatementGroup_in_switchStatement2659 = new BitSet(new long[]{0x0000000000000000L,0x0000000081000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_switchStatement2662 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_88_in_switchBlockStatementGroup2672 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_switchBlockStatementGroup2674 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_switchBlockStatementGroup2676 = new BitSet(new long[]{0x0200004000404002L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_95_in_switchBlockStatementGroup2679 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_switchBlockStatementGroup2681 = new BitSet(new long[]{0x0200004000404002L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_switchBlockStatementGroup2684 = new BitSet(new long[]{0x0200004000404002L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_147_in_throwStatement2694 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_throwStatement2696 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_158_in_localVarStatement2705 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_localVarStatement2707 = new BitSet(new long[]{0x1100004000000000L});
	public static final BitSet FOLLOW_optionalType_in_localVarStatement2709 = new BitSet(new long[]{0x1000000000000002L});
	public static final BitSet FOLLOW_60_in_localVarStatement2712 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_localVarStatement2714 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_forEachStatement2725 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_forEachStatement2733 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x00000405C6257D74L});
	public static final BitSet FOLLOW_expression_in_forEachStatement2736 = new BitSet(new long[]{0x0000008000000000L,0x0040000000000000L});
	public static final BitSet FOLLOW_indexVar_in_forEachStatement2738 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_158_in_forEachStatement2743 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_forEachStatement2746 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
	public static final BitSet FOLLOW_117_in_forEachStatement2748 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_forEachStatement2750 = new BitSet(new long[]{0x0000008000000000L,0x0240000000000000L});
	public static final BitSet FOLLOW_indexRest_in_forEachStatement2752 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_forEachStatement2756 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_forEachStatement2758 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_indexVar_in_indexRest2767 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
	public static final BitSet FOLLOW_iteratorVar_in_indexRest2769 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_iteratorVar_in_indexRest2786 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
	public static final BitSet FOLLOW_indexVar_in_indexRest2788 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_indexVar_in_indexRest2805 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_iteratorVar_in_indexRest2834 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_118_in_indexVar2853 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_indexVar2855 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_121_in_iteratorVar2864 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_iteratorVar2866 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_newExpr_in_assignmentOrMethodCall2918 = new BitSet(new long[]{0x948B366A80000000L,0x0000000000011120L,0x0000014000000000L});
	public static final BitSet FOLLOW_thisSuperExpr_in_assignmentOrMethodCall2922 = new BitSet(new long[]{0x948B366A80000000L,0x0000000000011120L,0x0000014000000000L});
	public static final BitSet FOLLOW_typeLiteralExpr_in_assignmentOrMethodCall2926 = new BitSet(new long[]{0x948B366A80000000L,0x0000000000011120L,0x0000014000000000L});
	public static final BitSet FOLLOW_parenthExpr_in_assignmentOrMethodCall2930 = new BitSet(new long[]{0x948B366A80000000L,0x0000000000011120L,0x0000014000000000L});
	public static final BitSet FOLLOW_StringLiteral_in_assignmentOrMethodCall2934 = new BitSet(new long[]{0x948B366A80000000L,0x0000000000011120L,0x0000014000000000L});
	public static final BitSet FOLLOW_indirectMemberAccess_in_assignmentOrMethodCall2964 = new BitSet(new long[]{0x9483342A00000002L,0x0000000000010000L,0x0000014000000000L});
	public static final BitSet FOLLOW_incrementOp_in_assignmentOrMethodCall2994 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentOp_in_assignmentOrMethodCall2998 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_assignmentOrMethodCall3000 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statementBlockBody_in_statementBlock3035 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_statementBlockBody3044 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000207E26F3F74L});
	public static final BitSet FOLLOW_statement_in_statementBlockBody3046 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000207E26F3F74L});
	public static final BitSet FOLLOW_169_in_statementBlockBody3049 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockLiteral_in_blockTypeLiteral3058 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_38_in_blockLiteral3067 = new BitSet(new long[]{0x0000008000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_blockLiteralArg_in_blockLiteral3070 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_46_in_blockLiteral3073 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_blockLiteralArg_in_blockLiteral3075 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_39_in_blockLiteral3080 = new BitSet(new long[]{0x0100000000000002L});
	public static final BitSet FOLLOW_56_in_blockLiteral3083 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_blockLiteral3085 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_blockLiteralArg3100 = new BitSet(new long[]{0x1000004000000000L});
	public static final BitSet FOLLOW_60_in_blockLiteralArg3103 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_blockLiteralArg3105 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockTypeLiteral_in_blockLiteralArg3109 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_blockLiteralArg3134 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_blockLiteralArg3136 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_blockLiteralArg3140 = new BitSet(new long[]{0x1000000000000002L});
	public static final BitSet FOLLOW_60_in_blockLiteralArg3143 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_blockLiteralArg3145 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_typeLiteral3157 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_36_in_typeLiteral3161 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_type_in_typeLiteral3163 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_typeLiteral_in_typeLiteralType3173 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeLiteral_in_typeLiteralExpr3182 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeLiteral_in_typeLiteralList3191 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_type3212 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_type3216 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_type3218 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
	public static final BitSet FOLLOW_86_in_type3231 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_blockLiteral_in_type3233 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_idclassOrInterfaceType_in_classOrInterfaceType3252 = new BitSet(new long[]{0x0408000000000000L});
	public static final BitSet FOLLOW_typeArguments_in_classOrInterfaceType3254 = new BitSet(new long[]{0x0008000000000002L});
	public static final BitSet FOLLOW_51_in_classOrInterfaceType3257 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_classOrInterfaceType3259 = new BitSet(new long[]{0x0408000000000000L});
	public static final BitSet FOLLOW_typeArguments_in_classOrInterfaceType3261 = new BitSet(new long[]{0x0008000000000002L});
	public static final BitSet FOLLOW_58_in_typeArguments3284 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C02L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeArgument_in_typeArguments3286 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_46_in_typeArguments3289 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C02L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeArgument_in_typeArguments3291 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_63_in_typeArguments3295 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeLiteralType_in_typeArgument3316 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_65_in_typeArgument3320 = new BitSet(new long[]{0x0000000000000002L,0x0000020000000000L,0x0000000000010000L});
	public static final BitSet FOLLOW_set_in_typeArgument3323 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteralType_in_typeArgument3331 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpr_in_expression3346 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalOrExpr_in_conditionalExpr3356 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000082L});
	public static final BitSet FOLLOW_65_in_conditionalExpr3359 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_conditionalExpr3361 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_conditionalExpr3363 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_conditionalExpr3365 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_71_in_conditionalExpr3369 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_conditionalExpr3371 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalAndExpr_in_conditionalOrExpr3381 = new BitSet(new long[]{0x0000000000000002L,0x4000000000000000L,0x0000008000000000L});
	public static final BitSet FOLLOW_orOp_in_conditionalOrExpr3384 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalAndExpr_in_conditionalOrExpr3386 = new BitSet(new long[]{0x0000000000000002L,0x4000000000000000L,0x0000008000000000L});
	public static final BitSet FOLLOW_bitwiseOrExpr_in_conditionalAndExpr3397 = new BitSet(new long[]{0x0000000400000002L,0x0000000000040000L});
	public static final BitSet FOLLOW_andOp_in_conditionalAndExpr3400 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseOrExpr_in_conditionalAndExpr3402 = new BitSet(new long[]{0x0000000400000002L,0x0000000000040000L});
	public static final BitSet FOLLOW_bitwiseXorExpr_in_bitwiseOrExpr3413 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
	public static final BitSet FOLLOW_163_in_bitwiseOrExpr3416 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseXorExpr_in_bitwiseOrExpr3418 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
	public static final BitSet FOLLOW_bitwiseAndExpr_in_bitwiseXorExpr3429 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008000L});
	public static final BitSet FOLLOW_79_in_bitwiseXorExpr3432 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseAndExpr_in_bitwiseXorExpr3434 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008000L});
	public static final BitSet FOLLOW_equalityExpr_in_bitwiseAndExpr3445 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_36_in_bitwiseAndExpr3448 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_equalityExpr_in_bitwiseAndExpr3450 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_relationalExpr_in_equalityExpr3461 = new BitSet(new long[]{0x6800000060000002L});
	public static final BitSet FOLLOW_equalityOp_in_equalityExpr3464 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_relationalExpr_in_equalityExpr3466 = new BitSet(new long[]{0x6800000060000002L});
	public static final BitSet FOLLOW_intervalExpr_in_relationalExpr3477 = new BitSet(new long[]{0x8400000000000002L,0x0000000000000000L,0x0000000001000000L});
	public static final BitSet FOLLOW_relOp_in_relationalExpr3480 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_intervalExpr_in_relationalExpr3482 = new BitSet(new long[]{0x8400000000000002L,0x0000000000000000L,0x0000000001000000L});
	public static final BitSet FOLLOW_152_in_relationalExpr3486 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteralType_in_relationalExpr3488 = new BitSet(new long[]{0x8400000000000002L,0x0000000000000000L,0x0000000001000000L});
	public static final BitSet FOLLOW_bitshiftExpr_in_intervalExpr3500 = new BitSet(new long[]{0x0030000000000002L,0x0000000000000000L,0x0000003000000000L});
	public static final BitSet FOLLOW_intervalOp_in_intervalExpr3503 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitshiftExpr_in_intervalExpr3505 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveExpr_in_bitshiftExpr3516 = new BitSet(new long[]{0x8400000000000002L});
	public static final BitSet FOLLOW_bitshiftOp_in_bitshiftExpr3519 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_additiveExpr_in_bitshiftExpr3521 = new BitSet(new long[]{0x8400000000000002L});
	public static final BitSet FOLLOW_multiplicativeExpr_in_additiveExpr3533 = new BitSet(new long[]{0x0000880018000002L,0x0000000000000018L});
	public static final BitSet FOLLOW_additiveOp_in_additiveExpr3536 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_multiplicativeExpr_in_additiveExpr3538 = new BitSet(new long[]{0x0000880018000002L,0x0000000000000018L});
	public static final BitSet FOLLOW_typeAsExpr_in_multiplicativeExpr3549 = new BitSet(new long[]{0x0040010104000002L,0x0000000000000045L});
	public static final BitSet FOLLOW_multiplicativeOp_in_multiplicativeExpr3552 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_typeAsExpr_in_multiplicativeExpr3554 = new BitSet(new long[]{0x0040010104000002L,0x0000000000000045L});
	public static final BitSet FOLLOW_unaryExpr_in_typeAsExpr3565 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L,0x0000000000800000L});
	public static final BitSet FOLLOW_typeAsOp_in_typeAsExpr3568 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_typeAsExpr3570 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L,0x0000000000800000L});
	public static final BitSet FOLLOW_set_in_unaryExpr3582 = new BitSet(new long[]{0x0000004082504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_unaryExprNotPlusMinus_in_unaryExpr3594 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExprNotPlusMinus_in_unaryExpr3598 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryOp_in_unaryExprNotPlusMinus3608 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_unaryExpr_in_unaryExprNotPlusMinus3610 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_77_in_unaryExprNotPlusMinus3639 = new BitSet(new long[]{0x0004000000004000L,0xA7462DC8287A0E00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_blockExpr_in_unaryExprNotPlusMinus3641 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_evalExpr_in_unaryExprNotPlusMinus3673 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primaryExpr_in_unaryExprNotPlusMinus3711 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_parameterDeclarationList_in_blockExpr3742 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_50_in_blockExpr3745 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_blockExpr3748 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statementBlock_in_blockExpr3752 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_parameterDeclaration_in_parameterDeclarationList3763 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_46_in_parameterDeclarationList3766 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0E00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_parameterDeclaration_in_parameterDeclarationList3768 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_annotation_in_parameterDeclaration3779 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0E00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_107_in_parameterDeclaration3782 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_parameterDeclaration3785 = new BitSet(new long[]{0x1100004000000002L});
	public static final BitSet FOLLOW_56_in_parameterDeclaration3789 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_parameterDeclaration3791 = new BitSet(new long[]{0x1000000000000002L});
	public static final BitSet FOLLOW_60_in_parameterDeclaration3794 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_parameterDeclaration3796 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockTypeLiteral_in_parameterDeclaration3803 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_parameterDeclaration3807 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_parameterDeclaration3809 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_annotationArguments3821 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_38_in_arguments3830 = new BitSet(new long[]{0x010088C092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_argExpression_in_arguments3833 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_46_in_arguments3836 = new BitSet(new long[]{0x0100884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_argExpression_in_arguments3838 = new BitSet(new long[]{0x0000408000000000L});
	public static final BitSet FOLLOW_39_in_arguments3844 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_optionalArguments3853 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_namedArgumentExpression_in_argExpression3863 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_argExpression3867 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_namedArgumentExpression3876 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_namedArgumentExpression3878 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_namedArgumentExpression3880 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_namedArgumentExpression3882 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_101_in_evalExpr3890 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_evalExpr3892 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_evalExpr3894 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_evalExpr3896 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_31_in_featureLiteral3905 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8387A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_featureLiteral3908 = new BitSet(new long[]{0x0400004000000000L});
	public static final BitSet FOLLOW_92_in_featureLiteral3912 = new BitSet(new long[]{0x0400004000000000L});
	public static final BitSet FOLLOW_typeArguments_in_featureLiteral3916 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_optionalArguments_in_featureLiteral3919 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_standAloneDataStructureInitialization3928 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000060586257D74L});
	public static final BitSet FOLLOW_initializerExpression_in_standAloneDataStructureInitialization3931 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_standAloneDataStructureInitialization3935 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_newExpr_in_primaryExpr3961 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_thisSuperExpr_in_primaryExpr4017 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_literal_in_primaryExpr4103 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_typeLiteralExpr_in_primaryExpr4159 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_parenthExpr_in_primaryExpr4207 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_standAloneDataStructureInitialization_in_primaryExpr4259 = new BitSet(new long[]{0x0008024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_indirectMemberAccess_in_primaryExpr4278 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_38_in_parenthExpr4300 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_parenthExpr4302 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_parenthExpr4304 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_123_in_newExpr4323 = new BitSet(new long[]{0x0000004000004000L,0xA7462DC8283A1C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_newExpr4325 = new BitSet(new long[]{0x0000004000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_arguments_in_newExpr4376 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_162_in_newExpr4380 = new BitSet(new long[]{0x0100884092504040L,0xBFC72DF93C7A2E00L,0x00000605C635FD7DL});
	public static final BitSet FOLLOW_initializer_in_newExpr4383 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_anonymousInnerClass_in_newExpr4387 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_newExpr4390 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_76_in_newExpr4479 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A6C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_78_in_newExpr4570 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L,0x0000000400000000L});
	public static final BitSet FOLLOW_76_in_newExpr4573 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_newExpr4575 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L,0x0000000400000000L});
	public static final BitSet FOLLOW_arrayInitializer_in_newExpr4579 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_newExpr4644 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_newExpr4646 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_newExpr4649 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_newExpr4651 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_newExpr4653 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_newExpr4658 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_newExpr4660 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
	public static final BitSet FOLLOW_classMembers_in_anonymousInnerClass4798 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_arrayInitializer4806 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000060586257D74L});
	public static final BitSet FOLLOW_expression_in_arrayInitializer4810 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_46_in_arrayInitializer4813 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_arrayInitializer4816 = new BitSet(new long[]{0x0000400000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_arrayInitializer4822 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_initializerExpression_in_initializer4831 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_objectInitializer_in_initializer4835 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_mapInitializerList_in_initializerExpression4846 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrayValueList_in_initializerExpression4850 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_arrayValueList4860 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_46_in_arrayValueList4863 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_arrayValueList4865 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_expression_in_mapInitializerList4876 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_50_in_mapInitializerList4878 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_mapInitializerList4880 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_46_in_mapInitializerList4883 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_mapInitializerList4885 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_50_in_mapInitializerList4887 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_mapInitializerList4889 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_initializerAssignment_in_objectInitializer4902 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_46_in_objectInitializer4905 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_initializerAssignment_in_objectInitializer4907 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_56_in_initializerAssignment4918 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_initializerAssignment4920 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_initializerAssignment4922 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_initializerAssignment4924 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_indirectMemberAccess4985 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_idAll_in_indirectMemberAccess4997 = new BitSet(new long[]{0x0408024080000000L,0x0000000000001120L});
	public static final BitSet FOLLOW_typeArguments_in_indirectMemberAccess4999 = new BitSet(new long[]{0x0008024080000002L,0x0000000000001120L});
	public static final BitSet FOLLOW_featureLiteral_in_indirectMemberAccess5030 = new BitSet(new long[]{0x0008024080000002L,0x0000000000001120L});
	public static final BitSet FOLLOW_set_in_indirectMemberAccess5086 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_indirectMemberAccess5092 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_indirectMemberAccess5095 = new BitSet(new long[]{0x0008024080000002L,0x0000000000001120L});
	public static final BitSet FOLLOW_arguments_in_indirectMemberAccess5141 = new BitSet(new long[]{0x0008024080000002L,0x0000000000001120L});
	public static final BitSet FOLLOW_NumberLiteral_in_literal5200 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_featureLiteral_in_literal5223 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_StringLiteral_in_literal5245 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CharLiteral_in_literal5268 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_149_in_literal5293 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_106_in_literal5323 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_125_in_literal5352 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_assignmentOp5403 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_45_in_assignmentOp5413 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_49_in_assignmentOp5423 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_42_in_assignmentOp5433 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_55_in_assignmentOp5443 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_37_in_assignmentOp5453 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_35_in_assignmentOp5463 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_166_in_assignmentOp5473 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_168_in_assignmentOp5483 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_80_in_assignmentOp5493 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_33_in_assignmentOp5503 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_assignmentOp5524 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_58_in_assignmentOp5528 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_assignmentOp5532 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_assignmentOp5565 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_assignmentOp5569 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_assignmentOp5573 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_assignmentOp5577 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_assignmentOp5608 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_assignmentOp5612 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_assignmentOp5616 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_relOp5716 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_relOp5720 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_relOp5749 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_relOp5753 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_relOp5773 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_relOp5783 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_bitshiftOp5813 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_58_in_bitshiftOp5817 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_bitshiftOp5848 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_bitshiftOp5852 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_bitshiftOp5856 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_bitshiftOp5885 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_bitshiftOp5889 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_idAll8203 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_82_in_idAll8233 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_126_in_idAll8260 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_124_in_idAll8288 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_117_in_idAll8315 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_158_in_idAll8343 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_96_in_idAll8370 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_135_in_idAll8392 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_154_in_idAll8412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_142_in_idAll8436 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_152_in_idAll8454 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_151_in_idAll8478 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_129_in_idAll8502 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_156_in_idAll8525 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_115_in_idAll8551 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_98_in_idAll8579 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_155_in_idAll8605 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_111_in_idAll8629 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_110_in_idAll8652 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_161_in_idAll8679 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_97_in_idAll8704 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_94_in_idAll8732 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_87_in_idAll8754 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_137_in_idAll8779 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_92_in_idAll8803 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_112_in_idAll8824 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_131_in_idAll8846 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_150_in_idAll8868 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_89_in_idAll8895 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_108_in_idAll8920 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_147_in_idAll8943 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_123_in_idAll8968 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_145_in_idAll8995 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_88_in_idAll9019 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_idAll9045 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_101_in_idAll9068 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_128_in_idAll9094 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_105_in_idAll9116 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_148_in_idAll9139 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_116_in_idAll9160 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_90_in_idAll9180 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_119_in_idAll9205 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_143_in_idAll9226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_100_in_idAll9247 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_157_in_idAll9273 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_synpred4_GosuProg75 = new BitSet(new long[]{0x0000000000000000L,0x0080001804000000L,0x0000000000008000L});
	public static final BitSet FOLLOW_gClass_in_synpred4_GosuProg78 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_gInterfaceOrStructure_in_synpred4_GosuProg82 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_gEnum_in_synpred4_GosuProg86 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_gEnhancement_in_synpred4_GosuProg90 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statementTop_in_synpred5_GosuProg95 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_synpred6_GosuProg107 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_synpred7_GosuProg161 = new BitSet(new long[]{0x0000000000000000L,0x0001000000000000L});
	public static final BitSet FOLLOW_functionDefn_in_synpred7_GosuProg163 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
	public static final BitSet FOLLOW_functionBody_in_synpred7_GosuProg165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classpathStatements_in_synpred9_GosuProg222 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeLoaderStatements_in_synpred10_GosuProg225 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_91_in_synpred13_GosuProg246 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_StringLiteral_in_synpred13_GosuProg248 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_153_in_synpred14_GosuProg260 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_synpred14_GosuProg262 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumConstants_in_synpred26_GosuProg461 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_46_in_synpred27_GosuProg479 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_enumConstant_in_synpred27_GosuProg481 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockTypeLiteral_in_synpred55_GosuProg1412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_134_in_synpred56_GosuProg1432 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_synpred62_GosuProg1500 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_synpred65_GosuProg1535 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assertStatement_in_synpred93_GosuProg2241 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_107_in_synpred94_GosuProg2273 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000040000000L});
	public static final BitSet FOLLOW_localVarStatement_in_synpred94_GosuProg2275 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentOrMethodCall_in_synpred97_GosuProg2366 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_synpred98_GosuProg2394 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_synpred100_GosuProg2452 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_98_in_synpred101_GosuProg2456 = new BitSet(new long[]{0x0200004000404000L,0xAF4EEDEA68FA0C00L,0x00000007E26F3F74L});
	public static final BitSet FOLLOW_statement_in_synpred101_GosuProg2458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_synpred112_GosuProg2588 = new BitSet(new long[]{0xEFFFFFFFFFFFFFF0L,0xFFFFFFFFFFFFFFFFL,0x000007FFFFFFFFFFL});
	public static final BitSet FOLLOW_set_in_synpred112_GosuProg2590 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_synpred135_GosuProg3083 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_synpred135_GosuProg3085 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_id_in_synpred137_GosuProg3100 = new BitSet(new long[]{0x1000004000000000L});
	public static final BitSet FOLLOW_60_in_synpred137_GosuProg3103 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_synpred137_GosuProg3105 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_blockTypeLiteral_in_synpred137_GosuProg3109 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_36_in_synpred140_GosuProg3161 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_type_in_synpred140_GosuProg3163 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_51_in_synpred143_GosuProg3257 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_id_in_synpred143_GosuProg3259 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_typeArguments_in_synpred143_GosuProg3261 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_synpred145_GosuProg3284 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C02L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeArgument_in_synpred145_GosuProg3286 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_46_in_synpred145_GosuProg3289 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C02L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeArgument_in_synpred145_GosuProg3291 = new BitSet(new long[]{0x8000400000000000L});
	public static final BitSet FOLLOW_63_in_synpred145_GosuProg3295 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_65_in_synpred149_GosuProg3359 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_synpred149_GosuProg3361 = new BitSet(new long[]{0x0100000000000000L});
	public static final BitSet FOLLOW_56_in_synpred149_GosuProg3363 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_synpred149_GosuProg3365 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_71_in_synpred150_GosuProg3369 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalExpr_in_synpred150_GosuProg3371 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_orOp_in_synpred151_GosuProg3384 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_conditionalAndExpr_in_synpred151_GosuProg3386 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andOp_in_synpred152_GosuProg3400 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseOrExpr_in_synpred152_GosuProg3402 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_163_in_synpred153_GosuProg3416 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseXorExpr_in_synpred153_GosuProg3418 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_79_in_synpred154_GosuProg3432 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitwiseAndExpr_in_synpred154_GosuProg3434 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_36_in_synpred155_GosuProg3448 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_equalityExpr_in_synpred155_GosuProg3450 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_equalityOp_in_synpred156_GosuProg3464 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_relationalExpr_in_synpred156_GosuProg3466 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_relOp_in_synpred157_GosuProg3480 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_intervalExpr_in_synpred157_GosuProg3482 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_152_in_synpred158_GosuProg3486 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteralType_in_synpred158_GosuProg3488 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_intervalOp_in_synpred159_GosuProg3503 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_bitshiftExpr_in_synpred159_GosuProg3505 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_bitshiftOp_in_synpred160_GosuProg3519 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_additiveExpr_in_synpred160_GosuProg3521 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveOp_in_synpred161_GosuProg3536 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_multiplicativeExpr_in_synpred161_GosuProg3538 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplicativeOp_in_synpred162_GosuProg3552 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_typeAsExpr_in_synpred162_GosuProg3554 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeAsOp_in_synpred163_GosuProg3568 = new BitSet(new long[]{0x0000000000004000L,0xA7462DC8287A0C00L,0x0000000182203D74L});
	public static final BitSet FOLLOW_typeLiteral_in_synpred163_GosuProg3570 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_synpred171_GosuProg3748 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_synpred181_GosuProg3853 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_synpred187_GosuProg4103 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_typeLiteralExpr_in_synpred188_GosuProg4159 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_initializer_in_synpred191_GosuProg4383 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_162_in_synpred192_GosuProg4380 = new BitSet(new long[]{0x0100884092504040L,0xBFC72DF93C7A2E00L,0x00000605C635FD7DL});
	public static final BitSet FOLLOW_initializer_in_synpred192_GosuProg4383 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_anonymousInnerClass_in_synpred192_GosuProg4387 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_169_in_synpred192_GosuProg4390 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_76_in_synpred196_GosuProg4649 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_synpred196_GosuProg4651 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_synpred196_GosuProg4653 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_mapInitializerList_in_synpred202_GosuProg4846 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_synpred208_GosuProg4985 = new BitSet(new long[]{0x0000000000004000L,0xFFFFFFFFFFFE0C00L,0x00000003FFFAFFFFL});
	public static final BitSet FOLLOW_idAll_in_synpred208_GosuProg4997 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_typeArguments_in_synpred208_GosuProg4999 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_featureLiteral_in_synpred209_GosuProg5030 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_synpred211_GosuProg5086 = new BitSet(new long[]{0x0000884092504040L,0xBF462DE8287A2C00L,0x0000040586257D74L});
	public static final BitSet FOLLOW_expression_in_synpred211_GosuProg5092 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_78_in_synpred211_GosuProg5095 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_synpred212_GosuProg5141 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_synpred232_GosuProg5514 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_58_in_synpred232_GosuProg5516 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_synpred232_GosuProg5518 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_synpred233_GosuProg5553 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred233_GosuProg5555 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred233_GosuProg5557 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_synpred233_GosuProg5559 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_synpred234_GosuProg5598 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred234_GosuProg5600 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_synpred234_GosuProg5602 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_synpred243_GosuProg5708 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_synpred243_GosuProg5710 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_synpred244_GosuProg5741 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_synpred244_GosuProg5743 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_synpred246_GosuProg5805 = new BitSet(new long[]{0x0400000000000000L});
	public static final BitSet FOLLOW_58_in_synpred246_GosuProg5807 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_synpred247_GosuProg5838 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred247_GosuProg5840 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred247_GosuProg5842 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_63_in_synpred248_GosuProg5877 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_synpred248_GosuProg5879 = new BitSet(new long[]{0x0000000000000002L});
}
