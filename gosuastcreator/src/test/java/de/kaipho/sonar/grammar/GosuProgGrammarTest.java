package de.kaipho.sonar.grammar;

import com.sonar.sslr.api.typed.ActionParser;
import de.kaipho.sonar.gosu.GosuParser;
import junit.framework.TestCase;
import org.sonar.plugins.java.api.tree.Tree;

import java.nio.file.Paths;

public class GosuProgGrammarTest extends TestCase {

    public void testParse() throws Exception {
        ActionParser<Tree> g = GosuParser.createParser();
        Tree tree = g.parse(Paths.get(getClass().getResource("/files/AddressCountrySettings.gs").toURI()).toFile());
        assertNotNull(tree);
    }
}