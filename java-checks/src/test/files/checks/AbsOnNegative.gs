class AbsOnNegative implements Comparable<AbsOnNegative> {
    public static final var MIN_VALUE: int = 0
    public static final var SOME_VALUE: int = 0

    public function method() {
      intmethod(0); // Compliant
      Math.abs(0); // Compliant
      Math.abs(intmethod(0)); // Compliant
      Math.abs(hashCode()); // Noncompliant [[sc=16;ec=26]] {{Use the original value instead.}}
      Math.abs((super.hashCode() as int)); // Noncompliant {{Use the original value instead.}}
      Math.abs(this.hashCode()); // Noncompliant {{Use the original value instead.}}
      Math.abs(new java.util.Random().nextInt()); // Noncompliant {{Use the original value instead.}}
      Math.abs(new java.util.Random().nextLong()); // Noncompliant {{Use the original value instead.}}
      Math.abs(this.compareTo(this)); // Noncompliant {{Use the original value instead.}}
      Math.abs(Integer.MIN_VALUE as long); // Compliant
      Math.abs(Integer.MIN_VALUE); // Noncompliant {{Use the original value instead.}}
      Math.abs(Long.MIN_VALUE); // Noncompliant {{Use the original value instead.}}
      Math.abs(AbsOnNegative.MIN_VALUE); // Compliant
      Math.abs(AbsOnNegative.SOME_VALUE); // Compliant

      -(0 as int); // Compliant
      -intmethod(0); // Compliant
      -this.compareTo(this); // Noncompliant {{Use the original value instead.}}
      -Integer.MIN_VALUE; // Noncompliant [[sc=8;ec=25]] {{Use the original value instead.}}
      -Long.MIN_VALUE; // Noncompliant {{Use the original value instead.}}
      -AbsOnNegative.MIN_VALUE; // Compliant
      -AbsOnNegative.SOME_VALUE; // Compliant
    }

    public function intmethod(arg: int): int {
      return arg;
    }

  }