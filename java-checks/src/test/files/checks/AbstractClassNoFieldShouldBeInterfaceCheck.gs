class AbstractClassNoFieldShouldBeInterfaceCheck {
  abstract class A {
    private var b: int;

    abstract function method();
  }

  abstract class B { // Noncompliant [[sc=18;ec=19]] {{Convert the abstract class "B" into an interface.}}
    function method(): int {
      return 1;
    }

    class F {
    }
  }

  class C {
    function method(): int {
      return 1;
    }
  }

  abstract class D {
    protected function method() {

    }
  }

  abstract class E extends A {
  }

  public abstract class F {
    public abstract function v(): double;

    @Override
    override public function toString(): String {
      return ":"
    }
  }

  public abstract class G {
    public abstract function v(): double;

    public function toString(): String {
      return ":";
    }
  }

  public abstract class Car { // Compliant - has private methods
    public function start() {
      turnOnLights();
      startEngine();
    }

    public abstract function stop();

    private function turnOnLights() {
    }

    private function startEngine() {
    }
  }
}