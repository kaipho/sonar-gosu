package java.lang;

class AbstractClassNoFieldShouldBeInterfaceCheck_javalang {
  abstract function method();

  abstract class B { // Noncompliant
  }
}
