class AbstractClassNoFieldShouldBeInterfaceCheck_no_version {
  abstract class A {
    private var b: int;

    abstract function method();
  }

  abstract class B { // Noncompliant {{Convert the abstract class "B" into an interface. (sonar.java.source not set. Assuming 8 or greater.)}}
    function method(): int {
      return 1;
    }

    class F {
    }
  }

  class C {
    function method(): int {
      return 1;
    }
  }

  abstract class D {
    protected function method() {

    }
  }

  abstract class E extends A {
  }

}