class AbstractClassWithoutAbstractMethodCheck {
  public abstract class Animal { // Noncompliant {{Convert this "Animal" class to an interface}}

    abstract function move();

    abstract function feed();

  }

  public abstract class AbstractColor { // Noncompliant {{Convert this "AbstractColor" class to a concrete class with a private constructor}} [[sc=25;ec=38]]
    private var red: int = 0;
    private var green: int = 0;
    private var blue = 0;

    public function getRed(): int {
      return red;
    }
  }

  public interface AnimalInterface {

    function move();

    function feed();

  }

  public class Color {
    private var red = 0;
    private var green = 0;
    private var blue = 0;

    private construct() {
    }

    public function getRed(): int {
      return red;
    }
  }

  public abstract class Lamp {

    private var switchLamp = false;

    public abstract function glow();

    public function flipSwitch() {
      switchLamp = !switchLamp;
      if (switchLamp) {
        glow();
      }
    }
  }

  public abstract class Empty { // Noncompliant {{Convert this "Empty" class to an interface}} [[sc=25;ec=30]]

  }

  abstract class A { // Noncompliant {{Convert this "A" class to an interface}}
    abstract function foo();

    abstract function bar();
  }

  abstract class B extends A { //Compliant, partial implementation.
    function foo() {
    };
  }

  interface I {
    function foo();

    function bar();
  }

  abstract class C implements I { //compliant, partial implementation
    var i = 0;

    override public function foo() {
    }
  }

  public abstract class Parametrized<T> { // Noncompliant {{Convert this "Parametrized" class to an interface}}
    abstract function foo()
  }

}