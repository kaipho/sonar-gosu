uses java.lang.String;

class A {
function method();
function method(a: int) {}
function method(a: String) {}
function method(a: String[]) {}
function method2(a: int);
}

class B {
function foo() {
  var a: A = new A();
  a.method()
  a.method(1);
  a.method("");
  a.method(new String[]{""});
  a.method2(1);
}
}