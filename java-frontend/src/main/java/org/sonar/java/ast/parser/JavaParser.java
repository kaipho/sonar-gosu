/*
 * SonarQube Java
 * Copyright (C) 2012-2017 SonarSource SA
 * mailto:info AT sonarsource DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.sonar.java.ast.parser;

import com.sonar.sslr.api.typed.ActionParser;
import org.sonar.java.model.JavaTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.sslr.grammar.LexerlessGrammarBuilder;

import java.io.File;
import java.io.IOError;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

public class JavaParser {

    private JavaParser() {

    }

    public static ActionParser<Tree> createParser() {
        try {
            ClassLoader loader = URLClassLoader.newInstance(
                    new URL[]{Paths.get("C:\\Users\\Neo\\IdeaProjects\\sonar-gosu\\gosuastcreator\\target\\gosu-ast-creator-4.11.0-SNAPSHOT.jar").toUri().toURL()},
                    JavaParser.class.getClassLoader()
            );

            Class<?> clazz = Class.forName("de.kaipho.sonar.gosu.GosuParser", true, loader);
            Class<? extends ActionParser<Tree>> runClass = (Class<? extends ActionParser<Tree>>) clazz;
            Method method = runClass.getMethod("createParser");
            return (ActionParser<Tree>) method.invoke(null);
        } catch (MalformedURLException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new IOError(e);
        }
    }
}
